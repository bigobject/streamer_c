#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <vector>
#include <unistd.h>
#include <dirent.h>

#include <sstream>

#include "coordinator.h"
#include "bo_counter.h"
#include "pass_thru_adapter.h"

#define KEY_ADAPTER_TRANSFORM_PREFERED  "pass_through"
#define KEY_ADAPTER_TABLE_TO_DUMP       "table_to_dump"
#define KEY_ADAPTER_IS_FILE             "is_file"
#define KEY_ADAPTER_OUT_DIR             "out_dir"
#define KEY_ADAPTER_BATCH_SIZE          "batch_size"
#define KEY_ADAPTER_OUT_DIR_DEFAULT     "/tmp/bo_csv_out"
#define KEY_ADAPTER_KEEP_ALIVE_FILES    "keep_alive_files"

#define KEY_ADAPTER_FLUSH_TIME_LIMIT    "flush_time_limit"

bool pass_thru_adapter::isChosen(map<string, string>& param) {
    bool is_chosen = false;
    string type_transform;
    if (get_param_value(KEY_ADAPTER_TRANSFORM, type_transform))
        is_chosen = type_transform.compare(KEY_ADAPTER_TRANSFORM_PREFERED) == 0;

    return is_chosen;
}

bool pass_thru_adapter::clear_cache()
{
    /*
    DIR* dir = opendir(_out_dir.c_str());
    if (!dir)
        return errno == ENOENT ? true : false;

    bool ret = true;
    struct dirent *ent = NULL;
    while ((ent = readdir(dir))) {
        if (!strcmp(ent->d_name, ".") || !strcmp(ent->d_name, ".."))
            continue;
        ostringstream path;
        path << _out_dir << "/" << ent->d_name;
        if (remove(path.str().c_str())) {
            Error("%s, remove %s fail: %s\n", __func__, path.str().c_str(), strerror(errno));
            ret = false;
            break;
        }
    }
    closedir(dir);
    return ret;
    */
    return true;
}

bool pass_thru_adapter::set_params(string& group, map<string, string>& param)
{
    if (!component::set_params(group, param))
        return false;

    string param_is_file;
    if (get_param_value(KEY_ADAPTER_IS_FILE, param_is_file) &&
        !param_is_file.empty())
        _is_file = true;

/*
    struct stat outdir_stat = {0};
    if (!get_param_value(KEY_ADAPTER_OUT_DIR, _out_dir))
        _out_dir = KEY_ADAPTER_OUT_DIR_DEFAULT;
//    if (!clear_cache())
//        return false;
    if (0 != stat(_out_dir.c_str(), &outdir_stat)) {
        if (0 != mkdir(_out_dir.c_str(), S_IRWXU)) {
            Error("%s, create temporary folder for output fail: %s\n", __func__, strerror(errno));
            return false;
        }
    }
*/

    string batch_size;
    if (get_param_value(KEY_ADAPTER_BATCH_SIZE, batch_size))
        _batch_checkpointing = stoi(batch_size);
    get_param_value(KEY_ADAPTER_KEEP_ALIVE_FILES, _keep_alive_files);

    string flush_time_limit;
    if(get_param_value(KEY_ADAPTER_FLUSH_TIME_LIMIT, flush_time_limit))
        _flush_time_limit = stoi(flush_time_limit);
    // set counter parameter for go program
//    string counter_url = bo_counter::get_counter()->get_counter_url();
//    int counter_retention = bo_counter::get_counter()->get_retention();
//    if (!counter_url.empty())
//        SetCounter({(char*)counter_url.c_str(), (GoInt)counter_url.length()}, (GoInt)counter_retention);

    if (!get_param_value(KEY_ADAPTER_TABLE_TO_DUMP, _table_to_dump)) {
        Error("%s, table to dump not assigned!\n", __func__);
        return false;
    }

    return true;
}

bool pass_thru_adapter::transform(vector<FILE*>& data, string& doc_id, size_t& count, msg_info_struct *msg_info)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    if (data.empty()) {
        if (_pending) {
            _pending = 0;
            check_flush_time("commit");
            return commit(_is_file_receiver, false);
        }
        return true;
    }

    int i = 0;
    int data_len = data.size();
    if (_is_file) {
        bool use_data_combo = false;
        vector<FILE*>::const_iterator itFilePtr = data.begin();
        source_combo_struct * data_combo_ptr = (source_combo_struct*)*itFilePtr;
        if (strcmp(data_combo_ptr->type, "MQTT_") == 0){
            use_data_combo = true;
            log_debug(string("<") +__func__+ "> cast to data_combo_ptr", true);
        }
        for (; itFilePtr != data.end(); ++itFilePtr) {
            int itFile = 0;

            if (use_data_combo) {
                data_combo_ptr = (source_combo_struct*)*itFilePtr;
                itFile = data_combo_ptr->file_descriptor;             
            } else {
                //cout << "transmitting file pointer: " << hex << *itFilePtr << endl;
                itFile = fileno(*itFilePtr);
            }

            lseek(itFile, 0, SEEK_SET);

            log_debug(string("<") +__func__+ "> transmitting fileno:" + to_string(itFile) + " to table:" + _table_to_dump, true);

            if (!transmit(_table_to_dump, itFile, 0))
                Error("%s, transmit file to channel '%s' fail (fd %d)\n",
                        __func__, _table_to_dump.c_str(), itFile);

            /*
            if (use_data_combo) {
                munmap(data_combo_ptr->msg_buf_ptr, data_combo_ptr->msg_buf_len);
            }
            */
                        
            close(itFile);
        }
    }
    else {
        Error("%s, not supported: only supports file base.", __func__);
        return false;
    }

    _pending += data_len;
    if (_pending >= _batch_checkpointing) {
        check_flush_time("commit");
        return commit(_is_file_receiver, false);
    }
    else
    {
        if(check_flush_time("compare") > _flush_time_limit) // $$ default is 300 sec
        {
            _pending = 0;
            check_flush_time("commit");
            return commit(_is_file_receiver, false);
        }
        
    }
    
    return true;
}

int pass_thru_adapter::pending_materials()
{
    return 0;
}

bool pass_thru_adapter::transform(vector<source_combo_struct*>& data_sources, string& doc_id, size_t& count, msg_info_struct *msg_info)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    if (data_sources.empty()) {
        if (_pending) {
            //_pending = 0;
            check_flush_time("commit");
            return commit(_is_file_receiver, false);
        }
        return true;
    }

    int i = 0;
    int data_len = data_sources.size();
    log_debugf("<%s> _pending/data_sources.size() : [%d][%d]",__func__, _pending, data_sources.size());

    if (_is_file) {
        
        //20230306 start breaking up input files
        string fact_table_name = _table_to_dump;
        //char break_up = '0';
        string pattern_topic ("{KAFKA_TOPIC}");
        int pattern_t_start = fact_table_name.find(pattern_topic);
        if (pattern_t_start==string::npos) {
            pattern_topic = string("_KAFKA_TOPIC_");
            pattern_t_start = fact_table_name.find(pattern_topic);
        }
        if (pattern_t_start==string::npos) {
            pattern_topic = string("{MQTT_TOPIC}");
            pattern_t_start = fact_table_name.find(pattern_topic);
        }
        if (pattern_t_start==string::npos) {
            pattern_topic = string("_MQTT_TOPIC_");
            pattern_t_start = fact_table_name.find(pattern_topic);
        }

        string pattern_cell ("$");
        int pattern_c_start = string::npos;
        if (pattern_t_start==string::npos) {
            pattern_c_start = fact_table_name.find(pattern_cell);
            if (pattern_c_start==string::npos) {
                pattern_cell = string("_CELL_"); // sample : _CELL_6_
                pattern_c_start = fact_table_name.find(pattern_cell);
            }
        }

        int cell_order = -1;
        char cell_order_buf[64];
        int pattern_postfix=0;
        if (pattern_c_start!=string::npos) {
            string cell_order_str = fact_table_name.substr(pattern_c_start+pattern_cell.length());
            cell_order = atoi(cell_order_str.c_str());
            sprintf(cell_order_buf, "%d", cell_order);
            if (pattern_cell.length()>1){
                pattern_postfix=1;
            }
        }

        vector<source_combo_struct*>::const_iterator it_profile_ptr = data_sources.begin();
        source_combo_struct * source_combo_ptr;

        for (; it_profile_ptr != data_sources.end(); ++it_profile_ptr) {
            int itFile = 0;

            source_combo_ptr = *it_profile_ptr;

            switch (source_combo_ptr->handle_type) {
                case 'D':
                    itFile = source_combo_ptr->file_descriptor;             
                    break;
                case 'P':
                default:
                    itFile = fileno(source_combo_ptr->file_ptr);
                    break;
            }

            lseek(itFile, 0, SEEK_SET);

            log_debug(string("<") +__func__+ "> transmitting fileno:" + to_string(itFile) + " to table:" + _table_to_dump, true);

            if (pattern_t_start!=string::npos) {
                fact_table_name = _table_to_dump;
                fact_table_name.replace(pattern_t_start, pattern_topic.length(), source_combo_ptr->topic); //{KEY}

                if (!transmit(fact_table_name, itFile, 0))
                    Error("%s, transmit file to channel '%s' fail (fd %d)\n",
                            __func__, _table_to_dump.c_str(), itFile);
            } else if (pattern_c_start!=string::npos) {
                //map<string, int> child_files;
                map<string, FILE *> child_files;
                break_up_csv_file(child_files, itFile, cell_order);

                for (const auto& pair_of_file_stream : child_files) {
                    std::cout << "cell : " << pair_of_file_stream.first << ", file stream ptr: " << pair_of_file_stream.second << "\n";

                    fact_table_name = _table_to_dump;
                    fact_table_name.replace(pattern_c_start, pattern_cell.length()+strlen(cell_order_buf)+pattern_postfix, pair_of_file_stream.first);
                    log_debugf("<%s> send data to table [%s]", __func__, fact_table_name.c_str());

                    int grouped_fd = fileno(pair_of_file_stream.second);
                    if (!transmit(fact_table_name, grouped_fd, 0))
                        Error("%s, transmit file to channel '%s' fail (fd %d)\n",
                                __func__, fact_table_name.c_str(), grouped_fd);

                    fclose(pair_of_file_stream.second);
                }
                child_files.clear();
            } else {
                if (!transmit(_table_to_dump, itFile, 0))
                    Error("%s, transmit file to channel '%s' fail (fd %d)\n",
                            __func__, _table_to_dump.c_str(), itFile);

                /*
                if (use_data_combo) {
                    munmap(data_combo_ptr->msg_buf_ptr, data_combo_ptr->msg_buf_len);
                }
                */
                if (_is_file_receiver) {
                    add_milestone(_table_to_dump, source_combo_ptr->file_name, count_csv_records(source_combo_ptr->file_ptr));
                }
            }
                                    
            close(itFile);
        }
    }
    else {
        Error("%s, not supported: only supports file base.", __func__);
        return false;
    }

    _pending += data_len;
    log_debugf("<%s> _pending/data_len : [%d][%d]",__func__, _pending, data_len);

    if (_pending >= _batch_checkpointing) {
        check_flush_time("commit");
        return commit(_is_file_receiver, false);
    }
    else
    {
        if(check_flush_time("compare") > _flush_time_limit) // $$ default is 300 sec
        {
            //_pending = 0;
            check_flush_time("commit");
            return commit(_is_file_receiver, false);
        }        
    }
    
    return true;
}
