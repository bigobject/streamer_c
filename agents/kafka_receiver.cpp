#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <fstream>
#include <sstream>

#include "bo_counter.h"
#include "kafka_receiver.h"
#include "component.h"

#include <iostream>
using namespace std;

#define KEY_RECV_KAFKA_BROKER   "broker"
#define KEY_RECV_KAFKA_TOPIC    "topics"
#define KEY_RECV_KAFKA_GROUP    "kafka_group"
#define KEY_RECV_KAFKA_CODEC    "codec"
#define KEY_RECV_KAFKA_OUT2FILE "out2file"
#define KEY_RECV_KAFKA_SYNCTYPE "sync_type"
#define KEY_RECV_KAFKA_READTIMEOUT  "read_timeout"
#define KEY_RECV_KAFKA_MSG_TAILING  "msg_tailing"
#define KEY_RECV_KAFKA_MSG_HEADING  "msg_heading"
#define KEY_RECV_KAFKA_MSG_APPEND_TIME  "msg_append_time"
#define KEY_RECV_KAFKA_BATCH_READING   "kafka_batch_reading"

bool kafka_receiver::isKafkaRecv(map<string, string>& param) {
    map<string, string>::const_iterator it;
    
    const char *keys[7] = {KEY_RECV_KAFKA_BROKER, KEY_RECV_KAFKA_TOPIC,
        KEY_RECV_KAFKA_GROUP, KEY_RECV_KAFKA_OUT2FILE, KEY_RECV_KAFKA_SYNCTYPE,
        KEY_RECV_KAFKA_READTIMEOUT, KEY_RECV_KAFKA_MSG_TAILING};

    for (int i=0; i<7; i++) {
        it = param.find( keys[i] );
        if (it == param.end()) {
            Info("Not Kafka Mode. Missing %s in config\n", keys[i]);
            return false;
        }
    }
    
    return true;
}

kafka_receiver::~kafka_receiver()
{
    //if (_kafka_conf)
    //    rd_kafka_conf_destroy(_kafka_conf);
    if (_kafka) {
        rd_kafka_resp_err_t err = rd_kafka_consumer_close(_kafka);
        if (err)
            Error("%s, Failed to close consumer: %s\n", __func__, rd_kafka_err2str(err));

        rd_kafka_destroy(_kafka);
    }
}

bool kafka_receiver::set_params(string& group, map<string, string>& param)
{
    receiver::set_type(RCV_KAFKA);

    if (!receiver::set_params(group, param))
        return false;

    string batch;
    if (get_param_value(KEY_RECV_KAFKA_BATCH_READING, batch)) {
        _batch_reading = stoi(batch);
        if (_batch_reading < 1)
            _batch_reading = 1;
    }
    log_debugf("<%s> init _batch_reading to [%d]", __func__, _batch_reading);

    string tailing, heading, appending;
    if (get_param_value(KEY_RECV_KAFKA_MSG_TAILING, tailing))
        _msg_tailing = tailing.compare("1") == 0;
    if (get_param_value(KEY_RECV_KAFKA_MSG_HEADING, heading))
        _msg_heading = heading.compare("1") == 0;
    if (get_param_value(KEY_RECV_KAFKA_MSG_APPEND_TIME, appending))
        _msg_appending = appending.compare("1") == 0;

    get_param_value(KEY_RECV_KAFKA_BROKER, _broker);
    get_param_value(KEY_RECV_KAFKA_GROUP, _kafka_group);

    string all_topics;
    get_param_value(KEY_RECV_KAFKA_TOPIC, all_topics);

    string::size_type pos = 0;
    string::size_type next_pos = 0;
    while (string::npos != (next_pos = all_topics.find_first_of(',', pos))) {
        _topics.push_back(all_topics.substr(pos, next_pos - pos));
        pos = next_pos + 1;
    }
    if (pos < all_topics.length())
        _topics.push_back(all_topics.substr(pos));

    if (_broker.empty() || _topics.empty() || _kafka_group.empty())
        return false;

	string read_timeout;
	if (get_param_value(KEY_RECV_KAFKA_READTIMEOUT, read_timeout))
		_read_timeout = stoi(read_timeout);
    
	string sync_type;
	if (get_param_value(KEY_RECV_KAFKA_SYNCTYPE, sync_type) && !sync_type.empty()) {
        switch (sync_type.at(0)) {
            case 'L':
                _stype = KRECV_SYNCTYPE_RECV_LAST;
                break;
            case 'R':
                _stype = KRECV_SYNCTYPE_RECV;
                break;
            case 'T':
                _stype = KRECV_SYNCTYPE_TRANS;
                break;
            case 'N':
            default:
                _stype = KRECV_SYNCTYPE_NORM;
                break;
        }
    }

    struct stat outdir_stat = {0};
    get_param_value(KEY_RECV_KAFKA_OUT2FILE, _out2file);
    if (!_out2file.empty()) {
        if (0 != stat(_out2file.c_str(), &outdir_stat)) {
            if (0 != mkdir(_out2file.c_str(), S_IRWXU)) {
                Error("%s, create temporary folder for output fail: %s\n", __func__, strerror(errno));
                return false;
            }
        }
    }

    char err_buf[1024] = {0};
    rd_kafka_topic_conf_t* topic_conf = rd_kafka_topic_conf_new();
    if (rd_kafka_topic_conf_set(topic_conf, "offset.store.method", "broker",
            err_buf, sizeof(err_buf)) != RD_KAFKA_CONF_OK) {
        Error("%s, %s\n", __func__, err_buf);
        return false;
    }

    _kafka_conf = rd_kafka_conf_new();
    if (rd_kafka_conf_set(_kafka_conf, "enable.auto.commit", "true",
            err_buf, sizeof(err_buf)) != RD_KAFKA_CONF_OK) {
        Error("%s, %s\n", __func__, err_buf);
        return false;
    }
    //if (rd_kafka_conf_set(_kafka_conf, "group.protocol.type", "consumer",
    //        err_buf, sizeof(err_buf)) != RD_KAFKA_CONF_OK) {
    //    Error("%s, %s\n", __func__, err_buf);
    //    return false;
    //}
    if (rd_kafka_conf_set(_kafka_conf, "group.id", _kafka_group.c_str(),
            err_buf, sizeof(err_buf)) != RD_KAFKA_CONF_OK) {
        Error("%s, %s\n", __func__, err_buf);
        return false;
    }

    if (rd_kafka_conf_set(_kafka_conf, "auto.offset.reset", "beginning",
            err_buf, sizeof(err_buf)) != RD_KAFKA_CONF_OK) {
        Error("%s, %s\n", __func__, err_buf);
        return false;
    }

    rd_kafka_conf_set_default_topic_conf(_kafka_conf, topic_conf);

    // optional
    string codec;
    get_param_value(KEY_RECV_KAFKA_CODEC, codec);
    if (!codec.empty()) {
        if (rd_kafka_conf_set(_kafka_conf, "compression.codec", codec.c_str(),
                err_buf, sizeof(err_buf)) != RD_KAFKA_CONF_OK) {
            Error("%s, %s\n", __func__, err_buf);
            return false;
        }
    }
    
    rd_kafka_conf_set_opaque(_kafka_conf, this);
    rd_kafka_conf_set_rebalance_cb(_kafka_conf, rebalance_cb);

    _kafka = rd_kafka_new(RD_KAFKA_CONSUMER, _kafka_conf, err_buf, sizeof(err_buf)); // $$ to librdkafka line 1487
    if (!_kafka || 0 == rd_kafka_brokers_add(_kafka, _broker.c_str())) {
        Error("%s, %s\n", __func__, err_buf);
        return false;
    }

    // get current stored offsets
    if (!get_partition_offset(_offsets))
        return false;

    // redirect rd_kafka_poll to consumer poll
    rd_kafka_poll_set_consumer(_kafka);

    // subscribe we interested topics and all partitions
    int tidx = 0;
    rd_kafka_topic_partition_list_t* topic_list = rd_kafka_topic_partition_list_new(_topics.size());
    for (tidx = 0; tidx < _topics.size(); ++tidx)
        rd_kafka_topic_partition_list_add(topic_list, _topics[tidx].c_str(), -1);
    rd_kafka_resp_err_t err = rd_kafka_subscribe(_kafka, topic_list);
    if (err)
        Error("%s, Fail to subscribe topic: %s\n", __func__, rd_kafka_err2str(err));
    rd_kafka_topic_partition_list_destroy(topic_list);

    return err ? false : true;
}

int kafka_receiver::find_topic_index(const char* topic)
{
    int idx = 0;
    for (; idx < _topics.size(); ++idx) {
        if (_topics[idx].compare(topic) == 0)
            return idx;
    }
    return -1;
}


static void get_msg_info(rd_kafka_message_t *rkmessage, msg_info_struct *msg_info)
{
    if(rkmessage)
    {
        msg_info->topic = rd_kafka_topic_name(rkmessage->rkt);
        msg_info->partition = rkmessage->partition;
        msg_info->offset = rkmessage->offset;
    }
}

//ssize_t kafka_receiver::read_receiver(string& data, FILE** fileptr, msg_info_struct *msg_info)
ssize_t kafka_receiver::read_receiver(string& data, source_combo_struct* data_picked_ptr, msg_info_struct *msg_info)
{
    log_debugf("<%s> size of _cooked_data_catalogue : [%d]", __func__, _cooked_data_catalogue.size());
    if (_cooked_data_catalogue.size()>0){
        *data_picked_ptr = *_cooked_data_catalogue[0];
        delete _cooked_data_catalogue[0];
        _cooked_data_catalogue.erase(_cooked_data_catalogue.begin());

        log_debugf("<%s> prepared data to return : [%d]", __func__, data_picked_ptr->serial);

        return 1;
    }

    /* marked @2023-03-14 for batch reading
    data_picked_ptr->handle_type = 'P';
    sprintf(data_picked_ptr->type, "KAFKA_");
    data_picked_ptr->status = 1;
    data_picked_ptr->file_descriptor = -1;
    FILE **fileptr = &(data_picked_ptr->file_ptr);
    rd_kafka_resp_err_t err = RD_KAFKA_RESP_ERR_NO_ERROR;
    rd_kafka_message_t* msg = NULL;
    int msg_count = 0;
    //int fd = -1;
    *fileptr = nullptr;
    */

    rd_kafka_resp_err_t err = RD_KAFKA_RESP_ERR_NO_ERROR;
    rd_kafka_message_t* msg = NULL;
    int msg_count = 0;
    
    Debug("Start read_receiver\n");
    map<string, FILE *> map_topic_to_fps;
    map<string, int> map_topic_to_fds;
    do { 
		data.clear();
        err = RD_KAFKA_RESP_ERR_NO_ERROR;
        
        // when msg is 0, means time out, flush
        msg = rd_kafka_consumer_poll(_kafka, _read_timeout); 

        // if(msg) 
        //     get_msg_info(msg, msg_info); 
        
        if (msg) 
        {            
            err = msg->err;

            if (err == RD_KAFKA_RESP_ERR_NO_ERROR) {
                const char * kafka_topic_name = rd_kafka_topic_name(msg->rkt);
                log_debugf("<%s> rd_kafka_topic_name : %s", __func__, kafka_topic_name);
                string kafka_topic = string(kafka_topic_name);
                // update offset
                int tidx = find_topic_index(kafka_topic_name);
                if (-1 == tidx) {
                    Error("%s, can't find topic '%s' index.\n", __func__, rd_kafka_topic_name(msg->rkt));
                    rd_kafka_message_destroy(msg);
                    continue;
                }

                if (msg->offset <= _offsets[tidx][msg->partition]) {
                    continue;
                }
                _offsets[tidx][msg->partition] = msg->offset;
                Debug("In loop, offset %d\n", msg->offset);

                int64_t enter_microsec = 0;
                if (_msg_appending) {
                    struct timeval cur_time = {0};
                    gettimeofday(&cur_time, NULL);
                    enter_microsec = cur_time.tv_sec * 1000000 + cur_time.tv_usec;
                }

                string doc_id;
                get_last_commitment(doc_id);

                if (!_out2file.empty()) {
                    //ostringstream outpath;
                    //outpath << _out2file << "/" << doc_id;
                    //
                    //ofstream outfile(outpath.str());
                    //if (outfile.is_open()) {
                    //    outfile.write((const char*)msg->payload, (streamsize)msg->len);
                    //    outfile.close();
                    //    
                    //    // copy output file path
                    //    data = outpath.str();
                    //}
                    //else
                    //    Error("%s, open output file '%s' fail: %s\n", __func__, outpath.str().c_str(), strerror(errno));
                
                    // FILE *filepointer = NULL;

                    int fd_for_msg;
                    FILE * msg_file_ptr;
                    source_combo_struct * focus_ptr = nullptr;
                    map<string, int>::iterator iter = map_topic_to_fds.find(kafka_topic);
                    char new_cooked_file;
                    if(iter == map_topic_to_fds.end()){
                        msg_file_ptr = tmpfile(); 
                        fd_for_msg = fileno(msg_file_ptr);
                        if (fd_for_msg == -1) {
                            log_error(string("tmpfile:failed to create a temp file"), true);
                            rd_kafka_message_destroy(msg);
                            continue;
                        }
                        new_cooked_file = 'Y';

                        map_topic_to_fds[kafka_topic] = fd_for_msg;
                        map_topic_to_fps[kafka_topic] = msg_file_ptr;

                        focus_ptr = new source_combo_struct;
                        focus_ptr->handle_type = 'P';
                        sprintf(focus_ptr->type, "KAFKA_");
                        focus_ptr->topic = kafka_topic;
                        focus_ptr->status = 1;
                        focus_ptr->file_descriptor = fd_for_msg;
                        focus_ptr->file_ptr = msg_file_ptr;
                        focus_ptr->data_offset = 0;
                        focus_ptr->data_length = 0;
                        focus_ptr->is_final_part = 'Y';

                        _cooked_data_catalogue.push_back(focus_ptr);          
                    } else {
                        fd_for_msg = iter->second;
                        new_cooked_file = 'N';
                    }

                    if (new_cooked_file == 'N') {
                        if (write(fd_for_msg, "\n", 1) <= 0) {
                            rd_kafka_message_destroy(msg);
                            Error("%s, [%s]write kafka message separator fail: %s\n", __func__, kafka_topic_name, strerror(errno));
                            //break;
                            continue;
                        }
                    }

                    if (-1 == fd_for_msg)
                        Error("%s, open temp file for kafka message fail: %s\n", __func__, strerror(errno));
                    else {
                        bool has_error = false;
                        if (_msg_heading) {
                            string heading(string(rd_kafka_topic_name(msg->rkt)) + ",");
                            if (write(fd_for_msg, heading.c_str(), heading.length()) <= 0) {
                                Error("%s, write kafka message header (%s) fail: %s\n", __func__, heading.c_str(), strerror(errno));
                                has_error = true;
                            }                        
                        }

                        if (!has_error) {                    
                            // Debug("msg->payload:%s\n", (const char*)msg->payload);
                            // Debug("msg->len:%d\n", (size_t)msg->len);
                            if (_msg_appending) {
                                char* payload = new char[msg->len + 1];
                                memcpy(payload, msg->payload, msg->len);
                                payload[msg->len] = 0;
                                char* token = strtok(payload, "\n");
                                char rec[1024] = {0};
                                while (token) {
                                    // char* rec = new char[strlen(token) + 128];
                                    struct timeval cur_time = {0};
                                    gettimeofday(&cur_time, NULL);

                                    int64_t microsec = cur_time.tv_sec * 1000000 + cur_time.tv_usec;
                                    sprintf(rec, "%s,%ld,%ld\n", token, enter_microsec, microsec);
                                    if (write(fd_for_msg, (const char*)rec, strlen(rec)) <= 0) {
                                        Error("%s, write appendind timestamp fail: %s\n", __func__, strerror(errno));
                                        has_error = true;
                                    }
                                    // delete [] rec;
                                    token = strtok(NULL, "\n");
                                }
                                delete [] payload;
                                delete token;
                            }
                            else if (write(fd_for_msg, (const char*)msg->payload, (size_t)msg->len) <= 0) {
                                Error("%s, write kafka message fail: %s\n", __func__, strerror(errno));
                                has_error = true;
                            }
                            //else if (_msg_appending) {
                            //    struct timeval cur_time = {0};
                            //    gettimeofday(&cur_time, NULL);
                            //
                            //    char buf[128] = {0};
                            //    int64_t microsec = cur_time.tv_sec * 1000000 + cur_time.tv_usec;
                            //    sprintf(buf, ",%ld", microsec);
                            //    if (write(fd, (const char*)buf, strlen(buf)) <= 0) {
                            //        Error("%s, write appendind timestamp fail: %s\n", __func__, strerror(errno));
                            //        has_error = true;
                            //    }
                            //}
                        }

                        if (has_error) { // failed to write msg
                            rd_kafka_message_destroy(msg);
                            /*
                            if (msg_count > 0) {
                                break;
                            }

                            close(fd_for_msg);
                            fclose(msg_file_ptr);
                            fd_for_msg = -1;
                            */
                        }
                        else {
                            ++msg_count;
                            if (_msg_tailing && ((const char*)msg->payload)[msg->len - 1] != '\n' &&
                                msg_count >= _batch_reading) { // this is the last msg in a batch
                                if (write(fd_for_msg, "\n", 1) <= 0) {
                                    Error("%s, write kafka message tail fail: %s\n", __func__, strerror(errno));
                                    rd_kafka_message_destroy(msg);
                                    continue;
                                }
                            }
                        }
                    }
                }
                else {
                    // copy data
                    data = (const char*)msg->payload;
                    Debug("%s, received message (topic %s [%d], offset %ld, %lu bytes): %s\n",
                        __func__, rd_kafka_topic_name(msg->rkt), msg->partition, msg->offset, msg->len, (const char*)msg->payload);
                }

                _adapt->set_doc_id(doc_id.c_str());
                bo_counter::get_counter()->inc_val(BCOUNT_KAFKA);
            }
            else
                Debug("%s, patiotion %d, offset %ld, err: %s\n", __func__, msg->partition, msg->offset, rd_kafka_err2str(err));
            rd_kafka_message_destroy(msg);
        }

        // !msg means read timeout, just leave it to have caller to handle timeout
        //} while (!msg || err == RD_KAFKA_RESP_ERR__PARTITION_EOF);
    
    } while (err == RD_KAFKA_RESP_ERR__PARTITION_EOF || (msg && msg_count < _batch_reading));

    if (_cooked_data_catalogue.size()>0){
        *data_picked_ptr = *_cooked_data_catalogue[0];
        delete _cooked_data_catalogue[0];
        _cooked_data_catalogue.erase(_cooked_data_catalogue.begin());

        log_debugf("<%s> instant data to return : [%d]", __func__, data_picked_ptr->serial);
    } else {
        data_picked_ptr->handle_type = 'P';
        sprintf(data_picked_ptr->type, "KAFKA_");
        data_picked_ptr->status = 1;
        data_picked_ptr->file_descriptor = -1;
        data_picked_ptr->file_ptr = nullptr;
    }

    // return 1 for timeout, b/c 0 means failure
    //return data.empty() ? 1 : data.length();
    return 1;
}

bool kafka_receiver::get_last_commitment(string& doc_id)
{
    bool is_empty = true;
    ostringstream last_commit;
    int i = 0;
    for (; i < _topics.size(); ++i) { 
        last_commit << _topics[i];

        vector<ssize_t>::const_iterator off = _offsets[i].begin();
        for (; off != _offsets[i].end(); ++off) { 
            last_commit << "_" << *off;
            is_empty &= (*off == RD_KAFKA_OFFSET_INVALID);
        }

        // separater of topic
        if (i <  (_topics.size() - 1))
            last_commit << "=";
    }

    if (is_empty)
        doc_id.clear();
    else
        doc_id = last_commit.str();
    Debug("kafka last commitment is '%s'\n",
        doc_id.empty() ? "NONE" : doc_id.c_str());
    return true;
}

bool kafka_receiver::backoff(string& doc_id)
{
    string last_doc;
    get_last_commitment(last_doc);

    // no change
    if (!last_doc.empty() && last_doc.compare(doc_id) == 0)
        return true;

    vector<vector<ssize_t> > org_offsets = _offsets;
    if (doc_id.empty()) {
        vector<vector<ssize_t> >::iterator off = _offsets.begin();
        for (; off != _offsets.end(); ++off)
            off->assign(off->size(), RD_KAFKA_OFFSET_INVALID);
    }
    else if (!get_partition_offset_from_docid(doc_id, _offsets))
        return false;
    else if (_offsets.size() != org_offsets.size() || _topics.size() != _offsets.size()) {
        Error("%s, number of topics is not match\n", __func__);
        return false;
    }
    else {
        int tidx = 0;
        for (; tidx < _topics.size(); ++tidx) {
            if (_offsets[tidx].size() < org_offsets[tidx].size())
                _offsets[tidx].resize(org_offsets[tidx].size(), RD_KAFKA_OFFSET_INVALID);
        }
    }

    return set_partition_offset();
}

bool kafka_receiver::sync_strategy(string& rdoc, string& tdoc, string& out)
{
    switch (_stype) {
        case KRECV_SYNCTYPE_RECV:
        case KRECV_SYNCTYPE_RECV_LAST:
            out = rdoc;
            return true;
        case KRECV_SYNCTYPE_TRANS:
            out = tdoc;
            return true;
    }

    if (rdoc.empty() || tdoc.empty()) {
        out.clear();
        return true;
    }

    if (rdoc.compare(tdoc) == 0 ) {
        out = rdoc;
        return true;
    }

    vector<vector<ssize_t> > part_offset1, part_offset2;
    if (!get_partition_offset_from_docid(rdoc, part_offset1))
        return false;
    if (!get_partition_offset_from_docid(tdoc, part_offset2))
        return false;
    if (part_offset1.size() != part_offset2.size() || _topics.size() != part_offset1.size()) {
        Error("%s, number of topics is not match.\n", __func__);
        return false;
    }

    int tidx = 0;
    out.clear();
    for (; tidx < _topics.size(); ++tidx) {
        vector<ssize_t>::size_type part_cnt1 = part_offset1[tidx].size();
        vector<ssize_t>::size_type part_cnt2 = part_offset2[tidx].size();
        if (part_cnt1 > part_cnt2)
            out = tdoc;
        else if (part_cnt1 < part_cnt2)
            out = rdoc;
        else {
            vector<ssize_t>::const_iterator it1 = part_offset1[tidx].begin();
            vector<ssize_t>::const_iterator it2 = part_offset2[tidx].begin();
            for (; it1 != part_offset1[tidx].end(); ++it1, ++it2) {
                if (*it1 == *it2)
                    continue;
                out = *it1 > *it2 ? tdoc : rdoc;
                break;
            }
        }
        if (!out.empty())
            break;
    }
    return true;
}

bool kafka_receiver::get_partition_offset_from_docid(const string& doc_id, vector<vector<ssize_t> >& part_offset)
{
    if (doc_id.empty())
        return false;

    part_offset.clear();
    part_offset.reserve(_topics.size());

    int tidx = 0;
    string::size_type pos = 0, next_pos = 0;
    string sub_id = doc_id;
    for (tidx = 0; tidx < _topics.size(); ++tidx) {
        if (sub_id.length() <= (_topics[tidx].length() + 1) ||
            sub_id.compare(0, _topics[tidx].length() + 1, _topics[tidx] + "_") != 0)
            break;

        vector<ssize_t> offsets;
        pos = _topics[tidx].length() + 1;
        while (string::npos != (next_pos = sub_id.find_first_of("_=", pos))) {
            offsets.push_back(stoll(sub_id.substr(pos, next_pos - pos)));
            if (sub_id.at(next_pos) == '=')
                break; // end of topic
            pos = next_pos + 1;
        }

        if (string::npos == next_pos) {
            offsets.push_back(stoll(sub_id.substr(pos)));
            part_offset.push_back(offsets);
            ++tidx;
            break;
        }

        part_offset.push_back(offsets);
        sub_id = sub_id.substr(next_pos + 1);
    }
    if (tidx != _topics.size()) {
        part_offset.clear();
        return false;
    }
    return true;
}

bool kafka_receiver::get_partition_offset(vector<vector<ssize_t> >& part_offset)
{
    int tidx = 0;
    part_offset.clear();
    part_offset.reserve(_topics.size());

    for (; tidx < _topics.size(); ++tidx) {
        vector<ssize_t> offsets;
        const struct rd_kafka_metadata *metadata;
        rd_kafka_topic_t* rkt = rd_kafka_topic_new(_kafka, _topics[tidx].c_str(), rd_kafka_topic_conf_new());
        if (!rkt) {
            Error("%s, Fail to create kafka topic object.\n", __func__);
            return false;
        }

        rd_kafka_resp_err_t err = rd_kafka_metadata(_kafka, 0, rkt, &metadata, 5000);
        if (err != RD_KAFKA_RESP_ERR_NO_ERROR) {
            Error("%s, Fail to get meta data: %s\n", __func__, rd_kafka_err2str(err));
            rd_kafka_topic_destroy(rkt);
            return false;
        }

        if (metadata->topic_cnt != 1 || metadata->topics[0].err) {
            Error("%s, Number of topic (%d) mismatched, or with error: %s\n",
                __func__, metadata->topic_cnt, rd_kafka_err2str(metadata->topics[0].err));
            rd_kafka_topic_destroy(rkt);
            return false;
        }

        int part_cnt = metadata->topics[0].partition_cnt;
        rd_kafka_metadata_destroy(metadata);
        offsets.reserve(part_cnt);

        int i = 0;
        if (_stype == KRECV_SYNCTYPE_RECV_LAST) {
            for (i = 0; i < part_cnt; ++i) {
                int64_t low = 0, high = 0;
                err = rd_kafka_query_watermark_offsets(_kafka, _topics[tidx].c_str(), i, &low, &high, 5000);
                if (err != RD_KAFKA_RESP_ERR_NO_ERROR) {
                    Error("%s, get water mark of %s[%d] fail: %s\n",
                        __func__, _topics[tidx].c_str(), i, rd_kafka_err2str(err));
                    return false;
                }
                offsets.push_back(high == 0 ? RD_KAFKA_OFFSET_INVALID : high - 1);
            }
            part_offset.push_back(offsets);
            continue;
        }

        // prepare partition list
        rd_kafka_topic_partition_list_t* topic_list = rd_kafka_topic_partition_list_new(part_cnt);
        for (i = 0; i < part_cnt; ++i)
            rd_kafka_topic_partition_list_add(topic_list, _topics[tidx].c_str(), i);

        err = rd_kafka_committed(_kafka, topic_list, 5000);

        if (err == RD_KAFKA_RESP_ERR_NO_ERROR) {
            for (i = 0; i < topic_list->cnt ; i++) {
                rd_kafka_topic_partition_t *p = &topic_list->elems[i];
                if (p->err) {
                    Error("%s, partition %d error: %s\n", __func__, i, rd_kafka_err2str(p->err));
                    return false;
                }
                offsets.push_back(p->offset);
                Debug("%s, partition %d offset: %ld\n", __func__, i, p->offset);
            }
        }
        rd_kafka_topic_partition_list_destroy(topic_list);
        part_offset.push_back(offsets);
    }

    return true;
}

bool kafka_receiver::set_partition_offset()
{
    if (_offsets.empty())
        return false;

    int tidx = 0, pidx = 0, part_cnt = 0;
    for (tidx = 0; tidx < _offsets.size(); ++tidx) 
        part_cnt += _offsets[tidx].size();

    rd_kafka_topic_partition_list_t* topic_list = rd_kafka_topic_partition_list_new(part_cnt);

    rd_kafka_resp_err_t err = RD_KAFKA_RESP_ERR_NO_ERROR;
    for (tidx = 0; tidx < _offsets.size(); ++tidx) { 
        for (pidx = 0; pidx < _offsets[tidx].size(); ++pidx) { 
            rd_kafka_topic_partition_list_add(topic_list, _topics[tidx].c_str(), pidx);
    
            int64_t offset = _offsets[tidx][pidx] == RD_KAFKA_OFFSET_INVALID ? RD_KAFKA_OFFSET_BEGINNING : _offsets[tidx][pidx] + 1;

            err = rd_kafka_topic_partition_list_set_offset(topic_list, _topics[tidx].c_str(), pidx, offset);
            if (err != RD_KAFKA_RESP_ERR_NO_ERROR) {
                Error("%s, set topic %s, partition %d, offet %ld fail: %s\n", __func__, _topics[tidx].c_str(), pidx, offset, rd_kafka_err2str(err));
                break;
            }
            Debug("%s, set topic %s, partition %d, offset: %ld, success\n", __func__, _topics[tidx].c_str(), pidx, offset);
        }
    }
    if (err == RD_KAFKA_RESP_ERR_NO_ERROR) {
        err = rd_kafka_assign(_kafka, topic_list);
        if (err != RD_KAFKA_RESP_ERR_NO_ERROR)
            Error("%s, assign partition offsets fail: %s\n", __func__, rd_kafka_err2str(err));
    }

    rd_kafka_topic_partition_list_destroy(topic_list);
    return err == RD_KAFKA_RESP_ERR_NO_ERROR;
}

void kafka_receiver::rebalance_cb(
    rd_kafka_t *kafka,
    rd_kafka_resp_err_t err,
    rd_kafka_topic_partition_list_t *partitions,
    void *arg)
{
	if (err == RD_KAFKA_RESP_ERR__ASSIGN_PARTITIONS) {
		Debug("%s, assigned\n", __func__);
        // reassign offset w/ our offset
        kafka_receiver* krecv = (kafka_receiver*)arg;
        krecv->set_partition_offset();
    }
    else if (err == RD_KAFKA_RESP_ERR__REVOKE_PARTITIONS) {
		Debug("%s, revoked\n", __func__);
        rd_kafka_assign(kafka, NULL);
    }
    else {
		Error("%s, rebalance fail: %s\n", __func__, rd_kafka_err2str(err));
        rd_kafka_assign(kafka, NULL);
		return;
	}
}

vector<string> kafka_receiver::get_prefixs() {
    return receiver::get_prefixs();
}
