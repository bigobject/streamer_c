#include "adapter.h"
#include "common_func.h"

class prophet_csv_adapter : public adapter
{
    public:
        prophet_csv_adapter() : _is_file(false), _out_dir(""),
        _keep_alive_files(""), _batch_checkpointing(1), _pending(0) {}
        virtual ~prophet_csv_adapter() {}

        virtual bool set_params(string& group, map<string, string>& param);
    protected:
        virtual bool transform(vector<int>& data, string& doc_id, size_t& count, msg_info_struct *msg_info);
    private:
        bool    _is_file;
        string  _out_dir;
        string  _keep_alive_files;
        int     _batch_checkpointing;
        int     _pending;
        int     _flush_time_limit = 300; // $$ default is 300, it can be changed in set_params()
        
        bool clear_cache();
};