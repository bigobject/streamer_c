#define _GNU_SOURCE
//#include <sys/mman.h>
//#include <sys/sendfile.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <vector>
#include <unistd.h>
#include <dirent.h>

#include <sstream>

#include "bo_counter.h"
//#include "go_json2csv.h"
#include "json2csv_adapter.h"

#include<iostream>

using namespace std;

#define KEY_ADAPTER_TRANSFORM_PREFERED  "SVT"
#define KEY_ADAPTER_IS_FILE             "is_file"
#define KEY_ADAPTER_OUT_DIR             "out_dir"
#define KEY_ADAPTER_BATCH_CHECKPOINTING_LEGACY          "batch_size"
#define KEY_ADAPTER_BATCH_CHECKPOINTING          "batch_checkpointing"
#define KEY_ADAPTER_OUT_DIR_DEFAULT     "/tmp/bo_csv_out"
#define KEY_ADAPTER_KEEP_ALIVE_FILES    "keep_alive_files"
#define KEY_ADAPTER_CONF                "conf"

#define KEY_ADAPTER_FLUSH_TIME_LIMIT    "flush_time_limit"
#define KEY_ADAPTER_FILE_CHUNK          "file_chunk"

#define MAX_FILE_CHUNK_IN_MB            32
#define SVT_CFG_FILE                    "/streamer/Transform.svt"

#define SIZE_DATA_LINE_CACHE            4*1024
#define SIZE_DATA_PLAYGROUND            4*1024
#define SIZE_DATA_SINGLE_ROUND          1024

////////20230325
vector<int> input_file_descs;
vector<string> input_file_names; // for checkpointing
vector<GoInt> input_files;
vector<string> input_topics;

bool json2csv_adapter::isChosen(map<string, string>& param) {
    bool is_chosen = true; // this one is the default
    string type_transform;
    if (get_param_value(KEY_ADAPTER_TRANSFORM, type_transform))
        is_chosen = type_transform.compare(KEY_ADAPTER_TRANSFORM_PREFERED) == 0;

    return is_chosen;
}

bool json2csv_adapter::clear_cache()
{
    DIR* dir = opendir(_out_dir.c_str());
    if (!dir)
        return errno == ENOENT ? true : false;

    bool ret = true;
    struct dirent *ent = NULL;
    while ((ent = readdir(dir))) {
        if (!strcmp(ent->d_name, ".") || !strcmp(ent->d_name, ".."))
            continue;
        ostringstream path;
        path << _out_dir << "/" << ent->d_name;
        if (remove(path.str().c_str())) {
            Error("%s, remove %s fail: %s\n", __func__, path.str().c_str(), strerror(errno));
            ret = false;
            break;
        }
    }
    closedir(dir);
    return ret;
}

bool json2csv_adapter::set_params(string& group, map<string, string>& param)
{
    if (!component::set_params(group, param))
        return false;

    string param_is_file;
    if (get_param_value(KEY_ADAPTER_IS_FILE, param_is_file) &&
        !param_is_file.empty())
        _is_file = true;

    struct stat outdir_stat = {0};
    if (!get_param_value(KEY_ADAPTER_OUT_DIR, _out_dir))
        _out_dir = KEY_ADAPTER_OUT_DIR_DEFAULT;

    if (0 != stat(_out_dir.c_str(), &outdir_stat)) {
        if (0 != mkdir(_out_dir.c_str(), S_IRWXU)) {
            Error("%s, create temporary folder for output fail: %s\n", __func__, strerror(errno));
            return false;
        }
    }

    // get_param_value(KEY_ADAPTER_CONF, _conf);
    // if (!_conf.empty() && _conf.length() > 0)
    //     SetConfig({(char*)_conf.c_str(), (GoInt)_conf.length()});

    string batch_cp_size;
    if (get_param_value(KEY_ADAPTER_BATCH_CHECKPOINTING, batch_cp_size)) {
        _batch_checkpointing = stoi(batch_cp_size);
    } else if (get_param_value(KEY_ADAPTER_BATCH_CHECKPOINTING_LEGACY, batch_cp_size)) {
        _batch_checkpointing = stoi(batch_cp_size);
    }    
    get_param_value(KEY_ADAPTER_KEEP_ALIVE_FILES, _keep_alive_files);

    string flush_time_limit;
    if(get_param_value(KEY_ADAPTER_FLUSH_TIME_LIMIT, flush_time_limit))
        _flush_time_limit = stoi(flush_time_limit);

    string file_chunk;
    int size_in_MB;
    if(get_param_value(KEY_ADAPTER_FILE_CHUNK, file_chunk)) {
        sscanf( file_chunk.c_str(), "%d", &size_in_MB);
        if (size_in_MB > 0){
            _file_chunk = size_in_MB;
        }
    }
    if (_file_chunk > MAX_FILE_CHUNK_IN_MB) {
        _file_chunk = MAX_FILE_CHUNK_IN_MB;
    }

    //peek svt settings
    //looking for:
    // SET json "1"
    // SET skip 1
    char * dont_cared_char_ptr;

    FILE * svt_cfg_ptr = fopen(SVT_CFG_FILE, "r");
    if (svt_cfg_ptr!=NULL){
        int buf_len=512;
        char buffer[buf_len];
        fseek(svt_cfg_ptr, 0, SEEK_SET);
        int key_json_found = 0;
        int key_skip_found = 0;
        string output_token ("OUTPUT+");
        while (fgets(buffer, buf_len-1, svt_cfg_ptr) != NULL) {
            int str_len = strlen(buffer);
            char * setting = trim_c_string(buffer);
            strReplace(setting, ' ', '+');
            strReplace(setting, '\t', '+');
            removeDuplicateChars(setting, '+');
            strupr(setting);
            log_debug(string("<") +__func__+ "> SVT cfg : " + setting, true);

            if (setting[0] == '/' && setting[1] == '/'){
                continue;
            }

            if (key_json_found == 0){
                int value_json_format;
                if (sscanf(setting, "SET+JSON+\"%d\"", &value_json_format) == 1) {
                    key_json_found = 1;
                    if (value_json_format==1) {
                        _is_json_input=true;
                    }
                    log_debugf("<%s> _is_json_input? [%d]", __func__, _is_json_input);
                    continue;
                }
            }

            if (key_skip_found == 0){
                if (sscanf(setting, "SET+SKIP+%d", &_csv_skip_lines) == 1) {
                    key_skip_found = 1;
                    if (_csv_skip_lines<0) {
                        _csv_skip_lines=0;
                    }
                    log_debug(string("<") +__func__+ "> _csv_skip_lines : " + to_string(_csv_skip_lines), true);
                    continue;
                }
            }

            string svt_stmt(setting);
            if (svt_stmt.find(output_token) != string::npos) {
                log_debug(string("<") +__func__+ "> svt OUTPUT stmt : " + svt_stmt, true);
                _output_table_def.push_back(svt_stmt);
            } 
        }
        fclose(svt_cfg_ptr);        
    }

    // set counter parameter for go program
    // string counter_url = bo_counter::get_counter()->get_counter_url();
    // int counter_retention = bo_counter::get_counter()->get_retention();
    // if (!counter_url.empty())
    //     SetCounter({(char*)counter_url.c_str(), (GoInt)counter_url.length()}, (GoInt)counter_retention);
    return true;
}

void freeJsonToCsvReturn(JsonToCsv_return ret) {
    // c_out_tbl
    for(int i=0; i<ret.r1; i++) {
        for(int j=0; j<ret.r2; j++) {
            if (ret.r0[i][j] == nullptr)
                break;
            free(ret.r0[i][j]);
        }
        free(ret.r0[i]);
    }
    free(ret.r0);

    // c_out_fd
    for(int i=0; i<ret.r4; i++) {
        if (ret.r3[i] == nullptr) 
            break;
        free(ret.r3[i]);
    }
    free(ret.r3);

    // c_err_str
    for(int i=0; i<ret.r7; i++) {
        if (ret.r6[i] == nullptr)
            break;
        free(ret.r6[i]);
    }
    free(ret.r6);
    return;
}

bool json2csv_adapter::transform(vector<FILE*>& files, string& doc_id, size_t& count, msg_info_struct *msg_info)
{
    return true;
}

bool json2csv_adapter::slice_data_file(int fd_data, int len_least, vector<int>& chunk_file_family){
    return slice_data_file(fd_data, len_least, 0 , 0, chunk_file_family);
}

bool json2csv_adapter::slice_data_file(int fd_data, int len_least, off64_t offset_part , off64_t len_part, vector<int>& chunk_file_family){
    log_debugf("<%s> slicing file (%d) from %ld, length %ld", __func__, fd_data, offset_part, len_part);
    off64_t total_bytes_wrote=0;
    off64_t milestone_final = offset_part+len_part;
    off64_t milestone_passed = offset_part;


    struct stat in_stat = {0};
    if (0 != fstat(fd_data, &in_stat)) 
    {
        log_error(string("<") +__func__+ "> fstat failed", true);
        return false;
    } else {
        //log_debug(string("<") +__func__+ "> send file to SVT with length " + to_string(in_stat.st_size), true);
        _level_gigabytes = in_stat.st_size/(1024*1024*1024) + 1;
        if (in_stat.st_size < len_least) {
            chunk_file_family.push_back(fd_data);
            log_debugf("<%s> new entry to chunk_file_family : %d", __func__, fd_data);
            return true;
        }

        if (len_part > 0) {
            if (len_part < len_least) {
                FILE * ptr_file_chunk = tmpfile();
                FILE * ptr_file_base = fdopen( dup(fd_data), "r");
                fseek(ptr_file_base, offset_part, SEEK_SET);

                char c;
                for (int _idx=0; _idx<len_part; _idx++){
                    c = fgetc(ptr_file_base);
                    fputc(c,ptr_file_chunk);
                }
                fflush(ptr_file_chunk);

                int fd_file_chunk = fileno(ptr_file_chunk);
                chunk_file_family.push_back(fd_file_chunk);
                log_debugf("<%s> new entry to chunk_file_family : %d", __func__, fd_file_chunk);
                fclose(ptr_file_base);
                return true;
            }
        }
    }

    bool finished = false;
    bool error_occurred = false;
    lseek64(fd_data , 0 , SEEK_SET);
    int milestone_next = 0;
    int len_step = len_least;
    char * first_hand_chunk = (char *)malloc(len_least+1);
    char * dyn_chunk_buf=NULL;

    //fetch lines to skip
    int header_lines_to_skip = _csv_skip_lines;
    
    char csv_header_lines[SIZE_DATA_LINE_CACHE] = {0};
    char buf_playground[SIZE_DATA_PLAYGROUND] = {0};
    char buf_single_round[SIZE_DATA_SINGLE_ROUND] = {0};

    FILE* source_fp = fdopen(fd_data, "r");
    int count_dq;
    int size_csv_header=0;

    char * dont_cared_char_ptr;
    while (header_lines_to_skip > 0){
        if (fgets(buf_playground, SIZE_DATA_PLAYGROUND, source_fp) != NULL) {

            count_dq = count_csv_double_quote(buf_playground);
            while (count_dq%2 == 1) {
                if (fgets(buf_single_round, SIZE_DATA_SINGLE_ROUND, source_fp) == NULL) {
                    log_error(string("<") +__func__+ "> read csv lines for skipping failed", true);
                    return false;
                }

                strcat(buf_playground, buf_single_round);
                count_dq = count_csv_double_quote(buf_playground);
            }

            strcat(csv_header_lines, buf_playground);
            header_lines_to_skip-=1;
        } else {
            log_error(string("<") +__func__+ "> read csv lines for skipping failed", true);
            return false;
        }
    }
    size_csv_header = strlen(csv_header_lines);
    if (size_csv_header>0) {
        log_debug(string("<") +__func__+ "> skipped lines : " + csv_header_lines, true);
    }

    //offset_part len_part
    off64_t diff_to_part = offset_part;
    long stepping = 1024*1024*1024;
    lseek64(fd_data , 0 , SEEK_SET);
    while (diff_to_part > stepping) {
        lseek64(fd_data , stepping , SEEK_CUR);
        diff_to_part-=stepping;
    }
    if (diff_to_part > 0) {
        lseek64(fd_data , diff_to_part , SEEK_CUR);
    }    
    bool is_file_top = false;
    if (offset_part==0){
        is_file_top = true;
    }
    int serial_chunk=0;
    int bytes_read=0;
    int len_next_read=len_least;
    int run_out=0;
    while (!finished){
        off64_t current_position = lseek64(fd_data, 0, SEEK_CUR);
        log_debugf("<%s> to find new chunk from position : %ld", __func__, current_position);

        if (len_part>0){
            if ((milestone_final-current_position) < len_least) {
                len_next_read=milestone_final-current_position;
                run_out = 1;
                log_debugf("<%s> final round read length : %ld", __func__, len_next_read);
            }
        }

        FILE * ptr_file_chunk = tmpfile();

        int fd_file_chunk = fileno(ptr_file_chunk);

        //memset(first_hand_chunk, '\0', len_least+1);

        //first phase start
        int len_chunk_buf;
        if (dyn_chunk_buf!=NULL) {
            free(dyn_chunk_buf);
        }
        dyn_chunk_buf=NULL;

        int size_batch_read=read(fd_data, first_hand_chunk, len_next_read);
        log_debug(string("<") +__func__+ "> new reading : " + to_string(size_batch_read) , true);
        if (size_batch_read>0){
            first_hand_chunk[size_batch_read] = '\0';
            bytes_read+=size_batch_read;
        }

        if (size_batch_read == 0) {
            finished = true;
        } else if (size_batch_read == -1) {
            log_error(string("<") +__func__+ "> failed to read data source", true);
            error_occurred = true;
        } else if ((size_batch_read < len_next_read) || (run_out==1)){
            serial_chunk++;
            log_debug(string("<") +__func__+ "> ......reached EOF in data file, no " + to_string(serial_chunk), true);
            
            if (size_csv_header>0) {
                if (is_file_top){
                    is_file_top=false;
                    
                    dyn_chunk_buf = (char *)malloc(size_batch_read + 1);
                    dyn_chunk_buf[0]='\0';   
                } else {
                    log_debug(string("<") +__func__+ "> ......write csv header lines...", true);

                    // final chunk with csv header
                    dyn_chunk_buf = (char *)malloc(size_csv_header+size_batch_read+1);
                    strncpy(dyn_chunk_buf, csv_header_lines, size_csv_header);
                    dyn_chunk_buf[size_csv_header]='\0';
                }
            }

            if (!error_occurred){
                // populate final chunk with csv data records
                log_debug(string("<") +__func__+ "> ......write csv data records...", true);

                len_chunk_buf = size_csv_header + size_batch_read;
                if (size_csv_header==0) {
                    dyn_chunk_buf = (char *)malloc(size_batch_read+1);
                    dyn_chunk_buf[0]='\0';
                }
                //strncpy(dyn_chunk_buf+size_csv_header, first_hand_chunk, size_batch_read);
                strcat(dyn_chunk_buf, first_hand_chunk);
            }

            if (!error_occurred){

                finished = true;
                log_debug(string("<") +__func__+ "> chunk file position : " + to_string(lseek64(fd_file_chunk, 0, SEEK_CUR)) , true);

                log_debug(string("<") +__func__+ "> ready to write chunk (" + to_string(len_chunk_buf) + ")", true);

                //write(fd_file_chunk, dyn_chunk_buf, len_chunk_buf);
                fwrite(dyn_chunk_buf, sizeof(char), len_chunk_buf, ptr_file_chunk);
                //fsync(fd_file_chunk);
                fflush(ptr_file_chunk);
                total_bytes_wrote+=len_chunk_buf;
                log_debugf("<%s> total_bytes_wrote : %ld", __func__, total_bytes_wrote);

                log_debug(string("<") +__func__+ "> chunk file length by ftell (" + to_string(ftell(ptr_file_chunk)) + ")", true);
                fseek(ptr_file_chunk, -20, SEEK_END);
                char tmp_buf[24];
                dont_cared_char_ptr=fgets(tmp_buf, 24, ptr_file_chunk);
                log_debug(string("<") +__func__+ "> chunk file tail : [" + string(tmp_buf) + "]", true);

                lseek64(fd_file_chunk , 0 , SEEK_SET);
                chunk_file_family.push_back(fd_file_chunk);
                log_debugf("<%s> new entry to chunk_file_family : %d", __func__, fd_file_chunk);

                // keep FINAL sample chunk in /tmp/streamer
                int num_chunks = get_num_file_chunks_to_keep();
                if (num_chunks>=2){
                    
                    if (true){
                        FILE * fptr2;

                        string random_serial = get_current_date("unix_ms");
                        string temp_file = string("/tmp/streamer/slice_") + random_serial + "_" + to_string(serial_chunk) + string(".csv");

                        fptr2 = fopen(temp_file.c_str(), "w");
                        if (fptr2 == NULL)
                        {
                            log_debug(string("<") +__func__+ "> cannot write file slice : " + temp_file , true);
                        }

                        fwrite(dyn_chunk_buf, sizeof(char), len_chunk_buf, fptr2);

                        /*
                        // Read contents from file
                        char c;
                        fseek(ptr_file_chunk, 0, SEEK_SET);
                        c = fgetc(ptr_file_chunk);
                        while (c != EOF)
                        {
                            fputc(c, fptr2);
                            c = fgetc(ptr_file_chunk);
                        }
                        */
                        fclose(fptr2);
                    }
                    
                }
            }
        }

        if (error_occurred) {
            free(first_hand_chunk);
            if (dyn_chunk_buf!=NULL) {
                free(dyn_chunk_buf);
            }
            dyn_chunk_buf=NULL;
            
            return false;
        }

        //just for debugging
        //char buf_rec[64] = {0};
        //memcpy(buf_rec, first_hand_chunk, 63);
        //log_debug(string("<") +__func__+ "> leading record read in : " + buf_rec, true);
        log_debugf("<%s> main record read in : %64s", __func__, first_hand_chunk);
        
        if (finished){
            log_debug(string("<") +__func__+ "> -------------------file processed", true);
            free(first_hand_chunk);
            if (dyn_chunk_buf!=NULL) {
                free(dyn_chunk_buf);
            }
            dyn_chunk_buf=NULL;
            
            return true;
        }

        // phase 1 end
        //------------------------------------------------------------------------
        // phase 2 start
        // populate preceding chunks

        milestone_next = milestone_passed+size_batch_read;

        int count_dq = count_csv_double_quote(first_hand_chunk);
        int need_patch=1;
        log_debug(string("<") +__func__+ "> initial chunk size:" + to_string(size_batch_read), true);
        if ((count_dq % 2)==0) {
            if (first_hand_chunk[size_batch_read-1]=='\n') {
                need_patch = 0;
            }
        }

        if (size_csv_header>0) {
            if (is_file_top){
                is_file_top=false;
                
                dyn_chunk_buf = (char *)malloc(size_batch_read + 1);
                dyn_chunk_buf[0]='\0';    
            } else {
                log_debug(string("<") +__func__+ "> ......write csv header lines...", true);

                dyn_chunk_buf = (char *)malloc(size_csv_header + size_batch_read + 1);
                strncpy(dyn_chunk_buf, csv_header_lines, size_csv_header);
                dyn_chunk_buf[size_csv_header]='\0';    
            }
        }

        if (!error_occurred) {
            log_debug(string("<") +__func__+ "> ......write csv data records...", true);

            len_chunk_buf = size_csv_header + size_batch_read;
            if (size_csv_header==0){
                dyn_chunk_buf = (char *)malloc(size_batch_read+1);
                dyn_chunk_buf[0]='\0';
            }
            //strncpy(dyn_chunk_buf+size_csv_header, first_hand_chunk, size_batch_read);
            strcat(dyn_chunk_buf, first_hand_chunk);
            dyn_chunk_buf[size_csv_header+size_batch_read]='\0';
        }

        if (!error_occurred) {
            if (need_patch) {
                //memset(first_hand_chunk, '\0', len_least+1);
                size_batch_read=read(fd_data, first_hand_chunk, len_step);
                first_hand_chunk[len_step]='\0';
            } else {
                size_batch_read=0;
                first_hand_chunk[len_step]='\0';
            }
            log_debug(string("<") +__func__+ "> successive reading : " + to_string(size_batch_read) , true);

            if ((size_batch_read == 0) && need_patch) {
                finished = true;
            } else if (size_batch_read == -1) {
                log_error(string("<") +__func__+ "> failed to read data source", true);
                error_occurred = true;
            } else { //還在檔案中段
                int len_chunk_patch = 0;
                if (need_patch) {
                    first_hand_chunk[size_batch_read]='\0';
                    len_chunk_patch = next_csv_cut(first_hand_chunk, count_dq);
                }

                if (len_chunk_patch==-1){
                    log_error(string("<") +__func__+ "> failed to fetch chunk patch, possibly record too long", true);
                    error_occurred = true;
                } else {
                    bytes_read+=len_chunk_patch;
                    if (len_part > 0){
                        if (bytes_read>=len_part){
                            finished=true;
                        }
                    }

                    first_hand_chunk[len_chunk_patch]='\0';
                    log_debug(string("<") +__func__+ "> chunk patch : (" + to_string(len_chunk_patch) + ")" + string(first_hand_chunk), true);

                    if (true){
                        serial_chunk++;
                        log_debug(string("<") +__func__+ "> ......found chunk in data file, no " + to_string(serial_chunk), true);

                        len_chunk_buf = strlen(dyn_chunk_buf);
                        log_debug(string("<") +__func__+ "> size of main segment (" + to_string(len_chunk_buf) + ")", true);

                        if (len_chunk_patch>0){
                            char * new_locn = (char *)realloc(dyn_chunk_buf, len_chunk_buf+len_chunk_patch+1);
                            if (new_locn==NULL){
                                log_debug(string("<") +__func__+ "> re-allocate memory failed (" + to_string(len_chunk_buf) + ")", true);
                            } else {
                                dyn_chunk_buf = new_locn;
                            }

                            //strncpy(dyn_chunk_buf+len_chunk_buf, first_hand_chunk, len_chunk_patch);
                            strcat(dyn_chunk_buf, first_hand_chunk);
                        }

                        log_debug(string("<") +__func__+ "> ready to write chunk (" + to_string(len_chunk_buf+len_chunk_patch) + ")", true);

                        //write(fd_file_chunk, dyn_chunk_buf, len_chunk_buf+len_chunk_patch);
                        fwrite(dyn_chunk_buf, sizeof(char), len_chunk_buf+len_chunk_patch, ptr_file_chunk);
                        //fsync(fd_file_chunk);
                        fflush(ptr_file_chunk);
                        total_bytes_wrote+=len_chunk_buf;
                        log_debugf("<%s> total_bytes_wrote : %ld", __func__, total_bytes_wrote);

                        log_debug(string("<") +__func__+ "> chunk file length by ftell (" + to_string(ftell(ptr_file_chunk)) + ")", true);
                        fseek(ptr_file_chunk, -20, SEEK_END);
                        char tmp_buf[24];
                        dont_cared_char_ptr=fgets(tmp_buf, 24, ptr_file_chunk);
                        log_debug(string("<") +__func__+ "> chunk file tail : [" + string(tmp_buf) + "]", true);

                        char buffer[512];
                        fseek(ptr_file_chunk, 0, SEEK_SET);
                        dont_cared_char_ptr=fgets(buffer, 512, ptr_file_chunk);
                        log_debug(string("<") +__func__+ "> chunk file line 1 : " + buffer , true);
                        dont_cared_char_ptr=fgets(buffer, 512, ptr_file_chunk);
                        log_debug(string("<") +__func__+ "> chunk file line 2 : " + buffer , true);
                        dont_cared_char_ptr=fgets(buffer, 512, ptr_file_chunk);
                        log_debug(string("<") +__func__+ "> chunk file line 3 : " + buffer , true);

                        // keep sample chunk in /tmp/streamer
                        int num_chunks = get_num_file_chunks_to_keep();
                        if (num_chunks>=1){
                            
                            if ((serial_chunk==1) || (serial_chunk<num_chunks)){
                                FILE * fptr2;

                                string random_serial = get_current_date("unix_ms");
                                string temp_file = string("/tmp/streamer/slice_") + random_serial + "_" + to_string(serial_chunk) + string(".csv");

                                fptr2 = fopen(temp_file.c_str(), "w");
                                if (fptr2 == NULL)
                                {
                                    log_debug(string("<") +__func__+ "> cannot create sample chunk file : " + temp_file , true);
                                }

                                fwrite(dyn_chunk_buf, sizeof(char), len_chunk_buf+len_chunk_patch, fptr2);

                                /*
                                // Read contents from file
                                char c;
                                fseek(ptr_file_chunk, 0, SEEK_SET);
                                c = fgetc(ptr_file_chunk);
                                while (c != EOF)
                                {
                                    fputc(c, fptr2);
                                    c = fgetc(ptr_file_chunk);
                                }
                                */
                                fclose(fptr2);
                            }
                            
                        }

                        //munmap(ptr_to_content, len_chunk_buf);
                        log_debug(string("<") +__func__+ "> file length by seek (" + to_string(lseek64(fd_file_chunk , 0 , SEEK_END)) + ")", true);

                        lseek64(fd_file_chunk , 0 , SEEK_SET);
                        chunk_file_family.push_back(fd_file_chunk);
                        log_debugf("<%s> new entry to chunk_file_family : %d", __func__, fd_file_chunk);

                        if (need_patch) {
                            lseek64(fd_data , len_chunk_patch-size_batch_read , SEEK_CUR);
                        }

                        free(dyn_chunk_buf);
                        dyn_chunk_buf=NULL;
                    }
                }
            }
        }

        if (error_occurred){
            free(first_hand_chunk);
            if (dyn_chunk_buf!=NULL) {
                free(dyn_chunk_buf);
            }
            dyn_chunk_buf=NULL;
                
            return false;
        }
        
        if (finished){
            log_debug(string("<") +__func__+ "> -------------------file processed", true);
            free(first_hand_chunk);
            if (dyn_chunk_buf!=NULL) {
                free(dyn_chunk_buf);
            }
            dyn_chunk_buf=NULL;
            
            return true;
        }

        if (dyn_chunk_buf!=NULL) {
            free(dyn_chunk_buf);
        }
        dyn_chunk_buf=NULL;
    }   // to find next chunk
}

JsonToCsv_return json2csv_adapter::svt_transform(GoSlice inputs) {
    JsonToCsv_return go_ret = JsonToCsv(
    inputs, // json input
    {(char*)_out_dir.c_str(), (GoInt)_out_dir.length()}); // output dir

    log_debug(string("<") +__func__+ "> .... r1 (num of output tables) : " + to_string(go_ret.r1), true);
    log_debug(string("<") +__func__+ "> .... r2 (size of longest slice): " + to_string(go_ret.r2), true);
    log_debug(string("<") +__func__+ "> .... r4 (num of fds)           : " + to_string(go_ret.r4), true);
    log_debug(string("<") +__func__+ "> .... r5 (size of longest slice): " + to_string(go_ret.r5), true);
    log_debug(string("<") +__func__+ "> .... r7 (num of err msqs)      : " + to_string(go_ret.r7), true);

    for(int i=0; i<go_ret.r1; i++) {
        for(int j=0; j<go_ret.r2; j++) {
            log_debugf("<%s> table[%d][%d] : [%s]", __func__, i, j, go_ret.r0[i][j]);
        }
    }

    for(int i=0; i<go_ret.r7; i++) {
        log_debugf("<%s> err msg %d : [%s]", __func__, i, go_ret.r6[i]);
    }

    return go_ret;
}

int json2csv_adapter::pending_materials()
{
    return _num_material_files;
}

bool json2csv_adapter::transform(vector<source_combo_struct*>& data_arrivals, string& doc_id, size_t& count, msg_info_struct *msg_info)
{
    log_debugf("------->   <%s>", __PRETTY_FUNCTION__);

    int control_flow=2;

    if (control_flow==1) {
        int transform_done;
        while (1) {
            transform_done = multipass_transform(data_arrivals, doc_id, count, msg_info);
            log_debugf("<%s> _num_material_files : %d", __func__, _num_material_files);

            if (!transform_done || _num_material_files==0) {
                break;
            }
            sleep(10);
        }
        return transform_done;
    } else {
        return multipass_transform(data_arrivals, doc_id, count, msg_info);
    }
}

bool json2csv_adapter::multipass_transform(vector<source_combo_struct*>& data_arrivals, string& doc_id, size_t& count, msg_info_struct *msg_info)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    if (data_arrivals.empty()) 
    {
        if (_pending) 
        {
            // $$ means it is time out
            //_pending = 0;            
            check_flush_time("commit");
            return commit(_is_file_receiver, _file_chunk_mode);
        }        
        return true;
    }

    int num_of_files = 0;
    int num_of_arrivals = data_arrivals.size();
    if (_is_file) {
        int least_file_seg = _file_chunk*1024*1024;
        int limited_file_len = least_file_seg*2;
        source_combo_struct * source_combo_ptr;
        int itFileNo;

        if (_num_material_files==0){
            _file_chunk_mode = 0;
            for (int idx_arrivals = 0; idx_arrivals < num_of_arrivals; ++idx_arrivals)
            {
                //source_combo_struct * source_combo_ptr;
                source_combo_ptr = data_arrivals[idx_arrivals];
                //log_debugf("<%s> send data file #%d, input topic [%s], is final part[%c]", __func__, idx_arrivals+1, source_combo_ptr->topic.c_str(), source_combo_ptr->is_final_part);
                if (source_combo_ptr->is_final_part=='Y'){
                    num_of_files+=1;
                    //log_debugf("<%s> num_of_files incresed, now = %d", __func__, num_of_files);
                }

                switch (source_combo_ptr->handle_type) {
                    case 'D':
                        itFileNo = source_combo_ptr->file_descriptor;             
                        break;
                    case 'P':
                    default:
                        itFileNo = fileno(source_combo_ptr->file_ptr);
                        break;
                }

                //log_debug(string("<") +__func__+ "> descriptor of file picked : " + to_string(itFileNo), true);

                struct stat in_stat = {0};
                if (0 != fstat(itFileNo, &in_stat)) 
                {
                    log_error(string("<") +__func__+ "> fstat failed", true);
                } else {
                    //log_debug(string("<") +__func__+ "> Prepare to work on file with length " + to_string(in_stat.st_size), true);

                    if (in_stat.st_size <= least_file_seg) {
                        input_file_descs.push_back(itFileNo);
                        input_file_names.push_back(source_combo_ptr->file_name);
                        input_files.push_back((GoInt)itFileNo);
                        input_topics.push_back(source_combo_ptr->topic);
                    } else {  // large files need to be splited
                        if (_is_json_input){
                            bool set_json_limit = false;
                            if (set_json_limit){
                                log_error(string("data file size exceeds limitation (") + to_string(least_file_seg) + ")", true );
                                return false;
                            } else {
                                input_file_descs.push_back(itFileNo);
                                input_file_names.push_back(source_combo_ptr->file_name);
                                input_files.push_back((GoInt)itFileNo);
                                input_topics.push_back(source_combo_ptr->topic);
                            }
                        } else {
                            if (source_combo_ptr->data_length <= least_file_seg){ // final chunk
                                FILE * ptr_file_chunk = tmpfile();
                                FILE * ptr_file_base = fdopen( dup(itFileNo), "r" );
                                fseek(ptr_file_base, source_combo_ptr->data_offset, SEEK_SET);

                                char c;
                                for (int _idx=0; _idx<source_combo_ptr->data_length; _idx++){
                                    c = fgetc(ptr_file_base);
                                    fputc(c,ptr_file_chunk);
                                }
                                fflush(ptr_file_chunk);

                                int fd_file_chunk = fileno(ptr_file_chunk);
                                input_file_descs.push_back(fd_file_chunk);
                                input_file_names.push_back(source_combo_ptr->file_name);
                                input_files.push_back((GoInt)fd_file_chunk);
                                input_topics.push_back(source_combo_ptr->topic);
                                fclose(ptr_file_base);
                            } else {
                                //log_debug(string("<") +__func__+ "> spliting file into chunks...", true);
                                _file_chunk_mode = 1;
                                vector<int> chunk_file_family;
                                if (slice_data_file(itFileNo, least_file_seg, source_combo_ptr->data_offset, source_combo_ptr->data_length, chunk_file_family)){
                                    int child_count = chunk_file_family.size();

                                    if (_level_gigabytes==1) {
                                        _num_chunk_bunch=child_count;
                                    } else {
                                        _num_chunk_bunch=child_count/_level_gigabytes+1;
                                    }
                                    //log_debug(string("<") +__func__+ "chunk files sliced : " + to_string(child_count), true );
                                    for (int child_idx = 0; child_idx < child_count; ++child_idx){
                                        log_debug(string("<") +__func__+ "----> chunk file's desc : " + to_string(chunk_file_family[child_idx]), true );
                                        input_file_descs.push_back(chunk_file_family[child_idx]);
                                        input_file_names.push_back(source_combo_ptr->file_name);
                                        input_files.push_back((GoInt)chunk_file_family[child_idx]);
                                        input_topics.push_back(source_combo_ptr->topic);
                                    }
                                } else {
                                    log_error(string("<") +__func__+ "failed to slice data file into chunks", true );
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }

        log_debugf("<%s> num_of_files pst1, now = %d", __func__, num_of_files);

        if (_num_material_files==0){
            _num_material_files = input_files.size();
            //int next_queue_head = 0;
            _next_queue_head = 0;
        }
        // 20230328 deprecated
        //int num_giga_chunk = _num_chunk_bunch;
        int num_batch_files;
        int queue_head = 0;

        while (_num_material_files > 0) {

            queue_head = _next_queue_head;

            if (_file_chunk_mode) {
                if (_num_material_files>=_svt_workers){
                    num_batch_files = _svt_workers;
                } else {
                    num_batch_files = _num_material_files;
                }
            } else {
                num_batch_files=_num_material_files;
            }

            _num_material_files-=num_batch_files;

            //GoSlice input = {&input_files[0], count_files, count_files};
            GoSlice inputs = {&input_files[queue_head], num_batch_files, num_batch_files};

            log_debugf("<%s> files finished : %d", __func__, queue_head);

            _next_queue_head += num_batch_files;

            log_debugf("<%s> start processing pre-SVT file #%d\n--------> JsonToCsv", __func__, input_files[queue_head]);

            /*
            JsonToCsv_return go_ret = JsonToCsv(
                input, // json input
                {(char*)_out_dir.c_str(), (GoInt)_out_dir.length()}); // output dir
            */

            JsonToCsv_return go_ret = svt_transform(inputs);

            vector<string> Err_msgs;
            vector<vector<string> > Tbl_names;
            vector<vector<string> > Milestones;
            vector<vector<FILE*> > Csv_fptrs;
            vector<vector<int> > Cooked_fd_lists;
            vector<string> Topics;

            // Err_msgs.reserve(count_files);
            // Tbl_names.reserve(count_files);
            // Csv_fptrs.reserve(count_files); 

            // we've to copy Go memory to avoid being GC.
            for (int idx_csv_batch= 0; idx_csv_batch < num_batch_files; ++idx_csv_batch) 
            {
                //log_debug(string("<") +__func__+ "> descriptor of file sliced : " + to_string(input_file_descs[queue_head+idx_csv_batch]), true);
                log_debugf("<%s> (No.%d)pre-SVT file : [%d][%s]", __func__, idx_csv_batch, input_file_descs[queue_head+idx_csv_batch], input_file_names[queue_head+idx_csv_batch].c_str());

                // svt may filter some files
                // 0813 based on err msgs
                // if (idx_csv_batch >= go_ret.r1 || idx_csv_batch >= go_ret.r7  ) {
                //     //fclose(data_arrivals[idx_csv_batch]->file_ptr);
                //     close(input_file_descs[queue_head+idx_csv_batch]);
                //     continue;
                // }

                //log_debugf("<%s> (%d)init sliced file processing, topic [%s]", __func__, idx_csv_batch, input_topics[queue_head+idx_csv_batch].c_str());
                Err_msgs.push_back(string(go_ret.r6[idx_csv_batch]));
                Tbl_names.push_back(vector<string>());
                Milestones.push_back(vector<string>());
                Csv_fptrs.push_back(vector<FILE*>());
                Cooked_fd_lists.push_back(vector<int>());
                Topics.push_back(input_topics[queue_head+idx_csv_batch]);
                //log_debugf("<%s> (%d)initialized", __func__, idx_csv_batch);

                if (idx_csv_batch<go_ret.r1){
                    if (go_ret.r0[idx_csv_batch][0] == nullptr) {   
                        //fclose(data_arrivals[idx_csv_batch]->file_ptr);
                        close(input_file_descs[queue_head+idx_csv_batch]);
                        // 0813 based on err msgs
                        //continue;
                    }
                }

                //log_debugf("<%s> (%d)start branch processing", __func__, idx_csv_batch);
                vector<string>& tbl_list = Tbl_names[idx_csv_batch]; 
                vector<string>& milestone_list = Milestones[idx_csv_batch]; 
                vector<FILE*>& csv_list = Csv_fptrs[idx_csv_batch];
                vector<int>& cooked_fd_list = Cooked_fd_lists[idx_csv_batch];
                //log_debugf("<%s> (%d)branch info fetched", __func__, idx_csv_batch);

                const char * lack_cstr = "---";
                if (strcmp(go_ret.r6[idx_csv_batch], "") != 0) {
                    //fclose(data_arrivals[idx_csv_batch]->file_ptr);
                    close(input_file_descs[queue_head+idx_csv_batch]);

                    tbl_list.push_back(lack_cstr);
                    milestone_list.push_back(input_file_names[queue_head+idx_csv_batch]);
                    csv_list.push_back(nullptr);
                    cooked_fd_list.push_back(-1);
                    log_debugf("<%s> exclouded file : [%s][%s]", __func__, input_file_names[queue_head+idx_csv_batch].c_str(), go_ret.r6[idx_csv_batch]);

                    continue;
                }

                for (int j = 0; j < go_ret.r2; ++j) // traverse table names
                {
                    //log_debugf("<%s> (%d)(%d)table name for the branch : [%s]", __func__, idx_csv_batch, j, go_ret.r0[idx_csv_batch][j]);
                    char * fact_table_name;
                    if (idx_csv_batch<go_ret.r1){
                        if (go_ret.r0[idx_csv_batch][j] == nullptr) {
                            if (j==0) {
                                fact_table_name = (char*)lack_cstr;
                            } else {
                                break;
                            }
                        } else {
                            fact_table_name = go_ret.r0[idx_csv_batch][j];
                        }
                    } else {
                        fact_table_name = (char*)lack_cstr;
                    }
                    string tbl_name(fact_table_name);
                    if (tbl_name.length()<=0 || tbl_name.length() > 49 || tbl_name == lack_cstr) // buf allocated in SVT is 50
                    { 
                        tbl_list.push_back(lack_cstr);
                        milestone_list.push_back(input_file_names[queue_head+idx_csv_batch]);
                        csv_list.push_back(nullptr);
                        cooked_fd_list.push_back(-1);
                        Error("invalid table name (%ld)\n", tbl_name.length());
                    }
                    else 
                    {
                        tbl_list.push_back(tbl_name);
                        milestone_list.push_back(input_file_names[queue_head+idx_csv_batch]);
                        csv_list.push_back(go_ret.r3[idx_csv_batch][j]);
                        cooked_fd_list.push_back(dup( fileno( go_ret.r3[idx_csv_batch][j] ) ));

                        /*
                        FILE * fp = go_ret.r3[idx_csv_batch][j];
                        fseek(fp, 0, SEEK_END);
                        long file_size = ftell(fp);
                        fseek(fp, 0, SEEK_SET);
                        log_debugf("<%s> SVT returned table = %s, FILE ptr = 0x%X, FILE len = %ld", __func__, tbl_name.c_str(), fp, file_size);
                        */
                    }
                }         
                //fclose(data_arrivals[idx_csv_batch]->file_ptr);
                close(input_file_descs[queue_head+idx_csv_batch]);
                //data_arrivals[idx_csv_batch]->file_ptr = nullptr;
            }


            // Here to delete array to free memory!!!!!
            // 20230408 not to free memory allocated in golang
            // may cause "fatal error: found bad pointer in Go heap (incorrect use of unsafe or cgo?)"
            int free_CGO_return = 0;
            if (free_CGO_return) {
                for(int i=0; i<go_ret.r1; i++) {
                    for(int j=0; j<go_ret.r2; j++)
                        free(go_ret.r0[i][j]);
                    free(go_ret.r0[i]);
                }
                free(go_ret.r0);

                // c_out_fd
                for(int i=0; i<go_ret.r4; i++)
                    free(go_ret.r3[i]);
                free(go_ret.r3);

                // c_err_str
                for(int i=0; i<go_ret.r7; i++) {
                    //log_debugf("<%s> err msg %d : [%s]", __func__, i, go_ret.r6[i]);
                    free(go_ret.r6[i]);
                }
                free(go_ret.r6);
                // freeJsonToCsvReturn(go_ret);
            }
        
            string temp_error_string;
            vector<string>::const_iterator it_err = Err_msgs.begin();
            vector<vector<string> >::const_iterator it_tbl_name = Tbl_names.begin();
            vector<vector<string> >::const_iterator it_milestone = Milestones.begin();
            vector<vector<FILE*> >::const_iterator it_csv_fptr = Csv_fptrs.begin();
            vector<vector<int> >::const_iterator it_cooked_fd_list = Cooked_fd_lists.begin();
            vector<string>::const_iterator it_topic = Topics.begin();

            //map<string, int> grouped_fd_map;
            map<string, FILE *> grouped_fstream_map;
            //for (; it_tbl_name != Tbl_names.end(); ++it_err, ++it_tbl_name, ++it_milestone, ++it_csv_fptr, ++it_cooked_fd_list, ++it_topic) 
            for (; it_err != Err_msgs.end(); ++it_err, ++it_tbl_name, ++it_milestone, ++it_csv_fptr, ++it_cooked_fd_list, ++it_topic) 
            {
                if (!it_err->empty()) {
                    if(temp_error_string.empty()) {
                        temp_error_string = it_err->c_str();
                    } else {
                        temp_error_string = temp_error_string + "; " + it_err->c_str();
                    }
                    Error("%s, transform json to csv fail: %s\n", __func__, it_err->c_str());

                    // if logging for empty output is needed...                    
                    if (_is_file_receiver && !_file_chunk_mode) {
                        _pending ++;
                        add_milestone("---", *(it_milestone->begin()), 0);
                    }
                    
                    continue;
                }
                if (it_tbl_name->empty() || it_csv_fptr->empty())
                    continue;

                vector<string>::const_iterator it_tbl = it_tbl_name->begin();
                vector<string>::const_iterator it_mstone = it_milestone->begin();
                vector<FILE*>::const_iterator it_file = it_csv_fptr->begin();
                vector<int>::const_iterator it_cooked_fd = it_cooked_fd_list->begin();


                for (; it_tbl != it_tbl_name->end(); ++it_tbl, ++it_mstone, ++it_file, ++it_cooked_fd) 
                {
                    /* //just for debugging
                    char log_buf[128];
                    FILE * fp = *it_file;
                    fseek(fp, 0, SEEK_END);
                    long file_size = ftell(fp);
                    fseek(fp, 0, SEEK_SET);
                    //sprintf(log_buf, "try dumping cooked file to db, table = %s, FILE ptr = %X, FILE len = %ld, fd = %d", (*it_tbl).c_str(), *it_file, file_size, *it_cooked_fd);
                    //log_debug(string("\n\n<") +__func__+ "> " + log_buf, true);
                    log_debugf("\n\n<%s> try dumping cooked file to db, table = %s, FILE ptr = %X, FILE len = %ld, fd = %d", __func__, (*it_tbl).c_str(), *it_file, file_size, *it_cooked_fd);

                    fgets(log_buf, 128, fp);
                    log_debug(string("\n\n<") +__func__+ "> file content : " + log_buf, true);
                    fseek(fp, 0, SEEK_SET);
                    fclose(fp);
                    */

                    if (*it_file < 0)
                        continue; 
                    FILE * cooked_csv_ptr = *it_file;
                    int cooked_csv_fd = *it_cooked_fd;

                    //20230307 start breaking up cooked files
                    string fact_table_name = *it_tbl;
                    //char break_up = '0';
                    string pattern_topic ("{KAFKA_TOPIC}");
                    int pattern_t_start = fact_table_name.find(pattern_topic);
                    if (pattern_t_start==string::npos) {
                        pattern_topic = string("_KAFKA_TOPIC_");
                        pattern_t_start = fact_table_name.find(pattern_topic);
                    }
                    if (pattern_t_start==string::npos) {
                        pattern_topic = string("{MQTT_TOPIC}");
                        pattern_t_start = fact_table_name.find(pattern_topic);
                    }
                    if (pattern_t_start==string::npos) {
                        pattern_topic = string("_MQTT_TOPIC_");
                        pattern_t_start = fact_table_name.find(pattern_topic);
                    }

                    string pattern_cell ("$");
                    int pattern_c_start = string::npos;
                    if (pattern_t_start==string::npos) {
                        pattern_c_start = fact_table_name.find(pattern_cell);
                        if (pattern_c_start==string::npos) {
                            pattern_cell = string("_CELL_"); // sample : _CELL_6_
                            pattern_c_start = fact_table_name.find(pattern_cell);
                        }
                    }

                    if (pattern_t_start!=string::npos) {
                        fact_table_name.replace(pattern_t_start, pattern_topic.length(), *it_topic); //{KAFKA_TOPIC}
                        log_debugf("<%s> send data to table [%s]", __func__, fact_table_name.c_str());

                        if (grouped_fstream_map.find(fact_table_name) != grouped_fstream_map.end()) {
                            FILE * family_ptr = grouped_fstream_map[fact_table_name];
                            fseek(cooked_csv_ptr, 0, SEEK_SET);
                            char c = fgetc(cooked_csv_ptr);
                            while (c != EOF){
                                fputc(c,family_ptr);
                                c = fgetc(cooked_csv_ptr);
                            }
                            fclose(cooked_csv_ptr);                            
                        } else {
                            //grouped_fd_map[fact_table_name]=cooked_csv_fd;
                            grouped_fstream_map[fact_table_name]=cooked_csv_ptr;
                            fseek(cooked_csv_ptr, 0, SEEK_END);
                        }
                        
                        /*
                        if (!transmit(fact_table_name, cooked_csv_fd)) 
                        {
                            Error("%s, transmit file to channel '%s' fail (cooked_csv_fd %d): %s\n",
                                    __func__, it_tbl->c_str(), cooked_csv_fd, strerror(errno));       
                        }
                        */
                    } else if (pattern_c_start!=string::npos) {
                        //break_up = 'c'; //cell based

                        string cell_order_str = fact_table_name.substr(pattern_c_start+pattern_cell.length());
                        int cell_order = atoi(cell_order_str.c_str());
                        char cell_order_buf[64];
                        sprintf(cell_order_buf, "%d", cell_order);

                        char key_for_cell_matching[64];
                        sprintf(key_for_cell_matching, "$%d", cell_order);

                        int opt_cell_order = cell_order;
                        size_t pos = 0;  
                        string field_list;
                        char fact_table_name_str[128];
                        //string fact_table_name_uCase = fact_table_name;
                        strcpy(fact_table_name_str, fact_table_name.c_str());
                        strupr(fact_table_name_str);
                        string fact_table_name_uCase = fact_table_name_str;
                        for (const auto& output_stmt : _output_table_def) {
                            //OUTPUT+IVS_$5+[$1,+$2,+$3,+$4,+$5)]
                            //OUTPUT+IVS_$5+ALL

                            log_debugf("<%s> ready entry : [%s]", __func__, output_stmt.c_str());
                            string judgement = "OUTPUT+" + fact_table_name_uCase + "+";
                            log_debugf("<%s> looking for OUTPUT entry with [%s]", __func__, judgement.c_str());

                            if ((pos = output_stmt.find(judgement)) != std::string::npos) {
                                string given_str = output_stmt;
                                given_str.erase(0, pos+judgement.length());
                                if (( pos = given_str.find ("[")) != std::string::npos) {  
                                    given_str.erase(0, pos + 1);  
                                    //cout << given_str << endl; // it print last token of the string.
                                    if (( pos = given_str.find ("]")) != std::string::npos) {  
                                        field_list = given_str.substr(0, pos);

                                        char field_list_str[512];
                                        strcpy(field_list_str, field_list.c_str());
                                        // first token
                                        char *token = strtok(field_list_str, ",");
                                        int new_order = 1;
                                        while (token != NULL) {
                                            //log_debugf("<%s> check output field : %s", __func__, token);

                                            if (*(token + strlen(token)-1) == '+') {
                                                *(token + strlen(token)-1) = 0;
                                            }
                                            if (*token == '+') {
                                                token+=1;
                                            }

                                            if (strcmp(key_for_cell_matching, token)==0) {
                                                opt_cell_order = new_order;
                                                log_debugf("<%s> output order for cell breaking set : %d", __func__, opt_cell_order);
                                                break;
                                            }
                                            new_order++;
                                            token = strtok(NULL, ",");
                                        }
                                    }  
                                }  
                                break;
                            } 
                        }

                        //map<string, int> child_files;
                        map<string, FILE *> child_files;
                        break_up_csv_file(child_files, cooked_csv_fd, opt_cell_order);

                        for (const auto& pair_of_file_stream : child_files) {
                            std::cout << "cell : " << pair_of_file_stream.first << ", file stream ptr: " << pair_of_file_stream.second << "\n";

                            int pattern_postfix=0;
                            if (pattern_cell.length()>1){
                                pattern_postfix=1;
                            }
                            string fact_table_division = fact_table_name;
                            fact_table_division.replace(pattern_c_start, pattern_cell.length()+strlen(cell_order_buf)+pattern_postfix, pair_of_file_stream.first); // $123
                            log_debugf("<%s> send data to table [%s]", __func__, fact_table_division.c_str());

                            if (grouped_fstream_map.find(fact_table_division) != grouped_fstream_map.end()) {
                                FILE * family_ptr = grouped_fstream_map[fact_table_division];
                                //int division_fd = pair_of_file_stream.second;
                                //FILE * division_fs = fdopen(division_fd, "r");
                                FILE * division_fs = pair_of_file_stream.second;
                                fseek(division_fs, 0, SEEK_SET);
                                char c = fgetc(division_fs);
                                while (c != EOF){
                                    fputc(c,family_ptr);
                                    c = fgetc(division_fs);
                                }
                                fclose(division_fs);
                            } else {
                                //int tmp_fd = pair_of_file_stream.second;
                                //FILE * tmp_fs = fdopen(tmp_fd, "a");
                                grouped_fstream_map[fact_table_division]=pair_of_file_stream.second;
                                //fseek(tmp_fs, 0, SEEK_END);
                            }

                            /*
                            if (!transmit(fact_table_division, pair_of_file_stream.second, 0))
                                Error("%s, transmit file to channel '%s' fail (cooked_csv_fd %d)\n",
                                        __func__, fact_table_division.c_str(), pair_of_file_stream.second);
                            */
                        }
                        child_files.clear();
                    } else {
                        fact_table_name = *it_tbl;

                        // for debugging
                        // char line_buf[1024];
                        // fseek(cooked_csv_ptr, 0, SEEK_SET);
                        // while( fgets(line_buf, 1023, cooked_csv_ptr) ) {
                        //     log_debugf("<%s>[%s]line[%s]", __func__, (*it_tbl).c_str(), line_buf);
                        //     //fseek(cooked_csv_ptr, 0, SEEK_SET);
                        // }

                        if (_is_file_receiver && !_file_chunk_mode) {
                            _pending ++;
                            add_milestone(fact_table_name, *it_mstone, count_csv_records(cooked_csv_ptr));
                        }

                        if (grouped_fstream_map.find(fact_table_name) != grouped_fstream_map.end()) {
                            FILE * family_ptr = grouped_fstream_map[fact_table_name];
                            fseek(cooked_csv_ptr, 0, SEEK_SET);
                            /*
                            char c = fgetc(cooked_csv_ptr);
                            while (c != EOF){
                                fputc(c,family_ptr);
                                c = fgetc(cooked_csv_ptr);
                            }
                            */

                            int chunk_size = 1024;
                            char buffer[chunk_size];
                            size_t bytes;
                            while (0 < (bytes = fread(buffer, 1, sizeof(buffer), cooked_csv_ptr)))
                                fwrite(buffer, 1, bytes, family_ptr);

                            fclose(cooked_csv_ptr);
                        } else {
                            //grouped_fd_map[fact_table_name]=cooked_csv_fd;
                            grouped_fstream_map[fact_table_name]=cooked_csv_ptr;
                            fseek(cooked_csv_ptr, 0, SEEK_END);
                            //log_debugf("<%s> populating new data file, fp = 0x%X", __func__, cooked_csv_ptr);
                            //log_debugf("<%s> populating new data file, length = %d", __func__, ftell(cooked_csv_ptr));
                        }
        
                        /*
                        if (!transmit(*it_tbl, cooked_csv_fd)) 
                        {
                            Error("%s, transmit file to channel '%s' fail (cooked_csv_fd %d): %s\n",
                                    __func__, it_tbl->c_str(), cooked_csv_fd, strerror(errno));       
                        }
                        */
                    }

                    //close(cooked_csv_fd); 
                }
            }

            for (const auto& fstream_map : grouped_fstream_map) {
                //log_debugf("<%s> data file to send, length = %d", __func__, ftell(fstream_map.second));
                fflush(fstream_map.second);
                int grouped_fd = fileno(fstream_map.second);
                //log_debugf("\n\n<%s> try dumping grouped file to db, table = %s, fp = 0x%X", __func__, fstream_map.first.c_str(), fstream_map.second);
                if (!transmit(fstream_map.first, grouped_fd)) 
                {
                    Error("%s, transmit file to channel '%s' fail (fd %d): %s\n",
                            __func__, fstream_map.first.c_str(), grouped_fd, strerror(errno));       
                }
                close(grouped_fd); 
            }

            Err_msgs.clear();
            Tbl_names.clear();
            Csv_fptrs.clear();
            
            // if(!temp_error_string.empty())
            // {
            //     Write_Parse_ErrorLog(msg_info, temp_error_string, true);
            // }
        }
        _next_queue_head = 0;

        log_debugf("<%s> num_of_files pst2, now = %d", __func__, num_of_files);

        //vector<int> input_file_descs;
        //vector<GoInt> input_files;
        //vector<string> input_topics;
        
        input_file_descs.clear();
        input_file_descs.shrink_to_fit();
        input_file_names.clear();
        input_file_names.shrink_to_fit();
        input_files.clear();
        input_files.shrink_to_fit();
        input_topics.clear();
        input_topics.shrink_to_fit();
    }
    else {
        Error("%s, not supported: only supports file base.", __func__);
        return false;
    }
    
    //log_debugf("<%s> pending files [%d], new files [%d], new arrivals [%d]", __func__, _pending, num_of_files, num_of_arrivals);
    if (!(_is_file_receiver && !_file_chunk_mode)) {
        _pending += num_of_files;
    }
    
    num_of_files=0;
    log_debugf("<%s> commit verification : now [%d], need [%d]", __func__, _pending, _batch_checkpointing);

    if (_pending >= _batch_checkpointing) {
        check_flush_time("commit");
        return commit(_is_file_receiver, _file_chunk_mode);
    }
    else
    {
        if(check_flush_time("compare") > _flush_time_limit*100) // $$ default is 600 sec
        {
            //_pending = 0;
            check_flush_time("commit");
            return commit(_is_file_receiver, _file_chunk_mode);
        }
    
    }    
    return true;
}
