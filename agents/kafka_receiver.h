#pragma once

#include <vector>

#include <rdkafka.h>
#include "receiver.h"
#include "common_func.h"

typedef enum {
    KRECV_SYNCTYPE_NORM,
    KRECV_SYNCTYPE_RECV_LAST,
    KRECV_SYNCTYPE_RECV,
    KRECV_SYNCTYPE_TRANS
} kafka_recv_sync_type;

class kafka_receiver : public receiver
{
    public:
        kafka_receiver() :
        _broker("127.0.0.1:9092"), _kafka_group("bigobject.stream.agent"),
        _out2file(""), _read_timeout(1000), _stype(KRECV_SYNCTYPE_NORM),
		_kafka(NULL), _kafka_conf(NULL), _msg_tailing(false), _msg_heading(false), _msg_appending(false)
        {}
        virtual ~kafka_receiver();

        virtual ssize_t read_receiver(string& data, source_combo_struct* data_picked, msg_info_struct *msg_info);

        virtual bool get_last_commitment(string& doc_id);

        virtual bool backoff(string& doc_id);

        virtual bool sync_strategy(string& rdoc, string& tdoc, string& out);

        virtual bool set_params(string& group, map<string, string>& param);

        bool isKafkaRecv(map<string, string>& param);
        virtual vector<string> get_prefixs();
    private:
        string      _broker;
        string      _kafka_group;
        string      _out2file;
		int			_read_timeout;
        kafka_recv_sync_type _stype;

        rd_kafka_t*         _kafka;
        rd_kafka_conf_t*    _kafka_conf;
        bool                _msg_tailing;
        bool                _msg_heading;
        bool                _msg_appending;

        vector<string>              _topics;
        vector<vector<ssize_t> >    _offsets; // $$ offset for each partition and each topic, $$ _offsets[topic][partition] = offset
        vector<source_combo_struct *>    _cooked_data_catalogue;

        bool set_partition_offset();
        bool get_partition_offset(vector<vector<ssize_t> >& part_offset);
        bool get_partition_offset_from_docid(const string& doc_id, vector<vector<ssize_t> >& part_offset);

        int find_topic_index(const char* topic);

        static void rebalance_cb(
            rd_kafka_t *kafka,
            rd_kafka_resp_err_t err,
            rd_kafka_topic_partition_list_t *partitions,
            void *arg);
};