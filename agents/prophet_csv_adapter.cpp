#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <vector>
#include <unistd.h>
#include <dirent.h>

#include <sstream>

#include "bo_counter.h"
#include "prophet_csv_adapter.h"

#define KEY_ADAPTER_IS_FILE             "is_file"
#define KEY_ADAPTER_OUT_DIR             "out_dir"
#define KEY_ADAPTER_BATCH_SIZE          "batch_size"
#define KEY_ADAPTER_OUT_DIR_DEFAULT     "/tmp/bo_csv_out"
#define KEY_ADAPTER_KEEP_ALIVE_FILES    "keep_alive_files"

#define KEY_ADAPTER_FLUSH_TIME_LIMIT    "flush_time_limit"

bool prophet_csv_adapter::clear_cache()
{
    DIR* dir = opendir(_out_dir.c_str());
    if (!dir)
        return errno == ENOENT ? true : false;

    bool ret = true;
    struct dirent *ent = NULL;
    while ((ent = readdir(dir))) {
        if (!strcmp(ent->d_name, ".") || !strcmp(ent->d_name, ".."))
            continue;
        ostringstream path;
        path << _out_dir << "/" << ent->d_name;
        if (remove(path.str().c_str())) {
            Error("%s, remove %s fail: %s\n", __func__, path.str().c_str(), strerror(errno));
            ret = false;
            break;
        }
    }
    closedir(dir);
    return ret;
}

bool prophet_csv_adapter::set_params(string& group, map<string, string>& param)
{
    if (!component::set_params(group, param))
        return false;

    string param_is_file;
    if (get_param_value(KEY_ADAPTER_IS_FILE, param_is_file) &&
        !param_is_file.empty())
        _is_file = true;

    struct stat outdir_stat = {0};
    if (!get_param_value(KEY_ADAPTER_OUT_DIR, _out_dir))
        _out_dir = KEY_ADAPTER_OUT_DIR_DEFAULT;
//    if (!clear_cache())
//        return false;
    if (0 != stat(_out_dir.c_str(), &outdir_stat)) {
        if (0 != mkdir(_out_dir.c_str(), S_IRWXU)) {
            Error("%s, create temporary folder for output fail: %s\n", __func__, strerror(errno));
            return false;
        }
    }

    string batch_size;
    if (get_param_value(KEY_ADAPTER_BATCH_SIZE, batch_size))
        _batch_checkpointing = stoi(batch_size);
    get_param_value(KEY_ADAPTER_KEEP_ALIVE_FILES, _keep_alive_files);

    string flush_time_limit;
    if(get_param_value(KEY_ADAPTER_FLUSH_TIME_LIMIT, flush_time_limit))
        _flush_time_limit = stoi(flush_time_limit);
    // set counter parameter for go program
//    string counter_url = bo_counter::get_counter()->get_counter_url();
//    int counter_retention = bo_counter::get_counter()->get_retention();
//    if (!counter_url.empty())
//        SetCounter({(char*)counter_url.c_str(), (GoInt)counter_url.length()}, (GoInt)counter_retention);
    return true;
}

bool prophet_csv_adapter::transform(vector<FILE*>& data, string& doc_id, size_t& count, msg_info_struct *msg_info)
{
    if (data.empty()) {
        if (_pending) {
            _pending = 0;
            check_flush_time("commit");
            return commit();
        }
        return true;
    }

    int i = 0;
    int data_len = data.size();
    if (_is_file) {
        vector<FILE*>::const_iterator itFilePtr = data.begin();
        for (; itFilePtr != data.end(); ++itFilePtr) {
            int itFile = fileno(*itFilePtr);
            lseek(itFile, 0, SEEK_SET);
            char tbl_name[31] = {0};
            ssize_t tbl_len = read(itFile, tbl_name, sizeof(tbl_name) - 1);
            if (tbl_len <= 0) {
                Error("%s, read fd %d fail: %s\n", __func__, itFile, strerror(errno));

                close(itFile);

                continue;
            }
            // 1st column is table name
            char* delimiter = strchr(tbl_name, ',');
            if (!delimiter) {
                Error("%s, first column MUST be table name.\n", __func__);

                close(itFile);

                continue;
            }
            *delimiter = 0;
            if (!transmit(tbl_name, itFile, (int)(delimiter - tbl_name) + 1))
                Error("%s, transmit file to chanel '%s' fail (fd %d): %s\n",
                        __func__, tbl_name, itFile, strerror(errno));
                        
            close(itFile);
        }
    }
    else {
        Error("%s, not supported: only supports file base.", __func__);
        return false;
    }

    _pending += data_len;
    if (_pending >= _batch_checkpointing) {
        _pending = 0;
        check_flush_time("commit");
        return commit();
    }
    else
    {
        if(check_flush_time("compare") > _flush_time_limit) // $$ default is 300 sec
        {
            _pending = 0;
            check_flush_time("commit");
            return commit();
        }
        
    }
    
    return true;
}