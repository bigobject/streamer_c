#define _GNU_SOURCE         /* See feature_test_macros(7) */
#include <sys/mman.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <component.h>

#include <sstream>

#include <dirent.h> 
#include <limits.h> 

#include "mqtt_receiver.h"

#include <iostream>
#include <bits/stdc++.h>
using namespace std;

#define KEY_RECV_MQTT_KEEPALIVE_INTERVAL "MQTT_keepalive_interval"
#define KEY_RECV_MQTT_CLEAN_SESSION "MQTT_clean_session"

#define KEY_RECV_MQTT_BROKER    "MQTT_broker"
#define KEY_RECV_MQTT_TOPICS    "MQTT_topics"
#define KEY_RECV_MQTT_CLIENT_ID "MQTT_client_id"
#define KEY_RECV_MQTT_QOS       "MQTT_QOS"
#define KEY_RECV_MQTT_IGNORE_MESSAGE       "MQTT_ignore_message"
#define KEY_RECV_MQTT_DISCARD_MESSAGE       "MQTT_discard_message"
#define KEY_RECV_MQTT_LOG_MESSAGE       "MQTT_log_message"

MQTTClient client;
MQTTClient monitor;

int num_mqtt_arrivals = 0;
int num_mqtt_materials = 0;
int num_mqtt_to_send = 0;
int remainder_mqtt = 0;
int diff_stages=0;

int msg_registry_cap = 64;
int msg_registry[64] = {-1};
int msg_registry_cursor = 0;

sem_t _mutex_arrivals;

int mqtt_ignore_msg;
int mqtt_discard_msg;

vector<source_combo_struct*> arrivals_from_broker;
sem_t _mutex_materials;

static mqtt_receiver * ref_to_mqtt_receiver = NULL;

volatile MQTTClient_deliveryToken deliveredtoken;

void delivered(void *context, MQTTClient_deliveryToken dt)
{
    printf("Message with token value %d delivery confirmed\n", dt);
    deliveredtoken = dt;
}

int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message)
{
    /*
    printf("Message arrived\n");
    printf("     topic: %s\n", topicName);
    printf("message ID: %d\n", message->msgid);
    //printf(" struct ID: %c%c%c%c\n", message->struct_id[0], message->struct_id[1], message->struct_id[2], message->struct_id[3]);
    printf("   message: %.*s\n\n", message->payloadlen, (char*)message->payload);
    */
   
    source_combo_struct* inbound_data_ptr = new source_combo_struct;
    *inbound_data_ptr = {};
    if (!mqtt_ignore_msg) {
        int msg_buf_len = message->payloadlen + 1;
        char * mqtt_msg = new char[msg_buf_len];
        snprintf(mqtt_msg, msg_buf_len, "%s", (char*)message->payload);

        log_debugf("<%s> got mqtt msg : [#%d][q%d][mid%d][rtd%d][len%d][%s]", __func__, num_mqtt_arrivals+1, message->qos, message->msgid, message->retained, message->payloadlen, mqtt_msg);
        if (message->qos > 0) {
            if (message->qos == 1 && message->retained > 0) {
                return 1;
            }
            bool duplicated = find(begin(msg_registry), end(msg_registry), message->msgid) != end(msg_registry);
            if (duplicated) {
                log_debugf("<%s> duplicated msg received : [#%d][q%d][mid%d][rtd%d][len%d]", __func__, num_mqtt_arrivals+1, message->qos, message->msgid, message->retained, message->payloadlen);
                return 1;
            } else {
                msg_registry[msg_registry_cursor] = message->msgid;
                msg_registry_cursor+=1;
                if (msg_registry_cursor==msg_registry_cap) {
                    msg_registry_cursor=0;
                }
            }
        }

        inbound_data_ptr->handle_type = 'D';
        sprintf(inbound_data_ptr->type, "MQTT_MSG_NORM");
        inbound_data_ptr->msg_buf_ptr = mqtt_msg;
        //sprintf(inbound_data_ptr->msg_buf_ptr, "%.*s\0",  message->payloadlen, (char*)message->payload);
        inbound_data_ptr->msg_buf_len = msg_buf_len;
        inbound_data_ptr->topic = string(topicName);
        inbound_data_ptr->data_offset = 0;
        inbound_data_ptr->data_length = 0;
        inbound_data_ptr->is_final_part = 'Y';

        string temp_file_serial = get_current_date("unix_ms")+"."+to_string(message->msgid);
        //string temp_file_serial = inbound_data_ptr->topic;
        string temp_file = string("mqtt_") + temp_file_serial + string(".csv");

        inbound_data_ptr->serial = temp_file_serial;
        inbound_data_ptr->file_name = temp_file;

        inbound_data_ptr->status = 1;
    } else {
        sprintf(inbound_data_ptr->type, "MQTT_MSG_NULL");
        inbound_data_ptr->status = -1;
    }
    sem_wait(&_mutex_materials);
    arrivals_from_broker.push_back(inbound_data_ptr);
    num_mqtt_arrivals+=1;
    sem_post(&_mutex_materials);

    sem_post(&_mutex_arrivals);

    MQTTClient_freeMessage(&message);
    MQTTClient_free(topicName);
    return 1;
}

void connlost(void *context, char *cause)
{
    printf("\nConnection lost\n");
    printf("     cause: %s\n", cause);

    source_combo_struct* ind_disconn = new source_combo_struct;
    *ind_disconn = {};
    //ind_disconn->handle_type = 'D';
    sprintf(ind_disconn->type, "MQTT_CONN_DOWN");
    ind_disconn->status = -1;
    sem_wait(&_mutex_materials);
    arrivals_from_broker.push_back(ind_disconn);
    sem_post(&_mutex_materials);

    sem_post(&_mutex_arrivals);
}

bool mqtt_receiver::isChosen(map<string, string>& param) {
    map<string, string>::const_iterator it;
    
    const char *keys[4] = {KEY_RECV_MQTT_BROKER, KEY_RECV_MQTT_TOPICS,
        KEY_RECV_MQTT_CLIENT_ID, KEY_RECV_MQTT_QOS};

    for (int i=0; i<4; i++) {
        it = param.find( keys[i] );
        if (it == param.end()) {
            Info("Not MQTT Mode. Missing %s in config\n", keys[i]);
            return false;
        }
    }
    
    return true;
}

void mqtt_receiver::set_self_ref(mqtt_receiver * obj_ref){
    _obj_ref = obj_ref;
    ref_to_mqtt_receiver = obj_ref;
}


void mqtt_receiver::set_topics(string _topic_picked){
    string::size_type parenthese_l = 0;
    string::size_type parenthese_r = 0;
    bool data_format_set = false;

    _topic_picked = trim_string(_topic_picked);

    parenthese_l = _topic_picked.find_first_of('(', 0);
    parenthese_r = _topic_picked.find_first_of(')', parenthese_l);
    
    string topic_name;
    if (parenthese_l != string::npos) {
        topic_name = _topic_picked.substr(0, parenthese_l);
        topic_name = trim_string(topic_name);
        if (topic_name.empty()) {
            return;
        }

        data_format_set = true;
    } else {
        topic_name = _topic_picked;
    }

    if (data_format_set) {
        if (parenthese_r == string::npos) {
            data_format_set = false;
        } else {
            if (parenthese_r-parenthese_l<=1) {
                data_format_set = false;
            }
        }
    }

    _topic_names.push_back(topic_name);
    _prefixs.push_back(topic_name);
    _topic_traces[topic_name] = string("-1");

    if (data_format_set){
        string fmt_str = _topic_picked.substr(parenthese_l+1, parenthese_r-parenthese_l-1);
        fmt_str = trim_string(fmt_str);
        _topic_types[topic_name] = fmt_str;
    } else {
        _topic_types[topic_name] = string();
    }
    _topic_data.push_back("");
}

bool mqtt_receiver::set_params(string& group, map<string, string>& param)
{
    receiver::set_type(RCV_MQTT);
    if (!receiver::set_params(group, param))
        return false;

    string host_n_port;
    if (get_param_value(KEY_RECV_MQTT_BROKER, host_n_port))
        _broker = string("tcp://") + host_n_port;

    string mqtt_keepalive_itvl;
    if (get_param_value(KEY_RECV_MQTT_KEEPALIVE_INTERVAL, mqtt_keepalive_itvl))
        _keepalive_interval = stoi(mqtt_keepalive_itvl);
    if (_keepalive_interval<=0 || _keepalive_interval>65535){
        _keepalive_interval = 65535;
    }

    string mqtt_clean_session;
    if (get_param_value(KEY_RECV_MQTT_CLEAN_SESSION, mqtt_clean_session))
        _clean_session = stoi(mqtt_clean_session);
    if (_clean_session!=0 && _clean_session!=1){
        _clean_session=0;
    }
    
    string all_topics;
    get_param_value(KEY_RECV_MQTT_TOPICS, all_topics);
    string::size_type pos = 0;
    string::size_type next_pos = 0;
    string _topic_picked;
    string::size_type parenthese_l = 0;
    string::size_type parenthese_r = 0;
    
    bool data_format_set;
    while (string::npos != (next_pos = all_topics.find_first_of(',', pos))) {
        _topic_picked = all_topics.substr(pos, next_pos - pos);

        set_topics(_topic_picked);
        pos = next_pos + 1;
    }
    if (pos < all_topics.length()) {
        _topic_picked = all_topics.substr(pos);

        set_topics(_topic_picked);
    }

    string mqtt_client_id;
    if (get_param_value(KEY_RECV_MQTT_CLIENT_ID, mqtt_client_id))
        _client_id = mqtt_client_id;

    string mqtt_qos;
    if (get_param_value(KEY_RECV_MQTT_QOS, mqtt_qos))
        _qos = stoi(mqtt_qos);

    mqtt_ignore_msg = 0;
    string str_ignore_msg;
    if (get_param_value(KEY_RECV_MQTT_IGNORE_MESSAGE, str_ignore_msg))
        mqtt_ignore_msg = stoi(str_ignore_msg);
    if (mqtt_ignore_msg==1){
        _mqtt_msgs_ignored=0;
    } else {
        _mqtt_msgs_ignored=-1;
    }

    mqtt_discard_msg = 0;
    string str_discard_msg;
    if (get_param_value(KEY_RECV_MQTT_DISCARD_MESSAGE, str_discard_msg))
        mqtt_discard_msg = stoi(str_discard_msg);
    if (mqtt_discard_msg==1){
        _mqtt_msgs_deserted=0;
    } else {
        _mqtt_msgs_deserted=-1;
    }

    int need_to_log_msg;
    string str_log_msg;
    if (get_param_value(KEY_RECV_MQTT_LOG_MESSAGE, str_log_msg))
        need_to_log_msg = stoi(str_log_msg);
    if (need_to_log_msg==1){
        _fd_for_mqtt_msgs_log = open("/tmp/streamer/mqtt_msgs_received.csv", O_CREAT|O_RDWR|O_APPEND|O_TRUNC, 0777);
    } else {
        _fd_for_mqtt_msgs_log = -1;
    }

    sem_init(&_mutex_arrivals, 0, 0);
    sem_init(&_mutex_materials, 0, 1);


    return true;
}

// file id can skip (0,3,5,8...)
//ssize_t mqtt_receiver::read_receiver(string& data, FILE **fileptr, msg_info_struct *msg_info)
ssize_t mqtt_receiver::read_receiver(string& data, source_combo_struct* data_picked_ptr, msg_info_struct *msg_info)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    log_debugf("<%s> size of _cooked_data_catalogue : [%d]", __func__, _cooked_data_catalogue.size());

    if (_cooked_data_catalogue.size()>0){
        *data_picked_ptr = *_cooked_data_catalogue[0];
        delete _cooked_data_catalogue[0];
        _cooked_data_catalogue.erase(_cooked_data_catalogue.begin());

        log_debugf("<%s> prepared data to return : [%d]data to return : [%d], diff = %d", __func__, data_picked_ptr->serial, num_mqtt_arrivals-num_mqtt_to_send);

        return 1;
    }

    FILE **fileptr = &(data_picked_ptr->file_ptr);

    string doc_id;

    int rc;
    int failures = 0;
    //source_combo_struct* object_to_kill;

    FILE *file_ptr;
    bool use_fs = false; // memfd is prefered

    if (_mqtt_client == nullptr) {
        MQTTClient_connectOptions conn_opts = MQTTClient_connectOptions_initializer;
    
        if ((rc = MQTTClient_create(&client, _broker.c_str(), _client_id.c_str(),
            MQTTCLIENT_PERSISTENCE_DEFAULT, NULL)) != MQTTCLIENT_SUCCESS)
        {
            printf("Failed to create client, return code %d\n", rc);
            rc = EXIT_FAILURE;
            goto exit;
        }

        if ((rc = MQTTClient_setCallbacks(client, NULL, connlost, msgarrvd, delivered)) != MQTTCLIENT_SUCCESS)
        {
            printf("Failed to set callbacks, return code %d\n", rc);
            rc = EXIT_FAILURE;
            goto destroy_exit;
        }

        conn_opts.keepAliveInterval = _keepalive_interval;
        conn_opts.cleansession = _clean_session;
        if ((rc = MQTTClient_connect(client, &conn_opts)) != MQTTCLIENT_SUCCESS)
        {
            printf("Failed to connect MQTT broker, return code %d\n", rc);
            rc = EXIT_FAILURE;
            goto destroy_exit;
        }


        for (int i=0; i < _topic_names.size(); i++) {
            printf("Subscribing to topic %s\nfor client %s using QoS:%d\n\n", _topic_names[i].c_str(), _client_id.c_str(), _qos);

            if ((rc = MQTTClient_subscribe(client, _topic_names[i].c_str(), _qos)) != MQTTCLIENT_SUCCESS)
            {
                printf("Failed to subscribe, return code %d\n", rc);
                rc = EXIT_FAILURE;
                failures++;
            }
        }

        // add monitor
        if (failures == 0) {
            string client_id_moniter = _client_id + "-mntr";
            if ((rc = MQTTClient_create(&monitor, _broker.c_str(), client_id_moniter.c_str(),
                MQTTCLIENT_PERSISTENCE_NONE, NULL)) != MQTTCLIENT_SUCCESS)
            {
                printf("Failed to create mqtt monitor, return code %d\n", rc);
                rc = EXIT_FAILURE;
                goto exit;
            }

            if ((rc = MQTTClient_setCallbacks(monitor, NULL, connlost, msgarrvd, delivered)) != MQTTCLIENT_SUCCESS)
            {
                printf("Failed to set callbacks for mqtt monitor, return code %d\n", rc);
                rc = EXIT_FAILURE;
                goto destroy_exit;
            }

            conn_opts.keepAliveInterval = 10;
            conn_opts.cleansession = 1;
            if ((rc = MQTTClient_connect(monitor, &conn_opts)) != MQTTCLIENT_SUCCESS)
            {
                printf("Failed to connect MQTT broker, return code %d\n", rc);
                rc = EXIT_FAILURE;
                goto destroy_exit;
            }
            
            _mqtt_monitor = &monitor;
        } else {
            goto destroy_exit;
        }

        if (failures == 0) {
            _mqtt_client = &client;
            _adapt->set_session_status(1);
        } else {
            goto destroy_exit;
        }
    }

    //sem_wait(&_mutex_arrivals);

    struct timespec ts;
    while (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
        Error("clock_gettime failed\n");
        sleep(5);
    }

    ts.tv_sec += 1;

    int fn_ret;
    while ((fn_ret = sem_timedwait(&_mutex_arrivals, &ts)) == -1 && errno == EINTR)
        continue;       /* Restart if interrupted by handler. */

    /* Check what happened. */

    if (fn_ret == -1) {
        if (errno == ETIMEDOUT){
            log_debug(string("<") +__func__+ "> sem_timedwait() timed out", true);
            sprintf(data_picked_ptr->type, "MQTT_MSG_T_OUT");
            data_picked_ptr->status = -1;
            return -1;
        } else {
            perror("sem_timedwait");
        }
    } else { // new inbound
        log_debug(string("<") +__func__+ "> sem_timedwait() succeeded", true);

        if (arrivals_from_broker.empty()){
            sprintf(data_picked_ptr->type, "MQTT_MSG_NULL");
            data_picked_ptr->status = -1;
            return -1;
        }

        vector<source_combo_struct*> material_vector;

        sem_wait(&_mutex_materials);
        /* v1
        int len_material_vector = arrivals_from_broker.size();
        material_vector.assign(arrivals_from_broker.begin(), arrivals_from_broker.begin()+len_material_vector);
        arrivals_from_broker.erase(arrivals_from_broker.begin(), arrivals_from_broker.begin()+len_material_vector);
        */

        // v2
        material_vector = arrivals_from_broker;
        arrivals_from_broker.clear();
        sem_post(&_mutex_materials);

        log_debugf("<%s> previous materials : [%d]", __func__, num_mqtt_materials, remainder_mqtt);
        int len_material_vector = material_vector.size();
        num_mqtt_materials += len_material_vector;
        remainder_mqtt = num_mqtt_arrivals-num_mqtt_materials;
        log_debugf("<%s> total materials : [%d], single round : [%d], remainder [%d]", __func__, num_mqtt_materials, len_material_vector, remainder_mqtt);

        map<string, int> map_topic_to_fds;
        for(int idx_v=0; idx_v<len_material_vector; idx_v++) {
            source_combo_struct * focus_ptr = material_vector[idx_v];
            log_debugf("<%s> inbound mqtt addr : [0x%X]", __func__, focus_ptr);
            /*
            //memcpy(data_picked_ptr, arrivals_from_broker[0], sizeof(source_combo_struct));
            *data_picked_ptr = *arrivals_from_broker[0];
            delete arrivals_from_broker[0];
            arrivals_from_broker.erase(arrivals_from_broker.begin());
            */

            if (strncmp(focus_ptr->type, "MQTT_", 5)!=0) {
                // weird condition, delete memory allocated may crash
                // just ignore it at present
                Error("%s, weird msg received, type[0] : [%c][%d]\n", __func__, focus_ptr->type[0], focus_ptr->type[0]);
                continue;
            }

            if (strcmp(focus_ptr->type, "MQTT_CONN_DOWN")==0){
                log_debug(string("<") +__func__+ "> lost connection to broker! ", true);

                _adapt->set_session_status(0);

                if (_mqtt_client != nullptr) {
                    MQTTClient_destroy(&client);
                    _mqtt_client = nullptr;
                }

                //log_debugf("<%s> MQTTClient_destroy(&client)", __func__);

                if (_mqtt_monitor != nullptr) {
                    MQTTClient_destroy(&monitor);
                    _mqtt_monitor = nullptr;
                }

                //log_debugf("<%s> MQTTClient_destroy(&monitor)", __func__);

                focus_ptr->status = -1;

                //log_debugf("<%s> status = -1", __func__);

                _cooked_data_catalogue.push_back(focus_ptr);

                //log_debugf("<%s> _cooked_data_catalogue.push_back", __func__);      

                continue;
            }

            log_debugf("<%s> inbound mqtt type : [%s]", __func__, focus_ptr->type);
            if (focus_ptr->msg_buf_ptr == nullptr) {
                log_debugf("<%s> MQTT packet with content lost", __func__);
                // weird condition, delete memory allocated may crash
                // just ignore it at present
                //delete focus_ptr;
                focus_ptr = nullptr;
                continue;
            }
            if (*focus_ptr->msg_buf_ptr == '\0') {
                if (focus_ptr->msg_buf_len>1) { // should have something inside
                    log_debugf("<%s> MQTT packet with zero length msg", __func__);
                    // weird condition, delete memory allocated may crash
                    // just ignore it at present
                    //delete focus_ptr;
                    focus_ptr = nullptr;
                    continue;
                }
            }

            if (use_fs) {
                string temp_file_path = string("/csv/") + focus_ptr->file_name;
                file_ptr = fopen(focus_ptr->file_name.c_str(), "w+");
                /* write to file */
                fprintf(file_ptr, "%.*s", focus_ptr->msg_buf_len-1, focus_ptr->msg_buf_ptr);
                focus_ptr->file_ptr = file_ptr;
            } else {
                if (_mqtt_msgs_ignored>=0){
                    _mqtt_msgs_ignored++;
                    if (_mqtt_msgs_ignored % 1000 == 0) {
                        printf("%s -------------------%9d Messages ignored\n", get_current_date("unix_ms").c_str(), _mqtt_msgs_ignored);
                    }
                    focus_ptr->status = -1;
                    _cooked_data_catalogue.push_back(focus_ptr);
                    continue;
                }

                log_debugf("<%s> inbound mqtt msg : [%d][%s]", __func__, focus_ptr->msg_buf_len, focus_ptr->msg_buf_ptr);
                int fd_for_msg;
                char new_cooked_file;

                if ( _adapt->is_json_input() ) {
                    fd_for_msg = memfd_create(focus_ptr->file_name.c_str(), MFD_ALLOW_SEALING);
                    if (fd_for_msg == -1) {
                        log_error(string("memfd_create:failed to create an anonymous file"), true);
                        focus_ptr->status = -1;
                        _cooked_data_catalogue.push_back(focus_ptr);
                        continue;
                    }
                    new_cooked_file = 'Y';
                } else {
                    map<string, int>::iterator iter = map_topic_to_fds.find(focus_ptr->topic);
                    if(iter == map_topic_to_fds.end()){
                        fd_for_msg = memfd_create(focus_ptr->file_name.c_str(), MFD_ALLOW_SEALING);
                        if (fd_for_msg == -1) {
                            log_error(string("memfd_create:failed to create an anonymous file"), true);
                            focus_ptr->status = -1;
                            _cooked_data_catalogue.push_back(focus_ptr);
                            continue;
                        }
                        map_topic_to_fds[focus_ptr->topic] = fd_for_msg;
                        new_cooked_file = 'Y';
                    } else {
                        fd_for_msg = iter->second;
                        new_cooked_file = 'N';
                    }
                }

                focus_ptr->file_descriptor = fd_for_msg;

                if (_topic_types[focus_ptr->topic].compare("text") == 0 ) {
                    log_debug(string("converting text to CSV..."), true);  

                    char * escape_buf = new char[focus_ptr->msg_buf_len*2+2];
                    int j=1;
                    escape_buf[0]='"';
                    char char_x;
                    for (int i=0; i<focus_ptr->msg_buf_len-1; i++) {
                        char_x = focus_ptr->msg_buf_ptr[i];
                        if (char_x == '"'){
                            escape_buf[j++]='"';
                            escape_buf[j++]='"';
                        } else {
                            escape_buf[j++]=char_x;
                        }
                    }
                    escape_buf[j]='"';
                    escape_buf[j+1]='\n';
                    escape_buf[j+2]=0;
                    focus_ptr->msg_buf_len = j+3;
                    delete focus_ptr->msg_buf_ptr;
                    focus_ptr->msg_buf_ptr = escape_buf;
                }
                log_debugf("<%s> rendered msg : [%d][%s]", __func__, focus_ptr->msg_buf_len, focus_ptr->msg_buf_ptr);

                /*
                int file_len = focus_ptr->msg_buf_len-1;
                ftruncate(fd_for_msg, file_len);
                void* ptr_to_content = mmap(NULL, file_len, PROT_READ|PROT_WRITE, MAP_SHARED, fd_for_msg, 0);
                memset(ptr_to_content, '\0', file_len);
                strncpy((char*)ptr_to_content, focus_ptr->msg_buf_ptr, file_len);
                munmap(ptr_to_content, file_len);
                */
                int len_msg = focus_ptr->msg_buf_len-1;
                if (len_msg>0) {
                    write(fd_for_msg, focus_ptr->msg_buf_ptr, len_msg);
                    if (focus_ptr->msg_buf_ptr[len_msg-1] != '\n'){
                        if (!_adapt->is_json_input()) {
                            write(fd_for_msg, "\n", 1);
                            log_debugf("<%s> newline appended : [#%d]", __func__, num_mqtt_to_send+1);
                        }
                    }
                    num_mqtt_to_send+=1;

                    if (_fd_for_mqtt_msgs_log > 2) {
                        write(_fd_for_mqtt_msgs_log, focus_ptr->msg_buf_ptr, len_msg);
                        if (focus_ptr->msg_buf_ptr[len_msg-1] != '\n'){
                            write(_fd_for_mqtt_msgs_log, "\n", 1);
                            //log_debugf("<%s> newline appended : [#%d]", __func__, num_mqtt_to_send+1);
                        }
                    }
                }

                delete focus_ptr->msg_buf_ptr;
                focus_ptr->msg_buf_ptr = NULL;

                focus_ptr->file_ptr = (FILE*)focus_ptr;

                if (_mqtt_msgs_deserted>=0){
                    log_debugf("<%s> desert msg : [%d]", __func__, _mqtt_msgs_deserted);

                    close(fd_for_msg);
                    sprintf(focus_ptr->type, "MQTT_MSG_NULL");
                    _mqtt_msgs_deserted++;
                    if (_mqtt_msgs_deserted % 1000 == 0) {
                        printf("%s -------------------%9d Messages deserted\n", get_current_date("unix_ms").c_str(), _mqtt_msgs_deserted);
                    }
                    focus_ptr->status=-1;
                }

                if (new_cooked_file == 'Y'){
                    _cooked_data_catalogue.push_back(focus_ptr);
                } else {
                    delete focus_ptr;
                    focus_ptr = nullptr;
                }
            }  
        }

        log_debugf("<%s> msg collected, size of _cooked_data_catalogue : [%d]", __func__, _cooked_data_catalogue.size());
        if (_cooked_data_catalogue.size()>0){
            *data_picked_ptr = *_cooked_data_catalogue[0];
            delete _cooked_data_catalogue[0];
            _cooked_data_catalogue.erase(_cooked_data_catalogue.begin());

            _topic_traces[data_picked_ptr->topic] = data_picked_ptr->serial;
            get_last_commitment(doc_id);
            _adapt->set_doc_id(doc_id);

            log_debugf("<%s> instant data to return : [%s], diff = %d", __func__, data_picked_ptr->serial.c_str(), num_mqtt_arrivals-num_mqtt_to_send);

            return 1;
        } else {
            sprintf(data_picked_ptr->type, "MQTT_MSG_NULL");
            data_picked_ptr->status = -1;
            return -1;
        }
    }

destroy_exit:
    if (_mqtt_client != nullptr) {
        MQTTClient_destroy(&client);
        _mqtt_client = nullptr;
    }
    if (_mqtt_monitor != nullptr) {
        MQTTClient_destroy(&monitor);
        _mqtt_monitor = nullptr;
    }

exit:
    sprintf(data_picked_ptr->type, "MQTT_CONN_FAIL");
    log_info(string("problem connecting to server, try to reconnect in 5 secs..."), true);
    sleep(5);
    return -1;
}

bool mqtt_receiver::get_last_commitment(string& doc_id)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    ostringstream last_commit;

    for (int i=0; i < _prefixs.size(); i++) {
        last_commit << _prefixs[i] << "_" << _topic_traces[_prefixs[i]];
        if (i <  (_prefixs.size() - 1))
            last_commit << "=";
    }
    
    doc_id = last_commit.str();
    
    log_debugf("lastest mqtt receiver commitment is '%s'\n", doc_id.empty() ? "NONE" : doc_id.c_str());
    return true;
}

bool mqtt_receiver::backoff(string& doc_id)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    return true;

    string last_doc;
    get_last_commitment(last_doc);

    // no change
    if (!last_doc.empty() && last_doc.compare(doc_id) == 0)
        return true;
    
    stringstream ss(doc_id);
    string pch;
      
    for (int i=0; getline(ss, pch, '='); i++)
    {
        if (i>=_prefixs.size()) {
            std::ostringstream prefix;
            if (_prefixs.size()) {
                std::copy(_prefixs.begin(), _prefixs.end()-1,
                        std::ostream_iterator<string>(prefix, ","));

                prefix << _prefixs.back();
            } 
            Error("Prefixs(%s) counts less than doc counts(%s)\n", prefix.str().c_str(), doc_id.c_str() );
            return false;
        }
        string prefix = _prefixs[i];
        if (pch.length() <= prefix.length() ||
            0 != pch.substr(0, prefix.length()).compare(prefix)) {
            Error("Prefix not match: %s, %s\n", pch.c_str(), prefix.c_str());
            return false;
        }
        _file_ids[i] = stoul(pch.substr(prefix.length()));
    }
    return true;
}

bool mqtt_receiver::sync_strategy(string& rdoc, string& tdoc, string& out)
{
    switch (_stype) {
        case MQTT_RECV_SYNCTYPE_RECV:
        case MQTT_RECV_SYNCTYPE_RECV_LAST:
            out = rdoc;
            return true;
        case MQTT_RECV_SYNCTYPE_TRANS:
            out = tdoc;
            return true;
    }

    if (rdoc.empty() || tdoc.empty()) {
        out.clear();
        return true;
    }

    out = rdoc.compare(tdoc) > 0 ? tdoc : rdoc;
    return true;
}

vector<string> mqtt_receiver::get_prefixs() {
    return receiver::get_prefixs();
}