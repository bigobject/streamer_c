#include "adapter.h"
#include "common_func.h"

class pass_thru_adapter : public adapter
{
    public:
        pass_thru_adapter() : _is_file(false), _out_dir(""),
        _keep_alive_files("") {}
        virtual ~pass_thru_adapter() {}

        virtual bool set_params(string& group, map<string, string>& param);
        bool isChosen(map<string, string>& param);
    protected:
        virtual bool transform(vector<FILE*>& data, string& doc_id, size_t& count, msg_info_struct *msg_info);
        virtual bool transform(vector<source_combo_struct*>& data_sources, string& doc_id, size_t& count, msg_info_struct *msg_info);
        virtual int pending_materials();
    private:
        bool    _is_file;
        string  _out_dir;
        string  _keep_alive_files;
        //int     _batch_checkpointing;
        //int     _pending;
        int     _flush_time_limit = 300; // $$ default is 300, it can be changed in set_params()

        string  _table_to_dump;

        bool clear_cache();
};