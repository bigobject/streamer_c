#include <iostream>
#include <string>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

#include "kafka_receiver.h"
#include "file_receiver.h"
#include "mqtt_receiver.h"
#include "json2csv_adapter.h"
#include "pass_thru_adapter.h"
#include "bo_transmitter.h"
//#include "influxdb_transmitter.h"
#include "coordinator.h"
#include "component.h"

#ifndef VERSION_DATE
#define VERSION_DATE "1970-01-01"
#endif

void signal_handler(int);
coordinator *agent_dup = NULL;    

int main(int argc, char** argv)
{
    signal(SIGINT, signal_handler);
    signal(SIGHUP, signal_handler);
    signal(SIGQUIT, signal_handler);
    signal(SIGTERM, signal_handler);      

    char* env_var = getenv("DEBUG");
    if(env_var) {
        Info("checking for debug mode : %s\n", env_var);   
        int setting = atoi(env_var);
        if (setting > 0){
            toggle_debug_mode(setting);
        } else {
            log_info(string("<") +__func__ + "> env var DEBUG = " + env_var, true);
        }
    } else {
        Info("It's in production mode\n");   
    }

    env_var = getenv("USE_9091_SYN_MARK");
    if(env_var) {
        Info("checking for packet mode : %s\n", env_var);   
        if (strcmp(env_var, "1")==0){
            toggle_packet_mode(true);
        } else {
            log_info(string("<") +__func__ + "> env var USE_9091_SYN_MARK = " + env_var, true);
        }
    } else {
        Info("It's in SQL script mode\n");   
    }

    env_var = getenv("SAVE_SAMPLE_FILE_CHUNKS");
    if(env_var) {
        Info("will file chunks to keep : %s\n", env_var);
        set_num_file_chunks_to_keep(atoi(env_var));
    } else {
        Info("No need to keep file chunks\n");   
    }

    Info("BigObject Streamer Vesrion 2.1.3.%s\n", VERSION_DATE );   
    Write_System_ErrorLog("streamer start by shell script !!! ", true);
    int ret = 0;

    coordinator         *agent = NULL;    
    kafka_receiver      *kafka_recv = NULL;
    file_receiver       *file_recv = NULL;
    mqtt_receiver       *mqtt_recv = NULL;
    adapter             *adapter_ptr = NULL;
    transmitter         *transmitter_ptr = NULL;
    
//    json2csv_adapter    *j2c_adapt = NULL;
//    influxdb_transmitter    *influxdb_trans = NULL;
//    csv2influxdb_adapter *c2i_adapt;


    if (argc < 2) {
        cout << "Usage: " << argv[0] << " <config file>" << endl;
        return -1;
    }

    // disable sigpipe
    // sine we use blocking mode and doesn't receive data from bo when sendfile,
    // we've no chance to aware of peer closed, thus we've to handle sigpipe at
    // sendfile. Instead of handle signal, we prefer to handle errno = EPIPE.
    if (SIG_ERR == signal(SIGPIPE, SIG_IGN))
        Error("%s, ignore SIGPIPE failed: %s\n", __func__, strerror(errno));
    //=======================================================================
    
    // set buffer null for docker logs
    setbuf(stdout, NULL);

    int retry = 30;
    int sleep_seconds = 30;
    do
    {
        Write_System_ErrorLog("streamer start by while loop (" + to_string(retry) + " times left)", true);

        ret = 0;
    
        agent = new coordinator(argv[1]);
        agent_dup = agent;
        kafka_recv = new kafka_receiver;
        file_recv = new file_receiver;
        mqtt_recv = new mqtt_receiver;
        //adapter_ptr = new json2csv_adapter;

        ini_conf conf(argv[1]);
        map<string, string> section;
        conf.get_section(SECT_RECV, section);

        if (file_recv->isFileRecv(section)) {
            Info("File receiver mode\n");
            agent->set_receiver(file_recv, true);            
        } else if (kafka_recv->isKafkaRecv(section)){
            Info("Kafka receiver mode\n");
            agent->set_receiver(kafka_recv, false);            
        } else if (mqtt_recv->isChosen(section)){
            Info("MQTT receiver mode\n");
            agent->set_receiver(mqtt_recv, false);
            mqtt_recv->set_self_ref(mqtt_recv);
        } else {
            Error("No receiver can be chosen, review configuration file please.\n");
            return -1;
        }

        string type_transform;
        if (conf.get_value(SECT_ADPT, KEY_ADAPTER_TRANSFORM, type_transform)) {
            log_debug(string("transform setting found : ") + type_transform, true);
            if (type_transform.compare(KEY_ADAPTER_TRANSFORM_PASS_THRU)==0) {
                adapter_ptr = new pass_thru_adapter;
                log_info("pass through adapter activated!\n", true);
            }           
        } else {
            log_debug(string("transform setting NOT found !!"), true);
        }
        if (adapter_ptr == NULL) {
            // SVT is the default
            adapter_ptr = new json2csv_adapter;
            log_info("SVT adapter activated!\n", true);
        }        
        
        if(argc > 2)
            agent->set_write_to(argv[2]); 
        
//        if (agent->get_write_to() != "influxdb")
            transmitter_ptr = new bo_transmitter;
//        else
//            transmitter_ptr = new influxdb_transmitter;

        agent->set_transmitter(transmitter_ptr);

        agent->set_adapter(adapter_ptr);

        if (!agent->initialize())
        {
            ret = -2;
            
            delete agent;
            delete kafka_recv;
            delete file_recv;
            delete mqtt_recv;
            delete adapter_ptr;   
            delete transmitter_ptr;
            
            agent = NULL;
            kafka_recv = NULL;
            file_recv = NULL;
            mqtt_recv = NULL;
            adapter_ptr = NULL;
            transmitter_ptr = NULL;

            Write_System_ErrorLog("initialize failed, sleep " + to_string(sleep_seconds) + " seconds and retry (" + to_string(retry)+ " times left)", true);
            
            sleep(sleep_seconds);
        }
        else
        {
            break; // success;
        }
        

    } while(--retry >= 0);

    if(retry >= 0 && ret >= 0)
    {
        if (agent->get_write_to() != "influxdb")
        {
            // synchronize both sides in advance
            if (!agent->sync()) 
            {
                ret = -3;
                Write_System_ErrorLog("sync failed", true);     
            }
        }
        if (ret >= 0)
            agent->run(); 
    }

    delete agent;
    delete kafka_recv;
    delete file_recv;
    delete mqtt_recv;
    delete adapter_ptr;   
    delete transmitter_ptr;

    agent = NULL;
    kafka_recv = NULL;
    file_recv = NULL;
    mqtt_recv = NULL;
    adapter_ptr = NULL;
    transmitter_ptr = NULL;

    exit(ret);
    return ret;
}

void signal_handler(int sig) {
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    log_debug(string("<") +__func__+ "> signal incoming : " + to_string(sig), true);

    switch (sig) {
        case SIGINT:
            Info( "Ctrl-C got caught...\n");
            if (agent_dup != NULL){
                agent_dup->terminate();
            }
            break;
        case SIGHUP:
            Info("SIGHUP got caught...\n");
            break;
        case SIGQUIT:
            Info("SIGQUIT got caught...\n");
            break;
        case SIGTERM:
            Info( "SIGTERM got caught...\n");
            if (agent_dup != NULL){
                agent_dup->terminate();
            }
            break;
        default:
            Info("%d got caught...\n", sig);
    }
}
