#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <vector>
#include <unistd.h>
#include <dirent.h>

#include "libstdf.h"
#include "stdio.h"
#include "time.h"
#include "stdlib.h"
#include "sys/time.h"
//#include "sys/stat.h"
//#include "sys/types.h"
//#include "fcntl.h"
#include "math.h"
#include "stdint.h"
#include <atomic>
#include <pthread.h>
#include <unistd.h>
#include <string>
#include <mutex>


#include <sstream>

#include "stdf2csv_adapter.h"

#define KEY_ADAPTER_IS_FILE             "is_file"
#define KEY_ADAPTER_OUT_DIR             "out_dir"
#define KEY_ADAPTER_BATCH_SIZE          "batch_size"
#define KEY_ADAPTER_OUT_DIR_DEFAULT     "/tmp/bo_csv_out"
#define KEY_ADAPTER_KEEP_ALIVE_FILES    "keep_alive_files"

#define KEY_ADAPTER_FLUSH_TIME_LIMIT    "flush_time_limit"

bool stdf2csv_adapter::clear_cache()
{
    DIR* dir = opendir(_out_dir.c_str());
    if (!dir)
        return errno == ENOENT ? true : false;
    
    bool ret = true;
    struct dirent *ent = NULL;
    while ((ent = readdir(dir))) {
        if (!strcmp(ent->d_name, ".") || !strcmp(ent->d_name, ".."))
            continue;
        ostringstream path;
        path << _out_dir << "/" << ent->d_name;
        if (remove(path.str().c_str())) {
            Error("%s, remove %s fail: %s\n", __func__, path.str().c_str(), strerror(errno));
            ret = false;
            break;
        }
    }
    closedir(dir);
    return ret;
}

bool stdf2csv_adapter::set_params(string& group, map<string, string>& param)
{
    if (!component::set_params(group, param))
        return false;
    
    string param_is_file;
    if (get_param_value(KEY_ADAPTER_IS_FILE, param_is_file) &&
        !param_is_file.empty())
        _is_file = true;
    
    struct stat outdir_stat = {0};
    if (!get_param_value(KEY_ADAPTER_OUT_DIR, _out_dir))
        _out_dir = KEY_ADAPTER_OUT_DIR_DEFAULT;
    
    if (0 != stat(_out_dir.c_str(), &outdir_stat)) {
        if (0 != mkdir(_out_dir.c_str(), S_IRWXU)) {
            Error("%s, create temporary folder for output fail: %s\n", __func__, strerror(errno));
            return false;
        }
    }
    
    string batch_size;
    if (get_param_value(KEY_ADAPTER_BATCH_SIZE, batch_size))
        _batch_checkpointing = stoi(batch_size);
    get_param_value(KEY_ADAPTER_KEEP_ALIVE_FILES, _keep_alive_files);

    string flush_time_limit;
    if(get_param_value(KEY_ADAPTER_FLUSH_TIME_LIMIT, flush_time_limit))
        _flush_time_limit = stoi(flush_time_limit);

    return true;
}

bool stdf2csv_adapter::transform(vector<int>& data, string& doc_id, size_t& count, msg_info_struct *msg_info)
{
    if (data.empty()) {
        if (_pending) {
            _pending = 0;
            check_flush_time("commit");
            return commit();
        }
        return true;
    }
    
    int i = 0;
    int data_len = data.size();
    if (_is_file) {
        map<string, vector<int>> csv_out;
        if (!stdf2csv(data, csv_out)) {
            Error("%s, transform STDF to CSV fail: %s\n", __func__, strerror(errno));
            return false;
        }
        
        map<string, vector<int>>::const_iterator itCsv = csv_out.begin();
        for (; itCsv != csv_out.end(); ++itCsv) {
            vector<int>::const_iterator itFd = itCsv->second.begin();
            for (; itFd != itCsv->second.end(); ++itFd) {
                if (!transmit(itCsv->first, *itFd))
                    Error("%s, transmit file to chanel '%s' fail (fd %d): %s\n",
                          __func__, itCsv->first.c_str(), *itFd, strerror(errno));
                close(*itFd);
            }
        }
    }
    else {
        Error("%s, not supported: only supports file base.", __func__);
        return false;
    }
    
    _pending += data_len;
    if (_pending >= _batch_checkpointing) {
        _pending = 0;
        check_flush_time("commit");

        return commit();
    }
    else
    {
        if(check_flush_time("compare") > _flush_time_limit) // $$ default is 300 sec
        {
            _pending = 0;
            check_flush_time("commit");
            return commit();
        }
    }
    return true;
}


#define print_intfd(n,i, fname) dprintf(fname, "%i,", i);
#define print_intfd2(n,i, fname) dprintf(fname, "%i", i);

#define print_strfd(n,s, fname) dprintf(fname, "%s,",(*(s) ? (s)+1 : " "));
#define print_strfd2(n,s, fname) dprintf(fname, "%s",(*(s) ? (s)+1 : " "));

#define print_chrfd(n,c, fname) dprintf(fname, "%c,", c == '\0' ? ' ' : c);
#define print_chrfd2(n,c, fname) dprintf(fname, "%c", c == '\0' ? ' ' : c);

#define print_hexfd(n,h, fname) dprintf(fname, "%X,", h);
#define print_hexfd2(n,h, fname) dprintf(fname, "%X", h);

#define print_relfd(n,r, fname) dprintf(fname, "%f,", r);
#define print_relfd2(n,r, fname) dprintf(fname, "%f", r);


#define print_timfd(n,d, fname) \
do {char buffer[50];\
time_t t = d; \
struct tm * p = localtime(&t); \
strftime(buffer, 1000, "%Y-%m-%d %X", p);\
dprintf(fname, "%s,", buffer); } while(0)

#define print_timfd2(n,d, fname) \
do {char buffer[50];\
time_t t = d; \
struct tm * p = localtime(&t); \
strftime(buffer, 1000, "%Y-%m-%d %X", p);\
dprintf(fname, "%s", buffer); } while(0)

#define MAKE_PRINTFD_X(DTC, OUTPUT_FUNC, FORMAT) \
void printfd_x ## DTC(const char *n, dtc_x ## DTC u, dtc_U2 c, int fname, uint64_t foreign_key) \
{ \
dtc_U2 i; \
for (i=0; i<c; ++i) { \
dprintf(fname, "%llu,%s,", foreign_key, n);\
OUTPUT_FUNC(fname, FORMAT, u[i]); \
} \
}
//    fprintf(fname, "\n"); \
}
MAKE_PRINTFD_X(U1, dprintf, "%u,")
MAKE_PRINTFD_X(U2, dprintf, "%u,")
MAKE_PRINTFD_X(R4, dprintf, "%f,")

#define _dprintf_xCn(fname, fmt,Cn) dprintf(fname, fmt, (*Cn ? Cn+1 : " "))
MAKE_PRINTFD_X(Cn, _dprintf_xCn, "%s,")


void printfd_xN1(const char *member, dtc_xN1 xN1, dtc_U2 c, int fname)
{
    dtc_N1 *n = xN1;
    //    dprintf(fname, "%s, ", member);
    while (c > 0) {
        if (c > 1) {
            dprintf(fname, "%X %X ", ((*n) & 0xF0) >> 4, (*n) & 0x0F);
            c -= 2;
        } else {
            dprintf(fname, "%X", ((*n) & 0xF0) >> 4);
            break;
        }
        ++n;
    }
    dprintf(fname, "\n");
}


void printfd_Vn(const char *n, dtc_Vn v, int c, int fname, uint64_t foreign_key)
{
    int i;
    --c;
    //     fprintf(fname, "%s\n", n);
    for (i=0; i<=c; ++i) {
        dprintf(fname, "%llu,%s,",foreign_key, stdf_get_Vn_name(v[i].type));
        switch (v[i].type) {
            case GDR_B0: dprintf(fname, "(pad)\n"); break;
            case GDR_U1: dprintf(fname, "%i\n", *((dtc_U1*)v[i].data)); break;
            case GDR_U2: dprintf(fname, "%i\n", *((dtc_U2*)v[i].data)); break;
            case GDR_U4: dprintf(fname, "%i\n", *((dtc_U4*)v[i].data)); break;
            case GDR_I1: dprintf(fname, "%i\n", *((dtc_I1*)v[i].data)); break;
            case GDR_I2: dprintf(fname, "%i\n", *((dtc_I2*)v[i].data)); break;
            case GDR_I4: dprintf(fname, "%i\n", *((dtc_I4*)v[i].data)); break;
            case GDR_R4: dprintf(fname, "%f\n", *((dtc_R4*)v[i].data)); break;
            case GDR_R8: dprintf(fname, "%f\n", *((dtc_R8*)v[i].data)); break;
            case GDR_Cn: {
                dtc_Cn Cn = *((dtc_Cn*)v[i].data);
                dprintf(fname, "%s\n",(*Cn ? Cn+1 : ""));
                break;
            }
            case GDR_Bn: dprintf(fname, "[??]\n"); break;
            case GDR_Dn: dprintf(fname, "[??]\n"); break;
            case GDR_N1: dprintf(fname, "%X\n", *((dtc_N1*)v[i].data)); break;
        }
        //        fprintf(fname, "\n");
    }
    if (c == -1){
    }
    //        fprintf(fname, "\n");
    //    fprintf(fname, "\n");
}

void printfd_Bn(dtc_C1 *n, dtc_Bn b, int fname)
{
    int i;
    //     fprintf(fname, "%s,", n);
    for (i=1; i<=*b; ++i)
        dprintf(fname, "%X", *(b+i));
    if (*b == 0)
        dprintf(fname, " ");
    //    fprintf(fname, "\n");
}


void printfd_Bn2(const dtc_C1 *n, dtc_Bn b, int fname)
{
    int i;
    //     fprintf(fname, "%s,", n);
    for (i=1; i<=*b; ++i)
        dprintf(fname, "%X,", *(b+i));
    if (*b == 0)
        dprintf(fname, " ,");
    //    fprintf(fname, "\n");
}


void printfd_Dn(const dtc_C1 *n, dtc_Dn d, int fname)
{
    int i;
    dtc_U2 *num_bits = (dtc_U2*)d, len;
    len = *num_bits / 8;
    if (*num_bits % 8) ++len;
    //         fprintf(fname, "%s,", n);
    for (i=2; i<len; ++i)
        dprintf(fname, "%X,", *(d+i));
    if (len == 0)
        dprintf(fname, " ,");
    //    fprintf(fname, "\n");
}

void printfd_Dn2(const dtc_C1 *n, dtc_Dn d, int fname)
{
    int i;
    dtc_U2 *num_bits = (dtc_U2*)d, len;
    len = *num_bits / 8;
    if (*num_bits % 8) ++len;
    //     fprintf(fname, "%s", n);
    for (i=2; i<len; ++i)
        dprintf(fname, "%X,", *(d+i));
    if (len == 0)
        dprintf(fname, " ,");
    //    fprintf(fname, "\n");
}

volatile std::atomic<std::uint64_t> init_time_;
//std::atomic_uint64_t timeKey;

uint64_t init_time()
{
    struct timeval tv;
    struct timezone tz;
    gettimeofday(&tv,&tz);
    //    std::atomic_uint64_t init_time_ = tv.tv_sec * pow(10,6) + tv.tv_usec;
    uint64_t time_key = tv.tv_sec * pow(10,6) + tv.tv_usec;
    return time_key;
}

uint64_t get_id()
{
    ++init_time_;
    return init_time_.load();
}

struct stdfFile_
{
    int my_pipe;
    map<string, vector<int>> *my_map;
};


bool isEmpty(FILE *file){
    long savedOffset = ftell(file);
    fseek(file, 0, SEEK_END);
    
    if (ftell(file) == 0){
        return true;
    }
    
    fseek(file, savedOffset, SEEK_SET);
    return false;
}


std::mutex map_lock;

void map_insert (string theKey, int fd, map<string, vector<int>>& theMap)
{
    std::lock_guard<std::mutex> lock(map_lock);
    //chck if keyexist
    if ( theMap.find(theKey) == theMap.end()) {
        // not found
        theMap.insert(pair<string,vector<int>>(theKey,{fd}));
    } else {
        // found
        theMap[theKey].push_back(fd);
    }
}

void * stdf_parse (void* arg)
//void * stdf_parse ()
{
    struct stdfFile_ *args = (struct stdfFile_*)arg;
    int buff;
    //    int pipekey = *(int*)arg;
    int pipekey = args->my_pipe;
    map<string, vector<int>> *mapkey = args->my_map;
    //    int ret;
    while(read(pipekey, &buff, sizeof(buff))>0)
    {
        //        printf("asfasd%d:\n", ret);
        uint64_t LocalPrimKey = get_id();
        uint64_t timeKeyLocal;
        uint64_t prr_key = get_id();
        
        //        printf("LocalKey: %llu \n\n", timeKeyLocal);
        //        timeKey = get_id(timeKey);
        
        stdf_file *f;
        char *recname;
        rec_unknown *rec;
        int i;
        dtc_U4 stdf_ver;
        
        int fd = buff;
        lseek(fd, 0, SEEK_SET);
        f = stdf_dopen(fd);
        
        printf("id_key:%llu\n", LocalPrimKey);
        
        if (!f) {
            perror("Could not open file");
            //        exit;
        }
        stdf_get_setting(f, STDF_SETTING_VERSION, &stdf_ver);
        
        FILE *far_ = tmpfile();
        FILE *atr_ = tmpfile();
        FILE *mir_ = tmpfile();
        FILE *mrr_ = tmpfile();
        FILE *pcr_ = tmpfile();
        FILE *hbr_ = tmpfile();
        FILE *sbr_ = tmpfile();
        FILE *pmr_ = tmpfile();
        FILE *pgr_ = tmpfile();
        FILE *pgr_2 = tmpfile();
        
        FILE *plr_ = tmpfile();
        FILE *plr_2 = tmpfile();
        
        FILE *rdr_ = tmpfile();
        FILE *rdr_2 = tmpfile();
        
        FILE *sdr_ = tmpfile();
        FILE *sdr_2 = tmpfile();
        
        FILE *wir_ = tmpfile();
        FILE *wrr_ = tmpfile();
        FILE *wcr_ = tmpfile();
        FILE *pir_ = tmpfile();
        FILE *prr_ = tmpfile();
#ifdef STDF_VER3
        FILE *pdr_ = tmpfile();
        FILE *fdr_ = tmpfile();
#endif
        FILE *tsr_ = tmpfile();
        FILE *ptr_ = tmpfile();
        FILE *mpr_ = tmpfile();
        FILE *mpr_2 = tmpfile();
        
        FILE *ftr_ = tmpfile();
        FILE *ftr_2 = tmpfile();
        
        FILE *bps_ = tmpfile();
        FILE *eps_ = tmpfile();
#ifdef STDF_VER3
        FILE *shb_ = tmpfile();
        FILE *ssb_ = tmpfile();
        FILE *sts_ = tmpfile();
        FILE *scr_ = tmpfile();
#endif
        FILE *gdr_fldCnt = tmpfile();
        FILE *gdr_gen = tmpfile();
        FILE *dtr_ = tmpfile();
        
        // FILE *prr2_ptr = tmpfile();
        
        int fd_far = fileno(far_);
        int fd_atr = fileno(atr_);
        int fd_mir = fileno(mir_);
        int fd_mrr = fileno(mrr_);
        int fd_pcr = fileno(pcr_);
        int fd_hbr = fileno(hbr_);
        int fd_sbr = fileno(sbr_);
        int fd_pmr = fileno(pmr_);
        int fd_pgr = fileno(pgr_);
        int fd_pgr2 = fileno(pgr_2);
        int fd_plr = fileno(plr_);
        int fd_plr2 = fileno(plr_2);
        int fd_rdr = fileno(rdr_);
        int fd_rdr2 = fileno(rdr_2);
        int fd_sdr = fileno(sdr_);
        int fd_sdr2 = fileno(sdr_2);
        int fd_wir = fileno(wir_);
        int fd_wrr = fileno(wrr_);
        int fd_wcr = fileno(wcr_);
        int fd_pir = fileno(pir_);
        int fd_prr = fileno(prr_);
#ifdef STDF_VER3
        int fd_pdr = fileno(pdr_);
        int fd_fdr = fileno(fdr_);
#endif
        int fd_tsr = fileno(tsr_);
        int fd_ptr = fileno(ptr_);
        int fd_mpr = fileno(mpr_);
        int fd_mpr2 = fileno(mpr_2);
        int fd_ftr = fileno(ftr_);
        int fd_ftr2 = fileno(ftr_2);
        int fd_bps = fileno(bps_);
        int fd_eps = fileno(eps_);
#ifdef STDF_VER3
        int fd_shb = fileno(shb_);
        int fd_ssb = fileno(ssb_);
        int fd_sts = fileno(sts_);
        int fd_scr = fileno(scr_);
#endif
        int fd_gdr1 = fileno(gdr_fldCnt);
        int fd_gdr2 = fileno(gdr_gen);
        int fd_dtr = fileno(dtr_);
        
        // int fd_prr2_ptr = fileno(prr2_ptr);
        
        while ((rec=stdf_read_record(f)) != NULL) {
            recname = stdf_get_rec_name(rec->header.REC_TYP, rec->header.REC_SUB);
            switch (HEAD_TO_REC(rec->header)) {
                case REC_FAR: {
                    rec_far *far = (rec_far*)rec;
                    
                    dprintf(fd_far, "%llu,%i,%i\n", LocalPrimKey, far->CPU_TYPE, far->STDF_VER);
                    break;
                }
                case REC_ATR: {
                    rec_atr *atr = (rec_atr*)rec;
                    
                    timeKeyLocal = get_id();
                    
                    dprintf(fd_atr, "%llu,%llu,", LocalPrimKey, timeKeyLocal);
                    print_timfd("MOD_TIM", atr->MOD_TIM, fd_atr);
                    print_strfd2("CMD_LINE", atr->CMD_LINE, fd_atr);
                    dprintf(fd_atr, "\n");
                    break;
                }
                case REC_MIR: {
                    rec_mir *mir = (rec_mir*)rec;
                    
                    
#ifdef STDF_VER3
                    if (stdf_ver == 4) {
#endif
                        
                        dprintf(fd_mir, "%llu,", LocalPrimKey);
                        print_timfd("SETUP_T", mir->SETUP_T, fd_mir);
                        print_timfd("START_T", mir->START_T, fd_mir);
                        
                        dprintf(fd_mir, "%i,%c,%c,%c,%i,%c,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n", \
                                mir->STAT_NUM, \
                                mir->MODE_COD == '\0' ? ' ' : mir->MODE_COD, \
                                mir->RTST_COD == '\0' ? ' ' : mir->RTST_COD, \
                                mir->PROT_COD == '\0' ? ' ' : mir->PROT_COD, \
                                mir->BURN_TIM, \
                                mir->CMOD_COD == '\0' ? ' ' : mir->CMOD_COD, \
                                (*(mir->LOT_ID) ? (mir->LOT_ID)+1 : " "), \
                                (*(mir->PART_TYP) ? (mir->PART_TYP)+1 : " "), \
                                (*(mir->NODE_NAM) ? (mir->NODE_NAM)+1 : " "), \
                                (*(mir->TSTR_TYP) ? (mir->TSTR_TYP)+1 : " "), \
                                (*(mir->JOB_NAM) ? (mir->JOB_NAM)+1 : " "), \
                                (*(mir->JOB_REV) ? (mir->JOB_REV)+1 : " "), \
                                (*(mir->SBLOT_ID) ? (mir->SBLOT_ID)+1 : " "), \
                                (*(mir->OPER_NAM) ? (mir->OPER_NAM)+1 : " "), \
                                (*(mir->EXEC_TYP) ? (mir->EXEC_TYP)+1 : " "), \
                                (*(mir->EXEC_VER) ? (mir->EXEC_VER)+1 : " "), \
                                (*(mir->TEST_COD) ? (mir->TEST_COD)+1 : " "), \
                                (*(mir->TST_TEMP) ? (mir->TST_TEMP)+1 : " "), \
                                (*(mir->USER_TXT) ? (mir->USER_TXT)+1 : " "), \
                                (*(mir->AUX_FILE) ? (mir->AUX_FILE)+1 : " "), \
                                (*(mir->PKG_TYP) ? (mir->PKG_TYP)+1 : " "), \
                                (*(mir->FAMILY_ID) ? (mir->FAMILY_ID)+1 : " "), \
                                (*(mir->DATE_COD) ? (mir->DATE_COD)+1 : " "), \
                                (*(mir->FACIL_ID) ? (mir->FACIL_ID)+1 : " "), \
                                (*(mir->FLOOR_ID) ? (mir->FLOOR_ID)+1 : " "), \
                                (*(mir->PROC_ID) ? (mir->PROC_ID)+1 : " "), \
                                (*(mir->OPER_FRQ) ? (mir->OPER_FRQ)+1 : " "), \
                                (*(mir->SPEC_NAM) ? (mir->SPEC_NAM)+1 : " "), \
                                (*(mir->SPEC_VER) ? (mir->SPEC_VER)+1 : " "), \
                                (*(mir->FLOW_ID) ? (mir->FLOW_ID)+1 : " "), \
                                (*(mir->SETUP_ID) ? (mir->SETUP_ID)+1 : " "), \
                                (*(mir->DSGN_REV) ? (mir->DSGN_REV)+1 : " "), \
                                (*(mir->ENG_ID) ? (mir->ENG_ID)+1 : " "), \
                                (*(mir->ROM_COD) ? (mir->ROM_COD)+1 : " "), \
                                (*(mir->SERL_NUM) ? (mir->SERL_NUM)+1 : " "), \
                                (*(mir->SUPR_NAM) ? (mir->SUPR_NAM)+1 : " "));
                        
#ifdef STDF_VER3
                    } else {
                        dprintf(mir_, "%llu,", LocalPrimKey);
                        print_intfd("CPU_TYPE", mir->CPU_TYPE, fd_mir);
                        print_intfd("STDF_VER", mir->STDF_VER, fd_mir);
                        print_chrfd("MODE_COD", mir->MODE_COD, fd_mir);
                        print_intfd("STAT_NUM", mir->STAT_NUM, fd_mir);
                        print_strfd("TEST_COD", mir->TEST_COD, fd_mir);
                        print_chrfd("RTST_COD", mir->RTST_COD, fd_mir);
                        print_chrfd("PROT_COD", mir->PROT_COD, fd_mir);
                        print_chrfd("CMOD_COD", mir->CMOD_COD, fd_mir);
                        print_timfd("SETUP_T", mir->SETUP_T, fd_mir);
                        print_timfd("START_T", mir->START_T, fd_mir);
                        print_strfd("LOT_ID", mir->LOT_ID, fd_mir);
                        print_strfd("PART_TYP", mir->PART_TYP, fd_mir);
                        print_strfd("JOB_NAM", mir->JOB_NAM, fd_mir);
                        print_strfd("OPER_NAM", mir->OPER_NAM, fd_mir);
                        print_strfd("NODE_NAM", mir->NODE_NAM, fd_mir);
                        print_strfd("TSTR_TYP", mir->TSTR_TYP, fd_mir);
                        print_strfd("EXEC_TYP", mir->EXEC_TYP, fd_mir);
                        print_strfd("SUPR_NAM", mir->SUPR_NAM, fd_mir);
                        print_strfd("HAND_ID", mir->HAND_ID, fd_mir);
                        print_strfd("SBLOT_ID", mir->SBLOT_ID, fd_mir);
                        print_strfd("JOB_REV", mir->JOB_REV, fd_mir);
                        print_strfd("PROC_ID", mir->PROC_ID, fd_mir);
                        print_strfd2("PRB_CARD", mir->PRB_CARD, fd_mir);
                        fprintf(fd_mir, "\n");
                    }
#endif
                    break;
                }
                case REC_MRR: {
                    rec_mrr *mrr = (rec_mrr*)rec;
                    dprintf(fd_mrr, "%llu,", LocalPrimKey);
                    print_timfd("FINISH_T", mrr->FINISH_T, fd_mrr);
                    
#ifdef STDF_VER3
                    if (stdf_ver == 3) {
                        print_intfd("PART_CNT", mrr->PART_CNT, fd_mrr);
                        print_intfd("RTST_CNT", mrr->RTST_CNT, fd_mrr);
                        print_intfd("ABRT_CNT", mrr->ABRT_CNT, fd_mrr);
                        print_intfd("GOOD_CNT", mrr->GOOD_CNT, fd_mrr);
                        print_intfd("FUNC_CNT", mrr->FUNC_CNT, fd_mrr);
                    }
#endif
                    dprintf(fd_mrr, "%c,%s,%s\n", \
                            mrr->DISP_COD == '\0' ? ' ' : mrr->DISP_COD, \
                            (*(mrr->USR_DESC) ? (mrr->USR_DESC)+1 : " "), \
                            (*(mrr->EXC_DESC) ? (mrr->EXC_DESC)+1 : " "));
                    break;
                }
                case REC_PCR: {
                    rec_pcr *pcr = (rec_pcr*)rec;
                    
                    dprintf(fd_pcr, "%llu,%i,%i,%i,%i,%i,%i,%i\n", LocalPrimKey, pcr->HEAD_NUM, pcr->SITE_NUM, pcr->PART_CNT, pcr->RTST_CNT, pcr->ABRT_CNT, pcr->GOOD_CNT, pcr->FUNC_CNT);
                    break;
                }
                case REC_HBR: {
                    rec_hbr *hbr = (rec_hbr*)rec;
                    
                    timeKeyLocal = get_id();
                    
                    dprintf(fd_hbr, "%llu,%llu,%i,%i,%i,%i,%c,%s\n", LocalPrimKey, timeKeyLocal, hbr->HEAD_NUM, hbr->SITE_NUM, hbr->HBIN_NUM, hbr->HBIN_CNT, \
                            hbr->HBIN_PF == '\0' ? ' ' : hbr->HBIN_PF, \
                            (*(hbr->HBIN_NAM) ? (hbr->HBIN_NAM)+1 : " "));
                    break;
                }
                case REC_SBR: {
                    rec_sbr *sbr = (rec_sbr*)rec;
                    
                    timeKeyLocal = get_id();
                    
                    dprintf(fd_sbr, "%llu,%llu,%i,%i,%i,%i,%c,%s\n", LocalPrimKey, timeKeyLocal, sbr->HEAD_NUM, sbr->SITE_NUM, sbr->SBIN_NUM, sbr->SBIN_CNT, \
                            sbr->SBIN_PF == '\0' ? ' ' : sbr->SBIN_PF, \
                            (*(sbr->SBIN_NAM) ? (sbr->SBIN_NAM)+1 : " "));
                    break;
                }
                case REC_PMR: {
                    rec_pmr *pmr = (rec_pmr*)rec;
                    
                    timeKeyLocal = get_id();
                    dprintf(fd_pmr, "%llu,%llu,%i,%i,%s,%s,%s,%i,%i\n", LocalPrimKey, timeKeyLocal, pmr->PMR_INDX, pmr->CHAN_TYP, \
                            (*(pmr->CHAN_NAM) ? (pmr->CHAN_NAM)+1 : " "), \
                            (*(pmr->PHY_NAM) ? (pmr->PHY_NAM)+1 : " "), \
                            (*(pmr->LOG_NAM) ? (pmr->LOG_NAM)+1 : " "), \
                            pmr->HEAD_NUM, pmr->SITE_NUM);
                    break;
                }
                case REC_PGR: {
                    rec_pgr *pgr = (rec_pgr*)rec;
                    
                    timeKeyLocal = get_id();
                    
                    dprintf(fd_pgr, "%llu,%llu,%i,%s,%i,", LocalPrimKey, timeKeyLocal, pgr->GRP_INDX, \
                            (*(pgr->GRP_NAM) ? (pgr->GRP_NAM)+1 : " "), \
                            pgr->INDX_CNT);
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    printfd_xU2("PMR_INDX", pgr->PMR_INDX, pgr->INDX_CNT, fd_pgr2, timeKeyLocal);
                    
                    dprintf(fd_pgr, "\n");
                    break;
                }
                case REC_PLR: {
                    rec_plr *plr = (rec_plr*)rec;
                    
                    timeKeyLocal = get_id();
                    
                    dprintf(fd_plr, "%llu,%llu,%i", LocalPrimKey, timeKeyLocal, plr->GRP_CNT);
                    
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    printfd_xU2("GRP_INDX", plr->GRP_INDX, plr->GRP_CNT, fd_plr2, timeKeyLocal);
                    printfd_xU2("GRP_MODE", plr->GRP_MODE, plr->GRP_CNT, fd_plr2, timeKeyLocal);
                    printfd_xU1("GRP_RADX", plr->GRP_RADX, plr->GRP_CNT, fd_plr2, timeKeyLocal);
                    printfd_xCn("PGM_CHAR", plr->PGM_CHAR, plr->GRP_CNT, fd_plr2, timeKeyLocal);
                    printfd_xCn("RTN_CHAR", plr->RTN_CHAR, plr->GRP_CNT, fd_plr2, timeKeyLocal);
                    printfd_xCn("PGM_CHAL", plr->PGM_CHAL, plr->GRP_CNT, fd_plr2, timeKeyLocal);
                    printfd_xCn("RTN_CHAL", plr->RTN_CHAL, plr->GRP_CNT, fd_plr2, timeKeyLocal);
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    
                    dprintf(fd_plr, "\n");
                    
                    break;
                }
                case REC_RDR: {
                    rec_rdr *rdr = (rec_rdr*)rec;
                    
                    timeKeyLocal = get_id();
                    
                    dprintf(fd_rdr, "%llu,%llu,%i", LocalPrimKey, timeKeyLocal, rdr->NUM_BINS);
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    printfd_xU2("RTST_BIN", rdr->RTST_BIN, rdr->NUM_BINS, fd_rdr2, timeKeyLocal);
                    
                    dprintf(fd_rdr, "\n");
                    break;
                }
                case REC_SDR: {
                    rec_sdr *sdr = (rec_sdr*)rec;
                    
                    timeKeyLocal = get_id();
                    
                    dprintf(fd_sdr, "%llu,%llu,%i,%i,%i,", LocalPrimKey, timeKeyLocal, sdr->HEAD_NUM, sdr->SITE_GRP, sdr->SITE_CNT);
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    printfd_xU1("SITE_NUM", sdr->SITE_NUM, sdr->SITE_CNT, fd_sdr2, timeKeyLocal);
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    dprintf(fd_sdr, "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n", \
                            (*(sdr->HAND_TYP) ? (sdr->HAND_TYP)+1 : " "),\
                            (*(sdr->HAND_ID) ? (sdr->HAND_ID)+1 : " "),\
                            (*(sdr->CARD_TYP) ? (sdr->CARD_TYP)+1 : " "),\
                            (*(sdr->CARD_ID) ? (sdr->CARD_ID)+1 : " "),\
                            (*(sdr->LOAD_TYP) ? (sdr->LOAD_TYP)+1 : " "),\
                            (*(sdr->LOAD_ID) ? (sdr->LOAD_ID)+1 : " "),\
                            (*(sdr->DIB_TYP) ? (sdr->DIB_TYP)+1 : " "),\
                            (*(sdr->DIB_ID) ? (sdr->DIB_ID)+1 : " "),\
                            (*(sdr->CABL_TYP) ? (sdr->CABL_TYP)+1 : " "),\
                            (*(sdr->CABL_ID) ? (sdr->CABL_ID)+1 : " "), \
                            (*(sdr->CONT_TYP) ? (sdr->CONT_TYP)+1 : " "),\
                            (*(sdr->CONT_ID) ? (sdr->CONT_ID)+1 : " "),\
                            (*(sdr->LASR_TYP) ? (sdr->LASR_TYP)+1 : " "),\
                            (*(sdr->LASR_ID) ? (sdr->LASR_ID)+1 : " "),\
                            (*(sdr->EXTR_TYP) ? (sdr->EXTR_TYP)+1 : " "),\
                            (*(sdr->EXTR_ID) ? (sdr->EXTR_ID)+1 : " "));
                    break;
                }
                case REC_WIR: {
                    rec_wir *wir = (rec_wir*)rec;
                    
                    dprintf(fd_wir, "%llu,%i,", LocalPrimKey, wir->HEAD_NUM);
                    
#ifdef STDF_VER3
                    if (stdf_ver == 3)
                        print_hexfd("PAD_BYTE", wir->PAD_BYTE, fd_wir);
                    else
#endif
                        print_intfd("SITE_GRP", wir->SITE_GRP, fd_wir);
                    print_timfd("START_T", wir->START_T, fd_wir);
                    print_strfd2("WAFER_ID", wir->WAFER_ID, fd_wir);
                    
                    dprintf(fd_wir, "\n");
                    break;
                }
                case REC_WRR: {
                    rec_wrr *wrr = (rec_wrr*)rec;
                    
                    dprintf(fd_wrr, "%llu,", LocalPrimKey);
                    
#ifdef STDF_VER3
                    
                    if (stdf_ver == 4) {
#endif
                        print_intfd("HEAD_NUM", wrr->HEAD_NUM, fd_wrr);
                        print_intfd("SITE_GRP", wrr->SITE_GRP, fd_wrr);
                        print_timfd("FINISH_T", wrr->FINISH_T, fd_wrr);
#ifdef STDF_VER3
                    } else {
                        print_timfd("FINISH_T", wrr->FINISH_T, fd_wrr);
                        print_intfd("HEAD_NUM", wrr->HEAD_NUM, fd_wrr);
                        print_hexfd("PAD_BYTE", wrr->PAD_BYTE, fd_wrr);
                    }
#endif
                    print_intfd("PART_CNT", wrr->PART_CNT, fd_wrr);
                    print_intfd("RTST_CNT", wrr->RTST_CNT, fd_wrr);
                    print_intfd("ABRT_CNT", wrr->ABRT_CNT, fd_wrr);
                    print_intfd("GOOD_CNT", wrr->GOOD_CNT, fd_wrr);
                    print_intfd("FUNC_CNT", wrr->FUNC_CNT, fd_wrr);
                    print_strfd("WAFER_ID", wrr->WAFER_ID, fd_wrr);
#ifdef STDF_VER3
                    if (stdf_ver == 4) {
#endif
                        print_strfd("FABWF_ID", wrr->FABWF_ID, fd_wrr);
                        print_strfd("FRAME_ID", wrr->FRAME_ID, fd_wrr);
                        print_strfd("MASK_ID", wrr->MASK_ID, fd_wrr);
#ifdef STDF_VER3
                    } else {
                        print_strfd("HAND_ID", wrr->HAND_ID, fd_wrr);
                        print_strfd("PRB_CARD", wrr->PRB_CARD, fd_wrr);
                    }
#endif
                    print_strfd("USR_DESC", wrr->USR_DESC, fd_wrr);
                    print_strfd2("EXC_DESC", wrr->EXC_DESC, fd_wrr);
                    
                    dprintf(fd_wrr, "\n");
                    break;
                }
                case REC_WCR: {
                    rec_wcr *wcr = (rec_wcr*)rec;
                    
                    dprintf(fd_wcr, "%llu,%f,%f,%f,%i,%c,%i,%i,%c,%c\n", LocalPrimKey, wcr->WAFR_SIZ, wcr->DIE_HT, wcr->DIE_WID, wcr->WF_UNITS, \
                            wcr->WF_FLAT == '\0' ? ' ' : wcr->WF_FLAT, \
                            wcr->CENTER_X, wcr->CENTER_Y, \
                            wcr->POS_X == '\0' ? ' ' : wcr->POS_X, \
                            wcr->POS_Y == '\0' ? ' ' : wcr->POS_Y);
                    break;
                }
                case REC_PIR: {
                    rec_pir *pir = (rec_pir*)rec;
                    
                    timeKeyLocal = get_id();
                    
                    if (stdf_ver == 4) {
                        dprintf(fd_pir, "%llu,%llu,%i,%i\n", LocalPrimKey, timeKeyLocal, pir->HEAD_NUM, pir->SITE_NUM);
                    }
                    else {
                        dprintf(fd_pir, "%llu,", LocalPrimKey);
                        dprintf(fd_pir, "%llu,", timeKeyLocal);
                        print_intfd("HEAD_NUM", pir->HEAD_NUM, fd_pir);
                        print_intfd("SITE_NUM", pir->SITE_NUM, fd_pir);
                    }
                    
                    
                    
                    if (stdf_ver == 4) {
                    } else {
                        
                    }
#ifdef STDF_VER3
                    if (stdf_ver == 3) {
                        print_intfd("X_COORD", pir->X_COORD, fd_pir);
                        print_intfd("Y_COORD", pir->Y_COORD, fd_pir);
                        print_strfd2("PART_ID", pir->PART_ID, fd_pir);
                        dprintf(fd_pir, "\n");
                    }
#endif
                    //                    fclose(pir_);
                    break;
                }
                case REC_PRR: {
                    rec_prr *prr = (rec_prr*)rec;
                    
                    prr_key  = get_id();
                    
                    dprintf(fd_prr, "%llu,%llu,%i,%i,%X,%i,%i,%i,%i,%i,", LocalPrimKey, prr_key, prr->HEAD_NUM, prr->SITE_NUM, prr->PART_FLG, prr->NUM_TEST, prr->HARD_BIN, prr->SOFT_BIN, prr->X_COORD, prr->Y_COORD);
                    
#ifdef STDF_VER3
                    if (stdf_ver == 4)
#endif
                        
#ifdef STDF_VER3
                        if (stdf_ver == 3) {
                            print_hexfd("PART_FLG", prr->PART_FLG, fd_prr);
                            print_hexfd("PAD_BYTE", prr->PAD_BYTE, fd_prr);
                        }
#endif
                    
#ifdef STDF_VER3
                    if (stdf_ver == 4)
#endif
                        print_timfd("TEST_T", prr->TEST_T, fd_prr);
                    print_strfd("PART_ID", prr->PART_ID, fd_prr);
                    print_strfd("PART_TXT", prr->PART_TXT, fd_prr);
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    printfd_Bn2("PART_FIX", prr->PART_FIX, fd_prr);
                    
                    dprintf(fd_prr, "\n");
                    break;
                }
#ifdef STDF_VER3
                case REC_PDR: {
                    rec_pdr *pdr = (rec_pdr*)rec;
                    
                    dprintf(fd_pdr, "%llu,", LocalPrimKey);
                    timeKeyLocal = get_id();
                    dprintf(fd_pdr, "%llu,", timeKeyLocal);
                    
                    print_intfd("TEST_NUM", pdr->TEST_NUM, fd_pdr);
                    print_hexfd("DESC_FLG", pdr->DESC_FLG, fd_pdr);
                    print_hexfd("OPT_FLAG", pdr->OPT_FLAG, fd_pdr);
                    print_intfd("RES_SCAL", pdr->RES_SCAL, fd_pdr);
                    print_strfd("UNITS", pdr->UNITS, fd_pdr);
                    print_intfd("RES_LDIG", pdr->RES_LDIG, fd_pdr);
                    print_intfd("RES_RDIG", pdr->RES_RDIG, fd_pdr);
                    print_intfd("LLM_SCAL", pdr->LLM_SCAL, fd_pdr);
                    print_intfd("HLM_SCAL", pdr->HLM_SCAL, fd_pdr);
                    print_intfd("LLM_LDIG", pdr->LLM_LDIG, fd_pdr);
                    print_intfd("LLM_RDIG", pdr->LLM_RDIG, fd_pdr);
                    print_intfd("HLM_LDIG", pdr->HLM_LDIG, fd_pdr);
                    print_intfd("HLM_RDIG", pdr->HLM_RDIG, fd_pdr);
                    print_relfd("LO_LIMIT", pdr->LO_LIMIT, fd_pdr);
                    print_relfd("HI_LIMIT", pdr->HI_LIMIT, fd_pdr);
                    print_strfd("TEST_NAM", pdr->TEST_NAM, fd_pdr);
                    print_strfd2("SEQ_NAME", pdr->SEQ_NAME, fd_pdr);
                    
                    dprintf(fd_pdr, "\n");
                    
                    break;
                }
                case REC_FDR: {
                    rec_fdr *fdr = (rec_fdr*)rec;
                    dprintf(fd_fdr, "%llu,", LocalPrimKey);
                    timeKeyLocal = get_id();
                    dprintf(fd_fdr, "%llu,", timeKeyLocal);
                    
                    print_intfd("TEST_NUM", fdr->TEST_NUM, fd_fdr);
                    print_hexfd("DESC_FLG", fdr->DESC_FLG, fd_fdr);
                    print_strfd("TEST_NAM", fdr->TEST_NAM, fd_fdr);
                    print_strfd("SEQ_NAME", fdr->SEQ_NAME, fd_fdr);
                    
                    dprintf(fd_fdr, "\n");
                    break;
                }
#endif
                case REC_TSR: {
                    rec_tsr *tsr = (rec_tsr*)rec;
                    
                    timeKeyLocal = get_id();
                    
                    if (stdf_ver == 4)
                        dprintf(fd_tsr, "%llu,%llu,%i,%i,%c,%i,%i,%i,%i,%s,%s,%s,%X,%f,%f,%f,%f,%f\n", LocalPrimKey, timeKeyLocal, tsr->HEAD_NUM, tsr->SITE_NUM, \
                                tsr->TEST_TYP == '\0' ? ' ' : tsr->TEST_TYP, \
                                tsr->TEST_NUM, tsr->EXEC_CNT, tsr->FAIL_CNT, tsr->ALRM_CNT,\
                                (*(tsr->TEST_NAM) ? (tsr->TEST_NAM)+1 : " "), \
                                (*(tsr->SEQ_NAME) ? (tsr->SEQ_NAME)+1 : " "), \
                                (*(tsr->TEST_LBL) ? (tsr->TEST_LBL)+1 : " "), \
                                tsr->OPT_FLAG, tsr->TEST_TIM,tsr->TEST_MIN, tsr->TEST_MAX, tsr->TST_SUMS, tsr->TST_SQRS);
                    
#ifdef STDF_VER3
                    if (stdf_ver == 4)
#endif
                        
#ifdef STDF_VER3
                        if (stdf_ver == 4) {
#endif
                            
#ifdef STDF_VER3
                        } else {
                            print_hexfd("OPT_FLAG", tsr->OPT_FLAG, fd_tsr);
                            print_hexfd("PAD_BYTE", tsr->PAD_BYTE, fd_tsr);
                            print_relfd("TEST_MIN", tsr->TEST_MIN, fd_tsr);
                            print_relfd("TEST_MAX", tsr->TEST_MAX, fd_tsr);
                            print_relfd("TST_MEAN", tsr->TST_MEAN, fd_tsr);
                            print_relfd("TST_SDEV", tsr->TST_SDEV, fd_tsr);
                            print_relfd("TST_SUMS", tsr->TST_SUMS, fd_tsr);
                            print_relfd("TST_SQRS", tsr->TST_SQRS, fd_tsr);
                            print_strfd("TEST_NAM", tsr->TEST_NAM, fd_tsr);
                            print_strfd2("SEQ_NAME", tsr->SEQ_NAME, fd_tsr);
                        }
#endif
                    dprintf(fd_tsr, "\n");
                    break;
                }
                case REC_PTR: {
                    rec_ptr *ptr = (rec_ptr*)rec;
                    
                    timeKeyLocal = get_id();
                    // dprintf(fd_prr2_ptr, "%llu,%llu\n", timeKeyLocal, prr_key);
                    
                    
                    dprintf(fd_ptr, "%llu,%llu,%llu,%i,%i,%i,%X,%X,%f,%s,%s,%X,%i,%i,%i,%f,%f,%s,%s,%s,%s,%f,%f\n", LocalPrimKey, timeKeyLocal, prr_key, ptr->TEST_NUM, ptr->HEAD_NUM,ptr->SITE_NUM, ptr->TEST_FLG, ptr->PARM_FLG, ptr->RESULT, (*(ptr->TEST_TXT) ? (ptr->TEST_TXT)+1 : " "), (*(ptr->ALARM_ID) ? (ptr->ALARM_ID)+1 : " "), ptr->OPT_FLAG, ptr->RES_SCAL, ptr->LLM_SCAL, ptr->HLM_SCAL, ptr->LO_LIMIT, ptr->HI_LIMIT, (*(ptr->UNITS) ? (ptr->UNITS)+1 : " "), (*(ptr->C_RESFMT) ? (ptr->C_RESFMT)+1 : " "), (*(ptr->C_LLMFMT) ? (ptr->C_LLMFMT)+1 : " "), (*(ptr->C_HLMFMT) ? (ptr->C_HLMFMT)+1 : " "), ptr->LO_SPEC, ptr->HI_SPEC);
                    
                    break;
                }
                case REC_MPR: {
                    rec_mpr *mpr = (rec_mpr*)rec;
                    
                    timeKeyLocal = get_id();
                    
                    dprintf(fd_mpr, "%llu,%llu,%i,%i,%i,%X,%X,%i,%i,", LocalPrimKey, timeKeyLocal, mpr->TEST_NUM, mpr->HEAD_NUM, mpr->SITE_NUM, mpr->TEST_FLG, mpr->PARM_FLG, mpr->RTN_ICNT, mpr->RSLT_CNT);
                    
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    printfd_xN1("RTN_STAT", mpr->RTN_STAT, mpr->RTN_ICNT, fd_mpr);
                    printfd_xR4("RTN_RSLT", mpr->RTN_RSLT, mpr->RSLT_CNT, fd_mpr2, timeKeyLocal);
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    
                    dprintf(fd_mpr, "%s,%s,%X,%i,%i,%i,%f,%f,%f,%f,", \
                            (*(mpr->TEST_TXT) ? (mpr->TEST_TXT)+1 : " "), \
                            (*(mpr->ALARM_ID) ? (mpr->ALARM_ID)+1 : " "), \
                            mpr->OPT_FLAG, mpr->RES_SCAL, mpr->LLM_SCAL, mpr->HLM_SCAL, mpr->LO_LIMIT, mpr->HI_LIMIT, mpr->START_IN, mpr->INCR_IN);
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    printfd_xU2("RTN_INDX", mpr->RTN_INDX, mpr->RTN_ICNT, fd_mpr2, timeKeyLocal);
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    dprintf(fd_mpr, "%s,%s,%s,%s,%s,%f,%f\n", \
                            (*(mpr->UNITS) ? (mpr->UNITS)+1 : " "), \
                            (*(mpr->UNITS_IN) ? (mpr->UNITS_IN)+1 : " "), \
                            (*(mpr->C_RESFMT) ? (mpr->C_RESFMT)+1 : " "), \
                            (*(mpr->C_LLMFMT) ? (mpr->C_LLMFMT)+1 : " "), \
                            (*(mpr->C_HLMFMT) ? (mpr->C_HLMFMT)+1 : " "), \
                            mpr->LO_SPEC, mpr->HI_SPEC);
                    
                    break;
                }
                case REC_FTR: {
                    rec_ftr *ftr = (rec_ftr*)rec;
                    
                    timeKeyLocal = get_id();
                    
                    dprintf(fd_ftr, "%llu,%llu,%i,%i,%i,%X,%X,%i,%i,%i,%i,%i,%i,%i,%i,%i,", LocalPrimKey, timeKeyLocal, ftr->TEST_NUM, ftr->HEAD_NUM, ftr->SITE_NUM, ftr->TEST_FLG, ftr->OPT_FLAG, ftr->CYCL_CNT, ftr->REL_VADR, ftr->REPT_CNT, ftr->NUM_FAIL, ftr->XFAIL_AD, ftr->YFAIL_AD, ftr->VECT_OFF, ftr->RTN_ICNT, ftr->PGM_ICNT);
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    printfd_xU2("RTN_INDX", ftr->RTN_INDX, ftr->RTN_ICNT, fd_ftr2, timeKeyLocal);
                    printfd_xN1("RTN_STAT", ftr->RTN_STAT, ftr->RTN_ICNT, fd_ftr);
                    printfd_xU2("PGM_INDX", ftr->PGM_INDX, ftr->PGM_ICNT, fd_ftr2, timeKeyLocal);
                    printfd_xN1("PGM_STAT", ftr->PGM_STAT, ftr->PGM_ICNT, fd_ftr);
                    printfd_Dn("FAIL_PIN", ftr->FAIL_PIN, fd_ftr);
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    dprintf(fd_ftr, "%s,%s,%s,%s,%s,%s,%s,%i,", \
                            (*(ftr->VECT_NAM) ? (ftr->VECT_NAM)+1 : " "),\
                            (*(ftr->TIME_SET) ? (ftr->TIME_SET)+1 : " "),\
                            (*(ftr->OP_CODE) ? (ftr->OP_CODE)+1 : " "),\
                            (*(ftr->TEST_TXT) ? (ftr->TEST_TXT)+1 : " "),\
                            (*(ftr->ALARM_ID) ? (ftr->ALARM_ID)+1 : " "),\
                            (*(ftr->PROG_TXT) ? (ftr->PROG_TXT)+1 : " "),\
                            (*( ftr->RSLT_TXT) ? ( ftr->RSLT_TXT)+1 : " "), \
                            ftr->PATG_NUM);
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    printfd_Dn2("SPIN_MAP", ftr->SPIN_MAP, fd_ftr);
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    dprintf(fd_ftr, "\n");
                    break;
                }
                case REC_BPS: {
                    rec_bps *bps = (rec_bps*)rec;
                    
                    timeKeyLocal = get_id();
                    
                    dprintf(fd_bps, "%llu,%llu,%s\n", LocalPrimKey, timeKeyLocal, \
                            (*(bps->SEQ_NAME) ? (bps->SEQ_NAME)+1 : " "));
                    break;
                }
                case REC_EPS: {
                    dprintf(fd_eps, "%llu,%llu\n", LocalPrimKey, timeKeyLocal);
                    
                    break;
                }
#ifdef STDF_VER3
                case REC_SHB: {
                    rec_shb *shb = (rec_shb*)rec;
                    dprintf(fd_shb, "%llu,", LocalPrimKey);
                    timeKeyLocal = get_id();
                    dprintf(fd_shb, "%llu,", timeKeyLocal);
                    
                    print_intfd("HEAD_NUM", shb->HEAD_NUM, fd_shb);
                    print_intfd("SITE_NUM", shb->SITE_NUM, fd_shb);
                    print_intfd("HBIN_NUM", shb->HBIN_NUM, fd_shb);
                    print_intfd("HBIN_CNT", shb->HBIN_CNT, fd_shb);
                    print_strfd2("HBIN_NAM", shb->HBIN_NAM, fd_shb);
                    
                    dprintf(fd_shb, "\n");
                    break;
                }
                case REC_SSB: {
                    rec_ssb *ssb = (rec_ssb*)rec;
                    
                    dprintf(fd_ssb, "%llu,", LocalPrimKey);
                    timeKeyLocal = get_id();
                    dprintf(fd_ssb, "%llu,", timeKeyLocal);
                    
                    print_intfd("HEAD_NUM", ssb->HEAD_NUM, fd_ssb);
                    print_intfd("SITE_NUM", ssb->SITE_NUM, fd_ssb);
                    print_intfd("SBIN_NUM", ssb->SBIN_NUM, fd_ssb);
                    print_intfd("SBIN_CNT", ssb->SBIN_CNT, fd_ssb);
                    print_strfd2("SBIN_NAM", ssb->SBIN_NAM, fd_ssb);
                    
                    fprintf(fd_ssb, "\n");
                    break;
                }
                case REC_STS: {
                    rec_sts *sts = (rec_sts*)rec;
                    
                    dprintf(fd_sts, "%llu,", LocalPrimKey);
                    timeKeyLocal = get_id();
                    dprintf(fd_sts, "%llu,", timeKeyLocal);
                    
                    print_intfd("HEAD_NUM", sts->HEAD_NUM, fd_sts);
                    print_intfd("SITE_NUM", sts->SITE_NUM, fd_sts);
                    print_intfd("TEST_NUM", sts->TEST_NUM, fd_sts);
                    print_intfd("EXEC_CNT", sts->EXEC_CNT, fd_sts);
                    print_intfd("FAIL_CNT", sts->FAIL_CNT, fd_sts);
                    print_intfd("ALRM_CNT", sts->ALRM_CNT, fd_sts);
                    print_hexfd("OPT_FLAG", sts->OPT_FLAG, fd_sts);
                    print_hexfd("PAD_BYTE", sts->PAD_BYTE, fd_sts);
                    print_relfd("TEST_MIN", sts->TEST_MIN, fd_sts);
                    print_relfd("TEST_MAX", sts->TEST_MAX, fd_sts);
                    print_relff("TST_MEAN", sts->TST_MEAN, fd_sts);
                    print_relfd("TST_SDEV", sts->TST_SDEV, fd_sts);
                    print_relfd("TST_SUMS", sts->TST_SUMS, fd_sts);
                    print_relfd("TST_SQRS", sts->TST_SQRS, fd_sts);
                    print_strfd("TEST_NAM", sts->TEST_NAM, fd_sts);
                    print_strfd("SEQ_NAME", sts->SEQ_NAME, fd_sts);
                    print_strfd2("TEST_LBL", sts->TEST_LBL, fd_sts);
                    
                    dprintf(fd_sts, "\n");
                    break;
                }
                case REC_SCR: {
                    rec_scr *scr = (rec_scr*)rec;
                    
                    dprintf(fd_scr, "%llu,", LocalPrimKey);
                    timeKeyLocal = get_id();
                    dprintf(fd_scr, "%llu,", timeKeyLocal);
                    
                    print_intfd("HEAD_NUM", scr->HEAD_NUM, fd_scr);
                    print_intfd("SITE_NUM", scr->SITE_NUM, fd_scr);
                    print_intfd("FINISH_T", scr->FINISH_T, fd_scr);
                    print_intfd("PART_CNT", scr->PART_CNT, fd_scr);
                    print_intfd("RTST_CNT", scr->RTST_CNT, fd_scr);
                    print_intfd("ABRT_CNT", scr->ABRT_CNT, fd_scr);
                    print_intfd("GOOD_CNT", scr->GOOD_CNT, fd_scr);
                    print_intfd2("FUNC_CNT", scr->FUNC_CNT, fd_scr);
                    
                    dprintf(fd_scr, "\n");
                    break;
                }
#endif
                case REC_GDR: {
                    rec_gdr *gdr = (rec_gdr*)rec;
                    
                    timeKeyLocal = get_id();
                    
                    dprintf(fd_gdr1, "%llu,%llu,%i\n", LocalPrimKey, timeKeyLocal, gdr->FLD_CNT);
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    
                    printfd_Vn("GEN_DATA", gdr->GEN_DATA, gdr->FLD_CNT, fd_gdr2, timeKeyLocal);
                    
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    //            -------------------------------------------------------------------------
                    
                    break;
                }
                case REC_DTR: {
                    rec_dtr *dtr = (rec_dtr*)rec;
                    
                    timeKeyLocal = get_id();
                    
                    dprintf(fd_dtr, "%llu,%llu,%s\n", LocalPrimKey, timeKeyLocal, \
                            (*(dtr->TEXT_DAT) ? (dtr->TEXT_DAT)+1 : " "));
                    break;
                }
                case REC_UNKNOWN: {
                    rec_unknown *unk = (rec_unknown*)rec;
                    printf("\tBytes: %i\n", unk->header.REC_LEN);
                    printf("\tTYP: 0x%X [%i]\n", unk->header.REC_TYP, unk->header.REC_TYP);
                    printf("\tSUB: 0x%X [%i]\n", unk->header.REC_SUB, unk->header.REC_SUB);
                }
            }
            stdf_free_record(rec);
        }
        
        
        map_insert ("FAR", fd_far, *mapkey);
        if (!isEmpty(atr_))
            map_insert ("ATR", fd_atr, *mapkey);
        else
            fclose(atr_);
        map_insert ("MIR", fd_mir, *mapkey);
        map_insert ("MRR", fd_mrr, *mapkey);
        map_insert ("PCR", fd_pcr, *mapkey);
        map_insert ("HBR", fd_hbr, *mapkey);
        map_insert ("SBR", fd_sbr, *mapkey);
        //        if (!isEmpty(pmr_))
        map_insert ("PMR", fd_pmr, *mapkey);
        if (!isEmpty(pgr_))
            map_insert ("PGR", fd_pgr, *mapkey);
        else
            fclose(pgr_);
        if (!isEmpty(pgr_2))
            map_insert ("PGR_SUB", fd_pgr2, *mapkey);
        else
            fclose(pgr_2);
        if (!isEmpty(plr_))
            map_insert ("PLR", fd_plr, *mapkey);
        else
            fclose(plr_);
        if (!isEmpty(plr_2))
            map_insert ("PLR_SUB", fd_plr2, *mapkey);
        else
            fclose(plr_2);
        if (!isEmpty(rdr_))
            map_insert ("RDR", fd_rdr, *mapkey);
        else
            fclose(rdr_);
        if (!isEmpty(rdr_2))
            map_insert ("RDR_SUB", fd_rdr2, *mapkey);
        else
            fclose(rdr_2);
        if (!isEmpty(sdr_))
            map_insert ("SDR", fd_sdr, *mapkey);
        else
            fclose(sdr_);
        if (!isEmpty(sdr_2))
            map_insert ("SDR_SUB", fd_sdr2, *mapkey);
        else
            fclose(sdr_2);
        if (!isEmpty(wir_))
            map_insert ("WIR", fd_wir, *mapkey);
        else
            fclose(wir_);
        if (!isEmpty(wrr_))
            map_insert ("WRR", fd_wrr, *mapkey);
        else
            fclose(wrr_);
        if (!isEmpty(wcr_))
            map_insert ("WCR", fd_wcr, *mapkey);
        else
            fclose(wcr_);
        if (!isEmpty(pir_))
            map_insert ("PIR", fd_pir, *mapkey);
        else
            fclose(pir_);
        if (!isEmpty(prr_))
            map_insert ("PRR", fd_prr, *mapkey);
        else
            fclose(prr_);
#ifdef STDF_VER3
        if (!isEmpty(pdr_))
            map_insert ("PDR", fd_pdr, *mapkey);
        else
            fclose(pdr_);
        if (!isEmpty(fdr_))
            map_insert ("FDR", fd_fdr, *mapkey);
        else
            fclose(fdr_);
#endif
        map_insert ("TSR", fd_tsr, *mapkey);
        if (!isEmpty(ptr_))
            map_insert ("PTR", fd_ptr, *mapkey);
        else
            fclose(ptr_);
        if (!isEmpty(mpr_))
            map_insert ("MPR", fd_mpr, *mapkey);
        else
            fclose(mpr_);
        if (!isEmpty(mpr_2))
            map_insert ("MPR_SUB", fd_mpr2, *mapkey);
        else
            fclose(mpr_2);
        if (!isEmpty(ftr_))
            map_insert ("FTR", fd_ftr, *mapkey);
        else
            fclose(ftr_);
        if (!isEmpty(ftr_2))
            map_insert ("FTR_SUB", fd_ftr2, *mapkey);
        else
            fclose(ftr_2);
        if (!isEmpty(bps_))
            map_insert ("BPS", fd_bps, *mapkey);
        else
            fclose(bps_);
        if (!isEmpty(eps_))
            map_insert ("EPS", fd_eps, *mapkey);
        else
            fclose(eps_);
#ifdef STDF_VER3
        if (!isEmpty(shb_))
            map_insert ("SHB", fd_shb, *mapkey);
        else
            fclose(shb_);
        if (!isEmpty(ssb_))
            map_insert ("SSB", fd_ssb, *mapkey);
        else
            fclose(ssb_);
        if (!isEmpty(sts_))
            map_insert ("STS", fd_sts, *mapkey);
        else
            fclose(sts_);
        if (!isEmpty(scr_))
            map_insert ("SCR", fd_scr, *mapkey);
        else
            fclose(scr_);
#endif
        if (!isEmpty(gdr_fldCnt))
            map_insert ("GDR", fd_gdr1, *mapkey);
        else
            fclose(gdr_fldCnt);
        
        if (!isEmpty(gdr_gen))
            map_insert ("GDR_SUB", fd_gdr2, *mapkey);
        else
            fclose(gdr_gen);
        
        if (!isEmpty(dtr_))
            map_insert ("DTR", fd_dtr, *mapkey);
        else
            fclose(dtr_);
        
        // if (!isEmpty(prr2_ptr))
            // map_insert ("PTR2PRR", fd_prr2_ptr, *mapkey);
        // else
            // fclose(prr2_ptr);
        
        //        fclose(far_);
        //        fclose(atr_);
        //        fclose(mir_);
        //        fclose(mrr_);
        //        fclose(pcr_);
        //        fclose(hbr_);
        //        fclose(sbr_);
        //        fclose(pmr_);
        //        fclose(pgr_);
        //        fclose(plr_);
        //        fclose(rdr_);
        //        fclose(sdr_);
        //        fclose(wir_);
        //        fclose(wrr_);
        //        fclose(wcr_);
        //        fclose(pir_);
        //        fclose(prr_);
        //#ifdef STDF_VER3
        //        fclose(pdr_);
        //        fclose(fdr_);
        //#endif
        //        fclose(tsr_);
        //        fclose(ptr_);
        //        fclose(mpr_);
        //        fclose(ftr_);
        //        fclose(bps_);
        //        fclose(eps_);
        //#ifdef STDF_VER3
        //        fclose(shb_);
        //        fclose(ssb_);
        //        fclose(sts_);
        //        fclose(scr_);
        //#endif
        //        fclose(gdr_fldCnt);
        //        fclose(gdr_gen);
        //        fclose(dtr_);
        //
        //        stdf_close(f);
    }
    return NULL;
}


// stdf2csv transforms STDF to CSV
//   Parameters:
//      in_fd: a vector of input STDF file descriptors.
//      out_fd: key, value pairs of output data where key is table name, value is a vector of CSV file descriptors.
//
//   Return value:  true (success)| false (fail)
//
bool stdf2csv_adapter::stdf2csv(vector<int>& in_fd, map<string, vector<int>>& out_fd)
{
    //    uint64_t round_start = init_time();
    //    printf("roundstart: %llu\n", round_start);
    
    stdf_file *f;
    char *recname;
    rec_unknown *rec;
    int i;
    dtc_U4 stdf_ver;
    
    if (in_fd.size() < 1) {
        printf("Need some files to open!\n");
        return EXIT_FAILURE;
    }
    
    init_time_ = init_time();
    printf("Initial time:%llu\n", init_time_.load());
    
    //    timeKey = get_id(init_time_);
    //    printf("getkey1: %llu\n", timeKey);
    
    
    int pipefd[2];
    FILE *fp;
    int filefd;
    if (pipe(pipefd) == -1) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }
    
    for (i = 0; i < in_fd.size(); ++i) {
        filefd = in_fd[i];
        write(pipefd[1], &filefd, sizeof(filefd));
    }
    close(pipefd[1]);
    
    stdfFile_ theFile;
    theFile.my_pipe = pipefd[0];
    theFile.my_map = &out_fd;
    
    pthread_t t1,t2, t3, t4, t5, t6, t7, t8;
    
    pthread_create(&t1,NULL,stdf_parse,&theFile);
    pthread_create(&t2,NULL,stdf_parse,&theFile);
    pthread_create(&t3,NULL,stdf_parse,&theFile);
    // pthread_create(&t4,NULL,stdf_parse,&theFile);
    // pthread_create(&t5,NULL,stdf_parse,&theFile);
    // pthread_create(&t6,NULL,stdf_parse,&theFile);
    // pthread_create(&t7,NULL,stdf_parse,&theFile);
    // pthread_create(&t8,NULL,stdf_parse,&theFile);
//    pthread_create(&t9,NULL,stdf_parse,&theFile);
//    pthread_create(&t10,NULL,stdf_parse,&theFile);
//    pthread_create(&t11,NULL,stdf_parse,&theFile);
//    pthread_create(&t12,NULL,stdf_parse,&theFile);
    
    pthread_join(t1,NULL);
    pthread_join(t2,NULL);
    pthread_join(t3,NULL);
    // pthread_join(t4,NULL);
    // pthread_join(t5,NULL);
    // pthread_join(t6,NULL);
    // pthread_join(t7,NULL);
    // pthread_join(t8,NULL);
//    pthread_join(t9,NULL);
//    pthread_join(t10,NULL);
//    pthread_join(t11,NULL);
//    pthread_join(t12,NULL);
    //    pthread_join(t5,NULL);
    
    //    uint64_t round_stop = init_time();
    //    printf("totaltime: %llu\n",(round_stop - round_start));
    close(pipefd[0]);
    
    return true;
}
