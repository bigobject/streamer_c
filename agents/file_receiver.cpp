#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <component.h>
#include <time.h>
#include <stdlib.h>
#include <sstream>

#include <dirent.h> 
#include <limits.h> 

#include "file_receiver.h"

#include <iostream>
#include <bits/stdc++.h>
using namespace std;

#define KEY_RECV_FILE_PATH      "data_path"
#define KEY_RECV_FILE_PREFIX    "prefix"
#define KEY_RECV_FILE_INTERVAL  "interval"
#define KEY_RECV_FILE_SYNCTYPE  "sync_type"

bool file_receiver::isFileRecv(map<string, string>& param) {
    map<string, string>::const_iterator it;
    
    const char *keys[4] = {KEY_RECV_FILE_PATH, KEY_RECV_FILE_PREFIX,
        KEY_RECV_FILE_INTERVAL, KEY_RECV_FILE_SYNCTYPE};

    for (int i=0; i<4; i++) {
        it = param.find( keys[i] );
        if (it == param.end()) {
            Info("Not File Mode. Missing %s in config\n", keys[i]);
            return false;
        }
    }
    
    return true;
}

bool file_receiver::set_params(string& group, map<string, string>& param)
{
    receiver::set_type(RCV_FILE);

    if (!receiver::set_params(group, param))
        return false;

    string interval;
    if (get_param_value(KEY_RECV_FILE_INTERVAL, interval))
        _interval = stoul(interval);
    get_param_value(KEY_RECV_FILE_PATH, _path);

    string all_prefixs;
    get_param_value(KEY_RECV_FILE_PREFIX, all_prefixs);
    string::size_type pos = 0;
    string::size_type next_pos = 0;
    while (string::npos != (next_pos = all_prefixs.find_first_of(',', pos))) {
        _prefixs.push_back(all_prefixs.substr(pos, next_pos - pos));
        pos = next_pos + 1;
    }
    if (pos < all_prefixs.length())
        _prefixs.push_back(all_prefixs.substr(pos));

    _file_ids.clear();
    _file_ids.insert(_file_ids.begin(), _prefixs.size(), -1);

	string sync_type;
	if (get_param_value(KEY_RECV_FILE_SYNCTYPE, sync_type) && !sync_type.empty()) {
        switch (sync_type.at(0)) {
            case 'L':
                _stype = FRECV_SYNCTYPE_RECV_LAST;
                break;
            case 'R':
                _stype = FRECV_SYNCTYPE_RECV;
                break;
            case 'T':
                _stype = FRECV_SYNCTYPE_TRANS;
                break;
            case 'N':
            default:
                _stype = FRECV_SYNCTYPE_NORM;
                break;
        }
    }

    return true;
}

// file id can skip (0,3,5,8...)
//virtual ssize_t read_receiver(string& data, source_combo_struct* data_picked, msg_info_struct *msg_info) = 0;
//ssize_t file_receiver::read_receiver(string& data, FILE **fileptr, msg_info_struct *msg_info)
ssize_t file_receiver::read_receiver(string& data, source_combo_struct* data_picked, msg_info_struct *msg_info)
{
    if (_working_part_count>0){
        _working_part_seek+=1;
        if (_working_part_seek < _working_part_count) {
            data_picked->handle_type = 'P';
            sprintf(data_picked->type, "FILE_");
            data_picked->topic = string("streamer");
            data_picked->status = 1;
            data_picked->file_descriptor = _working_file_desc;
            data_picked->file_ptr = fdopen( dup(_working_file_desc), "r");
            data_picked->data_offset = _working_file_next_stop;
            data_picked->data_length = _csv_part_len[_working_part_seek];
            _working_file_next_stop += _csv_part_len[_working_part_seek];
            if (_working_part_seek == _working_part_count-1){
                data_picked->is_final_part='Y';
            } else {
                data_picked->is_final_part='N';
            }
            log_debugf("<%s> partition[%d/%d] in file (%d) picked, range[%ld+%ld], is final[%c]", __func__, _working_part_seek, _working_part_count, data_picked->file_descriptor, data_picked->data_offset, data_picked->data_length, data_picked->is_final_part);

            return 1;
        } else {
            _working_part_count=0;
            _working_part_seek=0;
            close(_working_file_desc);
            _working_file_desc=-1;
        }
    }

    data_picked->handle_type = 'P';
    sprintf(data_picked->type, "FILE_");
    data_picked->topic = string("streamer");
    data_picked->status = 1;
    data_picked->file_descriptor = -1;
    FILE **fileptr = &(data_picked->file_ptr);
    static bool retry_timeout = false;
    static int prefixIdx = 0;
        
    // if you have many prefix a,b,c
    // it will read file b->c->a->b->c ...
    // maybe prefixIdx should set -1 
    if (++prefixIdx >= _prefixs.size() )
        prefixIdx = 0;
    
    string prefix = _prefixs[prefixIdx];
    ssize_t file_id = _file_ids[prefixIdx];
    
    // Find next file_id, travse every file which has min id( {id} > pre_id)
    ssize_t pre_file_id = file_id;
    file_id = INT_MAX;
    DIR *d;
    struct dirent *dir;
    string file_base_name;
    d = opendir(_path.c_str());
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            string name = dir->d_name;
            string process_name = name;
            name = name.substr(0, name.find_last_of(".")); // remove Filename Extension
            if (name.substr(0, prefix.length()) == (prefix) ){
                try {
                    int id = stoi(name.substr(prefix.length()));
                    if (id > pre_file_id && id < file_id) {
                        file_id = id;
                        file_base_name = process_name;
                    }
                } catch(exception &err) {
                }
            }
        }
        closedir(d);
    }

    if (file_id == INT_MAX) {
        *fileptr = nullptr;
        sleep(_interval);
        return 1;
    }

    ostringstream file_path;
    file_path << _path << "/" << file_base_name;

    if (!retry_timeout) // avoid printing out same message constantly
        Info("%s: next doc to read : '%s'\n", __func__, file_path.str().c_str());

    char path_abs[1024] = {0};
    char *path_abs_p = realpath(file_path.str().c_str(), NULL);
    if (path_abs_p==NULL) {
        log_error(string("realpath returned NULL"), true);
        return 1;
    } else {
        strcpy(path_abs, path_abs_p);
        free(path_abs_p);

        log_debugf("<%s> realpath [%s]", __func__, path_abs);
        data_picked->file_name = string(path_abs);
    }
    
    int time_now;
    int time_file_lm;
    struct stat64 file_stat;
    while (true) {
        time_now=(int)time(NULL);
        if (stat64(path_abs, &file_stat) == -1) {
            return 1;
        } else {
            time_file_lm = file_stat.st_mtime;
        }

        log_debugf("<%s> data file modified %d sec ago, size = %lld", __func__, time_now-time_file_lm, file_stat.st_size);
        if ((time_now-time_file_lm) >= 2) {
            break;
        } else {
            sleep(1);
        }
    }

    int retry = 10;
    while (-1 == (_working_file_desc = open64(file_path.str().c_str(), O_RDONLY|O_CLOEXEC))) {
    //while (nullptr == (*fileptr = fopen64(file_path.str().c_str(), "r"))) {
        if (--retry <= 0) {
            retry_timeout = true;
            break;
        }

        sleep(_interval);
    }

    if (_working_file_desc == -1) // might read timeout
    //if (nullptr == *fileptr) // might read timeout
        return 1;

    _working_file = file_path.str();
    //_working_file_desc = fileno(*fileptr);
    *fileptr = fdopen( dup(_working_file_desc), "r");

    _working_part_seek = 0;

    // fseek(*fileptr, 0, SEEK_END);
    // _working_file_len = ftell(*fileptr);
    // data_picked->file_length=_working_file_len;
    // fseek(*fileptr, 0, SEEK_SET);
    if (stat64(path_abs, &file_stat) == -1) {
        return 1;
    } else {
        _working_file_len = file_stat.st_size;
    }
    data_picked->file_length=_working_file_len;
    log_debugf("<%s> data file len : %ld, partition size = %d", __func__, _working_file_len, _working_file_part_len);

    if (_working_file_len > _working_file_part_len) {
        bool success = parse_csv_record_len(1024*1024);
        if (!success){
            data_picked->status = -1;
            log_error(string("<") +__func__+ "> parse csv file failed, bad format or not readable", true);
            return -1;
        }

        _csv_part_len.clear();
        _csv_part_len.shrink_to_fit();

        unsigned int vecSize = _csv_rec_len.size();
        long file_part_len = 0;
        long file_part_next = 0;
        for (unsigned int i = 0; i < vecSize; i++) {
            file_part_next = file_part_len + _csv_rec_len[i];
            if (file_part_next > _working_file_part_len) {
                _csv_part_len.push_back(file_part_len);
                file_part_len = _csv_rec_len[i];
            } else {
                file_part_len = file_part_next;
            }
        }
        if (file_part_len > 0) {
            _csv_part_len.push_back(file_part_len);
        }
        data_picked->is_final_part='N';
        _working_part_count = _csv_part_len.size();
    } else {
        _csv_part_len.push_back(_working_file_len);
        data_picked->is_final_part='Y';
        _working_part_count = 1;
    }
    //_working_file_next_stop = _csv_part_len.size();
    data_picked->file_descriptor = _working_file_desc;
    data_picked->data_offset=0;
    data_picked->data_length=_csv_part_len[0];
    _working_file_next_stop = _csv_part_len[0];

    retry_timeout = false;

    log_debugf("<%s> new file (%d) picked, range[%ld+%ld]", __func__, data_picked->file_descriptor, data_picked->data_offset, data_picked->data_length);

    _file_ids[prefixIdx] = file_id;
    string doc_id;
    get_last_commitment(doc_id);
    _adapt->set_doc_id(doc_id);
    
    return 1;
}

bool file_receiver::get_last_commitment(string& doc_id)
{
    ostringstream last_commit;

    for (int i=0; i < _prefixs.size(); i++) {
        last_commit << _prefixs[i] << _file_ids[i];
        if (i <  (_prefixs.size() - 1))
            last_commit << "=";
    }
    
    doc_id = last_commit.str();
    
    Info("lastest file receiver commitment is '%s'\n", doc_id.empty() ? "NONE" : doc_id.c_str());
    return true;
}

bool file_receiver::backoff(string& doc_id)
{
    string last_doc;
    get_last_commitment(last_doc);

    // no change
    if (!last_doc.empty() && last_doc.compare(doc_id) == 0)
        return true;
    
    stringstream ss(doc_id);
    string pch;
      
    for (int i=0; getline(ss, pch, '='); i++)
    {
        if (i>=_prefixs.size()) {
            std::ostringstream prefix;
            if (_prefixs.size()) {
                std::copy(_prefixs.begin(), _prefixs.end()-1,
                        std::ostream_iterator<string>(prefix, ","));

                prefix << _prefixs.back();
            } 
            Error("Prefixs(%s) counts less than doc counts(%s)\n", prefix.str().c_str(), doc_id.c_str() );
            return false;
        }
        string prefix = _prefixs[i];
        if (pch.length() <= prefix.length() ||
            0 != pch.substr(0, prefix.length()).compare(prefix)) {
            Error("Prefix not match: %s, %s\n", pch.c_str(), prefix.c_str());
            return false;
        }
        _file_ids[i] = stoul(pch.substr(prefix.length()));
    }
    return true;
}

bool file_receiver::sync_strategy(string& rdoc, string& tdoc, string& out)
{
    switch (_stype) {
        case FRECV_SYNCTYPE_RECV:
        case FRECV_SYNCTYPE_RECV_LAST:
            out = rdoc;
            return true;
        case FRECV_SYNCTYPE_TRANS:
            out = tdoc;
            return true;
    }

    if (rdoc.empty() || tdoc.empty()) {
        out.clear();
        return true;
    }

    out = rdoc.compare(tdoc) > 0 ? tdoc : rdoc;
    return true;
}

bool file_receiver::parse_csv_record_len(int step) {
    log_debugf("------->   <%s> ", __PRETTY_FUNCTION__);

    _csv_rec_len.clear();
    _csv_rec_len.shrink_to_fit();

    //char * line = NULL;
    size_t len_line_buf = 1024;
    char * line_buf = (char *)malloc(len_line_buf);
    char * record_buf = (char *)malloc(len_line_buf);
    record_buf[0]=0;
    int len_record_buf = len_line_buf;
    //log_debugf("<%s> ................................initial line buf addr : 0x%X", __func__, line_buf);

    int num_dq=0;
    int len_read=0;
    int finished = 0;
    int read;
    int bunch_bytes=0;
    long total_bytes=0;
    //int bunch_count=0;
    int serial=0;

    FILE* fp = fdopen( dup(_working_file_desc), "r");
    while ((read = getline(&line_buf, &len_line_buf, fp)) != -1) {
        //log_debugf("<%s> ................................single round buf addr : 0x%X", __func__, line_buf);

        bunch_bytes+=read;
        len_read += read;
        if (len_record_buf < len_read+1) {
            record_buf = (char *)realloc(record_buf, len_read+1);
            len_record_buf = len_read+1;
        }

        strncat(record_buf, line_buf, read);
        //log_debugf("<%s> getline then concatenate , read len = %d, buf len = %d", __func__, read, strlen(record_buf));

        num_dq = count_csv_double_quote(record_buf);
        if ((num_dq % 2) == 0) {
            //free(line);
            //bunch_count++;
            if (bunch_bytes>=step){
                serial+=1;
                //log_debugf("<%s> (%8d)unit bytes read : %d", __func__, serial, bunch_bytes);
                //bunch_count=0;
                _csv_rec_len.push_back(bunch_bytes);
                total_bytes+=bunch_bytes;
                bunch_bytes=0;
            }
            len_read=0;
            record_buf[0]=0;
        }
    }

    if (bunch_bytes > 0) {
        serial+=1;
        //log_debugf("<%s> (%8d)unit bytes read : %d (final)", __func__, serial, bunch_bytes);
        _csv_rec_len.push_back(bunch_bytes);
        total_bytes+=bunch_bytes;
    }

    //log_debugf("<parse_csv_record_len> total bytes read : %ld", total_bytes);

    getc(fp);
    int is_eof = feof(fp);
    fseek( fp, 0, SEEK_SET);
    fclose(fp);
    free(line_buf);
    free(record_buf);

    if (is_eof) {
        log_debugf("<%s> return true", __func__);
        return true;
    } else {
        log_debugf("<%s> return false", __func__);
        return false;
    }
}
