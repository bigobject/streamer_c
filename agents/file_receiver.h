#pragma once

#include "receiver.h"
#include "common_func.h"

typedef enum {
    FRECV_SYNCTYPE_NORM,
    FRECV_SYNCTYPE_RECV_LAST,
    FRECV_SYNCTYPE_RECV,
    FRECV_SYNCTYPE_TRANS
} file_recv_sync_type;

class file_receiver : public receiver
{
    public:
        file_receiver() : _path("."), _working_file(""), _working_part_seek(0),  _working_part_count(0), 
        _working_file_part_len(1000000000), _interval(0), _stype(FRECV_SYNCTYPE_NORM)
        {}
        virtual ~file_receiver() {}

        virtual ssize_t read_receiver(string& data, source_combo_struct* data_picked, msg_info_struct *msg_info);

        virtual bool get_last_commitment(string& doc_id);

        virtual bool backoff(string& doc_id);

        virtual bool sync_strategy(string& doc1, string& doc2, string& out);

        virtual bool set_params(string& group, map<string, string>& param);
        bool parse_csv_record_len(int step);

        bool isFileRecv(map<string, string>& param);
        virtual vector<string> get_prefixs()
        {return _prefixs;}
    private:
        string          _path;
        string          _working_file;
        int             _working_file_desc;
        long             _working_file_len;
        long             _working_file_next_stop;
        long             _working_file_part_len;
        vector<int>     _csv_rec_len;
        vector<long>     _csv_part_len;
        int             _working_part_seek;
        int             _working_part_count;

        unsigned int    _interval; // in second
        file_recv_sync_type     _stype;

        vector<string>     _prefixs;
        vector<ssize_t>    _file_ids; // $$ filer_id for each file 

};