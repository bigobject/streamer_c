#include <string>
#include <vector>
#include "adapter.h"
#include "common_func.h"
#include "go_json2csv.h"

class json2csv_adapter : public adapter
{
    public:
        json2csv_adapter() : _is_file(false), _csv_skip_lines(0), _out_dir(""),
        _keep_alive_files(""), _conf("") {}
        virtual ~json2csv_adapter() {}

        virtual bool set_params(string& group, map<string, string>& param);
        bool isChosen(map<string, string>& param);
    protected:
        virtual JsonToCsv_return svt_transform(GoSlice inputs);
        virtual bool transform(vector<FILE*>& data, string& doc_id, size_t& count, msg_info_struct *msg_info);
        virtual bool transform(vector<source_combo_struct*>& data_sources, string& doc_id, size_t& count, msg_info_struct *msg_info);
        virtual bool multipass_transform(vector<source_combo_struct*>& data_sources, string& doc_id, size_t& count, msg_info_struct *msg_info);
        virtual int pending_materials();
        virtual bool slice_data_file(int fd_data, int len_least, vector<int>& data_file_family);
        virtual bool slice_data_file(int fd_data, int len_least, long offset_part , long len_part, vector<int>& chunk_file_family);
    private:
        bool    _is_file;
        int     _csv_skip_lines;
        vector<string> _output_table_def;

        int     _file_chunk_mode=0;
        int     _file_chunk = 32; // $$ default is 32MB, it can be changed in set_params()
        int     _level_gigabytes;
        int     _num_chunk_bunch;
        int     _num_material_files = 0;
        int     _next_queue_head;
        int     _svt_workers = 8;
        string  _out_dir;
        string  _keep_alive_files;
        string  _conf;

        int     _flush_time_limit = 600; // $$ default is 600, it can be changed in set_params()

        bool clear_cache();
};