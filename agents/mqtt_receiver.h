#pragma once

#include <semaphore.h>
#include "MQTTClient.h"
#include "receiver.h"
#include "common_func.h"

typedef enum {
    MQTT_RECV_SYNCTYPE_NORM,
    MQTT_RECV_SYNCTYPE_RECV_LAST,
    MQTT_RECV_SYNCTYPE_RECV,
    MQTT_RECV_SYNCTYPE_TRANS
} mqtt_recv_sync_type;

class mqtt_receiver : public receiver
{
    public:
        mqtt_receiver() : _interval(0), _stype(MQTT_RECV_SYNCTYPE_TRANS), 
        _mqtt_client(nullptr), _mqtt_monitor(nullptr), _keepalive_interval(65535), _clean_session(0),
        _mqtt_msgs_ignored(-1), _mqtt_msgs_deserted(-1)
        {}
        virtual ~mqtt_receiver() {}

        virtual ssize_t read_receiver(string& data, source_combo_struct* data_picked, msg_info_struct *msg_info);

        virtual bool get_last_commitment(string& doc_id);

        virtual bool backoff(string& doc_id);

        virtual bool sync_strategy(string& doc1, string& doc2, string& out);

        virtual void set_topics(string _topic_picked);
        virtual bool set_params(string& group, map<string, string>& param);
        virtual void set_self_ref(mqtt_receiver * obj_ref);

        virtual bool isChosen(map<string, string>& param);
        virtual vector<string> get_prefixs();

/*
        virtual void delivered(void *context, MQTTClient_deliveryToken dt);
        virtual int msgarrvd(void *context, char *topicName, int topicLen, MQTTClient_message *message);
        virtual void connlost(void *context, char *cause);
        */

    private:
        mqtt_receiver * _obj_ref;
        unsigned int    _interval; // in second
        mqtt_recv_sync_type     _stype;

        vector<string>     _prefixs;
        vector<ssize_t>    _file_ids; // $$ filer_id for each file
        map<string, string> _topic_traces;

        string      _broker;
        int         _keepalive_interval;
        int         _clean_session;
        
        string      _client_id;
        int         _qos;

        long _mqtt_msgs_ignored;
        long _mqtt_msgs_deserted;

        vector<string>    _topic_names;
        map<string, string>    _topic_types;
        vector<string>    _topic_data;

        map<string, int> _topic_specific_fds;
        vector<source_combo_struct *>    _cooked_data_catalogue;


        MQTTClient * _mqtt_client;
        MQTTClient * _mqtt_monitor;
        
        int _fd_for_mqtt_msgs_log;
};