# Streamer C++ 1.4.5

## BO version must newer than 1.8.8.2021-11-16

Folder structure
=========================

| Folder | Description |
| ------ | ----------- |
| agents   | agents of different adapters  |
| Debug | Debug Makefile |
| include     | header files |
| Release     | Release Makefile |
| src     | source code of major program |


How to use
=========================

1. Built on your machine 
    1. Make sure your environment proper to build.
    2. cd Release or Debug folder and make it.
2. Built by streamer_builder
    1. Follow streamer_docker/streamer_builder and streamer_docker/streamer  README to build.

# Change Log
==========================

- 0.0.0 
    - [0.0.1]
        - 新增 flush time: 可讀agent_XXXX.conf裡面的 flush_time_limit來設定最久要幾秒flush一次, (沒寫的話, defualt為300秒)
        - 可從 Go的Parser那邊收到error message並寫進txt的error log, 一天最多一個error log
    - [0.0.2]
        - 加了 bo_transmitter::get_total_count_by_sql(): 為了解決 checkpoint 有時候會被歸 0 的問題, 按照 Edward 的建議, 寫了一個Work around
    - [0.0.3]
        - 加了checkpoint health check: 寫checkpoint之前, 計算實際table的筆數和checkpoint差多少, 異常則寫message log
        - 把多加了 common_func.h 和 common_func.cpp: 把寫log的功能移到外層, 盡量大家都可以隨時call
    - [0.0.4]
        - 修改 kafka_receiver.cpp 裡開啟tempfile的方式: 因為把streamer包進docker執行之後, 碰到file system不支援 open() 裡的 flag: O_TMPFILE, 要另找方法開tempfile
        目前是改用 tmpfile() 來開tempfile, 此方法預設是在 /tmp 裡建 tempfile, close之後就自動砍掉
        也就是說, 從config 讀進來的 _out2file 目前是沒用處了
        - 修改 Write_Log 以及相關 logging 的 function: 多一個parameter, bool print_on_screen, 來決定寫 log 的同時要不要 print 在螢幕上
        - 修改 get_total_count_by_sql: return 改成 bool, 代表exec_sql 是否成功
    - [0.0.5]
        - bo_transmitter.cpp 對 health check 方式有些許改變
        - size_t bo_channel::get_total_count() 不再 retry. 如果 read() 的 error == EINTR, try到成功為止, 否則直接跳出
        - 以 streamer_C: 0.0.4 為基礎新增 influxdb 功能:
            
            - file_receiver 和 kafka_receiver 的 read() 改成 read_receiver() 
                 (start() 的 read() 也改成 read_receiver() )
                 純粹只是想和 system call 的 read() 區分開來, 以免搞混

            
            - 以下多了跟 influxdb 相關的更動:
                - config裡多了 influxdb 的各項參數
                - run_OOXX.sh 裡可多傳一個參數 "influxdb", 有寫的話則寫入influxdb, 沒寫則寫入BO. 也就是兩者擇一
                - Makefile 要加入 csv2influxdb_adapter
                
                - 加了 csv2influxdb_adapter.cpp 和 csv2influxdb_adapter.h
                - sagent.cpp main()
                - bool coordinator::initialize()
                - bool coordinator::sync()
                
            - 以上更動, 重點: 
                - 寫入 influxdb 或 BO, 兩者擇一
                - 一個 influxdb 的 streamer 只負責寫入一個 measurement
                - influxdb 的資料不需 parsing, 所以 data flow 也只到 adapter 為止
                    , 不須經過 transmitter 和 bo_transmitter, 當然也不必經過 JsonToCsv()
                    , 所以在 main() 的時候一旦確定此 streamer 要寫入 influxdb, 就不必宣告和初始化跟 transmitter 相關的所有參數
                - 如果 streamer initialize時讀不到 topic 會 retry

    - [0.0.6]
        - 取消 SUSPEND 和 RESUME, 改用 "flush table OOXX;" 的 sql command
        - 讓所有使用 exec_sql() 都接它回傳的 bool 
        - 因為 influxdb 輸入的資料有可能順序會變來變去, 所以需要經過 JsonToCsv() 將欄位塞到 table 中正確的位置
            修改 influxdb 流程: 從 adpater 改成 transmitter:
            - 刪除 csv2influxdb_adapter.cpp 和 csv2influxdb_adapter.h
            - 新增 influxdb_transmitter.cpp 和 influxdb_transmitter.h
            - 修改 sagent.cpp 裡宣告 object 的方式
            - 修改 Release 裡的 Makefile, 並且 clean 時 rm $(INCDIR)/go_json2csv.h

            - 以上更動, 重點:
                - 寫入 influxdb 或 BO, 兩者擇一
                - 一個 influxdb 的 streamer 可寫入多個 measurement
                - 因為是 transmitter, 所以需要初始化, 也會經過 JsonToCsv()
                - 如果 streamer initialize時讀不到 topic 會 retry
                
- 0.2.0
    - Fix: csv mode
    - Fix: remove log.txt, all log to stdout
    - Fix: remove prefix last under line character
    - [0.2.1]
        - Fix: set stdout to null, let streamer docker show logs immediately 
    - [0.2.2]
	    - Fix: csv file name stoi() failed
    - [0.2.3]
	    - Fix: total_count can not use cp.get_total_count()
	    - Add: logging csv file name
    - [0.2.4]
        - Fix: csv shrink_db parameter make streamer stop
    - [0.2.5]
        - Fix: write checkpoint use insert into instead csv mode        
    - [0.2.6]
        - Fix: reset_chan will add one empty row
    - [0.2.7]
        - Fix: connection failed, reconnect in 10 sec
        - [0.2.7.1] Update ReadMe.md
    - [0.2.8]
	    - Fix: file receiver not use .csv use .* 
    - [0.2.9]
        - Add: Version 1.0.2

- 0.3.0
    - Add: auto receiver by config
    - Add: Streamer version 1.1.0
- 0.4.0
    - Add: file multi prefix
    - Add: Streamer version 1.2.0
    - [0.4.1]
	- Fix: version with date
- 1.2.0
    - [2021-06-10]
	    - Revision
	    - Use date version
    - [2021-06-15]
	    - Fix: date version by compiler time
    - [2021-06-24]
	    - Fix: remove config value quotes
    - [2021-06-25]
	    - Fix: remove config key quotes
    - [2021-08-06]
	    - Fix: add and remove some comments
    - [2021-08-17]
	    - Fix: remove comments
    - [2021-08-19]
	    - Fix: kafka recollect data

- 1.2.1
    - [2021-10-06]
        - first formal version
- 1.2.2
    - [2021-10-26]
        - Fix: restart bo can not receive next file
- 1.2.3
    - [2021-11-08]
        - Fix: Kafka offset out of order
- 1.2.4
    - [2021-11-16]
        - Fix: if reset connection it must wait pre connection finish and sync
	- BO version must newer than 1.8.8.2021-11-16
    - [2021-11-16-1]
        - Fix: stoi exception
- 1.2.5 
    - [2021-11-19]
        - Fix: retry check data channel connection and reset channel with empty csv file issue
	    - Bug #2139
	    - testcase: "kafka_streamer_test_v2"
    - [2021-11-25]
	    - Fix: if sync failed retry it
	    - Bug #2150
    - [2021-11-25-1]
        - Fix: set checkpoint
- 1.3.0
    - [2021-12-07]
        - Fix: golang return c pointer to avoid memory leak
        - Fix: memory leak
- 1.3.1
    - [2021-12-15]
        - Fix: use tmpfile
- 1.3.2
    - [2021-12-17]
        - Fix: remove tmpfile function, and limit one fd per time
- 1.3.3
    - [2021-12-23]
        - Add: check checkpoint value before insert it
    - [2021-12-30]
        - Fix: get_pre_cp_count()
- 1.3.4
    - [2022-01-03]
        - Fix: instead of fd, use file pointer between C and go
    - [2022-01-04]
        - Fix: file pointer for file mode
    - [2022-01-05]
        - Fix: batch_proc with file mode
- 1.3.5
    - [2022-01-06]
        - Add: multi allow cleanup config
    - [2022-01-20]
        - transform json to csv fail
    - [2022-01-20-1]
        - Remove some error string
    - [2022-01-21]
        - Fix: svt return empty result
    - [2022-01-26]
        - Add: check query success before insert checkpoint
    - [2022-01-27]
        - Fix: check last total when point table is empty
- 1.4.0
    - [2022-01-27]
        - Add: encrypt mode
- 1.4.1
    - [2022-02-07]
        - Fix: insert multi data in one query with encrypt mode
    - [2022-02-18]
        - Fix: datetime with encrypt mode
    - [2022-03-03]
        - Fix: remove some debug messages
- 1.4.2
    - [2022-03-25]
        - Fix: escape data with encryt mode
    - [2022-03-25-2]
        - Fix: change cp table doc column to VARSTRING(786432)
- 1.4.3
    - [2022-04-29]
        - Fix: processing of last data row not ended with new line
    - [2022-05-10]
        - Fix: processing for muitiple lines within cells of CSV files
- 1.4.4
    - [2022-07-01]
        - Fix: identify error occurance while reading socket interface communicates with BO
- 1.4.5
    - [2022-12-21]
        - Add: Add pass_thru_adapter for direct csv dump to BO