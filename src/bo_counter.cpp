#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <errno.h>

#include <sstream>

#include "component.h"
#include "bo_counter.h"

bo_counter* bo_counter::_counter = NULL;

bo_counter::bo_counter() :
    _is_running(false), _id(0), _url(""), _addr(""), _port(0), _retention(0)
{
    memset(_basic_val, 0, sizeof(_basic_val));
}

void bo_counter::set_counter(const string& addr, unsigned short port, int retention)
{
    _addr = addr;
    _port = port;
    _retention = retention;

    ostringstream tmp;
    tmp << "http://" << _addr << ":" << _port;
    _url = tmp.str();
}

void bo_counter::submit()
{
    uint64_t current_cnt[BCOUNT_MAX] = {0};
    memcpy(current_cnt, &_basic_val[BCOUNT_MAX], BCOUNT_MAX * sizeof(_basic_val[0]));
    memcpy(&_basic_val[BCOUNT_MAX], &_basic_val[0], BCOUNT_MAX * sizeof(_basic_val[0]));

    ostringstream content;
    for (int i = 0; i < BCOUNT_MAX; ++i) {
        current_cnt[i] = _basic_val[BCOUNT_MAX + i] - current_cnt[i];
        if (0 != current_cnt[i]) {
            content << "{\"index\":{\"_id\":" << ++_id << "}}\n";
            content << "{\"ctype\":" << i << ",\"count\":" << current_cnt[i];
            content << ",\"ts\":" << time(NULL) * 1000 << "}\n";
        }
    }
    string basic_count = content.str();
    if (basic_count.length() > 0)
        send_counter("sa_basic", basic_count);
}

void bo_counter::send_counter(const string& type, const string& content)
{
    static bool prev_fail = false;
    char http_req[4096] = {0};
    snprintf(http_req, sizeof(http_req) - 1,
        "POST /bo/%s/_bulk HTTP/1.1\r\nContent-length: %lu\r\nContent-type: application/json\r\n\r\n%s",
        type.c_str(), content.length(), content.c_str());

    int fd = -1;
    struct hostent *host;
    struct sockaddr_in addr;

    if ((host = gethostbyname(_addr.c_str())) == NULL)
        return;

    if (-1 == (fd = socket(PF_INET, SOCK_STREAM, 0)))
        return;

    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(_port);
    addr.sin_addr.s_addr = *(long*)(host->h_addr);

    if (::connect(fd, (struct sockaddr*)&addr, sizeof(addr)) != 0) {
        close(fd);

        // only print once
        if (!prev_fail) {
            Error("%s, connect to counter server fail: %s\n", __func__, strerror(errno));
            prev_fail = true;
        }
        return;
    }

    ssize_t req_len = strlen(http_req);
    ssize_t send_len = 0, send_ret = 0;
    while (0 < (send_ret = write(fd, &http_req[send_len], req_len - send_len))) {
        send_len += send_ret;
        if (send_len >= req_len)
            break;
    }
    if (send_ret <= 0) {
        close(fd);

        // only print once
        if (!prev_fail) {
            Error("%s, send counter data fail: %s\n",  __func__, strerror(errno));
            prev_fail = true;
        }
        return;
    }

    ssize_t rsp_hdr_len = 0, recv_ret = 0;
    ssize_t expect_content_len = 0, read_content_len = 0;
    bool rsp_hdr_ready = false;
    char rsp_buf[2048] = {0}; // so far, we don't care about what it responsed, just be sure received everything.
    
    while(0 < (recv_ret = read(fd, &rsp_buf[rsp_hdr_len], sizeof(rsp_buf) - rsp_hdr_len))) {
        if (!rsp_hdr_ready) {
            char* rsp_hdr_end = strstr(rsp_buf, "\r\n\r\n");
            if (!rsp_hdr_end) { 
                rsp_hdr_len += recv_ret;
                continue;
            }
            rsp_hdr_ready = true;

            char* content_len_tag = strstr(rsp_buf, "Content-Length:");
            if (!content_len_tag)
                content_len_tag = strstr(rsp_buf, "Content-length:");
            if (content_len_tag)
                expect_content_len = atoi(content_len_tag + 16);

            size_t hdr_len = (size_t)rsp_hdr_end - (size_t)rsp_buf + 4;
            read_content_len = rsp_hdr_len + recv_ret - hdr_len;
            rsp_hdr_len = 0; // reset buffer
        }
        else
           read_content_len += recv_ret;

        if (read_content_len >= expect_content_len)
            break;
    }
    close(fd);

    prev_fail = false;
}

void bo_counter::start_counter()
{
    _is_running = true;
    _id = time(NULL) * 1000000;

    while (_is_running) {
        sleep(1);
        submit();
    }
}

void bo_counter::stop_counter()
{
    _is_running = false;
}
