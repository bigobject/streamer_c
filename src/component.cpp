#include <fstream>
#include <string>

#include "component.h"

#define CONF_COMMENT    '#'

bool component::set_params(string& group, map<string, string>& param)
{
    if (_params.empty())
        _params = param;
    _group = group;

    return true;
}

bool component::get_param_value(string key, string& val)
{
    map<string, string>::const_iterator it = _params.find(key);
    if (it == _params.end())
        return false;
    val = it->second;
    Debug("%s: key '%s', value '%s'\n", __func__, key.c_str(), val.c_str());
    return true;
}

ini_conf::ini_conf(string conf)
{
    ifstream in_file(conf);
    if (!in_file)
        return;

    map<string, string> section_data;
    string buf, section("default");
    while (in_file) {
        getline(in_file, buf);

        if (buf.length() < 2)
            continue;

        if (buf.at(0) == CONF_COMMENT)
            continue;

        if (buf.at(0) == '[' && buf.at(buf.length()-1) ==']') {
            if (!section_data.empty())
                _conf.insert(pair<string, map<string, string> >(section, section_data));
            section = buf.substr(1, buf.length() - 2);
            section_data.clear();
            continue;
        }

        string key, val;
        string::size_type pos = buf.find_first_of('=');
        if (pos == string::npos)
            continue;

        string::size_type key_len= buf.find_first_of(" =");
        if (key_len == string::npos)
            continue;
        key = buf.substr(0, key_len);
        // Remove quotes
        if ( (key.at(0) == '\"' && key.at(key.length()-1) == '\"') 
            || (key.at(0) == '\'' && key.at(key.length()-1) == '\'') )
            key = key.substr(1, key.size() - 2);

        if (pos < buf.length() - 1) {
            pos = buf.find_first_not_of(' ', pos + 1);
            if (pos != string::npos) {
                val = buf.substr(pos);
                // Remove quotes
                if ( (val.at(0) == '\"' && val.at(val.length()-1) == '\"') 
                    || (val.at(0) == '\'' && val.at(val.length()-1) == '\'') )
                    val = val.substr(1, val.size() - 2);
            }
                
        }
        section_data.insert(pair<string, string>(key, val));
    }
    in_file.close();
    // last section
    if (!section_data.empty())
        _conf.insert(pair<string, map<string, string> >(section, section_data));
}

bool ini_conf::get_section(string section, map<string, string>& data)
{
    map<string, map<string, string> >::const_iterator it = _conf.find(section);
    if (it != _conf.end()) {
        data = it->second;
        return true;
    }
    return false;
}

bool ini_conf::get_value(string section, string key, string& value)
{
    map<string, map<string, string> >::const_iterator it_sec = _conf.find(section);
    if (it_sec != _conf.end()) {
        map<string, string>::const_iterator it_key = it_sec->second.find(key);
        if (it_key != it_sec->second.end()) {
            value = it_key->second;
            return true;
        }
    }
    return false;
}