#include <sys/types.h>
#include <sys/sendfile.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <strings.h>
#include <string.h>
#include <errno.h>

#include <sstream>

//#include "bo_counter.h"
#include "bo_transmitter.h"
#include "bo_stream.h"

#include <iostream>

#include "cryptopp/aes.h"
using CryptoPP::AES;

#include "cryptopp/modes.h"
using CryptoPP::OFB_Mode;

// encrypt libraty
#include "functions.h"


using namespace std;

#define KEY_TRAN_BO_HOST    "host"
#define KEY_TRAN_BO_PORT    "port"
#define KEY_TRAN_BO_BKPORT  "bk_port"
#define KEY_TRAN_BO_TYPE    "type"
#define KEY_TRAN_BO_SQL     "sql"
#define KEY_TRAN_BO_ENCRYPT "encrypt"
#define KEY_TRAN_BO_DIM_TBL "dimension"
#define KEY_TRAN_BO_PRECREATE   "pre_create"
#define KEY_TRAN_BO_CREATECP    "create_cp"
#define KEY_TRAN_BO_CPNAME      "cp_name"
#define KEY_TRAN_BO_CPRETENTION "cp_retention"
#define KEY_TRAN_BO_REPLICA     "replica"
#define KEY_TRAN_BO_NO_SYNC     "no_sync"
#define KEY_TRAN_BO_FIX_WS      "workspace"
#define KEY_TRAN_BO_REPLICA_ONLY    "replica_only"
#define KEY_TRAN_BO_ALLOW_CLEANUP   "alow_cleanup"
#define KEY_TRAN_BO_COMMIT_ACTION   "commit_action"
#define KEY_TRAN_BO_SOURCE          "source"
#define KEY_TRAN_BO_NO_SUSPEND      "no_suspend"
#define KEY_TRAN_BO_PRE_COMMIT_VERIFICATION      "pre_commit_verifications"
#define KEY_TRAN_BO_LEAST_XMIT_INTERVAL      "least_xmit_interval"
#define KEY_TRAN_BO_NUM_FIXED_TABLES      "table_num"
#define KEY_TRAN_BO_COLS_IN_EXT_PREFIX      "cols_in_ext"
#define KEY_TRAN_BO_COLS_IN_EXT_DEFAULT      "cols_in_ext_default"
#define KEY_TRAN_BO_LOG_DATA_TO_DB      "log_data_to_db"

#define PRIME_HTTP_DOMAIN      "bigobject.io"
#define STREAM_MAX_SIZE 50000  // 9091 size is uint16_t: 65535     
#define TRAN_BO_READ_TIMEOUT    3

int _fd_for_data_to_send;
//long current_time = stol(get_current_date("unix_ms"));
long current_time;
long last_db_access=0;
long last_table_size=0;
long last_table_increment=0;
long bytes_to_db=0;

bool bo_transmitter::_single_shot_conn = 1;

in_addr hostnameToIPv4(const char *hostname) {
  struct addrinfo hints, *servinfo, *p;
  struct sockaddr_in *h;
  int rv;
  string ip_str;
  in_addr ip;
  ip.s_addr = -1;

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  if ((rv = getaddrinfo(hostname, NULL, &hints, &servinfo))
    == 0) {
    for (p = servinfo; p != NULL; p = p->ai_next) {
      if (p->ai_family == AF_INET) { // IPv4
        h = (struct sockaddr_in*) p->ai_addr;
        ip = h->sin_addr;
        break;
      }
    }
    freeaddrinfo(servinfo);
  }
  return ip;
}

/* functions for debugging
void trace_shrink(int line_no, char * module, long shrink) {
    cout << "line " << line_no << "; module [" << module << "] : shrink = " << shrink << endl;
}

void trace_commitment(int line_no, commitment& cp, string extra) {
    string table_name = cp.get_table_name();
    string doc_id = cp.get_doc_id();
    size_t total_count = cp.get_total_count_by_pkt();
    size_t success_count = cp.get_success_count();

    cout << "line " << line_no << "; addr of cp [" << &cp << "] : "  << endl;
    cout << "\t\t table_name = " << table_name << endl;
    cout << "\t\t doc_id = " << doc_id << endl;
    cout << "\t\t total_count = " << total_count << endl;
    cout << "\t\t success_count = " << success_count << endl;
    if (extra.compare("") != 0){
        cout << "\t\t " << extra << endl;
    }
}
*/

//////////////////////////////////////////////////
// bo_channel
//////////////////////////////////////////////////
bo_channel::~bo_channel()
{
    if (-1 != _ctrl_channel) {
        close(_ctrl_channel);
        _ctrl_channel = -1;
    }

    if (-1 != _data_channel) {
        close(_data_channel);
        _data_channel = -1;
    }
}

bool bo_channel::create_table(string ws)
{
    if (-1 == _ctrl_channel)
        return false;

    string dummy;
    bool ret = exec_sql(_ctrl_channel, _is_dim ? DIRECTOR_WORKSPACE : ws, _sql, false, dummy);
    
    // marked by joey @ 20230226, might be unnecessary
    //close(_ctrl_channel);
    //_ctrl_channel = -1;

    return ret;
}

bool bo_channel::create_table(string ws, string &result)
{
    if (-1 == _ctrl_channel)
        return false;

    bool ret = exec_sql(_ctrl_channel, _is_dim ? DIRECTOR_WORKSPACE : ws, _sql, false, result);

    return ret;
}

void bo_channel::set_ctrl_channel(int fd)
{
    if (-1 != _ctrl_channel)
    {
        close(_ctrl_channel);
    }
    _ctrl_channel = fd;
}

void bo_channel::set_data_channel(int fd)
{
    if (-1 != _data_channel)
    {
        close(_data_channel);
    }
    _data_channel = fd;
}

void bo_channel::set_bo_info(string host, unsigned short port, string workspace, int table_watermark_verifications, int least_xmit_interval)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    log_debug(string("<") +__func__+ "> data workspace : " + workspace, true);

    _host = host;
    _port = port;
    _workspace = workspace;
    _table_watermark_verifications = table_watermark_verifications;
    _least_xmit_interval = least_xmit_interval;
}

bool bo_channel::close_chan(bool force)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    if (_ctrl_channel != -1) {
        if (!force){
            return false;
        } else {
            close(_ctrl_channel);
            log_debugf("<%s> control channel (%d) closed", __func__, _ctrl_channel);
            _ctrl_channel = -1;
        }
    }

    if (_data_channel != -1) {
        if (!is_empty())
            flush();

        if (!force){
            return false;
        } else {
            close(_data_channel);
            log_debugf("<%s> data channel (%d) closed", __func__, _data_channel);
            _data_channel = -1;
        }
    }

    return true;
}

ssize_t bo_channel::transmit(void* data, size_t size)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    if (-1 == _data_channel) {
        log_debug(string("<") +__func__+ "> session closed!!!", true);
        return 0;
    }

    std::string *sp = static_cast<std::string*>(data);

    //Debug("data: %s, size: %d\n", *sp, size);
    log_debug(string("<") +__func__+ "> send with MSG_DONTWAIT // data : " + *sp + "// size : " + to_string(size), true);
    return ::send(_data_channel, data, size, MSG_DONTWAIT);
}

ssize_t bo_channel::transmit(int infd)
{
    return transmit(infd, 0);
}

int bo_channel::open_connection(const string& peer, unsigned short port)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    int fd = -1;
    struct hostent *host;
    struct sockaddr_in addr;

    if (-1 == (fd = socket(PF_INET, SOCK_STREAM, 0)))
        return -1;

    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);

    if ((addr.sin_addr = hostnameToIPv4(peer.c_str())).s_addr == -1)
        return -1;

    if (::connect(fd, (struct sockaddr*)&addr, sizeof(addr)) != 0) {
        close(fd);
        Error("Connect fail: %s\n", strerror(errno));
        return -1;
    }

    return fd;
}

ssize_t bo_channel::transmit(int infd, int offset)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    if (-1 == infd)
        return 0;

    current_time = stol(get_current_date("unix_ms"));
    long itvl = current_time-last_db_access;
    //log_debugf("<%s> interval of db access : %d", __func__, itvl);
    //last_db_access = current_time;
    
    int least_intvl = 0;
    if (itvl < least_intvl) {
        usleep((least_intvl-itvl)*1000); // convert us to ms
        current_time = stol(get_current_date("unix_ms"));
        itvl = current_time-last_db_access;
    }
    log_debugf("<%s> interval of db access : %d", __func__, itvl);
    last_db_access = current_time;

    int fd_transmit;
    int new_fd;
    bool use_new_connection = bo_transmitter::is_single_shot_conn();
    log_debugf("<%s> use_new_connection : %d", __func__, use_new_connection);
    if (use_new_connection) {
        close_chan(true);

        for (int _retry_idx=0; _retry_idx < 8 ; _retry_idx++) {
            log_debug(string("<") +__func__+ "> try connecting " + _host + ":" + to_string(_port), true);
            new_fd = open_connection(_host, _port);
            if (new_fd != -1) {
                break;
            }
        }
        if (new_fd == -1)
            return 0;

        fd_transmit = new_fd;
    } else {
        if (_data_channel == -1) {
            for (int _retry_idx=0; _retry_idx < 8 ; _retry_idx++) {
                //log_debug(string("<") +__func__+ "> try connecting " + _host + ":" + to_string(_port), true);
                log_debugf("<%s> Create new data channel, try connecting %s:%d", __func__, _host.c_str(), _port);
                _data_channel = open_connection(_host, _port);
                if (_data_channel != -1) {
                    break;
                }
            }
            if (_data_channel == -1){
                log_debug(string("<") +__func__+ "> failed to create data channel", true);
                return 0;
            }
            //flush_table will destroy the channel
            //flush_table(true);

            //pre_transmit(_data_channel, channel::get_name(), string("csv"), -1);
            /*
            if (SYN_MARK_LEN != ::write(_data_channel, syn_term_mark, SYN_MARK_LEN)){
                log_debugf("<%s> failed to write term mark...", __func__);
            }
            */
        }
        fd_transmit = _data_channel;
    }

    log_debug(string("<") +__func__+ "> data channel descriptor : " + to_string(fd_transmit), true);

    int my_print = 0;

    off_t fsize;
    fsize = lseek(infd, 0, SEEK_END);
    log_debugf("<%s> cooked file(%d) len = %d", __func__, infd, fsize);
    lseek(infd, 0, SEEK_SET);

    off_t off_begin = offset; 
    struct stat in_stat = {0};
    if (0 != fstat(infd, &in_stat)) 
    {
        Write_System_ErrorLog(string(__PRETTY_FUNCTION__) + ": fstat of fd " + to_string(infd) + " failed", true);
        return 0;
    }
    // avoid send empty file fail and retry
    if (in_stat.st_size == 0)
    {
        log_debug(string("<") +__func__+ "> input file with descriptor " + to_string(infd) + " is empty!" + get_name(), true);
        return 1;
    }
    Debug("fd_transmit: %d, infd: %d, in_stat.st_size: %d \n", fd_transmit, infd, in_stat.st_size);

    log_debug(string("<") +__func__+ "> transmitting file with desc " + to_string(infd) + " to " + get_name(), true);

    if (_is_encrypt) {
        lseek(infd, 0, SEEK_SET);
        FILE* fp = fdopen(infd, "r");
        char *data_row = NULL;
        size_t len = 0;
        ssize_t nread;
        //rewind(fp);

        fseek(fp, 0, SEEK_END);
        long file_size = ftell(fp);
        long file_unread = file_size;
        fseek(fp, 0, SEEK_SET);

        long playground_size = 16*1024;
        if (file_size<playground_size){
            playground_size=file_size+1;//append 0
        }

        char *playground = (char *)malloc(playground_size);
        long playground_focus = 0;
        long playground_start = 0;
        long playground_end = 0;
        long playground_delimeter = -1;
        long playground_next_start = 0;

        size_t insertSize = 0; // Must less than STREAM_MAX_SIZE 
        string insertStr;

        insertStr = "insert into " + _workspace + "." + get_name() + " values";

        long read_len, insert_start;
        long size_legacy_buf = 0;
        char *data_legacy = NULL;
        int line_no = 0;

        while (file_unread>0) {
            if (size_legacy_buf>0){
                if (my_print){
                    cout << "\n\n\nsize_legacy_buf:" << size_legacy_buf << "\n" ;
                }

                memcpy(playground, data_legacy, size_legacy_buf); //include ending 0
                insert_start = size_legacy_buf-1;
                //wind up
                size_legacy_buf=0;
                free(data_legacy);
            } else {
                insert_start=0;
            }

            read_len = fread( playground+insert_start , 1 , playground_size-insert_start-1 , fp );
            if (my_print){
                cout << "read_len:" << read_len << "\n" ;
            }

            playground[insert_start + read_len] = '\0';
            file_unread -= read_len;
            if (file_unread<=0){
                playground_size=insert_start + read_len +1; //include ending 0
            }
            if (read_len == 0){
                if (insert_start==0){
                    //finished
                    break;
                }
            }

            //data processing
            int count_double_quote;

            playground_start = 0;
            playground_end = 0;
            playground_delimeter = -1;

            while (playground_start<playground_size ){
                count_double_quote=0;
                for (playground_focus=playground_start; playground_focus<playground_size; playground_focus++) {

                    if (my_print){
                        char _char = playground[playground_focus];
                        if (_char == 0){
                            _char='`';
                        }
                        cout << "\t[" << playground_focus << "](" << _char << ")" ;
                    }

                    if (playground[playground_focus] == '\n' || (playground[playground_focus] == '\0' && file_unread <= 0)) {
                        if(count_double_quote % 2 == 0) {
                            playground_delimeter=playground_focus;
                            playground_end=playground_focus;
                            playground_next_start=playground_focus+1;

                            if (my_print){
                                cout << "\nplayground_start : " << playground_start << endl;
                                cout << "playground_end : " << playground_end << endl;
                                cout << "playground_size : " << playground_size << endl;
                            }

                            break;
                        } else {
                            continue;
                        }
                    }

                    count_double_quote += (playground[playground_focus] == '"');
                }

                //playground_focus has been added by 1 while ending for loop
                if (playground_focus==playground_size){ //run out of playground
                    if (playground_end == 0) {
                        if (file_unread>0){ //to be continued
                            if (my_print){
                                cout << "\n\nTBC\nplayground_start : " << playground_start << endl;
                                cout << "playground_focus : " << playground_focus << endl;
                            }
                            size_legacy_buf = playground_size-playground_start;// a postfixed 0
                            data_legacy = (char *)malloc(size_legacy_buf);
                            memcpy(data_legacy, &playground[playground_start], size_legacy_buf-1);
                            data_legacy[size_legacy_buf-1] = '\0';
                            //read file again
                            break;
                        }
                    }
                }

                //data row collected
                playground[playground_end] = '\0';
                data_row = &playground[playground_start];
                //wind up
                playground_start=playground_next_start;
                playground_end = 0;

                if (*data_row != 0) {
                    stringstream ss(data_row);
                    string enc_bunch;
                    /*
                    cout << endl << "data row " << ++line_no << " START : ============================================================================" << endl;
                    cout << data_row << endl;
                    cout << "data row " << line_no << " OVER : ============================================================================" << endl;
                    cout << endl;
                    */

                    // parse encrypt string
                    // [ [2, "int32", "bo"], [4, "string", "bo"]]
                    string const delims{ ",\n" };
                    size_t idx = 0, pos = 0;
                    string dataType, passwd;
                    string str(data_row);
                    size_t len_data_row = str.length();
                    char * ending_0 = data_row + len_data_row;

                    enc_bunch += "(";

                    char *_cell_data;
                    char *fresh_data = data_row;
                    char *next_d_quote;
                    char *next_delimeter;
                    char successive;
                    long len_cell_data;
                    int count_d_quote;
                    int skip_this_row=0;

                    for (int _cell_idx=0; fresh_data <= ending_0 ; _cell_idx++) {
                        if (*fresh_data == '"') { //find another double quote
                            count_d_quote=1;
                            next_d_quote = fresh_data;
                            while(1) {
                                next_d_quote = strchr(next_d_quote + 1, '"');
                                if (next_d_quote==NULL) {
                                    //ERROR formated CSV
                                    skip_this_row=1;
                                    Error("malformatted CSV, no matching double quote found!\n");
                                    Error("%s\n", data_row);
                                    break;
                                } else {
                                    count_d_quote++;
                                }

                                if(count_d_quote % 2 == 0) {
                                    successive=*(next_d_quote+1);
                                    //cout << "next_d_quote : " << next_d_quote-fresh_data << endl;
                                    //cout << "successive : " << successive << endl;

                                    if (!(successive==',' || successive=='\0') && (next_d_quote < ending_0)){
                                        continue;
                                    } else {
                                        break;
                                    }
                                }
                            }

                            if (next_d_quote > fresh_data) {
                                len_cell_data = next_d_quote - fresh_data; //include ending 0
                                _cell_data = (char *)malloc(len_cell_data);
                                memcpy(_cell_data, fresh_data+1, len_cell_data-1); //exclude double quotes
                                _cell_data[len_cell_data-1]='\0';

                                fresh_data = next_d_quote +1 +1;
                            } else {
                                //ERROR formated CSV
                                skip_this_row=1;
                                Error("malformatted CSV, no matching double quote found!\n");
                                Error("%s\n", data_row);
                                break;
                            }
                        } else if (*fresh_data == ',') {
                            _cell_data = (char *)malloc(1);
                            _cell_data[0]='\0';

                            fresh_data = fresh_data +1;
                        } else {
                            next_delimeter = strchr(fresh_data, ',');
                            if (next_delimeter == NULL) {
                                next_delimeter = strchr(fresh_data, '\0');
                            }
                            //cout << "next_delimeter:" << next_delimeter << endl;

                            if (next_delimeter > fresh_data) {
                                len_cell_data = next_delimeter - fresh_data +1; //include ending 0
                                _cell_data = (char *)malloc(len_cell_data);
                                memcpy(_cell_data, fresh_data, len_cell_data-1); //exclude double quotes
                                _cell_data[len_cell_data-1]='\0';

                                fresh_data = next_delimeter +1;
                            }
                        }

                        //cout << "cell " << _cell_idx << "\t" << "<" << _cell_data << ">" << endl;
                        
                        if (_cell_idx!=0)
                            enc_bunch += ",";

                        if (_cell_data[0]=='\0') { //empty string
                            enc_bunch += "\"\"";
                        } else {
                            char *ptr_start = _cell_data;

                            int _idx_cell_data, count_b_slashs=0;

                            if ( strstr(ptr_start,"\\") != NULL ) {
                                for(_idx_cell_data=0; ptr_start[_idx_cell_data]; _idx_cell_data++) {
                                    if(ptr_start[_idx_cell_data]=='\\') {
                                        count_b_slashs++;
                                    }
                                }
                            }
                            if (count_b_slashs > 0) {
                                char * ori_start, * copy_start;
                                size_t seg_len;

                                char * ptr_back_slash;
                                char * _cell_data_ori = _cell_data;
                                _cell_data =  (char *)malloc( strlen(_cell_data) + count_b_slashs + 1);

                                ori_start = _cell_data_ori;
                                copy_start = _cell_data;
                                while ( (ptr_back_slash=strstr(ori_start,"\\")) != NULL ) {
                                    seg_len=ptr_back_slash-ori_start+1; //including '\'
                                    memcpy(copy_start, ori_start, seg_len);
                                    copy_start[seg_len] = '\\';
                                    copy_start[seg_len+1] = '\0';
                                    ori_start+=seg_len;
                                    copy_start+=seg_len+1;

                                    //cout << "-----------------------------------------escape back slash:" << _cell_data << endl;
                                }

                                free(_cell_data_ori);                       
                            }

                            char *ptr_double_quote;
                            ptr_start = _cell_data;
                            while ( (ptr_double_quote=strstr(ptr_start,"\"\"")) != NULL ) {
                                *ptr_double_quote='\\';
                                ptr_start=ptr_double_quote+2;

                                //cout << "-----------------------------------------escape double quote:" << _cell_data << endl;
                            }

                            string str_cell_data(_cell_data);
                            map<int, pair<string, string>>::iterator it = _encryptMap.find(_cell_idx);
                            if (it == _encryptMap.end())
                                enc_bunch += "\"" + str_cell_data + "\"";
                            else {
                                string dataType = it->second.first;
                                string passwd = it->second.second;
                                if (dataType == "INT8" || dataType == "INT16" || dataType == "INT32" || dataType == "INT64" ||
                                    dataType == "FLOAT" || dataType == "DOUBLE" ) {
                                        enc_bunch += "aes_encrypt( cast(" + str_cell_data + " as " + dataType + "), \"" + passwd + "\")";
                                    } else 
                                        enc_bunch += "aes_encrypt(\"" + str_cell_data + "\", \"" + passwd + "\")";
                            }
                        }

                        free(_cell_data);
                    }

                    if (skip_this_row) {
                        continue;
                    }
                    enc_bunch += ")";

                    /*
                    cout << endl << "encryption bunch start: ............................................................................" << endl;
                    cout << enc_bunch << endl;
                    cout << "encryption bunch end: ............................................................................" << endl;
                    */


                    if ( (insertStr.length() + enc_bunch.length()) < STREAM_MAX_SIZE) {
                        if (insertStr.back() == ')')
                            insertStr += "," + enc_bunch;
                        else 
                            insertStr += enc_bunch;
                    } else {
                        int fdInsert = open_connection(_host, _port);
                        string result;

                        if (-1 == fdInsert)
                            Error("fdInsert failed\n");
                        else 
                        {                    
                            if ( bo_channel::exec_sql(fdInsert, "", insertStr, false, result) == false )
                                Error("exec_sql fail, result: %s, error: %s\n", result.c_str(), strerror(errno));

                            close(fdInsert);
                        }
                        insertStr = "insert into " + _workspace + "." + get_name() + " values" + enc_bunch;
                    }
                }
            }

            if (size_legacy_buf>0){
                //read file again
                continue;
            }        
        }


        // insert buffer string
        if (insertStr.back() == ')') {
            int fdInsert = open_connection(_host, _port);
            string result;

            if (-1 == fdInsert)
                Error("fdInsert failed\n");
            else 
            {                    
                if ( bo_channel::exec_sql(fdInsert, "", insertStr, false, result) == false )
                    Error("exec_sql fail, result: %s, error: %s\n", result, strerror(errno));

                close(fdInsert);
            }
        } 

        free(playground);
        return 1;

    } else {    //    if (_is_encrypt) {
        ssize_t send_len = 0;
        bool use_sendfile = 1;
        FILE* fp = fdopen( dup(infd), "r");

        //char csv_trans_header[1024]={0};
        char * csv_trans_header = new char[4 + _workspace.length() + channel::get_name().length() + 3]();

        sprintf(csv_trans_header, "csv""\x01""%s.%s\n", _workspace.c_str(), channel::get_name().c_str());

        ssize_t dontcare = ::write(fd_transmit, csv_trans_header, strlen(csv_trans_header));
        log_debugf("<%s> csv_trans_header [%s]", __func__, csv_trans_header);
        free(csv_trans_header);

        if (use_sendfile) {
            send_len = sendfile(fd_transmit, infd, &off_begin, in_stat.st_size); 
            log_debug(string("<") +__func__+ "> ......................(ACKED)" + to_string(send_len) + " bytes sent ", true);
            bytes_to_db+=send_len;

            char teminator_ch = 0;
            fseek(fp, in_stat.st_size-1, SEEK_SET);
            teminator_ch = fgetc(fp);
            fseek(fp, 0, SEEK_SET);

            bool show_file_content = true;
            if (show_file_content) {
                /*
                log_debugf("<%s> file content :", __func__);
                char ch;
                printf("[");
                while ((ch = fgetc(fp)) != EOF) {
                    if (ch=='\r') {
                        printf("\\r");
                    } else if (ch=='\n') {
                        printf("\\n%c", ch);
                    } else {
                        printf("%c", ch);
                    }
                }
                printf("]");
                */

                log_file_content(__func__, fp);

            }
            
            if (teminator_ch != '\n') {
                char term_char = '\n';
                ssize_t dontcare = ::write(fd_transmit, &term_char, 1);
                log_debug(string("<") +__func__+ "> lost newline appended!!", true);
            } else {
                log_debug(string("<") +__func__+ "> need not to patch newline for regular file.", true);
            }

            if (_fd_for_data_to_send > 2) { // log data sent to db
                //_fd_for_data_to_send = open("/tmp/streamer/db_transmit.csv", O_CREAT|O_RDWR|O_APPEND, 0777);
                FILE* to_fp = fdopen(dup(_fd_for_data_to_send), "w");
                fseek(to_fp, 0, SEEK_END);
                FILE* from_fp = fdopen(dup(infd), "r");            
                fseek(from_fp, 0, SEEK_SET);
                char last_ch;
                char ch;
                while ((ch = fgetc(from_fp)) != EOF) {
                    fputc(ch, to_fp);
                    last_ch = ch;
                }

                if (last_ch != '\n') {
                    fputc('\n', to_fp);
                }

                fclose(to_fp);
                fclose(from_fp);
            }
        } else {
            int buf_len=32;
            char buffer[buf_len];
            fseek(fp, offset, SEEK_SET);
            while (fgets(buffer, buf_len, fp) != NULL) {
                int str_len = strlen(buffer);
                ::write(fd_transmit, buffer, str_len);
                send_len+=str_len;
                if (buffer[str_len-1]!='\n'){
                    ::write(fd_transmit, "\n", 1);
                    send_len+=1;
                }
            }
            log_debug(string("<") +__func__+ "> close file", true);
        }

        bool write_syn_term_mark=false;
        if (write_syn_term_mark) {
            if (!use_new_connection) {
                // while using persistent connection, this will produce an empty record      
                if (SYN_MARK_LEN != ::write(fd_transmit, syn_term_mark, SYN_MARK_LEN)){
                    log_debugf("<%s> failed to write term mark...", __func__);
                }
            }
        }

        fclose(fp);

        // Debug for print transmit data
        // lseek(infd, 0, SEEK_SET);
        // std::string data;
        // data.resize( 1024 );
        // int len2 = read( infd, &data[0], data.size() );
        // Info("Send data to BO: %s", data.c_str());

        //if (send_len > 0)
        //    bo_counter::get_counter()->inc_val(BCOUNT_TRANS);

        if (use_new_connection) {
            //flush_table(true);

            // prevents tranimissions from parallel processing
            int watermark = get_steady_row_count(_table_watermark_verifications);
            log_debugf("<%s> before closing data channel, got table size : %d", __func__, watermark);

            close(new_fd);

            int table_increment = watermark-last_table_size;
            if (table_increment!=last_table_increment) {
                log_debugf("<%s> Warning : %d new records added this time, it's %d for the last time.(%d bytes sent)", __func__, table_increment, last_table_increment, bytes_to_db);
            }
            last_table_size = watermark;
            last_table_increment = table_increment;
        }
        
        return send_len;
    }
}

//deprecated @ 2023-01-02 
//may reset data table
bool bo_channel::get_total_count_by_pkt(size_t &total_count)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    // bool success = false;

    // if (-1 == _fd)
    //     return false;

    // uint64_t bt_size = 0;
    
    // int ret = -1;
    // ret = ::write(_fd, syn_bt_size_mark, sizeof(syn_bt_size_mark));
    // if(ret >= 0)
    //     ret = read(_fd, &bt_size, sizeof(bt_size));
    
    // if(ret >= 0)
    // {
    //     total_count = bt_size;
    //     success = true;
    // }
    // else
    //     Error("read or write failed: %s", strerror(errno));
    
    // return success;

    if (-1 == _ctrl_channel)
        return false;

    uint64_t bt_size = 0;
    
    int ret = -1;
    bool success = false;
    while(true){
        ret = ::write(_ctrl_channel, syn_bt_size_mark, sizeof(syn_bt_size_mark));
        //Debug("write syn_bt_size_mark, ret %d\n", ret);
        log_debug(string("<") +__func__+ "> write syn_bt_size_mark, ret " + to_string(ret), true);
        if(ret <= 0) {  
            if(errno==EINTR)
            {  
                Error("ERROR: EINTR, write again\n");  
                continue;              
            }  
            else if(errno==EAGAIN) // EAGAIN : Resource temporarily unavailable
            {  
                Error("ERROR: EAGAIN,sleep 3 seconds and write again\n");  
                sleep(3);
                continue;  
            }  
            else
            {  
                Error("ERROR: errno = %d, strerror = %s \n", errno, strerror(errno));  
                return false;  
            }  
        } else {
            success = true;
            break;
        }
    }
    if (!success) {
        return false;
    }

    success = false;
    while(true){
        ret = read(_ctrl_channel, &bt_size, sizeof(bt_size));
        //Debug("read bt_size, ret %d\n", ret);
        log_debug(string("<") +__func__+ "> read bt_size, ret " + to_string(ret), true);
        if(ret <= 0) {  
            if(errno==EINTR)
            {  
                Error("ERROR: EINTR, read again\n");  
                continue;              
            }  
            else if(errno==EAGAIN) // EAGAIN : Resource temporarily unavailable  
            {  
                Error("ERROR: EAGAIN,sleep 3 seconds and read again\n");  
                sleep(3);
                continue;  
            }  
            else
            {  
                Error("ERROR: errno = %d, strerror = %s \n", errno, strerror(errno));  
                return false;  
            }  
        } else {
            total_count = bt_size;
            success = true;
            log_debug(string("<") +__func__+ "> got total_count : " + to_string(total_count), true);
            break;
        }
    }
    if (!success) {
        return false;
    }

    return success;
}

bool bo_channel::get_total_count_by_sql(size_t &total_count){
    bool success = false;

    int fd_count = open_connection(_host, _port);
    string result;

    if (-1 == fd_count){
        Error("Unable to open connection");
        return false;
    }

    ostringstream sql;
    sql << "SELECT count(*) FROM `" << channel::get_name() << "`";
    success = exec_sql(fd_count, _workspace, sql.str(), true, result);

    if(success)
    {
        if (result.empty()){
            //Write_System_ErrorLog(string(__PRETTY_FUNCTION__)  + ": WARNING!! exec_sql result is empty : " + sql.str() +", RESET to 0; " + strerror(errno), true);
            log_error(string("<") +__func__+ "> WARNING!! exec_sql result is empty : " + sql.str() +", RESET to 0; " + strerror(errno), true);
            result.assign("0");  
        }
        std::stringstream string_to_size_t(result); 
        string_to_size_t >> total_count;
    }
    else
        Error("exec_sql fail, result: %s; error: %s", result, strerror(errno));

    close(fd_count);
    return success;
}

bool bo_channel::flush_table(bool use_persist_channel){
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    bool success = false;
    int fd_flush;
    string result;

    if (!use_persist_channel) {
        fd_flush = open_connection(_host, _port);

        if (-1 == fd_flush){
            Error("Unable to open connection");
            return false;
        }
    } else {
        fd_flush = _ctrl_channel;
    }

    ostringstream sql;
    sql << "flush table " << channel::get_name();
    success = exec_sql(fd_flush, _workspace, sql.str(), false, result);

    if(!success)
        Error("failed to flush table, result: %s; error: %s", result, strerror(errno));

    if (!use_persist_channel) {
        close(fd_flush);
    }
    return success;
}

bool bo_channel::pre_transmit(int fd, string table, string type, ssize_t shrink)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    if (-1 == fd)
        return false;

    Debug("prepare data stream header, table '%s', type '%s', shrink %ld (fd %d)\n",
        table.c_str(), type.c_str(), shrink, fd);
    if (type.compare("csv") == 0) {
        return bo_channel::send_csv_header(fd, table, shrink);
    }
    return true;
}

bool bo_channel::send_csv_header(int fd, string table, ssize_t shrink)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    log_debug(string("<") +__func__+ "> shrink table size to : " + to_string(shrink), true);

    // $$ send header
    const uint8_t csv_header[4] = {'c', 's', 'v', 1}; 
    if (4 != ::send(fd, csv_header, 4, 0)) {
        Error("send csv tag fail (%s)\n", strerror(errno));
        return false;
    }

    if (table.length() != ::send(fd, table.c_str(), table.length(), 0)) {
        Error("send csv table name fail (%s)\n", strerror(errno));
        return false;
    }

    char csv_param[64] = {0};
    ssize_t param_len = 1; // first \x0 for end of table name

    csv_param[0] = '\0';
    param_len += snprintf(csv_param + 1, sizeof(csv_param) - 1, "shrink_db=%ld\n", shrink);

    Debug("send csv data %s, length, %d\n", csv_param, param_len);
    // Debug("data:");
    // for(int i=0; i< param_len; i++){
    //     Debug("%c, %d\n", csv_param[i], csv_param[i]);
    // }
    // Debug("data End\n");

    if (param_len != ::send(fd, csv_param, param_len, 0)) {
        Error("%s: send csv parameter fail (%s)\n", __PRETTY_FUNCTION__, strerror(errno));
        return false;
    }

    if (SYN_MARK_LEN != ::write(fd, syn_term_mark, SYN_MARK_LEN))
        return false;
    return true;
}

bool bo_channel::exec_sql(int fd, string ws, string sql, bool csv_out, string& result)
{
    //  $$ {'i', 'n', 't', 1} + ws.table + {size + cmd + flag + flag_ex}} + sql command + SYN 'T'

    // send internal command
    // $$ {'i', 'n', 't', 1}
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    log_debug(string("<") +__func__+ "> sql stmt : " + sql, true);

    if (sizeof(int_tag) != ::write(fd, int_tag, sizeof(int_tag))) 
    {
        Write_System_ErrorLog(string(__func__)  + ", send internal command error", true);
        return false;
    }
    // send dummy table name
    string tbl_name =  ws + ".";
    if ((tbl_name.length() + 1) != ::write(fd, tbl_name.c_str(), tbl_name.length() + 1))
    {
        Write_System_ErrorLog(string(__func__)  + ", send dummy table name error", true);
        return false;
    }
    // send pull command
    bo_int_header_t int_header = {0};
    int_header.size = sizeof(bo_int_header_t) + sql.length() + 1;
    
    int_header.cmd = BO_INT_CMD_QUERY; 
    if (csv_out)
        int_header.flags_ex = BO_INT_FLAG_EX_CSV;
    else // only support csv output, otherwise no output
        int_header.flags_ex = BO_INT_FLAG_EX_NO_RET;


    // $$ For ad-hoc QUERY command, query string is followed after 8 bytes internal stream header. 
    // $$ {size + cmd + flag + flag_ex}
    if (sizeof(int_header) != ::write(fd, &int_header, sizeof(int_header)))
    {
        Write_System_ErrorLog(string(__func__)  + ", send internal header error", true);
        return false;
    }
    // send sql command

    ssize_t wl = ::write(fd, sql.c_str(), sql.length() + 1);
    char *sqlPtr = &sql[0];

    if (wl != (sql.length() + 1))
    {
        Error("Send sql command error: %s, error: %s\n", sql.c_str(), strerror(errno));
        return false;
    }

    // ssize_t wsize = 0, ret = 0, _write_cur = sql.length()+1 ;
    // while (wsize < _write_cur) {
    //     if ((ret = write(fd, sqlPtr + wsize, _write_cur - wsize)) <= 0) {
    //         Write_System_ErrorLog(string(__PRETTY_FUNCTION__)  + ", send sql command error: " + sql, true);
    //         Error("wl %d, sql.length()+1 %d\n", ret, sql.length()+1);
    //         Error("error: %s\n", strerror(errno));
    //         return false;
    //     }
    //     wsize += ret;
    // }
        
    // write end tag to avoid blocking
    if (SYN_MARK_LEN != ::write(fd, syn_term_mark, SYN_MARK_LEN))
    {
        Write_System_ErrorLog(string(__func__)  + ", write end tag error", true);
        return false;
    }
    //Debug("%s: %s\n", __PRETTY_FUNCTION__, sql.c_str());
    log_debug(string("<") +__func__+ "> rendered sql : " + sql, true);

    // wait for job done
    uint8_t dummy[1024] = {0};
    ssize_t r_size = 0;
    bool start_read = true;
    ostringstream out;
    while(0 < (r_size = ::read(fd, dummy, sizeof(dummy) - 1))) {
        dummy[r_size] = 0;
        // check error
        if (start_read && r_size > (sizeof(int_tag) + sizeof(bo_int_header_t) + sizeof(int64_t)) &&
            0 == memcmp(dummy, int_tag, sizeof(int_tag))) {
            size_t dummy_tbl_len = strlen((const char*)&dummy[sizeof(int_tag)]);
            bo_int_header_t *int_hdr = (bo_int_header_t*)&dummy[sizeof(int_tag) + dummy_tbl_len + 1];
            if (int_hdr->cmd == BO_INT_CMD_REPORT) {
                int64_t* errcode = (int64_t*)((int8_t*)int_hdr + sizeof(bo_int_header_t));
                char* errmsg = (char*)((char*)errcode + sizeof(int64_t));
                Error("%s, ws: '%s', sql: '%s', errcode: %ld, errmsg: '%s'\n",
                    __PRETTY_FUNCTION__, ws.c_str(), sql.c_str(), *errcode, errmsg);
                result = to_string(*errcode) + " " + errmsg;
                int64_t err_no = *errcode;
                if ((err_no == -6) || (err_no == -16)) {
                    return true; // -6 BOEEXIST / -16 invalid name
                } else {
                    Write_System_ErrorLog(string(__PRETTY_FUNCTION__)  + ", ws:" + ws + ", sql: " + sql + ", errcode: " + to_string(*errcode) + ", errmsg: " + string(errmsg), true);
                    Error("4\n");
                    return false;
                }
            }
        }
        start_read = false;

        if (csv_out) {
            out << dummy;
            log_debug(string("<") +__func__+ "> buffer : " + out.str(), true);
        }
        Debug("dummy section:%s\n", dummy);
    }
    log_debug(string("<") +__func__+ "> concluded buffer : " + out.str(), true);

    if (r_size == -1) {
        Error("Read from FD failed, cmd: %s, error: %s\n", sql.c_str(), strerror(errno));
        return false;
    }

    if (csv_out) {
        result = out.str();
        log_debug(string("<") +__func__+ "> result to return : " + result, true);

        // trim last '\n'
        if (!result.empty() && result.at(result.length() - 1) == '\n')
            result = result.substr(0, result.length() - 1);
    }
    return true;
}

int bo_channel::get_steady_row_count(int verifications) {
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    int steady=0;
    int threshold = verifications;
    int total_count;
    bool success;

    size_t total_count_0, total_count_1, total_count_steady;
    //size_t total_count_by_sql = -1;
    //success = get_total_count_by_sql(_workspace, cp.get_table_name(), total_count_by_sql);
    for (int i=0; i<1024; i++){
        success = get_total_count_by_sql(total_count_0);

        if(success)
            break;
    }

    if (success) {
        for (int i=0; i<1024; i++){
            success = get_total_count_by_sql(total_count_1);

            if(success) {
                log_debug(string("<") +__func__+ "> got table row count : " + to_string(total_count_1), true);
                if (total_count_1 == total_count_0) {
                    if (steady==0) {
                        steady=1;
                        total_count_steady = total_count_1;
                        continue;
                    } else if (steady>=1) {
                        steady+=1;
                        if (steady==threshold-1)
                            break;
                    }
                } else {
                    steady=0;
                    total_count_0 = total_count_1;
                }
            }
        }
    }
    
    if(steady==threshold-1)
        total_count = total_count_steady;
    else 
        total_count = -1;
    
    return total_count;
}

//////////////////////////////////////////////////
// bo_transmitter
//////////////////////////////////////////////////
bo_transmitter::~bo_transmitter()
{
    if (_fd_cp != -1) {
        close(_fd_cp);
        _fd_cp = -1;
    }
}

bool bo_transmitter::initialize()
{
    log_debugf(" ");
    log_debugf(" ");
    log_debugf("------->   <%s>", __PRETTY_FUNCTION__);

    if (!get_param_value(KEY_TRAN_BO_HOST, _host)) {
        Error("%s: missing conf key '%s'\n", __PRETTY_FUNCTION__, KEY_TRAN_BO_HOST);
        return false;
    }
    string port;
    if (!get_param_value(KEY_TRAN_BO_PORT, port)) {
        Error("%s: missing conf key '%s'\n", __PRETTY_FUNCTION__, KEY_TRAN_BO_PORT);
        return false;
    }
    _port = (unsigned short)atoi(port.c_str());

    string bkport;
    if (get_param_value(KEY_TRAN_BO_BKPORT, bkport))
        _bk_port = (unsigned short)atoi(bkport.c_str());

    if (!get_param_value(KEY_TRAN_BO_TYPE, _type)) {
        Error("%s: missing conf key '%s'\n", __PRETTY_FUNCTION__, KEY_TRAN_BO_TYPE);
        return false;
    }
    
    reset_workspace(false);

    // create all channels' table as well as data connection
    if (_pre_create) {
        if (!prepare_channels(true))
            return false;
    }

    return true;
}

int bo_transmitter::open_connection(uint64_t read_timeout, sa_svc_type svc_type, string file_name, int line, string function_name, string other_desc)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    int fd = -1;
    bool is_replica = (svc_type == SVC_REPLICA);
    const char* peer = is_replica ? _replica.host.c_str() : _host.c_str();
    struct hostent *host;
    struct sockaddr_in addr;

    // if ((host = gethostbyname(peer)) == NULL)
    //     return -1;

    if (-1 == (fd = socket(PF_INET, SOCK_STREAM, 0)))
        return -1;

    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(is_replica ? _replica.port : (svc_type == SVC_BACKUP ? _bk_port : _port));

    log_debug(string("<") +__func__+ "> _port : " + to_string(_port), true);

    if ((addr.sin_addr = hostnameToIPv4(peer)).s_addr == -1)
        return -1;

    // memcpy(&addr.sin_addr, host->h_addr_list[0], host->h_length);
    // addr.sin_addr.s_addr = *(long*)(host->h_addr);

    /* svc_type
        SVC_BO,
        SVC_REPLICA,
        SVC_BACKUP
    */

    while (::connect(fd, (struct sockaddr*)&addr, sizeof(addr)) != 0) {
        Error("Connect fail: %s\n", strerror(errno));
        Error("Try to reconnect (%s:%d) in 5 seconds\n", peer, _port);
        sleep(5);
    }

    if (read_timeout > 0) { 
        struct timeval timeout = {.tv_sec = (time_t)read_timeout};
        if (-1 == setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (const void*)&timeout, sizeof(timeout))) {

            close(fd);

            Error("set socket receive timeout fail: %s\n", strerror(errno));
            return -1;
        }

        if (is_replica && _replica.no_sync) {
            int flags = fcntl(fd, F_GETFL, 0);
            if (flags == -1) {
                Error("%s, fcntl F_GETFL fail: %s\n", __PRETTY_FUNCTION__, strerror(errno));
                return fd;
            }

            flags |= O_NONBLOCK;
            if (fcntl(fd, F_SETFL, flags) == -1) {
                Error("%s, set non-blocking fail: %s\n", __PRETTY_FUNCTION__, strerror(errno));
            }
        }
    }

    return fd;
}

int bo_transmitter::simple_connection(const string& peer, unsigned short port)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    int fd = -1;
    struct hostent *host;
    struct sockaddr_in addr;

    // if ((host = gethostbyname(peer.c_str())) == NULL)
    //     return -1;

    if (-1 == (fd = socket(PF_INET, SOCK_STREAM, 0)))
        return -1;

    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    // addr.sin_addr.s_addr = *(long*)(host->h_addr);

    if ((addr.sin_addr = hostnameToIPv4(peer.c_str())).s_addr == -1)
        return -1;

    if (::connect(fd, (struct sockaddr*)&addr, sizeof(addr)) != 0) {
        close(fd);
        Error("Connect fail: %s\n", strerror(errno));
        return -1;
    }

    return fd;
}

bool bo_transmitter::set_params(string& group, map<string, string>& param)
{
    if (!component::set_params(group, param))
        return false;

    string pre_create;
    if (get_param_value(KEY_TRAN_BO_PRECREATE, pre_create))
        _pre_create = pre_create.compare("1") == 0;
    string create_cp;
    if (get_param_value(KEY_TRAN_BO_CREATECP, create_cp))
        _create_cp = create_cp.compare("1") == 0;
    if (!get_param_value(KEY_TRAN_BO_CPNAME, _cp_name))
        return false;

    string cp_retention;
    if (get_param_value(KEY_TRAN_BO_CPRETENTION, cp_retention)) {
        _cp_retention = atoi(cp_retention.c_str());
    }

    string allow_cleanup;
    if (get_param_value(KEY_TRAN_BO_ALLOW_CLEANUP, allow_cleanup))
        _allow_cleanup_all = allow_cleanup.compare("1") == 0;

    // replica has to be set before add channels
    string rep_num;
    if (get_param_value(KEY_TRAN_BO_REPLICA, rep_num))
        _replica_num = atoi(rep_num.c_str());
    if (_replica_num) {
        if (!get_param_value(string(KEY_TRAN_BO_REPLICA) + "." + KEY_TRAN_BO_HOST, _replica.host)) {
            Error("%s: missing conf key '%s.%s'\n", __PRETTY_FUNCTION__, KEY_TRAN_BO_REPLICA, KEY_TRAN_BO_HOST);
            return false;
        }
        string rep_port;
        if (!get_param_value(string(KEY_TRAN_BO_REPLICA) + "." + KEY_TRAN_BO_PORT, rep_port)) {
            Error("%s: missing conf key '%s.%s'\n", __PRETTY_FUNCTION__, KEY_TRAN_BO_REPLICA, KEY_TRAN_BO_PORT);
            return false;
        }
        _replica.port = (unsigned short)atoi(rep_port.c_str());

        string rep_nosync;
        if (!get_param_value(string(KEY_TRAN_BO_REPLICA) + "." + KEY_TRAN_BO_NO_SYNC, rep_nosync)) {
            Error("%s: missing conf key '%s.%s'\n", __PRETTY_FUNCTION__, KEY_TRAN_BO_REPLICA, KEY_TRAN_BO_NO_SYNC);
            return false;
        }
        _replica.no_sync = rep_nosync.compare("1") == 0;
    }

    string commit_action;
    if (get_param_value(KEY_TRAN_BO_COMMIT_ACTION, commit_action))
        _commit_act = commit_action.compare("1") == 0;
    if (_commit_act) {
        if (!get_param_value(string(KEY_TRAN_BO_COMMIT_ACTION) + "." + KEY_TRAN_BO_SOURCE, _cmact.source)) {
            Error("%s: missing conf key '%s.%s'\n", __PRETTY_FUNCTION__, KEY_TRAN_BO_COMMIT_ACTION, KEY_TRAN_BO_SOURCE);
            return false;
        }
        if (!get_param_value(string(KEY_TRAN_BO_COMMIT_ACTION) + "." + KEY_TRAN_BO_HOST, _cmact.host)) {
            Error("%s: missing conf key '%s.%s'\n", __PRETTY_FUNCTION__, KEY_TRAN_BO_COMMIT_ACTION, KEY_TRAN_BO_HOST);
            return false;
        }
        string cmact_port;
        if (!get_param_value(string(KEY_TRAN_BO_COMMIT_ACTION) + "." + KEY_TRAN_BO_PORT, cmact_port)) {
            Error("%s: missing conf key '%s.%s'\n", __PRETTY_FUNCTION__, KEY_TRAN_BO_COMMIT_ACTION, KEY_TRAN_BO_PORT);
            return false;
        }
        _cmact.port = (unsigned short)atoi(cmact_port.c_str());
    }

    string no_suspend;
    if (get_param_value(KEY_TRAN_BO_NO_SUSPEND, no_suspend))
        _no_suspend = no_suspend.compare("1") == 0;

    //KEY_TRAN_BO_PRE_COMMIT_VERIFICATION      "pre_commit_verifications"
    _table_watermark_verifications = 3;
    string pre_commit_verifications;
    if (get_param_value(KEY_TRAN_BO_PRE_COMMIT_VERIFICATION, pre_commit_verifications)) {
        _table_watermark_verifications = atoi(pre_commit_verifications.c_str());
    }
    if (_table_watermark_verifications < 3)
        _table_watermark_verifications = 3;

    _least_xmit_interval = 0;
    string least_xmit_interval;
    if (get_param_value(KEY_TRAN_BO_LEAST_XMIT_INTERVAL, least_xmit_interval)) {
        _least_xmit_interval = atoi(least_xmit_interval.c_str());
    }
    if (_least_xmit_interval < 0)
        _least_xmit_interval = 0;

    get_param_value(KEY_TRAN_BO_FIX_WS, _fix_ws);

    /*
        int _num_fixed_tables;
        string _ext_default_sql;
        string _cols_in_ext_def_prefix;
    */
    _num_fixed_tables = 0;
    string num_fixed_tables;
    if (get_param_value(KEY_TRAN_BO_NUM_FIXED_TABLES, num_fixed_tables)) {
        _num_fixed_tables = atoi(num_fixed_tables.c_str());
    }
    if (_num_fixed_tables < 0)
        _num_fixed_tables = 0;

    string sql_template;
    _ext_default_sql = string("");
    if (get_param_value(KEY_TRAN_BO_COLS_IN_EXT_DEFAULT, sql_template)) {
        _ext_default_sql = sql_template;
    }

    _cols_in_ext_def_prefix = string(KEY_TRAN_BO_COLS_IN_EXT_PREFIX);

    int need_to_log_msg;
    string str_log_msg;
    if (get_param_value(KEY_TRAN_BO_LOG_DATA_TO_DB, str_log_msg))
        need_to_log_msg = stoi(str_log_msg);
    if (need_to_log_msg==1){
        _fd_for_data_to_send = open("/tmp/streamer/data_transmit_to_db.csv", O_CREAT|O_RDWR|O_APPEND|O_TRUNC, 0777);
    } else {
        _fd_for_data_to_send = -1;
    }

    if (!transmitter::set_params(group, param))
        return false;

    return initialize();
}

string decrypt(string cipher) {
    string recovered = "";
    //Define the key and iv
    string keyStr, ivStr;
    keyStr = "eiknjdfplswvbfds";
    ivStr = "asd123qwengdrjmd";

    byte key[AES::DEFAULT_KEYLENGTH];
    byte iv[AES::BLOCKSIZE];

    for (int i=0; i<keyStr.length(); i++) {
        key[i] = keyStr[i];
        iv[i] = ivStr[i];
    }

    //Decryption
    try
    {
        OFB_Mode< AES >::Decryption d;
        d.SetKeyWithIV(key, sizeof(key), iv);
        // The StreamTransformationFilter removes
        //  padding as required.
        string decodeCipher = DecodeHex(cipher);
        StringSource s(decodeCipher, true, new StreamTransformationFilter(d,new StringSink(recovered))); // StringSource
    }
    catch(const CryptoPP::Exception& e)
    {
        Error("Decrypt %s failed, %s\n", cipher, e.what());
    }
    return recovered;
}

bool bo_transmitter::add_chan(string chan_name, size_t size, bool allow_cleanup)
{
    channel* chan = get_chan(chan_name, false);
    if (chan)
        return false;

    string is_replica_only;
    ostringstream ro_key;
    ro_key << chan_name << "." << KEY_TRAN_BO_REPLICA_ONLY;
    get_param_value(ro_key.str(), is_replica_only);

    string sql;
    string encryptStr;
    map<int, pair<string, string>> encryptMap;

    if (is_replica_only.compare("1")) {
        //if (_pre_create) {
            ostringstream sql_key;
            sql_key << chan_name << "." << KEY_TRAN_BO_SQL;
            if (!get_param_value(sql_key.str(), sql)) {
                Error("%s: missing conf key '%s'\n", __PRETTY_FUNCTION__, sql_key.str().c_str());
                return false;
            }
        //}

        ostringstream encrypt_key;
        encrypt_key << chan_name << "." << KEY_TRAN_BO_ENCRYPT;
        if (get_param_value(encrypt_key.str(), encryptStr)) {
            // parse encrypt string
            // [ [2, "int32", "bo"], [4, "string", "bo"]]
            string const delims{ " [],\"" };
            size_t idx, pos = 0;
            int cnt, colIdx;
            string dataType, passwd, encryptColumns;

            for (cnt=0; (idx = encryptStr.find_first_not_of(delims, pos)) != string::npos; cnt++)
            {
                pos = encryptStr.find_first_of(delims, idx + 1);
                string part = encryptStr.substr(idx, pos - idx);
                switch(cnt%3) {
                    case 0:
                        colIdx = stoi(part);
                        if (cnt >= 3) encryptColumns += ", ";
                        encryptColumns += part;
                        break;
                    case 1:
                        for (int i = 0; i < part.length(); i++){ 
                            part[i] = toupper(part[i]);    
                        }
                        dataType = part;
                        break;
                    case 2:
                        passwd = decrypt(part);
                        encryptMap[colIdx] = pair<string, string>(dataType, passwd);
                        break;
                }
            } 
            Info("Table %s will encrypt column index with (%s)\n", chan_name.c_str(), encryptColumns.c_str());
        }

        string is_dim;
        ostringstream dim_key;
        dim_key << chan_name << "." << KEY_TRAN_BO_DIM_TBL;
        get_param_value(dim_key.str(), is_dim);

        _channels.insert(pair<string, channel*>(chan_name, new bo_channel(chan_name, size, sql, is_dim.compare("1") ? false : true, allow_cleanup, !(encryptMap.empty()), encryptMap)));
        Debug("Added channel: %s (size %lu)\n", chan_name.c_str(), size);
    }

    if (_replica_num) {
        ostringstream rep_sql_key;
        rep_sql_key << chan_name << "." << KEY_TRAN_BO_REPLICA << "." << KEY_TRAN_BO_SQL;
        if (!get_param_value(rep_sql_key.str(), sql)) {
            Error("%s: no replica for '%s'\n", __PRETTY_FUNCTION__, rep_sql_key.str().c_str());
            // return false;
            return true;
        }
        bo_channel* chan_rep = new bo_channel(chan_name, size, sql, true, allow_cleanup);
        chan_rep->set_replica(true);
        if (!_replica.no_sync)
            chan_rep->set_sync_replica(true);
        _channels.insert(pair<string, channel*>(chan_name + "._replica_", chan_rep));
        Debug("Added channel: %s._replica_ (size %lu)\n", chan_name.c_str(), size);
    }

    return true;
}

int bo_transmitter::get_num_conn(string table, string ws)
{
    int fd = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "SELECT ws, total FROM ");
    if (-1 == fd)
        return -1;

    bool ret;
    string result;
    ostringstream sql;
    if (ws != "") {
        ws = ws + ".";
    } else {
        ws = DIRECTOR_WORKSPACE + string(".");
    }
    sql << "SELECT COUNT(SUBSTR(INFO, 16)) FROM (SHOW PROCESSLIST) WHERE INFO = \"streaming CSV: " << ws << table << "\"";
    ret = bo_channel::exec_sql(fd, "", sql.str(), true, result);
    close(fd);

    // if (result.empty()) { // The table may not have data in the doc.
    //     return false
    // }
    if (ret) {
        if (result.empty()){
            Write_System_ErrorLog(string(__PRETTY_FUNCTION__)  + ": WARNING!! exec_sql result is empty:" + sql.str() +", RESET to 0; " + strerror(errno), true);
            result.assign("0");  
        }
        try {
            Debug("number of connection: %s\n", result.c_str());
            return stoi(result);
        } catch(exception &err) {
            return -1;
        }
    } else {
        return -1;
    }
}

bool bo_transmitter::get_pre_cp_count(string tbl_name, size_t &count)
{
    return get_pre_cp_count(_cp_name, _workspace, tbl_name, count);
}

bool bo_transmitter::get_pre_cp_count(string cp_name, string ws_name, string tbl_name, size_t &count)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    bool success = false;

    int fd_count = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "");
    string result;

    if (-1 == fd_count)
        Error("Unable to open connection");
    else 
    {
        ostringstream sql;
        sql << "select total from " << cp_name ;
        sql << " where ws = \"" << ws_name << "\" and table = \"" << tbl_name << "\"";
        sql << " order by mstime desc limit 1";
        // checkpoint database is defined in DIRECTOR_WORKSPACE
        success = bo_channel::exec_sql(fd_count, DIRECTOR_WORKSPACE, sql.str(), true, result);

        if(success)
        {
            if (result.empty()){
                //Write_System_ErrorLog(string(__PRETTY_FUNCTION__)  + ": WARNING!! exec_sql result is empty : " + sql.str() +", RESET to 0; " + strerror(errno), true);
                log_error(string("<") +__PRETTY_FUNCTION__+ "> WARNING!! exec_sql result is empty : " + sql.str() +", RESET to 0; " + strerror(errno), true);
                result.assign("0");  
            }
            std::stringstream string_to_size_t(result); 
            string_to_size_t >> count;
        }
        else
            Error("exec_sql fail, result: %s; error: %s", result, strerror(errno));

        close(fd_count);
    }

    return success;
}

bool bo_transmitter::get_total_count_by_sql(string workspace, string table_name, size_t &total_count)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    bool success = false;

    int fd_count = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "SELECT count(*) FROM " + table_name);
    string result;

    if (-1 == fd_count)
        Write_System_ErrorLog("fd_total_count failed", true);
    else 
    {
        //just for debugging
        //flush_cmd(SVC_BO);
        //flush_bo(SVC_BO);
        ostringstream sql;
        sql << "SELECT count(*) FROM `" << table_name << "`";
        //sql << "FLUSH TABLE " << table_name << ";SELECT count(*) FROM " << table_name;
        success = bo_channel::exec_sql(fd_count, workspace, sql.str(), true, result);
        if(success)
        {
            if (result.empty()){
                Write_System_ErrorLog(string(__PRETTY_FUNCTION__)  + ": WARNING!! exec_sql result is empty : " + sql.str() +", RESET to 0; " + strerror(errno), true);
                result.assign("0");  
            } 
            std::stringstream string_to_size_t(result); 
            string_to_size_t >> total_count;
        }
        else {
            Write_System_ErrorLog(string(__PRETTY_FUNCTION__)  + ": exec_sql fail, result: " + result + "; " + strerror(errno), true);
        }

        close(fd_count);
    }

    return success;
}

size_t bo_transmitter::get_steady_row_count(string table_name) {
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    int steady=0, threshold=_table_watermark_verifications;
    size_t total_count;
    bool success;

    size_t total_count_0, total_count_1, total_count_steady;
    //size_t total_count_by_sql = -1;
    //success = get_total_count_by_sql(_workspace, cp.get_table_name(), total_count_by_sql);
    for (int i=0; i<1024; i++){
        success = get_total_count_by_sql(_workspace, table_name, total_count_0);

        if(success)
            break;
    }

    if (success) {
        for (int i=0; i<1024; i++){
            success = get_total_count_by_sql(_workspace, table_name, total_count_1);

            if(success) {
                log_debug(string("<") +__func__+ "> got table row count : " + to_string(total_count_1), true);
                if (total_count_1 == total_count_0) {
                    if (steady==0) {
                        steady=1;
                        total_count_steady = total_count_1;
                        continue;
                    } else if (steady>=1) {
                        steady+=1;
                        if (steady==threshold-1)
                            break;
                    }
                } else {
                    steady=0;
                    total_count_0 = total_count_1;
                }
            }
        }
    }
    
    if(steady==threshold-1)
        total_count = total_count_steady;
    else 
        total_count = -1;
    
    return total_count;
}

bool bo_transmitter::set_checkpoint(commitment& cp, bool verify_total)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    size_t total_count = 0;
//  
    if (total_count == 0) 
    {
        // pre toal count
        size_t total_from_cp_table = -1; 
        bool success;

        // //int steady=0, threshold=_table_watermark_verifications;

        success = get_pre_cp_count(_cp_name, _workspace, cp.get_table_name(), total_from_cp_table);
        
        if(!success) {
            Error("get_pre_cp_count: %s\n", strerror(errno));
            return false;
        }
        //Info("pre total_count: %d\n", total_from_cp_table);
        log_info(string("<") +__func__+ "> pre total_count from cp table : " + to_string(total_from_cp_table), true);

        total_count = get_steady_row_count(cp.get_table_name());

        if (total_count<0){
            Error("Unable to select count from table %s.", cp.get_table_name().c_str());
            return false;
        }
        Write_System_ErrorLog(to_string(success) + " original total_count: " + to_string(total_from_cp_table) + ", work around " + _workspace + "." + cp.get_table_name() + ": " + to_string(total_count), true);

        // total_from_cp_table == -1 mean checkpoint table is empty
        if (total_count < total_from_cp_table && total_from_cp_table !=-1) {
            Error("table count(%ld) is less than last time(%ld)\n", total_count, total_from_cp_table);
            return false;
        }
    }

    if (verify_total) {
        size_t total_expected = cp.get_total_expected();
        log_debugf("<%s> total expected/asked : [%d][%d]", __func__, total_expected, total_count);
        if (total_count != total_expected) {
            log_debugf("<%s> total count mismatch", __func__);
            return false;
        }
    }

    size_t waterlevel = cp.get_subtotal();
    if (waterlevel==SIZE_MAX) { // subtotal not set
        waterlevel=total_count;
        total_count=0;
    }
    // Use insert into instead CSV mode

    ostringstream sql;
    char time_buf[64] = {0};
    struct timeval time_stamp = cp.get_time_stamp();

    sql << "INSERT INTO " << _cp_name << " VALUES ( ";
    sql << "\"" << _group << "\",\"" << _workspace << "\",\"" << cp.get_table_name() << "\",";
    sql << "\"" << cp.get_doc_id() << "\",\"" << total_count << "\",\"" << waterlevel << "\",";
    sql << "\"" << time_stamp.tv_sec * 1000 + time_stamp.tv_usec/1000 << "\",";
    strftime(time_buf, sizeof(time_buf) - 1, "%F %T", localtime(&time_stamp.tv_sec));
    sql << "\"" << time_buf << "\")";

    _fd_cp = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), sql.str());
    

    if (-1 == _fd_cp) {
        Error("create connection _fd_cp failed\n");
        return false;
    } else {
        string result;
        bool ret;
        ret = bo_channel::exec_sql(_fd_cp, DIRECTOR_WORKSPACE, sql.str(), false, result); 
        close(_fd_cp);
        if(!ret) {
            Error("exec_sql fail, result: %s, error: %s\n" ,result ,strerror(errno));
            return false;
        }        
    }

    //Debug("Set checkpoint: %s\n", sql.str().c_str());
    log_debug(string("<-------   <") +__PRETTY_FUNCTION__+ ">", true);
    return true;
}

bool bo_transmitter::get_last_commitment(string& doc_id)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    // use another connection to fetch last row
    int fd_cp = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "SELECT doc FROM (select doc, count(*) as cp_cnt, mstime from " + _cp_name);
    if (-1 == fd_cp) {
        Write_System_ErrorLog(string(__PRETTY_FUNCTION__) + " failed because " + strerror(errno) + ", fd_cp: " + to_string(fd_cp), true);
        Debug("%s: open connection fail because of '%s'\n", __PRETTY_FUNCTION__, strerror(errno));
        return false;
    }

    ostringstream sql;
    // BO doesn't support HAVING clause
    //sql << "SELECT doc, count(*) FROM " << _cp_name << " GROUP BY doc HAVING count(*) = " << _channels.size() << "ORDER BY mstime DESC limit 1";

    //// It's not necessary all tables have data in the last doc. Thus check cp_cnt == table_cnt is not necessary.
    //sql << "SELECT doc FROM " << _cp_name << " WHERE syn_group = '" << _group << "' ORDER BY mstime DESC LIMIT 1";

    // Each doc has to write commitment for all tables even no data change in some tables.
    // For strictly mode, it's necessary to check cp_cnt == table_cnt
    sql << "SELECT doc FROM (select doc, count(*) as cp_cnt, mstime from ";
    sql << _cp_name << " WHERE syn_group = '" << _group << "' group by doc) WHERE cp_cnt = " << get_main_channel_size() << " ORDER BY mstime DESC LIMIT 1";
    bool ret = bo_channel::exec_sql(fd_cp, DIRECTOR_WORKSPACE, sql.str(), true, doc_id);

    close(fd_cp);

    if(!ret)
    {
        Write_System_ErrorLog(string(__PRETTY_FUNCTION__) + " failed, because of " + strerror(errno) + ", sql: '" + sql.str() + "'", true);
        return false;
    }
    if (doc_id.empty()){
        Write_System_ErrorLog(string(__PRETTY_FUNCTION__)  + ": WARNING!! exec_sql result is empty : " + sql.str() +"; " + strerror(errno), true);
        //possibly not error
        //ret = false;
    } 
    Debug("bo_transmitter::%s: last commitment is '%s'\n", __PRETTY_FUNCTION__, doc_id.c_str());

    return true;
}

bool bo_transmitter::get_checkpoint(string table, string doc_id, string& ws, ssize_t& count)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    int fd_cp = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "SELECT ws, total FROM " + _cp_name);
    if (-1 == fd_cp)
        return false;

    bool ret;
    string result;
    ostringstream sql;
    sql << "SELECT ws, total FROM " << _cp_name << " WHERE syn_group='" << _group << "' AND table='" << table << "' AND doc='" << doc_id << "'";
    ret = bo_channel::exec_sql(fd_cp, DIRECTOR_WORKSPACE, sql.str(), true, result);

    close(fd_cp);

    if(!ret)
    {
        Write_System_ErrorLog(string(__PRETTY_FUNCTION__) + " failed, because of " + strerror(errno) + ", sql: '" + sql.str() + "'", true);
        return false;
    }

    if (result.empty()) { // The table may not have data in the doc.
        ostringstream sql_last;

        sql_last << "SELECT ws, total FROM " << _cp_name << " WHERE syn_group='" << _group << "' AND table='" << table << "' ORDER BY mstime DESC LIMIT 1";

        fd_cp = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "SELECT ws, total FROM " + _cp_name);
        if (-1 == fd_cp)
        {
            Write_System_ErrorLog(string(__PRETTY_FUNCTION__)  + ": " + " failed, because of " + strerror(errno) + ", fd_cp: "+ to_string(fd_cp), true);
            return false;
        }

        ret = bo_channel::exec_sql(fd_cp, DIRECTOR_WORKSPACE, sql_last.str(), true, result);

        close(fd_cp);

        if(!ret)
        {
            Write_System_ErrorLog(string(__PRETTY_FUNCTION__) + " failed, because of " + strerror(errno) + ", sql: '" + sql_last.str() + "'", true);
            return false;
        }
        if (result.empty()){
            Write_System_ErrorLog(string(__PRETTY_FUNCTION__)  + ": WARNING!! exec_sql result is empty : " + sql.str() +"; " + strerror(errno), true);
        } 
    }
    
    count = 0;
    ws = _workspace;
    cout << "get checkpoint result: " << result << endl;
    if (!result.empty()) {
        string::size_type pos = result.find_first_of(',');
        if (pos != string::npos) { 
            ws = result.substr(0, pos);
            count = stoll(result.substr(pos+1));

            if(_workspace.compare(ws) != 0) // $$ not equal
            {
                Write_System_ErrorLog("workspace: " + _workspace + " and " + ws +" is not equal", true);
            }
        }
    } 
    else {
        cout<<"result is empty"<<endl;
    }
 
    size_t sql_total_count;
    bool success;
    string success_str;
    success = get_total_count_by_sql(ws, table, sql_total_count);
    if(success)
        success_str = "true";
    else
        success_str = "false";
    
    
    int count_difference = int(sql_total_count) - int(count);

    if(count_difference < 0) 
    {
        Write_System_ErrorLog("health check " + success_str + ": sql_total_count < check_count:" + to_string(sql_total_count) + " < " + to_string(count), true);
        return false;
    }
    
    string batch_size;
    if (get_param_value("batch_size", batch_size))
        if(count_difference > stoi(batch_size)) 
        {
            Write_System_ErrorLog("health check " + success_str + ": sql_total_count - check_count:" + to_string(sql_total_count) + " - " + to_string(count) + " is greater than batch_size " + batch_size, true);
            return false;
        }

    Debug("%s: check point for '%s' with doc '%s' and total count %ld in workspace '%s'\n",
        __PRETTY_FUNCTION__, table.c_str(), doc_id.c_str(), count, ws.c_str());

    return true;
}

bool bo_transmitter::backoff(string& doc_id)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    int fd_chan = -1;
    ssize_t shrink = -1;

    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it) {
        bo_channel* bo_chan = (bo_channel*)it->second;
        string ws;

        if (bo_chan->is_replica() && _replica.no_sync)
            continue;

        // 1. stop all channels' connection
        bo_chan->close_chan(true);

        if (!doc_id.empty()) {
            // 2. get total count of the check point
            if (!get_checkpoint(bo_chan->get_name(), doc_id, ws, shrink))
                return false;            
        }
        else {
            shrink = 0; // no committed doc, shrink to 0;
            ws = _workspace;
        }

        if (shrink == 0 && !_allow_cleanup_all) {
            if (!bo_chan->is_allow_cleanup()) {
                Error("Clean up table '%s' is not allowed, _allow_cleanup is false. \n", bo_chan->get_name().c_str());
                return false;
            }
        }

        // // 3.1 Check preview connection is closed
        // while (!check_preConn(bo_chan->get_name(), ws))
        //     sleep(3); 
        

        // 3.2 re-connect and shrink for each channel
        while(true) {            
            ostringstream tbl_name;

            if (-1 == (fd_chan = open_connection(TRAN_BO_READ_TIMEOUT, bo_chan->is_replica() ? SVC_REPLICA : SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), bo_chan->get_name() + "'s fd")))
                return false;
            bo_chan->set_ctrl_channel(fd_chan);

            bo_chan->set_bo_info(_host, _port, _workspace, _table_watermark_verifications, _least_xmit_interval);
            if (!bo_chan->is_dim_tbl())
                tbl_name << ws << ".";
            tbl_name << bo_chan->get_name();
        
            bo_channel::pre_transmit(fd_chan, tbl_name.str(), _type, shrink); 
            
            size_t total_count = 0;
            if (flush_bo() && get_total_count_by_sql(ws,  bo_chan->get_name(), total_count) && total_count == shrink) {
                Error("bo_transmitter::%s: back off '%s' to doc '%s' at %ld\n",
                __PRETTY_FUNCTION__, tbl_name.str().c_str(), doc_id.c_str(), shrink);
                break;            
            }
           
            Error("table %s.%s total count %ld not equal to shrink %ld\n", ws.c_str(),  bo_chan->get_name().c_str(), total_count, shrink);
            sleep(3);
        }

        // if backoff doc is not current workspace, clean up all data in the current workspace
        if (!bo_chan->is_dim_tbl() && _workspace.compare(ws)) {
            ostringstream tbl_name;

            if (-1 == (fd_chan = open_connection(TRAN_BO_READ_TIMEOUT, bo_chan->is_replica() ? SVC_REPLICA : SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "")))
                return false;
            bo_chan->set_ctrl_channel(fd_chan);

            /*
            if (!_single_shot_conn) {
                if (-1 == (fd_chan = open_connection(TRAN_BO_READ_TIMEOUT, bo_chan->is_replica() ? SVC_REPLICA : SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "")))
                    return false;
                bo_chan->set_data_channel(fd_chan);
            }
            */

            bo_chan->set_bo_info(_host, _port, _workspace, _table_watermark_verifications, _least_xmit_interval);
            tbl_name << _workspace << "." << bo_chan->get_name();
            bo_channel::pre_transmit(fd_chan, tbl_name.str(), _type, 0);
            Error("bo_transmitter::%s: back off '%s' to doc '%s' at 0\n",
                __PRETTY_FUNCTION__, tbl_name.str().c_str(), doc_id.c_str());
        }
    }

    // 4. delete newer check point
    bool ret = false;
    string dummy;
    ostringstream delete_stmt;
    delete_stmt << "DELETE FROM " << _cp_name << " WHERE syn_group = '" << _group << "'";
    if (!doc_id.empty()) {
        ostringstream get_timestamp;
        string doc_time;
        if (-1 == (fd_chan = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "SELECT mstime FROM " + _cp_name)))
            return false;
        get_timestamp << "SELECT mstime FROM " << _cp_name << " WHERE syn_group = '" << _group;
        get_timestamp  << "' AND doc = '" << doc_id << "' ORDER BY mstime DESC LIMIT 1";
        ret = bo_channel::exec_sql(fd_chan, DIRECTOR_WORKSPACE, get_timestamp.str(), true, doc_time);

        close(fd_chan);

        if (!ret)
            return false;

        if (doc_time.empty()){
            Write_System_ErrorLog(string(__PRETTY_FUNCTION__)  + ": WARNING!! exec_sql result is empty:" + get_timestamp.str() + "; " + strerror(errno), true);
        }     
        delete_stmt << " AND mstime > " << doc_time << "";
    }
    
    if (-1 == (fd_chan = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "DELETE FROM " + _cp_name)))
        return false;
    ret = bo_channel::exec_sql(fd_chan, DIRECTOR_WORKSPACE, delete_stmt.str(), false, dummy);

    close(fd_chan);
    return ret;
}

bool bo_transmitter::set_bo_logging(int level)
{
    int retry = 5;
    int try_after = 1;
    bool ret = false;
    string dummy;
    while (!ret && --retry >= 0) {
        int fd = open_connection(0, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "CREATE WORKSPACE " + _workspace);
        if (-1 == fd) {
            Error("%s, open connection for setting logging on '%s' fail (%s), retry after %d sec (remains %d times retry.)\n",
                __PRETTY_FUNCTION__, _workspace.c_str(), strerror(errno), try_after, retry);
            sleep(try_after);
            continue;
        }

        ret = bo_channel::exec_sql(fd, _workspace, "set logging " + to_string(level), false, dummy);

        close(fd);
        if (!ret) {
            Error("%s, create workspace '%s' fail (%d), retry after %d sec (remains %d times retry.)\n",
                __PRETTY_FUNCTION__, _workspace.c_str(), errno, try_after, retry);
            sleep(try_after);
        }
    }
    return ret;
}

bool bo_transmitter::toggle_spp_mode(bool mode)
{
    int retry = 5;
    int try_after = 1;
    bool ret = false;
    string dummy;
    while (!ret && --retry >= 0) {
        int fd = open_connection(0, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "set spp");
        if (-1 == fd) {
            Error("%s, open connection for setting spp on '%s' fail (%s), retry after %d sec (remains %d times retry.)\n",
                __PRETTY_FUNCTION__, _workspace.c_str(), strerror(errno), try_after, retry);
            sleep(try_after);
            continue;
        }

        string mode_string;
        if (mode) {
            mode_string = string("true");
        } else {
            mode_string = string("false");
        }

        ret = bo_channel::exec_sql(fd, _workspace, "set spp " + mode_string, false, dummy);

        close(fd);
        if (!ret) {
            Error("%s, create workspace '%s' fail (%d), retry after %d sec (remains %d times retry.)\n",
                __PRETTY_FUNCTION__, _workspace.c_str(), errno, try_after, retry);
            sleep(try_after);
        }
    }
    return ret;
}

bool bo_transmitter::create_workspace(bool is_director)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    int retry = 5;
    int try_after = 1;
    bool ret = false;
    string dummy;
    string new_workspace;
    if (is_director) {
        new_workspace = DIRECTOR_WORKSPACE;
    } else {
        new_workspace = _workspace;
    }
    while (!ret && --retry >= 0) {
        int fd = open_connection(0, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "CREATE WORKSPACE " + _workspace);
        if (-1 == fd) {
            Error("%s, open connection for workspace '%s' fail (%s), retry after %d sec (remains %d times retry.)\n",
                __PRETTY_FUNCTION__, new_workspace.c_str(), strerror(errno), try_after, retry);
            sleep(try_after);
            continue;
        }

        ret = bo_channel::exec_sql(fd, new_workspace, "CREATE WORKSPACE " + new_workspace, false, dummy);

        close(fd);
        if (!ret) {
            Error("%s, create workspace '%s' fail (%d), retry after %d sec (remains %d times retry.)\n",
                __PRETTY_FUNCTION__, new_workspace.c_str(), errno, try_after, retry);
            sleep(try_after);
        }
    }
    return ret;
}

void bo_transmitter::reset_workspace(bool change_bo) 
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    // backup previous workspace
    if (_workspace.length() > 0 && _bk_port > 0) 
    {
        int fd_bk = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BACKUP, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "");
        if (fd_bk > 0) 
        {
            ostringstream bk_cmd;
            bk_cmd << "{\"cmd\":1,\"wait\":0,\"args\":[\"" << get_name() << "\",\"" << _workspace << "\"";
            if (change_bo)
                bk_cmd << ",\"1\"";
            bk_cmd << "]}";
            const string& bk_cmd_str = bk_cmd.str();
            if (write(fd_bk, bk_cmd_str.c_str(), bk_cmd_str.length()) <= 0)
                Error("bo_transmitter::%s, send backup command fail: %s\n", __PRETTY_FUNCTION__, strerror(errno));

            close(fd_bk);
        }
        else
            Error("bo_transmitter::%s, connect to backup service fail: %s\n", __PRETTY_FUNCTION__, strerror(errno));
    }
    if (change_bo) {
        // close check_point connection
        if (-1 != _fd_cp)
        {
            close(_fd_cp);
        }
        // send change bo request
        if (_bk_port > 0) {
            int fd_bk = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BACKUP, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "");
            if (fd_bk > 0) 
            {
                const char* change_bo_cmd = "{\"cmd\":2,\"wait\":1}";
                if (write(fd_bk, change_bo_cmd, strlen(change_bo_cmd)) <= 0)
                    Error("bo_transmitter::%s, send change bo command fail: %s\n", __PRETTY_FUNCTION__, strerror(errno));
                else {
                    char rsp_buf[1024] = {0};
                    ssize_t rsp_len = 0;
                    while ((rsp_len = read(fd_bk, rsp_buf, sizeof(rsp_buf) - 1)) < 0 && EAGAIN == errno)
                        ;
                    if (rsp_len < 0)
                        Error("bo_transmitter::%s, read bo change response fail: %s\n", __PRETTY_FUNCTION__, strerror(errno));
                }
                close(fd_bk);
            }
            else
                Error("bo_transmitter::%s, connect to backup service fail: %s\n", __PRETTY_FUNCTION__, strerror(errno));
        }
    }

    if (_fix_ws.empty()) {
        time_t now = time(&now);
        char time_buf[16] = {0};
        strftime(time_buf, sizeof(time_buf) - 1, "%Y%m%d", gmtime(&now));
        _workspace = string("WS") + time_buf;
    }
    else {
        _workspace = _fix_ws;
    }
    Debug("%s, current work space: %s\n", __PRETTY_FUNCTION__, _workspace.c_str());
}

bool bo_transmitter::prepare_channels(bool create_cp)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    //set_bo_logging(7);
    //toggle_spp_mode(false);
    // create work space first

    if (!create_workspace(true))
        return false;
    if (_workspace.compare(DIRECTOR_WORKSPACE)!=0){
        if (!create_workspace(false))
            return false;
    }

    int fd_chan = -1;
    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it) 
    {
        bo_channel* bo_chan = (bo_channel*)it->second;
        if (bo_chan->is_replica()) 
        {
            bo_chan->set_ctrl_channel(open_connection(0, SVC_REPLICA, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "")); 
            /*
            if (!_single_shot_conn) {
                bo_chan->set_data_channel(open_connection(0, SVC_REPLICA, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "")); 
            }
            */
            bo_chan->set_bo_info(_host, _port, _workspace, _table_watermark_verifications, _least_xmit_interval);
            if (!bo_chan->create_table(""))
                return false;
        }
        else 
        {
            int retry = 5;
            int try_after = 1;
            bool ret = false;
            while (!ret && --retry >= 0) 
            {
                bo_chan->set_ctrl_channel(open_connection(0, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), ""));
                bo_chan->set_bo_info(_host, _port, _workspace, _table_watermark_verifications, _least_xmit_interval); 
                if (!(ret = bo_chan->create_table(_workspace))) 
                {
                    bo_chan->close_chan(true);
                    Error("%s, create_table '%s.%s' fail (%d), retry after %d sec (remains %d times retry.)\n",
                        __PRETTY_FUNCTION__, _workspace.c_str(), bo_chan->get_name().c_str(), errno, try_after, retry);
                    sleep(try_after);
                }
                
            }
            if (!ret)
                return false;
        }

        // prepare connection
        /*
        if (-1 == (fd_chan = open_connection(TRAN_BO_READ_TIMEOUT, bo_chan->is_replica() ? SVC_REPLICA : SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "")))
            return false;

        ostringstream tbl_name;
        if (!bo_chan->is_dim_tbl())
            tbl_name << _workspace << ".";
        tbl_name << bo_chan->get_name();
    
        if (!bo_channel::pre_transmit(fd_chan, tbl_name.str(), _type, -1)) {
            close(fd_chan);
            return false;
        }
        bo_chan->set_ctrl_channel(fd_chan);

        if (!_single_shot_conn){
            if (-1 == (fd_chan = open_connection(TRAN_BO_READ_TIMEOUT, bo_chan->is_replica() ? SVC_REPLICA : SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "")))
                return false;
            bo_chan->set_data_channel(fd_chan);
        }
        */

        /*
        if (!_single_shot_conn) {
            int data_chan = open_connection(0, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "");
            if ( data_chan == -1 ){
                Error("Failed to create data channel");
                return false;
            } else {
                log_debugf("<%s> data channel created, id = %d", __func__, data_chan);    
            }

            bool sniff = 0;
            if (sniff) {
                ostringstream tbl_name;
                if (!bo_chan->is_dim_tbl())
                    tbl_name << _workspace << ".";
                tbl_name << bo_chan->get_name();

                if (!bo_channel::pre_transmit(data_chan, tbl_name.str(), _type, -1)) {
                    close(data_chan);
                    return false;
                }
            }

            bo_chan->set_data_channel(data_chan);
        }

        bo_chan->set_bo_info(_host, _port, _workspace, _table_watermark_verifications);
        */

        // It weird, use another connection to clean preconnection
        // Use new connection to get_total_count_by_sql
        /*
        size_t total_rows_in_table = 0;
        get_total_count_by_sql(_workspace, bo_chan->get_name(), total_rows_in_table);
        Debug("total_rows_in_table: %d\n",total_rows_in_table);
        */
    }

    if (create_cp) {
        // create check point table
        if (_create_cp) 
        {
            string dummy;
            ostringstream sql;
            sql << "CREATE DEFAULT TABLE " << _cp_name;
            sql << " (";
            sql << " 'syn_group' VARSTRING(4096),";
            sql << " 'ws' VARSTRING(4096),";
            sql << " 'table' VARSTRING(4096),";
            sql << " 'doc' VARSTRING(786432),";
            sql << " 'progress' INT64,";
            sql << " 'total' INT64,";
            sql << " 'mstime' INT64,";
            sql << " 'timestamp' DATETIME64";
            sql << ")";

            int retry = 5;
            int try_after = 1;
            bool ret = false;
            while (!ret && --retry >= 0) {
                _fd_cp = open_connection(0, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "CREATE DEFAULT TABLE " + _cp_name); // 94

                
                if (-1 == _fd_cp) {
                    Error("%s, open connection for check point fail (%s), retry after %d sec (remains %d times retry.)\n",
                        __PRETTY_FUNCTION__, strerror(errno), try_after, retry);
                    sleep(try_after);
                    continue;
                }
                ret = bo_channel::exec_sql(_fd_cp, DIRECTOR_WORKSPACE, sql.str(), false, dummy);
                close(_fd_cp);
                _fd_cp = -1;
                if (!ret) {
                    Error("%s, create table for check point fail (%d), retry after %d sec (remains %d times retry.)\n",
                        __PRETTY_FUNCTION__, errno, try_after, retry);
                    sleep(try_after);
                }
            }
            if (!ret)
                return false;
            Debug("%s: Created table '%s' for checkpoint\n", __PRETTY_FUNCTION__, _cp_name.c_str());
        }
    }

    return true;
}

channel * bo_transmitter::add_ext_channel(string channel_name)
{
    //log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    log_debugf("------->   <%s> channel_name [%s]", __PRETTY_FUNCTION__, channel_name.c_str());
    
    string sql_template = _ext_default_sql;

    size_t found = channel_name.find_last_of('_');
    if (found != string::npos) {
        string character_str = channel_name.substr(found);
        string key_for_def = _cols_in_ext_def_prefix + character_str;
        string cols_def;
        if (get_param_value(key_for_def, cols_def)) {
            sql_template = cols_def;
            log_debugf("<%s> character def[%s][%s]", __func__, key_for_def.c_str(), sql_template.c_str());
        }
    }

    string def_start ("(");
    int idx_def_start = sql_template.find(def_start);
    if (idx_def_start == std::string::npos) {
        return NULL;
    }
    
    string sql = string("CREATE TABLE `") + channel_name + string("` ") + sql_template.substr(idx_def_start);

    bo_channel * new_ext_channel = new bo_channel(channel_name, 0, sql, false, _allow_cleanup_all);

    int retry = 5;
    int try_after = 1;
    bool ret = false;

    string fault_table ("bad_tables_collection");
    int create_fault_table=0;
    while (!ret && --retry >= 0) 
    {
        new_ext_channel->set_ctrl_channel(open_connection(0, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), ""));
        new_ext_channel->set_bo_info(_host, _port, _workspace, _table_watermark_verifications, _least_xmit_interval);
        string result;
        if (!(ret = new_ext_channel->create_table(_workspace, result))) 
        {
            log_debugf("<%s> create ext table failed : [%s]", __func__, result.c_str());
            new_ext_channel->close_chan(true);

            string err_template ("Syntax error");
            int idx_syn_err = result.find(err_template);
            if (idx_syn_err != std::string::npos) { //bad table name
                map<string, channel*>::const_iterator it = _faults.find(fault_table);
                if (it == _faults.end()){
                    delete new_ext_channel;
                    sql = string("CREATE TABLE `") + fault_table + string("` ") + sql_template.substr(idx_def_start);
                    new_ext_channel = new bo_channel(fault_table, 0, sql, false, _allow_cleanup_all);
                    create_fault_table = 1;
                    continue;
                } else {                   
                    _faults.insert(pair<string, channel*>(channel_name, it->second));
                    return it->second;
                }
            } else {
                Error("%s, create_table '%s.%s' fail (%d), retry after %d sec (remains %d times retry.)\n",
                    __PRETTY_FUNCTION__, _workspace.c_str(), new_ext_channel->get_name().c_str(), errno, try_after, retry);
                sleep(try_after);
            }
        }

        if (create_fault_table == 1){
            _faults.insert(pair<string, channel*>(fault_table, new_ext_channel));
            _faults.insert(pair<string, channel*>(channel_name, new_ext_channel));

            return (channel *)new_ext_channel;
        }
        
    }
    if (!ret)
        return NULL;
    else {
        _channels.insert(pair<string, channel*>(channel_name, new_ext_channel));
    }

    return (channel *)new_ext_channel;
}

bool bo_transmitter::flush_bo()
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    if (!flush_bo(SVC_BO))
        return false;
    if (_replica_num > 0 && !_replica.no_sync)
        return flush_bo(SVC_REPLICA);
    return true;
}


bool bo_transmitter::flush_bo(sa_svc_type svc_type)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    
    char time_buf[64] = {0};
    time_t now = time(NULL);
    strftime(time_buf, sizeof(time_buf) - 1, "%F %T", localtime(&now));
    Info("%s, %s, start at %s\n", __func__, svc_type == SVC_BO ? "main" : "replica", time_buf);

    if (!_no_suspend) {
        flush_cmd(svc_type); // $$ SVC_BO or SVC_REPLICA
    }

    now = time(NULL);
    strftime(time_buf, sizeof(time_buf) - 1, "%F %T", localtime(&now));
    Info("%s, %s, finished at %s\n", __func__, svc_type == SVC_BO ? "main" : "replica", time_buf);

    if (svc_type == SVC_BO && _commit_act)
        send_commit_action();

    return true;
}

bool bo_transmitter::flush_cmd(sa_svc_type svc_type)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    string dummy;
    bool success = true;
    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it) 
    {
        string table_name = (string)it->first;

        int flush_table_fd = open_connection(0, svc_type, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "flush table " + table_name);
        string table_sql = "flush table " + table_name; //"select * from " + table_name;//"flush table " + table_name;
        if(!bo_channel::exec_sql(flush_table_fd, _workspace, table_sql, false, dummy))
        {
            success = false;
            Write_System_ErrorLog(string(__PRETTY_FUNCTION__)   + ": " + table_sql + "; failed, fd: ("+ to_string(flush_table_fd)+ "): " + strerror(errno), true);
        }
        close(flush_table_fd);
    }

    string cp_sql = "flush table `" + _cp_name + "`";
    int flush_checkpoint_fd = open_connection(0, svc_type, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "flush table " + _cp_name);
    if(!bo_channel::exec_sql(flush_checkpoint_fd, DIRECTOR_WORKSPACE, cp_sql, false, dummy))
    {
        success = false;
        Write_System_ErrorLog(string(__PRETTY_FUNCTION__)  + ": " + cp_sql + "; failed, fd: ("+ to_string(flush_checkpoint_fd)+ "): " + strerror(errno), true);
    }
    close(flush_checkpoint_fd);
    return success;
}

bool bo_transmitter::commit(string& doc_id)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    log_debug(string("<") +__func__+ "> doc_id for comitting : " + doc_id, true);

    if (!flush_bo()) {
        Error("%s, Failed to flush db before making commit!\n", __PRETTY_FUNCTION__);
    }
    if (!transmitter::commit(doc_id) || !flush_bo())
        return false;

    return true;
}

bool bo_transmitter::flush(string& chan_name, int infd)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    return flush(chan_name, infd, 0);
}

bool bo_transmitter::flush(string& chan_name, int infd, int offset)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    /* just for debugging
    size_t total_count_by_sql = 0;
    bool success;
    success = get_total_count_by_sql(_workspace, chan_name, total_count_by_sql);
    log_debug(string("total_count_by_sql BEFORE transmitting : ") + to_string(total_count_by_sql), true);
    */

    bool ret = true;
    //if (!_no_flushing) {
        ret = transmitter::flush(chan_name, infd, offset);
        if (_replica_num) {
            string rep_chan = chan_name + "._replica_";
            transmitter::flush(rep_chan, infd, offset);
        }
    //}

    /* just for debugging
    for (int i=0; i<4;) {
        //just for debugging
        success = get_total_count_by_sql(_workspace, chan_name, total_count_by_sql);
        log_debug(string("total_count_by_sql AFTER transmitting : ") + to_string(total_count_by_sql), true);

        cout << "---------------------------------------------sleeping 1 secs...(" << ++i << ")" << endl << endl;
        sleep(1);
    }
    */
    return ret;
}

bool bo_transmitter::reset_chan(channel* chan)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    bo_channel* bo_chan = (bo_channel*)chan;

    // Write_System_ErrorLog(string(__PRETTY_FUNCTION__) + ": reconnect because of " + strerror(errno) + ", close original connection and start a new one", true);
    Debug("reconnect because of %s(%d), close original connection and start a new one\n", strerror(errno), errno);

    Error("%s, close original connection and start a new one.\n", __PRETTY_FUNCTION__);
    bo_chan->close_chan(true);

        // 3.1 Check preview connection is closed
    while (get_num_conn(bo_chan->get_name(), _workspace)!=0)
        sleep(3); 

    int fd_chan = open_connection(TRAN_BO_READ_TIMEOUT, ((bo_channel*)chan)->is_replica() ? SVC_REPLICA : SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), bo_chan->get_name() + "'s fd");
    if (-1 == fd_chan)
        return false;

    Debug("fd_chan %d\n",fd_chan);

    ostringstream tbl_name;
    if (!bo_chan->is_dim_tbl())
        tbl_name << _workspace << ".";
    tbl_name << bo_chan->get_name();
    if (!bo_channel::pre_transmit(fd_chan, tbl_name.str(), _type, -1)) {
        close(fd_chan);
        return false;
    }

    /*
    * http://192.168.1.17:10083/issues/1622
    * if without  get_total_count_by_sql function
    * it will sent one empty row to table
    * it weired!!!!!!
    */
    size_t total_rows_in_table = 0;
    bool success;
    success = get_total_count_by_sql(_workspace, tbl_name.str(), total_rows_in_table);
    Debug("total_rows_in_table: %d\n",total_rows_in_table);

    bo_chan->set_ctrl_channel(fd_chan);
    
    /*
    if (!_single_shot_conn){
        fd_chan = open_connection(TRAN_BO_READ_TIMEOUT, ((bo_channel*)chan)->is_replica() ? SVC_REPLICA : SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), bo_chan->get_name() + "'s fd");
        if (-1 == fd_chan)
            return false;
        bo_chan->set_data_channel(fd_chan);
    }
    */

    bo_chan->set_bo_info(_host, _port, _workspace, _table_watermark_verifications, _least_xmit_interval);
    return true;
}

void bo_transmitter::close_chan(channel* chan)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    bo_channel* bo_chan = (bo_channel*)chan;
    bo_chan->close_chan(true);
}



void bo_transmitter::send_commit_action()
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    char http_req[1024] = {0};
    snprintf(http_req, sizeof(http_req) - 1,
        "POST /prediction/los?src=%s HTTP/1.1\r\nHost: %s\r\nContent-length: 0\r\n\r\n",
        _cmact.source.c_str(), PRIME_HTTP_DOMAIN);

    int fd = simple_connection(_cmact.host, _cmact.port);
    if (-1 == fd)
        return;

    ssize_t req_len = strlen(http_req);
    ssize_t send_len = 0, send_ret = 0;
    while (0 < (send_ret = write(fd, &http_req[send_len], req_len - send_len))) {
        send_len += send_ret;
        if (send_len >= req_len)
            break;
    }
    if (send_ret <= 0) {
        Error("%s, send commit action request fail: %s\n",  __func__, strerror(errno));
    }

    close(fd);
}

bool bo_transmitter::create_fact_table_registry()
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    // create fact_table_registry
    if (1) 
    {
        string dummy;
        ostringstream sql;
        string str_nnd = " not null default ";
        sql << "CREATE DEFAULT TABLE fact_table_registry";
        sql << " (";
        sql << " 'registry' VARCHAR(786432)" << str_nnd << "\"\",";
        //sql << " 'mstime' BIGINT" << str_nnd << "0,";
        sql << " 'timestamp' DATETIME" << str_nnd << "now()";
        sql << ")";

        int retry = 5;
        int try_after = 1;
        bool ret = false;
        while (!ret && --retry >= 0) {
            _fd_cp = open_connection(0, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "CREATE DEFAULT TABLE " + _session_table_name);

            
            if (-1 == _fd_cp) {
                Error("%s, open connection for creating session table fail (%s), retry after %d sec (remains %d times retry.)\n",
                    __PRETTY_FUNCTION__, strerror(errno), try_after, retry);
                sleep(try_after);
                continue;
            }
            ret = bo_channel::exec_sql(_fd_cp, DIRECTOR_WORKSPACE, sql.str(), false, dummy);
            close(_fd_cp);
            _fd_cp = -1;
            if (!ret) {
                Error("%s, create session table fail (%d), retry after %d sec (remains %d times retry.)\n",
                    __PRETTY_FUNCTION__, errno, try_after, retry);
                sleep(try_after);
            }
        }
        if (!ret)
            return false;
        Debug("%s: Created table '%s' for sessions\n", __PRETTY_FUNCTION__, _cp_name.c_str());
    }
    return true;
}


bool bo_transmitter::set_session_monitoring(bool how)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    _session_monitoring = how;

    // create session table
    if (_session_monitoring) 
    {
        string dummy;
        ostringstream sql;
        string str_nnd = " not null default ";
        sql << "CREATE DEFAULT TABLE " << _session_table_name;
        sql << " (";
        sql << " 'status' SMALLINT" << str_nnd << "0,";
        sql << " 'mstime' BIGINT" << str_nnd << "0,";
        sql << " 'timestamp' DATETIME" << str_nnd << "now()";
        sql << ")";

        int retry = 5;
        int try_after = 1;
        bool ret = false;
        while (!ret && --retry >= 0) {
            _fd_cp = open_connection(0, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), "CREATE DEFAULT TABLE " + _session_table_name);

            
            if (-1 == _fd_cp) {
                Error("%s, open connection for creating session table fail (%s), retry after %d sec (remains %d times retry.)\n",
                    __PRETTY_FUNCTION__, strerror(errno), try_after, retry);
                sleep(try_after);
                continue;
            }
            ret = bo_channel::exec_sql(_fd_cp, DIRECTOR_WORKSPACE, sql.str(), false, dummy);
            close(_fd_cp);
            _fd_cp = -1;
            if (!ret) {
                Error("%s, create session table fail (%d), retry after %d sec (remains %d times retry.)\n",
                    __PRETTY_FUNCTION__, errno, try_after, retry);
                sleep(try_after);
            }
        }
        if (!ret)
            return false;
        Debug("%s: Created table '%s' for sessions\n", __PRETTY_FUNCTION__, _cp_name.c_str());
    }
    return true;
}

bool bo_transmitter::set_session_status(int status)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    if (!_session_monitoring) {
        return true;
    }

    ostringstream sql;
    char time_buf[64] = {0};

    struct timeval cur_time = {0};
    gettimeofday(&cur_time, NULL);


    sql << "INSERT INTO " << _session_table_name << " VALUES ( ";
    sql << "\"" << status << "\",";
    sql << "\"" << cur_time.tv_sec * 1000 + cur_time.tv_usec/1000 << "\",";
    strftime(time_buf, sizeof(time_buf) - 1, "%F %T", localtime(&cur_time.tv_sec));
    sql << "\"" << time_buf << "\")";

    int _fd_session_table = open_connection(TRAN_BO_READ_TIMEOUT, SVC_BO, string(__FILE__), __LINE__, string(__PRETTY_FUNCTION__), sql.str());
    
    if (-1 == _fd_session_table) {
        Error("create connection _fd_session_table failed\n");
        return false;
    } else {
        string result;
        bool ret;
        ret = bo_channel::exec_sql(_fd_session_table, DIRECTOR_WORKSPACE, sql.str(), false, result); 
        close(_fd_session_table);
        if(!ret) {
            Error("failed to keep session status, result: %s, error: %s\n" ,result ,strerror(errno));
            return false;
        }        
    }

    //Debug("Set checkpoint: %s\n", sql.str().c_str());
    log_debug(string("<-------   <") +__PRETTY_FUNCTION__+ ">", true);
    return true;
}
