//#include <sys/types.h>
//#include <sys/sendfile.h>
//#include <sys/socket.h>
//#include <sys/stat.h>
//#include <arpa/inet.h>
//#include <netdb.h>
//#include <unistd.h>
//#include <fcntl.h>

#include <strings.h>
#include <string.h>
#include <errno.h>
#include <vector>
//#include <sstream>

//#include "bo_counter.h"
#include "go_json2csv.h"
#include "influxdb_transmitter.h"
// #include "bo_stream.h"

#include <iostream>

using namespace std;

#define KEY_TRAN_INFLUXDB_ADDR       "influxdb_addr"
#define KEY_TRAN_INFLUXDB_PORT       "influxdb_port"

#define KEY_TRAN_INFLUXDB_USERNAME   "influxdb_username"
#define KEY_TRAN_INFLUXDB_PASSWORD   "influxdb_password"

#define KEY_TRAN_INFLUXDB_DBNAME     "influxdb_dbname"
#define KEY_TRAN_INFLUXDB_MEASUREMENT    "influxdb_measurement"


#define TRAN_BO_READ_TIMEOUT    3

//////////////////////////////////////////////////
// bo_transmitter
//////////////////////////////////////////////////
influxdb_transmitter::~influxdb_transmitter()
{
/*
    if (_fd_cp != -1) {
        close(_fd_cp);
        _fd_cp = -1;
    }
    */
}

bool influxdb_transmitter::initialize()
{
//    allow_backoff = true;

    if (!get_param_value(KEY_TRAN_INFLUXDB_ADDR, _influxdb_addr)) {
    //    printf("%s: missing conf key '%s'\n", string(__func__), KEY_TRAN_INFLUXDB_ADDR);
        cout<<"influxdb_addr is not set"<<endl;
        return false;
    }
    
    if (!get_param_value(KEY_TRAN_INFLUXDB_PORT, _influxdb_port)) {
    //    printf("%s: missing conf key '%s'\n", string(__func__), KEY_TRAN_INFLUXDB_PORT);
        cout<<"influxdb_port is not set"<<endl;
        return false;
    } 
    
    
    if (!get_param_value(KEY_TRAN_INFLUXDB_USERNAME, _influxdb_username)) {
    //    printf("%s: missing conf key '%s'\n", string(__func__), KEY_TRAN_INFLUXDB_USERNAME);
        cout<<"influxdb_username is not set"<<endl;
        return false;
    }
    if (!get_param_value(KEY_TRAN_INFLUXDB_PASSWORD, _influxdb_dbname)) {
    //    printf("%s: missing conf key '%s'\n", string(__func__), KEY_TRAN_INFLUXDB_PASSWORD);
        cout<<"influxdb_dbname is not set"<<endl;
        return false;
    }
    if (!get_param_value(KEY_TRAN_INFLUXDB_DBNAME, _influxdb_dbname)) {
    //    printf("%s: missing conf key '%s'\n", string(__func__), KEY_TRAN_INFLUXDB_DBNAME);
        cout<<"influxdb_dbname is not set"<<endl;
        return false;
    }
    if (!get_param_value(KEY_TRAN_INFLUXDB_MEASUREMENT, _influxdb_measurement)) {
    //    printf("%s: missing conf key '%s'\n", string(__func__), KEY_TRAN_INFLUXDB_MEASUREMENT);
        cout<<"influxdb_measurement is not set"<<endl;
        return false;
    }

    return true;
}

bool influxdb_transmitter::set_params(string& group, map<string, string>& param)
{
    if (!component::set_params(group, param))
        return false;
//    if (!transmitter::set_params(group, param))
//        return false;
    return initialize();
}


bool influxdb_transmitter::flush(string& chan, int infd)
{
    if(infd < 0)
        return true;

    vector<int> files_fd; 
    files_fd.push_back(infd);
    
    int i = 0;
    int files_num = files_fd.size();

    
    vector<GoInt> input_files;
    input_files.reserve(files_num);

    for (i = 0; i < files_num; ++i)
    {
        input_files[i] = (GoInt)files_fd[i];
    }
    GoSlice input = {&input_files[0], files_num, files_num};

    CsvToInfluxDB_return influx_ret = CsvToInfluxDB(
        
        { (char*)(_influxdb_addr.c_str()), (GoInt)_influxdb_addr.length() }, // $$ influxdb IP address
        { (char*)(_influxdb_port.c_str()), (GoInt)_influxdb_port.length() }, // $$ influxdb port
        { (char*)(_influxdb_username.c_str()), (GoInt)_influxdb_username.length() }, // $$ influxdb username
        { (char*)(_influxdb_password.c_str()), (GoInt)_influxdb_password.length() }, // $$ influxdb password

        { (char*)(_influxdb_dbname.c_str()), (GoInt)_influxdb_dbname.length() }, // $$ dbname
        { (char*)(_influxdb_measurement.c_str()), (GoInt)_influxdb_measurement.length() }, // $$ measurement
        input   
    ); 

    vector<string> err_msg;
    err_msg.reserve(files_num);

    for (i = 0; i < files_num; ++i) 
    {
        GoString* err_go = &((GoString*)influx_ret.r1.data)[i];
        err_msg.push_back(string(err_go->p, err_go->n));
    }

    string temp_error_string;
    vector<string>::const_iterator it_err = err_msg.begin();
    for (; it_err != err_msg.end(); ++it_err) 
    {
        if (!it_err->empty()) {
            if(temp_error_string.empty())
                temp_error_string = it_err->c_str();
            else
                temp_error_string = temp_error_string + "; " + it_err->c_str();            
            continue;
        }
    }
    if(!temp_error_string.empty())
    {
        Write_System_ErrorLog(temp_error_string, true);
        return false;
    }        
    return true;
}
