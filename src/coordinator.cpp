#include <string.h>
#include <unistd.h>

#include "bo_counter.h"
#include "coordinator.h"

#define KEY_CORD_GROUP      "syn_group"
#define KEY_CORD_RETENTION  "retention"
#define KEY_CORD_LIFE_TYPE  "life_type"
#define KEY_CORD_START_DAY  "start_day"
#define KEY_CORD_ALOW_ZERO_START    "allow_zerostart"
#define KEY_CORD_EYE_ON_SESSION    "eye_on_session"

#define KEY_CORD_COUNTER_IP     "counter.ip"
#define KEY_CORD_COUNTER_PORT   "counter.port"
#define KEY_CORD_COUNTER_DB     "counter.db"
#define KEY_CORD_COUNTER_RETENTION "counter.retention"

bool coordinator::initialize()
{
    log_debugf("------->   <%s>", __PRETTY_FUNCTION__);

    if (!_receiver || !_adapter ||  !_transmitter )
        return false;

    ini_conf conf(_conf);
    map<string, string> section;
    string group;
    if (!conf.get_value(SECT_CORD, KEY_CORD_GROUP, group)) {
        Error("%s: missing conf key '%s::%s'\n", __func__, SECT_CORD, KEY_CORD_GROUP);
        return false;
    }
    string retain_days;
    if (conf.get_value(SECT_CORD, KEY_CORD_RETENTION, retain_days)) {
        _retention = stoi(retain_days);
        if (_retention < 1)
            _retention = 1;
    }

    int diff_day = 0;
    time_t start_time = time(NULL);
    struct tm start_utc = {0};
    memcpy(&start_utc, gmtime(&start_time), sizeof(start_utc));

    string start_day;
    if (conf.get_value(SECT_CORD, KEY_CORD_START_DAY, start_day)) {
        _start_day = stoi(start_day);
        if (_start_day < 0)
            _start_day = 0;
    }
    string life_type;
    if (conf.get_value(SECT_CORD, KEY_CORD_LIFE_TYPE, life_type)) {
        switch (life_type.at(0)) {
            case 'w':
                _life_type = LIFE_TYPE_WEEKLY;
                diff_day = start_utc.tm_wday - _start_day;
                if (diff_day < 0)
                    diff_day += 7;
                start_time -= diff_day * 86400;
                memcpy(&start_utc, gmtime(&start_time), sizeof(start_utc));
                break;
            case 'm':
                _life_type = LIFE_TYPE_MONTHLY;
                if (_start_day == 0)
                    _start_day = 1; // month day is 1 to 31
                if (start_utc.tm_mday < _start_day &&
                    --start_utc.tm_mon < 0) // month is 0 to 11
                    --start_utc.tm_year;
                start_utc.tm_mday = _start_day;
                break;
            case 'f':
                _life_type = LIFE_TYPE_FOREVER;
                memcpy(&start_utc, gmtime(&start_time), sizeof(start_utc));
                break;
        }
    }
    update_retention(true);

  
    char date_buf[16] = {0};
    strftime(date_buf, sizeof(date_buf) - 1, "BO%Y%2m%2d", &start_utc);
        
    _transmitter->set_name(date_buf);
    
    

    // set counter
    string counter_ip, counter_port;
    if (conf.get_value(SECT_CORD, KEY_CORD_COUNTER_IP, counter_ip) &&
        conf.get_value(SECT_CORD, KEY_CORD_COUNTER_PORT, counter_port)) 
    {
        unsigned short port = (unsigned short)atoi(counter_port.c_str());
        int retention = 1;
        string counter_retention;
        if (conf.get_value(SECT_CORD, KEY_CORD_COUNTER_RETENTION, counter_retention))
            retention = atoi(counter_retention.c_str());
        if (retention < 1)
            retention = 1;
        bo_counter::get_counter()->set_counter(counter_ip, port, retention);
        // comment out counter to temporary disable counter for kafka consumer
        //_enable_counter = true;
    }

    string zero_start;
    if (conf.get_value(SECT_CORD, KEY_CORD_ALOW_ZERO_START, zero_start))
        _alow_zero_start = zero_start.compare("1") == 0;

    if (!conf.get_section(SECT_RECV, section) ||
        !_receiver->set_params(group, section)) {
        Error("%s: missing conf key '%s' or set parameter fail\n", __func__, SECT_RECV);
        return false;
    }

    if (!conf.get_section(SECT_ADPT, section) ||
        !_adapter->set_params(group, section)) {
        Error("%s: missing conf key '%s' or set parameter fail\n", __func__, SECT_ADPT);
        return false;
    }

   
        
    if (!conf.get_section(SECT_TRAN, section) ||
        !_transmitter->set_params(group, section)) {

        Error("%s: missing conf key '%s' or set parameter fail\n", __func__, SECT_TRAN);
        return false;
    }
    
    _adapter->set_trnasmitter(_transmitter);
    
    _receiver->set_adapter(_adapter);

    log_debug(string("checking eye_on_session..."), true);
    string eye_on_session;
    if (conf.get_value(SECT_CORD, KEY_CORD_EYE_ON_SESSION, eye_on_session))
        _eye_on_session = eye_on_session.compare("1") == 0;
    if (_eye_on_session){
        if (_transmitter->set_session_monitoring(true)) {
            log_debug(string("eye_on_session activated!!"), true);
        } else {
            Error("%s: failed to create session table\n", __func__);
            return false;
        }
    }
   
    return true;
}

void coordinator::set_receiver(receiver* recv, bool is_file_receiver) {
    _receiver = recv;
    if (is_file_receiver) {
        _is_file_receiver=true;
    }
}

void* coordinator::counter_worker(void* arg)
{
    bo_counter::get_counter()->start_counter();
}

bool coordinator::run()
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    log_debugf("<%s> checking _is_json_input [%d]", __func__, _adapter->is_json_input());
    if (_adapter->is_json_input()) {
        _receiver->set_batch_reading(1);
        log_debugf("<%s> json input, set _batch_reading to 1", __func__);
    }

    if (_is_file_receiver) {
        _adapter->set_file_receiver(true);
        vector<string> prefix_list = _receiver->get_prefixs();
        for (int i=0; i<prefix_list.size(); i++) {
            _adapter->add_prefix(prefix_list[i]);
        }            
        _adapter->set_loaded_docs_str(_init_doc_str);
    }

    pthread_t counter_thr;
    if (_enable_counter) { 
        if (0 != pthread_create(&counter_thr, NULL, counter_worker, NULL)) {
            Error("coordinator::%s, create counter worker fail: %s\n", __func__, strerror(errno));
            _enable_counter = false;
        }
    }

    // $$ to receiver::start
    while (_receiver->start(_expire)) { 
        // receiver need to sync again maybe BO is crash
        int retry = 10;   
        do
        {
            if (sync()) 
                break;
            else {
                Error("Sync failed, retry after 3 sec (remains %d times retry.)\n", retry);
                sleep(3);
            }
        } while(--retry >= 0);

        if(retry <=0 ) {
            return false;
        }
        // reset_workspace() will issue back up previous workspace request,
        // so we do it before checking if it's time to switch bo.
        bool change_bo = update_retention(false);

        _transmitter->close_chan();
        _transmitter->reset_workspace(change_bo);
        if (change_bo) {
            // reset next retention time, but not check if expired
            update_retention(true);

            // update bo name
            time_t current_time = time(NULL);
            struct tm current_utc = {0};
            char bo_name[16] = {0};
            memcpy(&current_utc, gmtime(&current_time), sizeof(current_utc));
            strftime(bo_name, sizeof(bo_name) - 1, "BO%Y%2m%2d", &current_utc);
            _transmitter->set_name(bo_name);
        }
        _transmitter->prepare_channels(change_bo);
    }

    if (_enable_counter) {
        bo_counter::get_counter()->stop_counter();

        struct timespec timeout;
        clock_gettime(CLOCK_REALTIME, &timeout);
        timeout.tv_sec += 5;
        if (0 != pthread_timedjoin_np(counter_thr, NULL, &timeout))
            Error("coordinator::%s, join counter thread fail: %s\n", __func__, strerror(errno));
    }

    return true;
}

bool coordinator::sync()
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    if(_write_to == "influxdb") 
        return true;
    
    string src, dst, target; 

    if (!_receiver->get_last_commitment(src) ||
        !_transmitter->get_last_commitment(dst))
        return false;
    
    Info("receiver id: %s\ntransmitter id: %s\n", src.c_str(), dst.c_str());
    if (dst.length() == 0 && !_alow_zero_start)
    {
        Error("Sync failed, transmitter last commitment is empty and allow_zerostart is false.\n");
        return false;
    }
    // 2. negotiate
    if (!_receiver->sync_strategy(src, dst, target))  
        return false;

    
    // 3. back off a state that both sides agree if needs
    if (!_receiver->backoff(target) ||
        !_transmitter->backoff(target))
        return false;

    if (_is_file_receiver) {
        _init_doc_str=target;
    }
    return true;
}

bool coordinator::update_retention(bool init)
{
    time_t now = time(NULL);
    struct tm utc_time = {0};

    memcpy(&utc_time, gmtime(&now), sizeof(utc_time));

    // check if it's time to stop
    if (!init &&
        is_life_end(utc_time)) {
        return true;
    }

    // update next expire time
    time_t diff = (23 - utc_time.tm_hour) * 3600 + (59 - utc_time.tm_min) * 60 + (59 - utc_time.tm_sec);
    // +1: just to be sure it's at the begnning of day, especially negative leap second.
    _expire = now + diff + (_retention - 1) * 86400 + 1;

#ifdef BO_STREAM_AGENT_DEBUG
    char time_buf[64] = {0};
    strftime(time_buf, sizeof(time_buf) - 1, "%F %T %Z", localtime(&_expire));
    Debug("%s, next retention expired time is at %s.\n", __func__, time_buf);
#endif
    Error("%s, next expired time is %lu (now is %lu)\n", __func__, _expire, time(NULL));
    return false;
}

bool coordinator::is_life_end(struct tm& utc_time)
{
    // since we do check at the end/beginning of day, we can just check if
    // it's the date of  _start_day.
    switch (_life_type) {
        case LIFE_TYPE_WEEKLY:
        {
            return (utc_time.tm_wday == _start_day && 0 == utc_time.tm_hour && utc_time.tm_min < 5);
        }
        case LIFE_TYPE_MONTHLY:
        {
            return (utc_time.tm_mday == _start_day && 0 == utc_time.tm_hour && utc_time.tm_min < 5);
        }
        case LIFE_TYPE_FOREVER:
        {
            return false;
        }
    }
    return false;
}

bool coordinator::terminate()
{
    _receiver->stop();
    _transmitter->close_chan();
    return true;
}
