#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <sstream>

#include "transmitter.h"
#include <iostream>
#include <algorithm>

using namespace std;

#define KEY_TRAN_TABS           "table_num"
#define KEY_TRAN_TABNAME        "table_name_"
#define KEY_TRAN_TABBUF         "table_buf_"
#define KEY_TRAN_ALLOW_CLEANUP  "allow_cleanup_"

#define KEY_TRAN_STOP_AT_FAIL   "stop_at_fail"

//////////////////////////////////////////////////
// channel
//////////////////////////////////////////////////
channel::channel(string name, size_t cap) :
    _name(name), _capacity(cap), _size(cap), _head(0), _tail(0),
    _data(NULL), _progress(0), _is_replica(false)
{
    if (0 == cap) // probably use input file instead.
        return;

    _data = (char*)mmap(NULL, _capacity, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (_data == MAP_FAILED)
        _data = NULL;
}

channel::~channel()
{
    if (_data && _capacity) {
        munmap(_data, _capacity);
        _data = NULL;
    }
}

bool channel::flush()
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    ssize_t ret = 0;
    size_t size = 0;
    void* data = NULL;
    
    while (NULL != (data = get_data(size))) {
        ret = transmit(data, size);
        if (ret <= 0)
            break;
        release(ret);
    }
    return ret > 0 || errno == EAGAIN;
}

bool channel::flush(int infd)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    return transmit(infd) > 0 || errno == EAGAIN;
}

bool channel::flush(int infd, int offset)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    bool done;

    done = transmit(infd, offset) > 0 || errno == EAGAIN;

    return done; 
}

//////////////////////////////////////////////////
// transmitter
//////////////////////////////////////////////////
transmitter::~transmitter()
{
    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it)
        delete it->second;
    _channels.clear();
}

bool transmitter::flush(string& chan_name)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    channel* chan = get_chan(chan_name, true);
    if (!chan)
        return false;

    return flush(chan);
}

bool transmitter::flush(channel* chan)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    return chan->flush();
}

bool transmitter::flush(string& chan_name, int infd)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    return flush(chan_name, infd, 0);
}

bool transmitter::flush(string& chan_name, int infd, int offset)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    channel* chan = get_chan(chan_name, true);
    if (!chan)
    {
        Write_System_ErrorLog("just skip the channel: chan_name: " + chan_name, true);
        return true;
    }
    return flush(chan, infd, offset);
}

bool transmitter::flush(channel* chan, int infd)
{
    return flush(chan, infd, 0);
}

bool transmitter::flush(channel* chan, int infd, int offset)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    errno = 0;
    int retry = 5;
    bool ret;
    while (retry > 0) 
    {
        ret = chan->flush(infd, offset);
        
        if (!ret)
        {
            --retry;
            Debug("%s (fd: %d, offset: %d) failed(%s)(%d) retry %d time(s) left\n", 
                chan->get_name().c_str(), infd, offset, strerror(errno), errno, retry );
            if (errno == EINTR)
            {
                Write_System_ErrorLog(string(__func__) + ": sleep a few seconds and retry (" + to_string(retry)+ " times left)" , true);
                sleep(3);
                continue;
            }
            if(errno == EPIPE || errno == EAGAIN)
            {
                Debug("Start reset_chan, errno : %d\n", errno);
                if (!reset_chan(chan)) 
                {
                    Error("%s, reset sync channel fail: %s\n", __func__, strerror(errno));
                    continue;
                }
            }
            else 
                return false;

        }
        else   
            break;
    } 
    
    return ret;
}

bool transmitter::flushall()
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    bool ret = true;
    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it)
        ret &= flush(it->second);
    return ret;
}

void transmitter::close_chan()
{
    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it)
        close_chan(it->second);
}


bool transmitter::commit(string& doc_id)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it) {
        if (!commit(it->second, doc_id))
            return false;
    }
    return true;
}

// sample staged records : 
// doc_id : [api, api_err]
bool transmitter::exclusive_commit(map<string, vector<string>>& staged_records)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it) {
        string table_name = it->second->get_name();
        //string table_name = const_table_name;

        for (const auto& doc_record : staged_records) {
            string doc_id = doc_record.first;
            vector<string> v = doc_record.second;
            if(v.empty()){
                commit(it->second, doc_id);
                continue;
            }

            if (std::find(v.begin(), v.end(), table_name) != v.end()) {
                continue;
            } else {
                commit(it->second, doc_id);
            }
        }
    }
    return true;
}

/* bool transmitter::commit(string& table_name, string& milestone_summary, int rows_expected, int incremental){
    for (const auto& milestone : milestones) {
        channel * chan = _channels[milestone.db_table_name];
        string mstone = milestone.milestone;
        string& mstone_ref = mstone;
        commit(chan, mstone);
    }

    return true;
} */

bool transmitter::commit(string& chan_name, string& doc_id)
{
    return commit(chan_name, doc_id, false, 0, SIZE_MAX);
}

bool transmitter::commit(string& chan_name, string& doc_id, bool verify_total, size_t total_expected, size_t subtotal)
{
    channel* chan = get_chan(chan_name, false);
    if (!chan)
    {
        Write_System_ErrorLog(string(__func__)  + ", get_chan " + chan_name + " fail: " + strerror(errno), true);
        return false;
    }

    log_debugf("<%s> total expected [%d], subtotal [%d]",__func__, total_expected, subtotal);
        
    return commit(chan, doc_id, verify_total, total_expected, subtotal);
}

bool transmitter::commit(channel* chan, string& doc_id)
{
    return commit(chan, doc_id, false, 0, SIZE_MAX);
}

bool transmitter::commit(channel* chan, string& doc_id, bool verify_total, size_t total_expected, size_t subtotal)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    log_debug(string("<") +__func__+ "> channel name : " + chan->get_name(), true);

    if (chan->is_replica() && !chan->is_sync_replica())
        return true; // set check_point on master only

    int retry = 5;
    int ret = flush(chan);
    while (!chan->is_empty()) {
        usleep(1000); // sleep 1 ms
        flush(chan);
        --retry;
    }
    if (!chan->is_empty()) {
        Write_System_ErrorLog(string(__func__)  + ", channel is empty: "  + strerror(errno), true);
        return false;
    }
    size_t total_count;

    bool is_ok;
    if (use_syn_mark()){
        is_ok = chan->get_total_count_by_pkt(total_count);
    } else {
        is_ok = chan->get_total_count_by_sql(total_count);
    }
    if(!is_ok)
    {
        // if (errno == EINTR) {
        //     sleep(3);
        // } else if(errno == EPIPE || errno == EAGAIN) {
        if (!reset_chan(chan)) {
            Error("%s, reset sync channel fail: %s\n", __func__, strerror(errno));
        } 
        // } 
        return false;
    }
    log_debug(string("<") +__func__+ "> Got table row count : " + to_string(total_count), true);

    // bool success = false;
    // while(true) {
    //     chan->get_name
    //     if (chan->get_num_conn() <= 0 )
    //         return false;
    //     if(!chan->get_total_count_by_pkt(total_count))
    //     {
    //         if (errno == EINTR)
    //         {
    //             Error("sleep a few seconds and retry\n");
    //             sleep(3);
    //             continue;
    //         }
    //         if(errno == EPIPE || errno == EAGAIN)
    //         {
    //             if (get_num_conn == 1) {
    //                 Error("sleep a few seconds and retry\n");
    //                 sleep(3);
    //                 continue;
    //             }
    //             if (!reset_chan(chan)) 
    //                 Error("%s, reset sync channel fail: %s\n", __func__, strerror(errno));
    //             return false;
    //         }
    //         else 
    //             return false;
    //     }
    //     else {
    //         success = true;
    //         break;
    //     }            
    // }

    // if (!success) 
    //     return false;

    chan->set_progress(total_count);
    commitment cp(chan->get_name(), doc_id, total_expected, subtotal, chan->get_progress());
    
    size_t _chan_progress = chan->get_progress();
    log_debug(string("<") +__func__+ "> ready to set progress : " + to_string(_chan_progress), true);
    if (!set_checkpoint(cp, verify_total))
        return false;

    //chan->reset_progress();
    return true;
}

bool transmitter::del_chan(string chan_name)
{
    map<string, channel*>::const_iterator it = _channels.find(chan_name);
    if (it == _channels.end())
        return false;
    delete it->second;
    _channels.erase(it);
    return true;
}

bool transmitter::reset_chan(string chan_name)
{
    channel* chan = get_chan(chan_name, false);
    if (!chan)
        return false;
    return reset_chan(chan);
}

bool transmitter::set_params(string& group, map<string, string>& param)
{
    if (!component::set_params(group, param))
        return false;

    string stop_at_fail;
    if (get_param_value(KEY_TRAN_STOP_AT_FAIL, stop_at_fail))
        _stop_at_fail = stop_at_fail.compare("1") == 0;

    string tab_num;
    int chan_num = 0, i = 0;
    /* marked @2023-03-14 for extensible tables
    if (!get_param_value(KEY_TRAN_TABS, tab_num) ||
        0 >= (chan_num = stoi(tab_num)))
        return false;
    */
    if (get_param_value(KEY_TRAN_TABS, tab_num)) {
        chan_num = stoi(tab_num);
        if (chan_num < 0){
            chan_num=0;
        }
    }

    string chan_name, chan_size;
    size_t size = 0;
    for (i = 1; i <= chan_num; ++i) {
        ostringstream name_key, buf_key, cleanup_key;
        name_key << KEY_TRAN_TABNAME << i;
        buf_key << KEY_TRAN_TABBUF << i;
        cleanup_key << KEY_TRAN_ALLOW_CLEANUP << i;

        bool allow_cleanup = false;
        string allow_cleanup_str;
        if (get_param_value(cleanup_key.str(), allow_cleanup_str))
            allow_cleanup = allow_cleanup_str.compare("1") == 0;

        if (!get_param_value(name_key.str(), chan_name) ||
            !get_param_value(buf_key.str(), chan_size))
            return false;

        switch (chan_size.at(chan_size.length() - 1)) {
            case 'k': case 'K':
                size = stoi(chan_size.substr(0, chan_size.length() - 1)) * 1024;
                break;
            case 'm': case 'M':
                size = stoi(chan_size.substr(0, chan_size.length() - 1)) * 1024 * 1024;
                break;
            default:
                size = stoi(chan_size.substr(0, chan_size.length()));
                break;
        }
        
        if (!add_chan(chan_name, size, allow_cleanup))
        {
            return false;
        }
    }
    return true;
}

void transmitter::reset_progress()
{
    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it)
        it->second->reset_progress();
}

void transmitter::reset_progress(string& chan_name)
{
    channel* chan = get_chan(chan_name, false);
    if (chan)
        chan->reset_progress();
}

void transmitter::inc_progress(string& chan_name, size_t progress)
{
    channel* chan = get_chan(chan_name, false);
    if (chan)
        chan->inc_progress(progress);
}

size_t transmitter::get_progress(string& chan_name)
{
    channel* chan = get_chan(chan_name, false);
    if (chan)
        chan->get_progress();
}

size_t transmitter::get_main_channel_size()
{
    size_t ret = 0;
    map<string, channel*>::const_iterator it = _channels.begin();
    for (; it != _channels.end(); ++it) {
        if (!(it->second->is_replica() && !it->second->is_sync_replica()))
            ++ret;
    }
    return ret;
}
