#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string>
#include <vector>

#include "receiver.h"
#include<iostream>

using namespace std;

#define KEY_RECV_BATCH_PROCESSING   "batch_processing"
#define KEY_RECV_BATCH_PROCESSING_LEGACY   "batch_proc"

void receiver::set_type(receiver_type_t rcv_type)
{
    _rcv_type = rcv_type;
}

receiver_type_t receiver::get_type()
{
    return _rcv_type;
}

bool receiver::set_params(string& group, map<string, string>& param)
{
    if (!component::set_params(group, param))
        return false;

    string batch;
    if (get_param_value(KEY_RECV_BATCH_PROCESSING, batch)) {
        _batch_processing = stoi(batch);
    } else {
        if (get_param_value(KEY_RECV_BATCH_PROCESSING_LEGACY, batch)) {
            _batch_processing = stoi(batch);
        }    
    }

    return true;
}

void receiver::free_data_handles(vector<source_combo_struct*>& data_sources){
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    int data_len = data_sources.size();

    //source_combo_struct* object_to_kill;
    for (int i = 0; i < data_len; i++){
        /*
        object_to_kill = data_sources[i];
        //log_debug(string("<") +__func__+ "> deleting data collected : " + to_string((long)object_to_kill), true);
        log_debugf("<%s> ................................data collected : 0x%X", __func__, data_sources[i]);
        delete object_to_kill;
        */

        //log_debugf("<%s> ................................deleting data collected : 0x%X", __func__, data_sources[i]);
        delete data_sources[i];
    }

    //log_debug(string("<-------   <") +__PRETTY_FUNCTION__+ ">", true);
}

bool receiver::start(time_t expire)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    bool ret = false;
    int data_cnt = 0;
    FILE *fileptr;
    vector<FILE*> batch_fptr;
    vector<source_combo_struct*> raw_source_ptrs;
    set_state(AST_START);

    string data;
    msg_info_struct msg_info;
    //log_debugf("<%s> ................................new msg_info : 0x%X", __func__, msg_info);

    source_combo_struct data_picked;
    //log_debugf("<%s> ................................new data_picked : 0x%X", __func__, data_picked);

    int num_dots = 0;
    while (is_running()) {
        // !!! loop one time
        msg_info = {};
        data_picked = {};        

        /*
        delete data_picked;
        data_picked = new source_combo_struct;
        log_debug(string("<") +__func__+ "> new data_picked again : " + to_string((long)data_picked), true);
        */

        //log_debug(string("<") +__func__+ "> entering read_receiver ... " , true);
        //if (read_receiver(data, &fileptr, msg_info) <= 0)  
        int status_receiver = read_receiver(data, &data_picked, &msg_info);
        log_debugf("<%s> read_receiver retuened [%d][%d]", __func__, status_receiver, data_picked.status);

        if ((status_receiver <= 0) || (data_picked.status <= 0))
        {
            if (!raw_source_ptrs.empty()) {
                log_debugf("<%s> bitshift_departure [1]", __func__);
                if ( !(_adapt->bitshift_departure(raw_source_ptrs, &msg_info)) ) {
                    return true;
                }
                free_data_handles(raw_source_ptrs);
                raw_source_ptrs.clear();
                batch_fptr.clear();
            }

            if (strncmp(data_picked.type, "FILE_", 5)==0) {
                continue;
            }

            if (strncmp(data_picked.type, "MQTT_MSG_NULL", 14)==0) {
                //log_debug(string("<") +__func__+ "> delete data_picked : " + to_string((long)data_picked), true);
                //////delete data_picked;
                //////delete msg_info;
                continue;
            }

            if (strncmp(data_picked.type, "MQTT_MSG_WEIRD", 15)==0) {
                if (data_picked.msg_buf_ptr!=NULL) {
                    log_debugf("<%s> xxxxxxxxxxxxxxxxxxxxxx weird msg discared [%s]", __func__, data_picked.msg_buf_ptr);
                    //delete data_picked->msg_buf_ptr;
                }
                //////delete data_picked;
                //////delete msg_info;
                continue;
            }

            // send empty data to force commit
            log_debugf("<%s> Send empty data to force commit", __func__);
            if ( !(_adapt->bitshift_departure(raw_source_ptrs, &msg_info)) ) {
                return true;
            }
            data_cnt = 0;                    

            // may get MQTT_CONN_DOWN or MQTT_CONN_FAIL
            if ((strncmp(data_picked.type, "MQTT_CONN_", 10)==0) || (strncmp(data_picked.type, "MQTT_MSG_T_OUT", 14)==0)) {
                //log_debug(string("<") +__func__+ "> delete data_picked : " + to_string((long)data_picked), true);
                //////log_debugf("<%s> ................................deleting data_picked : 0x%X", __func__, data_picked);
                //////delete data_picked;
                //////log_debugf("<%s> ................................deleting msg_info : 0x%X", __func__, msg_info);
                //////delete msg_info;

                char dots[64];
                sprintf(dots, "%s", "............................................................");
                dots[(num_dots++%60)] = '*';
                log_debugf("<%s> going to the next round%s", __func__, dots);
                continue;
            }

            /*
            // send empty data to force commit
            log_debugf("<%s> Send empty data to force commit", __func__);
            if ( !(_adapt->bitshift_departure(raw_source_ptrs, &msg_info)) ) {
                //////delete msg_info;
                //////delete data_picked;
                return true;
            }

            //////delete msg_info;
            //////delete data_picked;

            data_cnt = 0;
            */
            //break;
            continue;
        }

        //////delete msg_info;
        fileptr = data_picked.file_ptr;
        //log_debug(string("<") +__func__+ "> returned file picked : " + to_string((long)fileptr), true);
        log_debugf("<%s> ................................returned file picked:0x%X, topic[%s]", __func__, fileptr, data_picked.topic.c_str());

        if (fileptr != nullptr || data_picked.file_descriptor > 2) 
        {
            //if (get_type() == RCV_MQTT){
            if (data_picked.handle_type=='D'){
                log_debug(string("<") +__func__+ "> data type = " + data_picked.type, true);

                int fd_msg = data_picked.file_descriptor;

                struct stat in_stat = {0};
                if (0 != fstat(fd_msg, &in_stat)) 
                {
                    log_error(string("<") +__func__+ "> fstat failed", true);
                }
                log_debug(string("<") +__func__+ "> file length = " + to_string(in_stat.st_size), true);

                bool debugging = false;
                if (debugging){
                    char buf[1024] = {0};
                    int offset = 0;
                    ssize_t ret = 0;
                    int count = 1023;
                    if((ret = pread(fd_msg, buf, count, offset)) == -1)
                    {
                        log_error(string("<") +__func__+ "> pread failed", true);
                        return true;
                    }
                    log_debug(string("<") +__func__+ "> data read through fd = " + buf, true);
                }
                
                //strncpy(buf, (char*)data_picked->msg_buf_ptr, data_picked->msg_buf_len);
                //log_debug(string("<") +__func__+ "> read buf 2 = " + buf, true);

                fileptr = (FILE*)&data_picked;
            }
            batch_fptr.push_back(fileptr);
            source_combo_struct *cooked_combo_ptr = new source_combo_struct;
            *cooked_combo_ptr = data_picked;
            raw_source_ptrs.push_back(cooked_combo_ptr);

            log_debugf("<%s> previous files collected = %d, need %d to go thru", __func__, data_cnt, _batch_processing);
            if (data_picked.is_final_part){
                ++data_cnt;
            }
            if (data_cnt >= _batch_processing)
            {
                //if ( !(_adapt->bitshift_departure(batch_fptr, msg_info)) )

                do {
                    log_debugf("<%s> bitshift_departure [normal]", __func__);
                    if ( !(_adapt->bitshift_departure(raw_source_ptrs, &msg_info)) )
                        return true;
                } while (_adapt->pending_materials()>0);

                free_data_handles(raw_source_ptrs);
                log_debug(string("<") +__func__+ "> reset handle buffer...", true);
                raw_source_ptrs.clear();
                batch_fptr.clear();
                log_debug(string("<") +__func__+ "> reset data_cnt...", true);
                data_cnt = 0;
            }
        } 
        else 
        {
            //////log_debugf("<%s> ................................deleting data_picked : 0x%X", __func__, data_picked);
            //////delete data_picked;

            if (!raw_source_ptrs.empty()) 
            {
                log_debugf("<%s> bitshift_departure [4]", __func__);
                if ( !(_adapt->bitshift_departure(raw_source_ptrs, &msg_info)) )
                    return true;
                free_data_handles(raw_source_ptrs);
                raw_source_ptrs.clear();
                batch_fptr.clear();
                data_cnt = 0;
            }
            log_debugf("<%s> bitshift_departure [5]", __func__);
            if ( !(_adapt->bitshift_departure(raw_source_ptrs, &msg_info)) )
                    return true;
        }

        if (is_expired(expire)) 
        {
            if (!raw_source_ptrs.empty()) {
                log_debugf("<%s> bitshift_departure [6]", __func__);
                if ( !(_adapt->bitshift_departure(raw_source_ptrs, &msg_info)) )
                    return true;
                free_data_handles(raw_source_ptrs);
                raw_source_ptrs.clear();
                batch_fptr.clear();
                data_cnt = 0;
            }
            log_debugf("<%s> bitshift_departure [7]", __func__);
            if ( !(_adapt->bitshift_departure(raw_source_ptrs, &msg_info)) )
                    return true;
            ret = true;
            break;
        }
        // delete msg_info;
    } //while is running
    return ret;
}

void receiver::stop()
{
    set_state(AST_STOP);
}

bool receiver::is_expired(time_t expire)
{
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    log_debug(string("<") +__func__+ "> expiry time = " + to_string(expire), true);
    log_debug(string("<") +__func__+ "> now = " + to_string(time(NULL)), true);

    return expire < time(NULL);
}

vector<string> receiver::get_prefixs(){
    vector<string> nobody;
    return nobody;
}