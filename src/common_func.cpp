#include <string.h>
#include <fstream>
#include <iostream>
#include <stdarg.h>
#include "common_func.h"
#include <unistd.h>
#include <chrono>

using namespace std;

static bool debug_mode = false;
static bool debug_file_content = false;
static bool packet_mode = false;
static int num_sample_file_chunks = 0;

void toggle_debug_mode(int level)
{
    if (level > 0) {
        debug_mode = true;
        level--;
        log_info("debug mode ENABLED!", true);
    }
    
    if (level > 0) {
        debug_file_content = true;
        level--;
        log_info("log data content ENABLED!", true);
    }
}

void toggle_packet_mode(bool enable_or_not)
{
    packet_mode=enable_or_not;
    if (packet_mode) {
        log_info("packet mode ENABLED!", true);
    } else {
        log_info("packet mode DISABLED!", true);
    }
}

bool use_syn_mark()
{
    return packet_mode;
}

void set_num_file_chunks_to_keep(int number)
{
    if (number>=0){
        num_sample_file_chunks=number;
    }
}

int get_num_file_chunks_to_keep()
{
    return num_sample_file_chunks;
}

void Write_Parse_ErrorLog(msg_info_struct *msg_info, string error_message, bool print_on_screen)
{
    string filename_ending_with = "_Parse_ErrorLog";
    string temp_msg = msg_info->topic + "\t" + to_string(msg_info->partition) + "\t" + to_string(msg_info->offset) +"\t"
            + error_message;

    Write_Log(filename_ending_with, temp_msg, print_on_screen);
    
}

void log_debug(string message_to_log, bool print_on_screen)
{
    if (debug_mode){
        logging("[DBG] ", message_to_log, print_on_screen);
    }
}

void log_debugf(char * format, ...)
{
    if (debug_mode){
        char message_to_log[512];

        va_list arg_ptr;
        va_start(arg_ptr, format);
        vsnprintf(message_to_log, 511, format, arg_ptr);
        va_end(arg_ptr);
        
        logging("[DBG] ", message_to_log, true);
    }
}

void log_file_content(const char caller_func_[], FILE * fp)
{
    if (debug_file_content){
        log_debugf("<%s> file content :", caller_func_);
        char ch;
        printf("[");
        while ((ch = fgetc(fp)) != EOF) {
            if (ch=='\r') {
                printf("\\r");
            } else if (ch=='\n') {
                printf("\\n%c", ch);
            } else {
                printf("%c", ch);
            }
        }
        printf("]");
    }
}

void log_info(string message_to_log, bool print_on_screen)
{
    logging("[INF] ", message_to_log, print_on_screen);
}
void log_error(string message_to_log, bool print_on_screen)
{
    logging("[ERR] ", message_to_log, print_on_screen);
}
void logging(string level, string message_to_log, bool print_on_screen)
{
    string temp_string = level + get_current_date("date_time") + "\t" + message_to_log + "\r\n";
    cout << temp_string;
}

void Write_System_ErrorLog(string error_message, bool print_on_screen)
{
    string filename_ending_with = "_System_ErrorLog";

    Write_Log(filename_ending_with, error_message, print_on_screen);
}
void Write_Log(string filename_ending_with, string log_message, bool print_on_screen)
{
    // fstream fp;
    // string file_name = get_current_date("date") + filename_ending_with + ".txt";

    string temp_string = get_current_date("date_time") + "\t" + log_message + "\r\n";
    // if(print_on_screen)
    //     cout<<temp_string;

    // fp.open(file_name, ios::out|ios::app);
    // if(!fp)
    //     cout<<"open file "<<file_name<<" failed\n";
    // else        
    //     fp<<temp_string;
    
    // fp.close();

    cout << temp_string;
}

void Write_ConnectionLog(int fd, string action, string file_name, int line, string function_name, string other_desc)
{
    string filename_ending_with = "_ConnectionLog";
    string message = action + " fd " + to_string(fd) 
        + ": " + function_name + "()" 
        + ": " + file_name 
        + ", line: " + to_string(line)
        + ": " + other_desc;
    Write_Log(filename_ending_with, message, false);
}

void Close_fd(int fd, string file_name, int line, string function_name, string other_desc)
{
    Close_fd(fd, string(__FILE__), __LINE__, string(__func__), "");
}

string get_current_date(string format)
{
    string output;
    time_t rawtime;
    tm* timeinfo;
    char buffer [64] = {0};
    // 2019-07-08
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    
    if(format == "unix")
        output = to_string(rawtime);
    else if(format == "date")
    {
        strftime(buffer,sizeof(buffer)-1,"%Y-%m-%d",timeinfo);
        output = string(buffer);
    }
    else if(format == "date_time")
    {
        strftime(buffer,sizeof(buffer)-1,"%Y-%m-%d %H:%M:%S",timeinfo);
        output = string(buffer);
    }
    else if(format == "unix_ms")
    {
        using namespace std::chrono;
        uint64_t ms = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
        output = to_string(ms);
    }
    
    return output;
}

string trim_string(string str) {
    //log_debug(string("string 0 : <") + str + ">", true);

    const char* spaceSet = " \t\n\r\f\v";
    str.erase(str.find_last_not_of(spaceSet) + 1);

    //log_debug(string("string 1 : <") + str + ">", true);

    str.erase(0,str.find_first_not_of(spaceSet));

    //log_debug(string("string 2 : <") + str + ">", true);

    return str;
}

string read_thru_fd(int file_desc) {
    log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);

    char buf[1024] = {0};
    int offset = 0;
    ssize_t ret = 0;
    int count = 1023;
    if((ret = pread(file_desc, buf, count, offset)) == -1)
    {
        log_error(string("<") +__func__+ "> pread failed", true);
        //return true;
    }
    log_debug(string("<") +__func__+ "> data read through fd = " + buf, true);
    log_debug(string("<-------   <") +__PRETTY_FUNCTION__+ ">", true);
}

int count_csv_double_quote(char * data_buf) {
    int count_dq=0;

    char * focus = data_buf;

    while (*focus != 0){
        if (*focus == '"') {
            if (*(focus+1) == '"') {
                focus+=2;
            } else {
                count_dq++;
                focus++;            
            }
        } else {
            focus++;            
        }
    }

    return count_dq;
}

int next_csv_cut(char * data_buf, int count_double_quote) {

    char * focus = data_buf;
    int cut = 0;

    while (*focus != 0){
        cut++;
        if (*focus == '\n') {
            if ((count_double_quote % 2) == 0) {
                return cut;
            } else {
                focus++;
                continue;    
            }
        } if (*focus == '"') {
            if (*(focus+1) == '"') {
                focus+=2;
                cut++;
            } else {
                focus++;            
                count_double_quote++;
            }
        } else {
            focus++;            
        }
    }

    return -1;
}

char *trim_c_string(char *str)
{
    char *end;

    while(isspace((unsigned char)*str)) str++;

    if(*str == 0)
        return str;

    end = str + strlen(str) - 1;
    while(end > str && isspace((unsigned char)*end)) end--;

    end[1] = '\0';

    return str;
}

void strReplace(char *base_str, char old_one, char new_one){
    char *focus = base_str;
    while(*focus!='\0') {
        if(*focus==old_one)
        {
            *focus=new_one;
        }
        focus++;
    }
}

void removeDuplicateChars(char* base_str, char uni_char) {
    if (base_str[0] == '\0')
        return;
 
    if (base_str[0] == uni_char && base_str[1] == uni_char) {
 
        // Shift character by one to left
        int i = 0;
        while (base_str[i] != '\0') {
            base_str[i] = base_str[i + 1];
            i++;
        }
 
        removeDuplicateChars(base_str, uni_char);
    }
 
    removeDuplicateChars(base_str + 1, uni_char);
}

void strupr(char* base_str) {
    while (*base_str) {
        *base_str = toupper((unsigned char) *base_str);
        base_str++;
    }
}

void get_full_filename(int fd, char* full_filename, int buf_len) {
    char fd_path[64];

    sprintf(fd_path, "/proc/self/fd/%d", fd);
    memset(full_filename, 0, buf_len);
    readlink(fd_path, full_filename, buf_len-1);
}

// char * line = NULL;
// read = get_csv_single_row(&line, &len, fp);
ssize_t get_csv_single_row(char **ptr_to_record_ptr, size_t *n, FILE *fp) {
    //log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    log_debugf("------->   <%s> ptr_to_record_ptr=0x%X, n=%d, fp=0x%X", __PRETTY_FUNCTION__, ptr_to_record_ptr, *n, fp);

    if (fp == NULL)
        return -1;

    size_t len_external_buf = 0;
    ssize_t read;

    char * ext_record_buf_ptr = *ptr_to_record_ptr;
    log_debugf("<%s> ................................return buf addr : 0x%X", __func__, *ptr_to_record_ptr);
    log_debugf("<%s> ................................return buf addr : 0x%X", __func__, ext_record_buf_ptr);

    if (ext_record_buf_ptr != NULL) {
        *ext_record_buf_ptr = '\0';
        len_external_buf=*n;
    }

    //char * line = NULL;
    size_t len_line_buf = 1024;
    char * line_buf = (char *)malloc(len_line_buf);
    log_debugf("<%s> ................................initial line buf addr : 0x%X", __func__, line_buf);

    int num_dq=0;
    int len_read=0;
    int finished = 0;
    while ((read = getline(&line_buf, &len_line_buf, fp)) != -1) {
        log_debugf("<%s> ................................single round buf addr : 0x%X", __func__, line_buf);

        len_read += read;
        if (*ptr_to_record_ptr == NULL) {
            *ptr_to_record_ptr = (char *)malloc(len_read+1);
            *n = len_read+1;
        } else {
            if (len_external_buf < len_read+1) {
                *ptr_to_record_ptr = (char *)realloc(*ptr_to_record_ptr, len_read+1);
                *n = len_read+1;
            }
        }

        strncat(*ptr_to_record_ptr, line_buf, read);
        log_debugf("<%s> getline then concatenate , read len = %d, buf len = %d", __func__, read, strlen(*ptr_to_record_ptr));

        num_dq = count_csv_double_quote(*ptr_to_record_ptr);
        if ((num_dq % 2) == 0) {
            //free(line);
            log_debugf("<%s> (success)total bytes read : %d", __func__, len_read);
            finished = 1;
            break;
        }
    }

    log_debugf("<%s> ................................FREE local single round buf addr : 0x%X", __func__, line_buf);
    free(line_buf);
    if (finished){
        return len_read;
    } else {
        log_debugf("<%s> (failure) return -1", __func__);
        return -1;
    }
}

char * get_csv_record(FILE *fp) {
    //log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    log_debugf("------->   <%s> fp=0x%X", __PRETTY_FUNCTION__, fp);

    int len_section = 512;
    int len_buf = 512;
    char * buf_record = (char *)std::malloc(len_buf);
    int len_record=0;
    char c;
    int num_dq;
    do {
        c = fgetc(fp);
        if (c == EOF){
            if (len_record==0){
                break;
            }
        }
        
        len_record++;
        if (len_record == len_buf) {
            len_buf+=len_section;
            buf_record = (char *)std::realloc((void *)buf_record, len_buf);
        }
        buf_record[len_record-1] = c;
        buf_record[len_record] = 0;
        if (c == '\n') {
            num_dq = count_csv_double_quote(buf_record);
            if ((num_dq % 2) == 0) {
                log_debugf("<%s> (success)[%s]", __func__, buf_record);
                log_debugf("<%s> (success)total bytes read : %d", __func__, len_record);
                return buf_record;
            }
        }

        if (c == EOF){
            buf_record[len_record-1] = '\n';
            log_debugf("<%s> (EOF)[%s]", __func__, buf_record);
            log_debugf("<%s> (EOF)total bytes read : %d", __func__, len_record);
            return buf_record;
        }

    } while(c != EOF);

    log_debugf("<%s> (failure)total bytes read : %d", __func__, len_record);
    std::free((void *)buf_record);
    return NULL;
}

ssize_t count_csv_records(FILE *fp) {
    //log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
    //log_debugf("------->   <%s> fp=0x%X", __PRETTY_FUNCTION__, fp);

    if (fp == NULL)
        return -1;

    fseek(fp, 0, SEEK_SET);

    ssize_t num_records=0;

    size_t len_line_buf = 1024;
    char * record_buf = (char *)malloc(len_line_buf);
    *record_buf = 0;
    size_t len_record_buf = 0;
    size_t len_record_content = 0;
    ssize_t bytes_read;

    //char * line = NULL;
    char * line_buf = (char *)malloc(len_line_buf);
    //log_debugf("<%s> ................................initial line buf addr : 0x%X", __func__, line_buf);

    int num_dq=0;
    int finished = 0;
    while ((bytes_read = getline(&line_buf, &len_line_buf, fp)) != -1) {
        //log_debugf("<%s> ................................single round buf addr : 0x%X", __func__, line_buf);

        int new_content_len = len_record_content+bytes_read+1;
        if (len_record_buf < new_content_len) {                
            record_buf = (char *)realloc(record_buf, new_content_len);
            len_record_buf = new_content_len;
        }

        strncat(record_buf, line_buf, bytes_read);
        len_record_content = strlen(record_buf);
        //log_debugf("<%s> getline then concatenate , bytes_read len = %d, record buf len = %d", __func__, bytes_read, len_record_content);

        num_dq = count_csv_double_quote(record_buf);
        if ((num_dq % 2) == 0) {
            num_records++;
            *record_buf = 0;
            //log_debugf("<%s> record(%d) length : %d", __func__, num_records, len_record_content);
            len_record_content = 0;
        }
    }

    log_debugf("<%s> total records : %d", __func__, num_records);
    //log_debugf("<%s> ................................FREE local single round buf addr : 0x%X", __func__, line_buf);
    free(line_buf);
    free(record_buf);
    fseek(fp, 0, SEEK_SET);

    return num_records;
}
void removeDuplicateDqInCell(char* stripped_cell) {
    if (stripped_cell[0] == '\0')
        return;
 
    if (stripped_cell[0] == '"' && stripped_cell[1] == '"') {
 
        // Shift character by one to left
        int i = 0;
        while (stripped_cell[i] != '\0') {
            stripped_cell[i] = stripped_cell[i + 1];
            i++;
        }
    }
 
    removeDuplicateDqInCell(stripped_cell + 1);
}

// order is 1 based
ssize_t get_csv_cell_in_order(char * csv_record, int order, char **ptr_to_cellptr, size_t *n) {
    log_debugf("------->   <%s> csv_record=0x%X, order=%d, *ptr_to_cellptr=0x%X, *n=%d", __PRETTY_FUNCTION__, csv_record, order, *ptr_to_cellptr, *n);

    char * focus = csv_record;
    char * l_cut;
    char * r_cut=0;

    int comma_to_skip = order-1;
    int comma_skiped = 0;

    int num_dq = 0;
    while (comma_skiped < comma_to_skip){
        num_dq = 0;
        while (*focus != 0){
            if (*focus == ',') {
                if ((num_dq % 2) == 0) {
                    comma_skiped++;
                    focus++;
                    break;
                }
            } else if (*focus == '"') {
                num_dq++;
            }
            focus++;            
        }
    }

    //log_debugf("<%s> raw cell data : %s", __func__, focus);

    if (*focus == ',') {
        return 0;
    }

    int warpped = 0;
    if (*focus == '"') {
        warpped = 1;
    }

    if (warpped) {
        focus++;
    }
    l_cut = focus;

    num_dq = 0;
    while (*focus != 0){
        if ((*focus == ',') || (*focus == '\n') || (*focus == '\r')) {
            if (warpped) {
                if (*(focus-1)=='"') {
                    if ((num_dq % 2) == 1) {
                        r_cut = focus-2;
                    }
                }
            } else {
                if ((num_dq % 2) == 0) {
                    r_cut = focus-1;
                }
            }

            if (r_cut!=0) {
                break;
            }
        } else if (*focus == '"') {
            num_dq++;
        }

        focus++;            
    }

    //log_debugf("<%s> aborted cell data : %s", __func__, focus);

    if (r_cut!=0) {
        int len_cell_buf = r_cut-l_cut+2;
        char *cell_buf = (char *)std::malloc(len_cell_buf);
        memcpy(cell_buf, l_cut, len_cell_buf);
        cell_buf[len_cell_buf-1] = '\0';

        log_debugf("<%s> raw cell : %s", __func__, cell_buf);
        removeDuplicateDqInCell(cell_buf);
        log_debugf("<%s> rendered cell : %s", __func__, cell_buf);

        int len_cell_content=strlen(cell_buf);

        if (*ptr_to_cellptr == NULL) {
            *ptr_to_cellptr = cell_buf;
            *n=len_cell_buf;
        } else {
            if (len_cell_content >= *n) {
                *n = len_cell_content+1;
                *ptr_to_cellptr = (char *)std::realloc(*ptr_to_cellptr, *n);
            }
            memcpy(*ptr_to_cellptr, cell_buf, len_cell_content+1);
            std::free(cell_buf);
        }
        log_debugf("<%s> returned cell : %s", __func__, *ptr_to_cellptr);

        return len_cell_content;
    }
    return -1;
}

void break_up_csv_file(map<string, FILE *>& child_files, int fd_csv, int cell_order){
    FILE* fp = fdopen(dup(fd_csv), "r");
    fseek(fp, 0, SEEK_SET);

    size_t len = 1024;
    // method 1
    //char * buf_for_record = (char *)std::malloc(len);
    // method 2
    char * buf_for_record;
    char * buf_for_cell = (char *)std::malloc(len);
    log_debugf("<%s> ................................buf_for_record : 0x%X, buf_for_cell: 0x%X", __func__, buf_for_record, buf_for_cell);

    //map<string, FILE *> child_file_streams;
    ssize_t row_read, cell_read;
    // method 1
    //while ((row_read = get_csv_single_row(&buf_for_record, &len, fp)) >= 0) {
    // method 2
    while ((buf_for_record = get_csv_record(fp)) != NULL) {
        // method 2
        row_read = strlen(buf_for_record);

        log_debugf("<%s> get_csv_record returned %d", __func__, row_read);
        if (row_read==0){
            continue;
        }
        cell_read = get_csv_cell_in_order(buf_for_record, cell_order, &buf_for_cell, &len);
        log_debugf("<%s> cell #%d in csv row : %s", __func__, cell_order, buf_for_cell);
        if (cell_read==0){
            continue;
        }
        //char * cell_content = trim_c_string(buf_for_cell);
        char * cell_content = buf_for_cell;
        if (strlen(cell_content)>0) {
            map<string, FILE *>::iterator it;
            FILE * ptr_child_stream;
            it = child_files.find(cell_content);
            if (it != child_files.end()) {
                ptr_child_stream = it->second;
            } else {
                // child file does not exist, create it
                ptr_child_stream = tmpfile();
                child_files[string(cell_content)] = ptr_child_stream;
            }

            std::fwrite(buf_for_record, row_read , 1, ptr_child_stream);
            std::fflush(ptr_child_stream);

            // method 2
            std::free(buf_for_record);
        }
    }

    for (const auto& s : child_files) {
        std::cout << "cell : " << s.first << ", stream_p: " << s.second << "\n";
        //fclose(s.second);
    }

    //child_file_streams.clear();
    //std::free(buf_for_record);
    std::free(buf_for_cell);
}

void break_up_csv_file2(map<string, int>& child_files, int fd_csv, int cell_order){
    FILE* fp = fdopen(dup(fd_csv), "r");
    fseek(fp, 0, SEEK_SET);

    size_t len = 1024;
    // method 1
    //char * buf_for_record = (char *)std::malloc(len);
    // method 2
    char * buf_for_record;
    char * buf_for_cell = (char *)std::malloc(len);
    log_debugf("<%s> ................................buf_for_record : 0x%X, buf_for_cell: 0x%X", __func__, buf_for_record, buf_for_cell);

    map<string, FILE *> child_file_streams;
    ssize_t row_read, cell_read;
    // method 1
    //while ((row_read = get_csv_single_row(&buf_for_record, &len, fp)) >= 0) {
    // method 2
    while ((buf_for_record = get_csv_record(fp)) != NULL) {
        // method 2
        row_read = strlen(buf_for_record);

        log_debugf("<%s> get_csv_record returned %d", __func__, row_read);
        if (row_read==0){
            continue;
        }
        cell_read = get_csv_cell_in_order(buf_for_record, cell_order, &buf_for_cell, &len);
        log_debugf("<%s> cell #%d in csv row : %s", __func__, cell_order, buf_for_cell);
        if (cell_read==0){
            continue;
        }
        //char * cell_content = trim_c_string(buf_for_cell);
        char * cell_content = buf_for_cell;
        if (strlen(cell_content)>0) {
            map<string, FILE *>::iterator it;
            FILE * ptr_child_stream;
            it = child_file_streams.find(cell_content);
            if (it != child_file_streams.end()) {
                ptr_child_stream = it->second;
            } else {
                // child file does not exist, create it
                ptr_child_stream = tmpfile();
                int fd_child_file = fileno(ptr_child_stream);
                child_files[cell_content] = dup(fd_child_file);
                child_file_streams[cell_content] = ptr_child_stream;
            }

            std::fwrite(buf_for_record, row_read , 1, ptr_child_stream);
            // method 2
            std::free(buf_for_record);
        }
    }

    for (const auto& s : child_file_streams) {
        std::cout << "cell : " << s.first << ", stream_p: " << s.second << "\n";
        fclose(s.second);
    }

    child_file_streams.clear();
    //std::free(buf_for_record);
    std::free(buf_for_cell);
}
