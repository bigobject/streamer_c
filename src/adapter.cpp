#include <string.h>

#include "adapter.h"

bool adapter::transmit()
{
    return _trans->flushall();
}

bool adapter::transmit(string chan)
{
    return _trans->flush(chan);
}

bool adapter::transmit(string chan, const string &data)
{
    void *buf = get_buffer(chan, data.length());
    if (!buf)
    {
        Error("%s: No enough buffer for data size %lu\n", __func__, data.length());
        return false;
    }
    memcpy(buf, data.c_str(), data.length());
    return transmit(chan);
}

bool adapter::transmit(string chan, int infd)
{
    log_debug(string("------->   <") + __PRETTY_FUNCTION__ + ">", true);
    return _trans->flush(chan, infd);
}

bool adapter::transmit(string chan, int infd, int offset)
{
    return _trans->flush(chan, infd, offset);
}

bool adapter::bitshift_departure(vector<FILE *> &data, msg_info_struct *msg_info)
{
    log_debug(string("------->   <") + __PRETTY_FUNCTION__ + ">", true);
    string doc_id;
    size_t count = 0;

    bool transform_success = transform(data, doc_id, count, msg_info);

    if (!transform_success)
    {
        Error("%s, transform failed:%s(%d)\n", msg_info, strerror(errno), errno);
        return false;
    }
    bool doc_id_empty = doc_id.empty();
    if (transform_success && !doc_id_empty)
        _doc_id = doc_id;

    log_debug(string("<") + __func__ + "> _doc_id in adapter exiting bitshift_departure : " + _doc_id, true);
    _trans->stop_flushing();

    return true;
}

bool adapter::bitshift_departure(vector<source_combo_struct *> &data_sources, msg_info_struct *msg_info)
{
    log_debug(string("------->   <") + __PRETTY_FUNCTION__ + ">", true);
    string doc_id;
    size_t count = 0;

    bool transform_success = transform(data_sources, doc_id, count, msg_info);
    /*
    if (pending_materials() > 0) {
        return true;
    }
    */

    if (!transform_success)
    {
        Error("%s, transform failed:%s(%d)\n", msg_info, strerror(errno), errno);
        return false;
    }
    bool doc_id_empty = doc_id.empty();
    if (transform_success && !doc_id_empty)
        _doc_id = doc_id;

    log_debug(string("<") + __func__ + "> _doc_id in adapter exiting bitshift_departure : " + _doc_id, true);
    _trans->stop_flushing();

    return true;
}
 
int adapter::check_flush_time(string condition)
{
    static int last_flush_time = 0;
    int time_difference = 0;
    int current_time = stoi(get_current_date("unix"));

    if (condition == "commit")
        last_flush_time = current_time;
    else if (condition == "compare")
    {
        if (last_flush_time == 0)
            last_flush_time = current_time; // $$ has never been flush before, set last_flush_time now;
        else
            time_difference = current_time - last_flush_time;
    }

    return time_difference;
}

void adapter::add_prefix(string prefix){
    _prefix_list.push_back(prefix);
    //_doc_serials[prefix]=-1;
}

void adapter::add_milestone(string table_name, string doc, int64_t num_records)
{
    if (!_is_file_receiver) {
        return;
    }

    if (table_name=="---"){
        table_name = _trans->get_master_table_name();
    }

    char doc_str[1024] = {0};
    strcpy(doc_str, doc.c_str());
    char * f_start;
    char dmtr = '/';
    f_start = strrchr(doc_str, dmtr);
    if (f_start==NULL) {
        f_start=doc_str;
    } else {
        f_start+=1;
    }
    log_debugf("<%s> file name/milestone : [%s][%s]",__func__,doc.c_str(), f_start);

    cp_info_struct cp_item;
    cp_item.db_table_name = table_name;
    cp_item.num_records = num_records;
    
    bool done = false;
    int _idx_prefix = -1;
    map<int, int> _prefix_machings;
    for (auto & _prefix : _prefix_list) {
        _idx_prefix++;
        char cstr_prefix[512] = {0};
        strcpy(cstr_prefix, _prefix.c_str());
        log_debugf("<%s> verify prefix : [%s][%s]",__func__,_prefix.c_str(), cstr_prefix);

        int len_prefix = strlen(cstr_prefix);
        if (strncmp(cstr_prefix, f_start, len_prefix)==0){
            _prefix_machings[_idx_prefix] = len_prefix;
        } else {
            _prefix_machings[_idx_prefix] = 0;
        }
    }

    int match_length = 0;
    int key_prefix = -1;
    for (int i=0; i<_prefix_machings.size(); i++) {
        if (_prefix_machings[i] > match_length) {
            match_length = _prefix_machings[i];
            key_prefix = i;
        }
    }

    if (key_prefix>=0) {
        cp_item.prefix = _prefix_list[key_prefix];
        char cstr_prefix[512] = {0};
        strcpy(cstr_prefix, cp_item.prefix.c_str());

        char * serial = f_start + strlen(cstr_prefix);
        char * ext_start;
        dmtr = '.';
        ext_start = strchr(serial, dmtr);
        if (ext_start!=NULL) {
            *ext_start=0;
        }
        cp_item.milestone = string(f_start);
        size_t cp_log;
        bool got;
        got = _trans->get_pre_cp_count(table_name, cp_log);
        if (got) {
            cp_item.checkpoint_log = cp_log;
        } else {
            cp_item.checkpoint_log = 0;
        }

        if (_table_size_map.find(table_name)==_table_size_map.end()) {
            _table_size_map[table_name] = cp_item.checkpoint_log + num_records;
        } else {
            _table_size_map[table_name] += num_records;
        }

        cp_item.table_waterlevel = _table_size_map[table_name];

        done = true;
        log_debugf("<%s> doc serial changed : [%s][%s]",__func__, cp_item.prefix.c_str(), f_start);
    }

    if (done) {
        _milestone_records.push_back(cp_item);
    }   

    print_milestones();
}

void adapter::print_milestones()
{
    int i=1;
    for (auto & _mstone_item : _milestone_records) {
        log_debugf("milestone<%d> [%d+%d][%s][%s]", i, _mstone_item.checkpoint_log, _mstone_item.num_records, _mstone_item.db_table_name.c_str(),_mstone_item.milestone.c_str());
        i++;
    }  
}

void adapter::set_loaded_docs_str(string& loaded_docs_str) {
    //log_debugf("adapter::set_loaded_docs_str[%s]", loaded_docs_str.c_str());

    for (auto & _prefix : _prefix_list) {
        if (_milestone_map.find(_prefix) == _milestone_map.end()){
            _milestone_map[_prefix] = _prefix + string("-1");
        } else {
            if (_milestone_map[_prefix].empty()){
                _milestone_map[_prefix] = _prefix + string("-1");
            }
        }
    }

    if (loaded_docs_str == "") {
        return;
    }

    char docs_str[1024] = {0};
    strcpy(docs_str, loaded_docs_str.c_str());
    char* token;
    char* rest = docs_str;
 
    while ((token = strtok_r(rest, "=", &rest))) {
        log_debugf("<%s> doc token : [%s]",__func__,token);
        for (auto & _prefix : _prefix_list) {
            char cstr_prefix[512] = {0};
            strcpy(cstr_prefix, _prefix.c_str());
            if (strncmp(cstr_prefix, token, strlen(cstr_prefix))==0){
                // char * serial = token + strlen(cstr_prefix);
                // _doc_serials[_prefix] = strtoll(serial, NULL, 10);
                // log_debugf("<%s> doc serial : [%s][%d]",__func__,_prefix.c_str(), _doc_serials[_prefix]);
                _milestone_map[_prefix] = string(token);
                break;
            }
        }
    }
}

bool adapter::commit()
{
    return commit(false, false);
}

bool adapter::commit(bool is_fileReceiver, bool is_file_chunk) {

    if (is_fileReceiver && !is_file_chunk){
        while (_pending >= _batch_checkpointing) {
            _pending -= _batch_checkpointing;
            check_flush_time("commit");
            staged_checkpointing();
            log_debugf("<%s> post commit _pending/_batch_checkpointing : [%d][%d]",__func__, _pending, _batch_checkpointing);
        }
        return true;
    } else {
        _pending = 0;
        return _trans->commit(_doc_id);
    }
}

bool adapter::bunch_checkpointing()
{
    _pending = 0;
    return _trans->commit(_doc_id);
}

bool adapter::staged_checkpointing()
{
    vector<string> ipt_file_list;
    map<string, bool> ipt_file_map;
    int file_count = 0;

    for (auto & _milestone : _milestone_records) {
        if (file_count>=_batch_checkpointing) {
            break;
        }
        if (ipt_file_map.find(_milestone.milestone) == ipt_file_map.end()) {
            ipt_file_map[_milestone.milestone]=true;
            ipt_file_list.push_back(_milestone.milestone);
            file_count+=1;
        }
    }

    vector<cp_info_struct> milestone_subset;
    map<string, bool> table_map;
    vector<string> table_order;
    //map<string, vector<cp_info_struct>> mstone_families;
    map<string, vector<int>> mstone_families;
    vector<int> mstone_indices;

    size_t batch_increment = 0;
    for (int i = 0; i < _milestone_records.size(); i++) {
        //std::cout << "Element " << _milestone_records[i] << " found at index " << i << std::endl;
        batch_increment += _milestone_records[i].num_records;
        for (auto & _ipt_file : ipt_file_list) {
            if (_ipt_file == _milestone_records[i].milestone) {
                mstone_indices.push_back(i);
                if (table_map.find(_milestone_records[i].db_table_name) == table_map.end()) {
                    table_map[_milestone_records[i].db_table_name]=true;
                    table_order.push_back(_milestone_records[i].db_table_name);
                    vector<int> mstone_list;
                    mstone_list.push_back(i);
                    mstone_families[_milestone_records[i].db_table_name]=mstone_list;
                } else {
                    mstone_families[_milestone_records[i].db_table_name].push_back(i);
                }
            }
        }
    }

    map<int, bool> records_to_delete;
    map<string, vector<string>> staged_records; // map doc_id to tables wrote
    for (auto & _table_name : table_order) {
        log_debugf("<%s> create mstone summary for table [%s]",__func__,_table_name.c_str());

        vector<int>& mstone_indices = mstone_families[_table_name];
        size_t cp_log = 0;
        size_t table_waterlevel = 0;
        for (auto & _mstone_idx : mstone_indices) {
            records_to_delete[_mstone_idx] = true;
            cp_log = _milestone_records[_mstone_idx].checkpoint_log;
            _milestone_map[_milestone_records[_mstone_idx].prefix] = _milestone_records[_mstone_idx].milestone;
            table_waterlevel = _milestone_records[_mstone_idx].table_waterlevel;
            log_debugf("<%s> \t update milestone [%s] -> [%s]",__func__,_milestone_records[_mstone_idx].milestone.c_str(), _milestone_records[_mstone_idx].prefix.c_str());
            staged_records[_milestone_records[_mstone_idx].milestone].push_back(_table_name);
        }

        string milestone_summary("");
        for (int i = 0; i < _prefix_list.size(); i++) {
            log_debugf("<%s> milestone_summary part[%d] [%s]",__func__, i, _prefix_list[i].c_str());
            if (i>0){
                milestone_summary += "=";
            }
            milestone_summary+=_milestone_map[_prefix_list[i]];
        }
        bool verify_total = true;
        if (_table_name=="---") {
            verify_total = false;
        }
        _trans->commit(_table_name, milestone_summary, verify_total, _table_size_map[_table_name], table_waterlevel);
    }
    _trans->exclusive_commit(staged_records);

    for (int i = _milestone_records.size()-1; i>=0; i--) {
        if (records_to_delete.find(i) != records_to_delete.end()) {
            _milestone_records.erase(_milestone_records.begin() + i);
        }
    }

//    return _trans->commit(_doc_id, 1234567);
    //return _trans->commit(_doc_id);
    //return _trans->commit(_milestone_records);
    return true;
}