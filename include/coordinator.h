#pragma once

#include "receiver.h"
#include "adapter.h"
#include "transmitter.h"
#include "common_func.h"

typedef enum
{
    LIFE_TYPE_WEEKLY,
    LIFE_TYPE_MONTHLY,
    LIFE_TYPE_FOREVER
} LIFE_TYPE;

class coordinator
{
    public:
        coordinator() : _conf(""), _retention(1), _expire(0),
        _life_type(LIFE_TYPE_WEEKLY), _start_day(2), _enable_counter(false),
        _alow_zero_start(false), _eye_on_session(0), 
        _receiver(NULL), _adapter(NULL), _transmitter(NULL),
        _init_doc_str("")
        {}
        coordinator(string conf) : _conf(conf), _retention(1), _expire(0),
        _life_type(LIFE_TYPE_WEEKLY), _start_day(1), _enable_counter(false),
        _alow_zero_start(false), _receiver(NULL), _adapter(NULL), _transmitter(NULL)
        {}
        virtual ~coordinator() {}

        void set_config(string conf)
        {_conf = conf;}

        void set_receiver(receiver* recv, bool is_file_receiver);
        //{_receiver = recv;}

        void set_adapter(adapter* adp)
        {_adapter = adp;}

        void set_transmitter(transmitter* trans)
        {_transmitter = trans;}

        void set_write_to(string write_to)
        {_write_to = write_to;}

        string get_write_to()
        { return _write_to; }

        bool initialize();
        bool run();
        bool sync();

        // set/update next time to switch workspace or bo
        // init: is this call for initialization
        // return 'false' means time to stop
        bool update_retention(bool init);

        bool terminate();

        static void* counter_worker(void* arg);

    private:
        string  _conf;

        string  _write_to;
        int     _retention;
        time_t  _expire;

        LIFE_TYPE   _life_type;
        int         _start_day;

        bool        _enable_counter;
        bool        _alow_zero_start;
        bool        _eye_on_session=0;

        receiver*       _receiver;
        adapter*        _adapter;
        transmitter*    _transmitter;

        bool is_life_end(struct tm& utc_time);

        bool        _is_file_receiver=0;
        string _init_doc_str;
};