#pragma once

#include "transmitter.h"
#include "common_func.h"

typedef struct bo_replica
{
    string host;
    unsigned short port;
    bool no_sync;
} bo_replica_t;

typedef struct bo_commit_act
{
    string source;
    string host;
    unsigned short port;
} bo_commit_act_t;

typedef enum {
    SVC_BO,
    SVC_REPLICA,
    SVC_BACKUP
} sa_svc_type; // service type

class bo_channel : public channel
{
    public:
        bo_channel() : _sql(""), _is_dim(false), _fd(-1), _ctrl_channel(-1), _data_channel(-1) {}
        bo_channel(string name, size_t cap) :
            channel(name, cap), _sql(""), _is_dim(false), _fd(-1), _ctrl_channel(-1), _data_channel(-1) {}
        bo_channel(string name, size_t cap, string sql, bool dim, bool allow_cleanup) :
            channel(name, cap), _sql(sql), _is_dim(dim), _fd(-1), _ctrl_channel(-1), _data_channel(-1), _allow_cleanup(allow_cleanup) {}
        bo_channel(string name, size_t cap, string sql, bool dim, bool allow_cleanup, bool is_encrypt, map<int, pair<string, string>> encryptMap) :
            channel(name, cap), _sql(sql), _is_dim(dim), _fd(-1), _ctrl_channel(-1), _data_channel(-1), _allow_cleanup(allow_cleanup), _is_encrypt(is_encrypt), _encryptMap(encryptMap) {}
        virtual ~bo_channel();

        bool create_table(string ws);
        bool create_table(string ws, string &result);
        void set_ctrl_channel(int fd);
        void set_data_channel(int fd);
        void set_bo_info(string host, unsigned short port, string workspace, int table_watermark_verifications, int least_xmit_interval);

        const int get_fd() const
        {return _fd;}

        bool close_chan(bool force);

        int open_connection(const string& peer, unsigned short port);

        virtual bool get_total_count_by_pkt(size_t &total_count);
        virtual bool get_total_count_by_sql(size_t &total_count);
        virtual bool flush_table(bool use_persist_channel);
        virtual ssize_t transmit(void* data, size_t size);
        virtual ssize_t transmit(int infd);
        virtual ssize_t transmit(int infd, int offset);

        virtual int get_steady_row_count(int threshold);

        bool is_dim_tbl() const
        {return _is_dim;}

        bool is_allow_cleanup() const
        {return _allow_cleanup;}

        static bool exec_sql(int fd, string ws, string sql, bool csv_out, string& result);
        static bool pre_transmit(int fd, string table, string type, ssize_t shrink);
        static bool send_csv_header(int fd, string table, ssize_t shrink);
    
    private:
        string _sql;
        bool _is_dim;
        int _fd;
        int _ctrl_channel;
        int _data_channel;
        bool _allow_cleanup;
        string _host;
        unsigned short _port;
        string _workspace;
        int _table_watermark_verifications;
        int _least_xmit_interval;
        bool _is_encrypt = false;
        map<int, pair<string, string>> _encryptMap;
};

class bo_transmitter : public transmitter
{
    public:
        bo_transmitter() : _host(""), _port(0), _bk_port(0), _type(""),
        _workspace(""), _fix_ws(""), _pre_create(false), _create_cp(false), _cp_name(""),
        _cp_retention(604800), _fd_cp(-1), _allow_cleanup_all(false), _replica_num(0),
        _commit_act(false), _no_suspend(false),
        _session_monitoring(0), _session_table_name("session_connect")
        {}
        virtual ~bo_transmitter();

        virtual bool get_last_commitment(string& doc_id);
        virtual bool backoff(string& doc_id);

        virtual bool set_params(string& group, map<string, string>& param);
        virtual bool add_chan(string chan_name, size_t size, bool allow_cleanup);

        virtual void reset_workspace(bool change_bo);
        virtual bool prepare_channels(bool create_cp);
        virtual channel* add_ext_channel(string channel_name);

        // overload the function to add special handling before and after commit
        virtual bool commit(string& doc_id);
        
        virtual bool flush(string& chan_name, int infd);
        virtual bool flush(string& chan_name, int infd, int offset);
        
        virtual bool reset_chan(channel* chan);

        virtual void close_chan(channel* chan);
        virtual bool get_pre_cp_count(string tbl_name, size_t &count);
        virtual bool get_pre_cp_count(string cp_name, string ws_name, string tbl_name, size_t &count);
        virtual bool get_total_count_by_sql(string workspace, string table_name, size_t &total_count);
        virtual const string get_class_name()
        {return "bo_transmitter";}
        size_t get_steady_row_count(string table_name);
        //static const int get_watermark_verifications()
        //{return _table_watermark_verifications;}

        static bool is_single_shot_conn()
        {return _single_shot_conn;}

        virtual bool set_session_monitoring(bool how);
        virtual bool set_session_status(int status);

        virtual bool create_fact_table_registry();

    protected:
        virtual bool set_checkpoint(commitment& cp, bool verify_total);
        bool get_checkpoint(string table, string doc_id, string& ws, ssize_t& count);
        int get_num_conn(string table, string ws);
    //    bool allow_backoff;
    private:
        
        string _host;
        unsigned short _port;
        unsigned short _bk_port; // backup service port
        string _type;
        string _workspace; // $$ workspace for data table, defined in conf "workspace"
        string _fix_ws;
        bool _pre_create;
        bool _create_cp;
        string _cp_name;
        int _cp_retention;
        int _fd_cp; // for check point

        int _replica_num;
        bo_replica_t _replica;
        bool _allow_cleanup_all;
        bool _commit_act;
        bo_commit_act_t _cmact;

        // new items for extensible tables
        int _num_fixed_tables;
        string _cols_in_ext_def_prefix;
        string _ext_default_sql;

        bool _no_suspend;
        int _table_watermark_verifications;
        int _least_xmit_interval;
        static bool _single_shot_conn;

        bool _session_monitoring=0;
        string _session_table_name;

        int open_connection(uint64_t read_timeout, sa_svc_type svc_type, string file_name, int line, string function_name, string other_desc);
        int simple_connection(const string& peer, unsigned short port);
        bool initialize();
        bool set_bo_logging(int level);
        bool toggle_spp_mode(bool mode);
        bool create_workspace(bool is_director);

        // suspend and resume bo to make sure bo data is flush to disk.
        bool flush_bo();
        bool flush_bo(sa_svc_type svc_type);

        bool flush_cmd(sa_svc_type svc_type);

        bool reset_sync_chan();

        void send_commit_action();
 
};