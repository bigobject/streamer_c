#pragma once

#include <vector>
#include <map>
#include <string>

#include <time.h>
#include "common_func.h"

#include "component.h"

#define DIRECTOR_WORKSPACE   "bigobject"
#define CHAN_BUF_SIZE_DEFAULT   16*1024*1024
#define EXIT_CODE_STOP_AT_FAIL  101

class channel
{
    public:
        channel() : _name(""),
        _capacity(CHAN_BUF_SIZE_DEFAULT), _size(CHAN_BUF_SIZE_DEFAULT),
        _head(0), _tail(0), _data(NULL), _progress(0),
        _is_replica(false), _sync_replica(false)
        {}
        channel(string name, size_t cap);
        virtual ~channel();

        bool is_empty()
        {return _head == _tail;}

        // buffer can't across end point, b/c we need a continuous buffer, 
        bool is_enough(size_t size)
        {return _head >= _tail ? _capacity > (_head + size) : _tail > (_head + size);}

        void* acquire(size_t size) {
            char* out = NULL;
            if (_head < _tail) {
                if (_tail <= (_head + size))
                    return NULL;
                out = _data + _head;
                _head += size;
                return out;
            }
            if (_capacity > (_head + size)) {
                out = _data + _head;
                _head += size;
            }
            else if (_tail > size) {
                _size = _head;
                _head = size;
                out = _data;
            }
            return (void*)out;
        }

        void* get_data(size_t& size) {
            log_debug(string("------->   <") +__PRETTY_FUNCTION__+ ">", true);
            if (is_empty()) {
                log_debug(string("<") +__func__+ "> no data", true);
                return NULL;
            }

            char* out = _data + _tail;
            size_t max_size = _head >= _tail ? _head - _tail : _size - _tail;
            if (size > max_size || size == 0)
                size = max_size;
            // Don't move data pointer until release() is called explicitly, b/c data is in use.
            //_tail += size;
            //_tail %= _size;
            return (void*)out;
        }

        // release used data buffer
        void release(size_t size)
        {_tail = (_tail + size) % _capacity;}

        // withdraw over-acquired buffer (assume it follows acquire())
        void withdraw(size_t size) {
            _head -= size;
            if (0 == _head) { // It only happens when returns all previously acquired and across boundary
                _head = _size;
                _size = _capacity;
            }
        }

        const string get_name() const
        {return _name;}

        void reset_progress()
        {_progress = 0;}
        void inc_progress(size_t progress)
        {_progress += progress;}
        void set_progress(size_t progress)
        {_progress = progress;}
        const size_t get_progress() const
        {return _progress;}

        bool flush();
        bool flush(int infd);
        bool flush(int infd, int offset);
        virtual bool get_total_count_by_pkt(size_t &total_count) = 0;
        virtual bool get_total_count_by_sql(size_t &total_count) = 0;

        bool is_replica() const
        {return _is_replica;}
        void set_replica(bool set)
        {_is_replica = set;}

        bool is_sync_replica() const
        {return _sync_replica;}
        void set_sync_replica(bool set)
        {_sync_replica = set;}


    protected:
        virtual ssize_t transmit(void* data, size_t size) = 0;
        virtual ssize_t transmit(int infd) = 0; // transmit the whole file data
        virtual ssize_t transmit(int infd, int offset) = 0; // transmit the whole file data start from offset

    private:
        string _name;

        size_t _capacity;   // total reserved size
        size_t _size;       // used size <= _capacity
        size_t _head;
        size_t _tail;

        char* _data;        // this is a mmap buffer(?)

        size_t _progress;       // number of records are processed

        bool _is_replica;
        bool _sync_replica;
};

class transmitter : public component
{
    public:
        transmitter() : _name(""), _stop_at_fail(false)
        {_channels.clear();}
        virtual ~transmitter();

        // Synchronize current state
        // Parameters:
        //      doc_id: last committed doc
        // Return value: success or not
        virtual bool get_last_commitment(string& doc_id) = 0;

        // Back to old position
        // Parameters:
        //      backto: back to the committed doc
        // Return value: success or not
        virtual bool backoff(string& doc_id) = 0;

        virtual bool add_chan(string chan_name, size_t size, bool allow_cleanup ) = 0;

        void* get_buffer(string chan_name, size_t size) {
            channel* chan = get_chan(chan_name, false);
            if (!chan)
                return NULL;
            return chan->acquire(size);
        }

        bool return_buffer(string chan_name, size_t size) {
            channel* chan = get_chan(chan_name, false);
            if (!chan)
                return false;
            chan->release(size);
            return true;
        }

        //transmitter& operator<< (seq_buf* buf);
        // Flush _buf to destination
        // Return value:
        //      = 0: Success
        //      < 0: Fail
        virtual bool flush(string& chan_name);
        virtual bool flush(channel* chan);
        virtual bool flush(string& chan_name, int infd);
        virtual bool flush(string& chan_name, int infd, int offset);
        virtual bool flush(channel* chan, int infd);
        virtual bool flush(channel* chan, int infd, int offset);
        virtual bool flushall();

        // Commit all channels

        virtual bool commit(string& doc_id);
        //virtual bool commit(string& table_name, string& milestone_summary, int rows_expected, int increment);
        virtual bool exclusive_commit(map<string, vector<string>>& staged_records);

        // Commit a specific channel
        virtual bool commit(string& chan_name, string& doc_id);
        virtual bool commit(string& chan_name, string& doc_id, bool verify_total, size_t total_expected, size_t subtotal);
        virtual bool commit(channel* chan, string& doc_id);
        virtual bool commit(channel* chan, string& doc_id, bool verify_total, size_t total_expected, size_t increment);


        void reset_progress();
        void reset_progress(string& chan_name);
        void reset_progress(channel* chan)
        {chan->reset_progress();}

        void inc_progress(string& chan_name, size_t progress);
        void inc_progress(channel* chan, size_t progress)
        {chan->inc_progress(progress);}

        size_t get_progress(string& chan_name);
        size_t get_progress(channel* chan)
        {return chan->get_progress();}

        virtual bool del_chan(string chan_name);

        virtual bool reset_chan(string chan_name);
        virtual bool reset_chan(channel* chan){return true;}

        virtual void close_chan(channel* chan) = 0;
        virtual void close_chan();

        string get_master_table_name() {
            return _channels.begin()->first;
        }
        channel* get_chan(string chan_name, bool add_new_if_absent) {
            if (chan_name == "---") {
                return _channels.begin()->second;
            }
            map<string, channel*>::const_iterator it = _channels.find(chan_name);
            if (it != _channels.end())
                return it->second;

            it = _faults.find(chan_name);
            if (it != _faults.end())
                return it->second;

            if (add_new_if_absent){
                return add_ext_channel(chan_name);
            } else {
                return NULL;
            }
        }

        virtual bool set_params(string& group, map<string, string>& param);

        virtual void reset_workspace(bool change_bo){}
        virtual bool prepare_channels(bool create_cp){}
        virtual channel* add_ext_channel(string channel_name){}

        void set_name(const string& name)
        {_name = name;}
        const string& get_name()
        {return _name;}
        virtual const string get_class_name()
        {return "transmitter";}
        
        void stop_flushing()
        {_no_flushing = true;}

        virtual bool set_session_monitoring(bool how) = 0;
        virtual bool set_session_status(int status) = 0;

        virtual bool get_pre_cp_count(string tbl_name, size_t &count) = 0;

    protected:
        // Write commitment signature (check point).
        // The signature should contain document id, and number of success/fail rows in the document.
        // Return value: success or not
        virtual bool set_checkpoint(commitment& cp, bool verify_total) = 0;
        size_t get_main_channel_size();

        string  _name;
        bool    _stop_at_fail;
        map<string, channel*>    _channels;
        map<string, channel*>    _faults;

        bool _no_flushing = false;
};