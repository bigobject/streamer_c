#pragma once

#include "adapter.h"
#include "common_func.h"

typedef enum
{
    AST_STOP,
    AST_START
} agent_state_t;

typedef enum
{
    RCV_FILE,
    RCV_KAFKA,
    RCV_MQTT
} receiver_type_t;

class receiver : public component
{
    public:
        receiver() : _adapt(NULL), _state(AST_STOP), _batch_reading(1), _batch_processing(1)
        {}
        virtual ~receiver() {}

        void set_type(receiver_type_t rcv_type);
        receiver_type_t get_type();

        virtual bool set_params(string& group, map<string, string>& param);

        virtual ssize_t read_receiver(string& data, source_combo_struct* data_picked, msg_info_struct *msg_info) = 0;

        // Synchronize current state
        // Parameters:
        //      doc_id: last committed doc
        // Return value: success or not
        virtual bool get_last_commitment(string& doc_id) = 0;

        // Back to old position
        // Parameters:
        //      doc_id: back to the committed doc
        // Return value: success or not
        virtual bool backoff(string& doc_id) = 0;

        // Determine how to synchronize two check points, doc1 and doc2.
        virtual bool sync_strategy(string& rdoc, string& tdoc, string& out) = 0;
        virtual vector<string> get_prefixs();

        void free_data_handles(vector<source_combo_struct*>& data_sources);

        bool start(time_t retention);
        void stop();

        void set_adapter(adapter* adapt)
        {_adapt = adapt;}

        void set_state(agent_state_t state)
        {_state = state;}

        bool is_running()
        {return _state == AST_START;}

        bool is_expired(time_t expire);

        void set_batch_reading(int batch_size)
        {
            _batch_reading = batch_size;
        }
    protected:
        adapter* _adapt;
        int _batch_reading;

    private:
        receiver_type_t _rcv_type;
        agent_state_t   _state;
        int     _batch_processing;
};