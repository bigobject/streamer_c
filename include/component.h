#pragma once

#include <sys/time.h>

#include <string>
#include <map>

#include "common_func.h"

#define SECT_CORD   "coordinator"
#define SECT_RECV   "receiver"
#define SECT_ADPT   "adapter"
#define SECT_TRAN   "transmitter"

// #ifdef BO_STREAM_AGENT_DEBUG
// #define Debug  printf
// #else
// #define Debug(...)
// #endif

// #ifdef BO_STREAM_AGENT_DEBUG
// #define STREAMER_DEBUG( header, os, msg ) \
//   (os) << pthread_self() << header << '<' << __FILE__ << ", " << __func__ << \
//   "():" << __LINE__ << "> " << msg << std::endl
// #else
// #define STREAMER_DEBUG( header, os, msg )
// #endif
// #define Debug(msg)  STREAMER_DEBUG( " [DBG] ", cout, msg )

#ifdef BO_STREAM_AGENT_DEBUG
#define Debug(format, arg...) printf("[DBG] %s:%s(%d): " format, __FILE__, __FUNCTION__, __LINE__, ##arg)
#define Error(format, arg...) printf("[ERR] %s:%s(%d): " format, __FILE__, __FUNCTION__, __LINE__, ##arg)
#define Info(format, arg...) printf("[INF] %s:%s(%d): " format, __FILE__, __FUNCTION__, __LINE__, ##arg)
#else
#define Debug(...)
#define DebugDev(format, tstamp, arg...) printf("[DBG] " tstamp " " format, ##arg)
#define Error(format, arg...) printf("[ERR] " format, ##arg)
#define Info(format, arg...) printf("[INF] " format,  ##arg)
#endif



using namespace std;

class component
{
    public:
        component() : _group("") {}
        virtual ~component() {}

        virtual bool set_params(string& group, map<string, string>& param);

        bool get_param_value(string key, string& val);
    protected:
        string _group;
        map<string, string>     _params;
};

class commitment
{
    public:
        commitment() :
        _table(""), _doc_id(""), _total_expected(0), _subtotal(SIZE_MAX), _success(0), _timestamp({0, 0})
        {}
        commitment(string table, string doc, size_t total_expected, size_t subtotal, size_t success) :
        _table(table), _doc_id(doc), _total_expected(total_expected), _subtotal(subtotal), _success(success)
        {gettimeofday(&_timestamp, NULL);}
        virtual ~commitment() {}

        const string get_table_name() const
        {return _table;}
        void set_table_name(string table)
        {_table = table;}

        const string get_doc_id() const
        {return _doc_id;}
        void set_doc_id(string doc_id)
        {_doc_id = doc_id;}

        const size_t get_total_expected() const
        {return _total_expected;}
        void set_total_count(size_t total)
        {_total_expected = total;}

        size_t get_subtotal()
        {return _subtotal;}

        const size_t get_success_count() const
        {return _success;}
        void set_success_count(size_t success)
        {_success = success;}

        const struct timeval get_time_stamp() const
        {return _timestamp;}
        void set_time_stamp(struct timeval timestamp)
        {_timestamp = timestamp;}
    private:
        string  _table;
        string  _doc_id;
        size_t  _total_expected;
        size_t  _subtotal;
        size_t  _success;
        struct timeval _timestamp;
};

class ini_conf
{
    public:
        ini_conf(string conf);
        virtual ~ini_conf(){}

        bool get_section(string section, map<string, string>& data);
        bool get_value(string section, string key, string& value);
    private:
        map<string, map<string, string> > _conf;
};
