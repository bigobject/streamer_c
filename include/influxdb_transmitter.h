#pragma once

#include "transmitter.h"
#include "common_func.h"

class influxdb_transmitter : public transmitter
{
    public:
        influxdb_transmitter() : _influxdb_addr(""), _influxdb_port(""), _influxdb_username(""), _influxdb_password(""),
        _influxdb_dbname(""), _influxdb_measurement("")
        {}
        virtual ~influxdb_transmitter();


        virtual bool set_params(string& group, map<string, string>& param);

        virtual bool commit(string& doc_id)
        {return true;}

        virtual bool flush(string& chan, int infd);
        virtual const string get_class_name()
        {return "influxdb_transmitter";}

        virtual bool get_last_commitment(string& doc_id)
        {}
        virtual bool backoff(string& doc_id)
        {}
        virtual bool add_chan(string chan_name, size_t size)
        {}
        virtual void close_chan(channel* chan)
        {}
    protected:
        virtual bool set_checkpoint(commitment& cp, bool verify_total)
        {}
    private:
        
        string  _influxdb_addr;
        string  _influxdb_port;
        string  _influxdb_username;
        string  _influxdb_password;
        
        string  _influxdb_dbname;
        string  _influxdb_measurement;
        
        bool initialize();
        
};