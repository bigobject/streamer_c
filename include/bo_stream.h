#pragma once

#define BO_INT_CMD_PULL     0x01
#define BO_INT_CMD_PUSH     0x02
#define BO_INT_CMD_QUERY    0x03
#define BO_INT_CMD_UNION    0x04
#define BO_INT_CMD_REPORT   0x05

#define BO_INT_FLAG_EX_CSV      0x01    // output in csv format
#define BO_INT_FLAG_EX_NO_RET   0x02    // no (empty) result set to client

// $$ Internal stream
const char int_tag[] = {'i', 'n', 't', 1};

#define SYN_MARK_LEN        4

// $$ P: progress, S: table size
const uint8_t syn_progress_mark[] = {0x17, 0x16, 'P', 0x16}; // ETB + SYN + 'P' + SYN
const uint8_t syn_bt_size_mark[] = {0x17, 0x16, 'S', 0x16}; // ETB + SYN + 'S' + SYN
const uint8_t syn_term_mark[] = {0x17, 0x16, 'T', 0x16}; // ETB + SYN + 'T' + SYN

typedef struct __attribute__((packed)) bo_int_header
{
    // size of this header:
    // sizeof(bo_int_header_t) + sizeof(pull or push header) +
    // (sizeof(bo_alt_channel_t) or 0)
    uint16_t    size;       // should be BE or LE depends on flags
    uint8_t     cmd;        // BO_INT_CMD_XXXX
    uint8_t     flags;
    uint8_t     flags_ex;
    uint8_t     key_cnt;    // number of keys, for push command only
    uint16_t    col_cnt;    // number of columns, for push command only
} bo_int_header_t;
