#pragma once

#include <vector>
#include <iostream>
#include <cstring>
#include <fstream>
#include <sstream>

#include "transmitter.h"
#include "common_func.h"

#define KEY_ADAPTER_TRANSFORM           "transform"
#define KEY_ADAPTER_TRANSFORM_SVT       "SVT"
#define KEY_ADAPTER_TRANSFORM_PASS_THRU "pass_through"

using namespace std;
/*
struct msg_info_struct
{
    string topic;
    int32_t partition;
    int64_t offset;
};
*/

class adapter : public component
{
    public:
        adapter() : _trans(NULL), _is_json_input(false), _is_file_receiver(false), _xformed_docs_str(""), _batch_checkpointing(1), _pending(0) {}
        virtual ~adapter() {}

        //virtual adapter& operator<< (vector<string>& data);
    //    virtual adapter& operator<< (vector<int>& data);

        bool bitshift_departure(vector<FILE*>& data, msg_info_struct *msg_info);
        bool bitshift_departure(vector<source_combo_struct*>& data_sources, msg_info_struct *msg_info);
        virtual int pending_materials() = 0;

        // Transmit buffered data of all channels
        bool transmit();

        // Transmit buffered data of a specific channel
        bool transmit(string chan);

        // Transmit string data
        bool transmit(string chan, const string& data);

        // Transmit a file through a specific channel
        bool transmit(string chan, int infd);
        bool transmit(string chan, int infd, int offset);

        // Commit current state
        bool commit();
        bool commit(bool is_fileReceiver, bool is_file_chunk);
        bool bunch_checkpointing();
        bool staged_checkpointing();

        void set_trnasmitter(transmitter* trans)
        {_trans = trans;}

        void set_loaded_docs_str(string& xformed_docs_str);
        //{ _xformed_docs_str = xformed_docs_str; }
        void set_file_receiver(bool choice)
        {_is_file_receiver = choice;}

        void set_doc_id(string doc_id)
        {_doc_id = doc_id;}

        int check_flush_time(string condition);
    //    string get_current_date(string format);
    //    void Write_Log(string filename_ending_with, string log_message);
    //    void Write_Error_Log(msg_info_struct *msg_info, string error_message);
        bool set_session_status(int status)
        {
            return _trans->set_session_status(status);
        }

        bool is_json_input()
        {
            return _is_json_input;
        }

        void add_prefix(string prefix);
        void add_milestone(string table, string doc, int64_t num_records);
        void print_milestones();
    protected:
        // Transform received data from receiver into target format.
        // The result had better be stored in the buffer of transmitter channel.
        // Thus, it should call get_buffer() in advance, and call return_buffer() if not all
        // buffer is consumed.
        // This function also has to call transmit to pass transformed data to next component.
        //virtual bool transform(vector<string>& data, string& doc_id, size_t& count) = 0;
        virtual bool transform(vector<FILE*>& data, string& doc_id, size_t& count, msg_info_struct *msg_info) = 0;
        virtual bool transform(vector<source_combo_struct*>& data, string& doc_id, size_t& count, msg_info_struct *msg_info) = 0;

        void* get_buffer(string chan, size_t size)
        {return _trans->get_buffer(chan, size);}

        bool return_buffer(string chan, size_t size)
        {return _trans->return_buffer(chan, size);}

        bool    _is_json_input;
        bool    _is_file_receiver;
        int     _batch_checkpointing;
        int     _pending;
        map<string, size_t> _table_size_map;
        map<string, size_t> _table_subtotal_map;
    private:
        transmitter* _trans;

        string _doc_id;    // current document id
        string _xformed_docs_str;
        vector<string> _prefix_list;
        map<string, string> _milestone_map;
        //map<string, int64_t> _doc_serials;
        vector<cp_info_struct> _milestone_records;
};