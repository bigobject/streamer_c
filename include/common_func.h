#pragma once

#include <string.h>
#include <fstream>
#include <iostream>
#include <map>

using namespace std;

struct msg_info_struct
{
    string topic;
    int32_t partition;
    int64_t offset;
};

struct source_combo_struct
{
    char handle_type; //P or D
    char type[16];
    FILE * file_ptr;
    int file_descriptor;
    long file_length;
    long data_offset;
    long data_length;
    char is_final_part; // Y or N
    char* msg_buf_ptr;
    int msg_buf_len;
    string topic;
    string serial;
    string file_name;
    int status;
};

struct cp_info_struct
{
    string db_table_name;
    string prefix;
    string milestone;
    size_t checkpoint_log;
    size_t num_records;
    size_t table_waterlevel;
};

void Write_Log(string filename_ending_with, string log_message, bool print_on_screen);
void Write_Parse_ErrorLog(msg_info_struct *msg_info, string error_message, bool print_on_screen);    
void Write_System_ErrorLog(string error_message, bool print_on_screen);    
void Write_ConnectionLog(int fd, string action, string file_name, int line, string function_name, string other_desc);
void Close_fd(int fd, string file_name, int line, string function_name, string other_desc);

void log_debug(string message_to_log, bool print_on_screen);
void log_debugf(char * format, ...);
void log_file_content(const char caller_func_[], FILE * fp);
void log_info(string message_to_log, bool print_on_screen);
void log_error(string message_to_log, bool print_on_screen);
void logging(string level, string message_to_log, bool print_on_screen);

string get_current_date(string format);

void toggle_debug_mode(int level);
void toggle_packet_mode(bool enable_or_not);
bool use_syn_mark();
void set_num_file_chunks_to_keep(int number);
int get_num_file_chunks_to_keep();

string trim_string(string str);

string read_thru_fd(int file_desc);

int count_csv_double_quote(char * data_buf);
int next_csv_cut(char * data_buf, int count_double_quote);

char *trim_c_string(char *str);
void strReplace(char *base_str, char old_one, char new_one);
void removeDuplicateChars(char* base_str, char uni_char);
void strupr(char* base_str);

void get_full_filename(int fd, char* full_filename, int buf_len);
ssize_t get_csv_single_row(char **ptr_to_lineptr, size_t *n, FILE *fp);
char * get_csv_record(FILE *fp);
ssize_t count_csv_records(FILE *fp);
void removeDuplicateDqInCell(char* stripped_cell);
ssize_t get_csv_cell_in_order(char * csv_row, int order, char **ptr_to_cellptr, size_t *n);

void break_up_csv_file(map<string, FILE *>& child_files, int fd_csv, int cell_order);
void break_up_csv_file2(map<string, int>& child_files, int fd_csv, int cell_order);
